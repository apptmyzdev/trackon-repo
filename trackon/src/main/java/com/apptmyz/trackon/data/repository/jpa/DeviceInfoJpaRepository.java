
package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DeviceInfoEntity;

/**
 * Repository : DeviceInfo.
 */
public interface DeviceInfoJpaRepository extends PagingAndSortingRepository<DeviceInfoEntity, Integer> {

	DeviceInfoEntity findByDeviceImei(String imei);

	List<DeviceInfoEntity> findByActiveFlagIsTrue();
	
	List<DeviceInfoEntity> findByUserIdAndActiveFlagIsTrue(String userId);
	
	DeviceInfoEntity findByDeviceImeiAndActiveFlagAndUserId(String imei, Integer activeflag, String userId);
}
