package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingPkupScanDataEntity;

/**
 * Repository : BookingPkupScanData.
 */
public interface BookingPkupScanDataJpaRepository extends PagingAndSortingRepository<BookingPkupScanDataEntity, Integer> {

	List<BookingPkupScanDataEntity> findByDocketNo(String docketNo);

	BookingPkupScanDataEntity findTopByDocketNoOrderByCreatedTimestampDesc(String dktno);

}
