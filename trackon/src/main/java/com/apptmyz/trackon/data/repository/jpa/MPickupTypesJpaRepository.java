package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MPickupTypesEntity;

/**
 * Repository : MPickupTypes.
 */
public interface MPickupTypesJpaRepository extends PagingAndSortingRepository<MPickupTypesEntity, String> {

	List<MPickupTypesEntity> findByActiveFlag(int i);

	List<MPickupTypesEntity> findByVersionGreaterThanAndActiveFlag(Integer version,Integer activeFlag);
}
