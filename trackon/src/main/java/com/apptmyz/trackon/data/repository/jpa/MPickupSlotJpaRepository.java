package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MPickupSlotEntity;

/**
 * Repository : MPickupSlot.
 */
public interface MPickupSlotJpaRepository extends PagingAndSortingRepository<MPickupSlotEntity, Integer> {
	List<MPickupSlotEntity> findByActiveFlag(Integer activeFlag);
	
	List<MPickupSlotEntity> findByVersionGreaterThanAndActiveFlag(Integer version, Integer activeFlag);
}
