package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MaContentEntity;

/**
 * Repository : MaContent.
 */
public interface MaContentJpaRepository extends PagingAndSortingRepository<MaContentEntity, Integer> {

	List<MaContentEntity> findByVersionGreaterThan(Integer version);
}
