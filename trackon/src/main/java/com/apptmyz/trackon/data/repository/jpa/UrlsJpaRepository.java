package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.UrlsEntity;

/**
 * Repository : Urls.
 */
public interface UrlsJpaRepository extends PagingAndSortingRepository<UrlsEntity, Integer> {

}
