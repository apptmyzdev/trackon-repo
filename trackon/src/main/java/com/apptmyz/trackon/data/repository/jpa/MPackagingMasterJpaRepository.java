package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPackagingMasterEntity;

/**
 * Repository : MPackagingMaster.
 */
public interface MPackagingMasterJpaRepository extends PagingAndSortingRepository<MPackagingMasterEntity, Integer> {

	List<MPackagingMasterEntity> findByActiveFlag(int activeFlag);

	List<MPackagingMasterEntity> findByVersionGreaterThanAndActiveFlag(Integer version, int activeFlag);

}
