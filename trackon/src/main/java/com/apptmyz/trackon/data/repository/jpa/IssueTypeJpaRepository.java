package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.IssueTypeEntity;

/**
 * Repository : IssueType.
 */
public interface IssueTypeJpaRepository extends PagingAndSortingRepository<IssueTypeEntity, Integer> {

}
