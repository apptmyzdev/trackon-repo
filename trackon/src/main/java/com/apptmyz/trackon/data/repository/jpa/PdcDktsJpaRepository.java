package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.PdcDktsEntity;

/**
 * Repository : PdcDkts.
 */
public interface PdcDktsJpaRepository extends PagingAndSortingRepository<PdcDktsEntity, Integer> {

}
