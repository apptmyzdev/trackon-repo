package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MtBranchEntity;

/**
 * Repository : MtBranch.
 */
public interface MtBranchJpaRepository extends PagingAndSortingRepository<MtBranchEntity, Integer> {

}
