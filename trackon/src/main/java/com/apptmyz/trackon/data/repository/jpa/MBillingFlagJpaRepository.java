package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MBillingFlagEntity;

/**
 * Repository : MBillingFlag.
 */
public interface MBillingFlagJpaRepository extends PagingAndSortingRepository<MBillingFlagEntity, String> {

}
