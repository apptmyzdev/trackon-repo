package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.MaCustomerDetailsEntity;

/**
 * Repository : MaCustomerDetails.
 */
public interface MaCustomerDetailsJpaRepository extends PagingAndSortingRepository<MaCustomerDetailsEntity, Integer> {

	@Query(value = "Select c.cust_code, c.customer_name from ma_customer_details c where c.active_flag = 1 and c.cust_code like :custCode%" , nativeQuery = true)
	List<Object[]> findByCustCodeContaining(@Param("custCode") String custCode );
	
	MaCustomerDetailsEntity findByCustCode(String custCode);
}
