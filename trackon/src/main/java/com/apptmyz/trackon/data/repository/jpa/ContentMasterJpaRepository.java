package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.ContentMasterEntity;

/**
 * Repository : ContentMaster.
 */
public interface ContentMasterJpaRepository extends PagingAndSortingRepository<ContentMasterEntity, Integer> {

}
