package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DeliveriesUpdatedEntity;

/**
 * Repository : DeliveriesUpdated.
 */
public interface DeliveriesUpdatedJpaRepository extends PagingAndSortingRepository<DeliveriesUpdatedEntity, Integer> {

	List<DeliveriesUpdatedEntity> findByDeliverySheetIdAndDocketNumber(Integer deliverySheetId, Integer docketNumber);

	List<DeliveriesUpdatedEntity> findByErpResultAndDeliveryDateBetween(Boolean erpResult, Date from, Date to);
	
	List<DeliveriesUpdatedEntity> findByCreatedByAndErpResultAndDeliveryDateBetween(Integer vaId, Boolean erpResult, Date from, Date to);

	List<DeliveriesUpdatedEntity> findByErpResultAndDeliveryDateBetweenAndUserId(Boolean b, Date from, Date to,	String user);

	List<DeliveriesUpdatedEntity> findByUserIdAndDeliveryDateBetweenAndErpResult(String user, Date from, Date to, Boolean erpResult);
	
	List<DeliveriesUpdatedEntity> findByErpResultIsTrueOrderByCreatedTimestamp();
	
	List<DeliveriesUpdatedEntity> findByErpResultIsTrueAndDeliveryDateBetween(Date from, Date to);
	
}
