package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DeliveriesUpdatedDepsEntity;

/**
 * Repository : DeliveriesUpdatedDeps.
 */
public interface DeliveriesUpdatedDepsJpaRepository extends PagingAndSortingRepository<DeliveriesUpdatedDepsEntity, Integer> {

}
