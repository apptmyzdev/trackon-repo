package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.ZoneMasterEntity;

/**
 * Repository : ZoneMaster.
 */
public interface ZoneMasterJpaRepository extends PagingAndSortingRepository<ZoneMasterEntity, Integer> {

}
