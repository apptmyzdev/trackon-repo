package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.NdrsMasterEntity;

/**
 * Repository : NdrsMaster.
 */
public interface NdrsMasterJpaRepository extends PagingAndSortingRepository<NdrsMasterEntity, Integer> {

}
