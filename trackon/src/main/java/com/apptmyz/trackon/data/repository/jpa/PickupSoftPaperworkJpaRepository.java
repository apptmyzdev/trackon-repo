package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupSoftPaperworkEntity;

/**
 * Repository : PickupSoftPaperwork.
 */
public interface PickupSoftPaperworkJpaRepository extends PagingAndSortingRepository<PickupSoftPaperworkEntity, Integer> {

}
