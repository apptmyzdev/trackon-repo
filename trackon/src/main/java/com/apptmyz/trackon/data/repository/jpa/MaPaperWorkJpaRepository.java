package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MaPaperWorkEntity;

/**
 * Repository : MaPaperWork.
 */
public interface MaPaperWorkJpaRepository extends PagingAndSortingRepository<MaPaperWorkEntity, Integer> {

}
