package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MShipTypeEntity;

/**
 * Repository : MShipType.
 */
public interface MShipTypeJpaRepository extends PagingAndSortingRepository<MShipTypeEntity, String> {
	List<MShipTypeEntity> findByActiveFlag(Integer activeFlag);
}
