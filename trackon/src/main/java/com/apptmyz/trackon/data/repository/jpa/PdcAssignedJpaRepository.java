package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.PdcAssignedEntity;

/**
 * Repository : PdcAssigned.
 */
public interface PdcAssignedJpaRepository extends PagingAndSortingRepository<PdcAssignedEntity, Integer> {

	List<PdcAssignedEntity> findByPdcDateGreaterThanEqualAndAgentUserNameAndBranchCode(Date from, String userId, String branchCode);
	List<PdcAssignedEntity> findByPdcDateGreaterThanEqualAndAgentUserName(Date from, String userId);
	
	List<PdcAssignedEntity> findByPdcDateBetween(Date from, Date to);
	List<PdcAssignedEntity> findByPdcDateBetweenAndBranchCodeAndAgentUserName(Date from, Date to, String branchCode, String agentId);
	List<PdcAssignedEntity> findByPdcDateBetweenAndBranchCode(Date from, Date to, String branchCode);
	List<PdcAssignedEntity> findByPdcDateBetweenAndAgentUserName(Date from, Date to, String agentId);
	
}
