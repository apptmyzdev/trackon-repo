package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPickupAssignTypeEntity;

/**
 * Repository : MPickupAssignType.
 */
public interface MPickupAssignTypeJpaRepository extends PagingAndSortingRepository<MPickupAssignTypeEntity, String> {

	List<MPickupAssignTypeEntity> findByActiveFlag(int activeFlag);

}
