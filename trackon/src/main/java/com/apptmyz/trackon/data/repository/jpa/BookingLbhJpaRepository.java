package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingLbhEntity;

/**
 * Repository : BookingLbh.
 */
public interface BookingLbhJpaRepository extends PagingAndSortingRepository<BookingLbhEntity, Integer> {
	BookingLbhEntity findTopByDocketNoOrderByCreatedTimestampDesc(String docketNo);

	List<BookingLbhEntity> findByBookingData_Id(Integer bookingId);
}
