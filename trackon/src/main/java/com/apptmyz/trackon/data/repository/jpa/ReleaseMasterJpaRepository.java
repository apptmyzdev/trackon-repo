package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.ReleaseMasterEntity;

/**
 * Repository : ReleaseMaster.
 */
public interface ReleaseMasterJpaRepository extends PagingAndSortingRepository<ReleaseMasterEntity, Integer> {

}
