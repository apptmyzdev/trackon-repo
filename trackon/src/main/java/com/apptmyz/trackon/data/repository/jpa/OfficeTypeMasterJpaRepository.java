package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OfficeTypeMasterEntity;

/**
 * Repository : OfficeTypeMaster.
 */
public interface OfficeTypeMasterJpaRepository extends PagingAndSortingRepository<OfficeTypeMasterEntity, Integer> {

}
