package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MDocketStatusEntity;
import com.apptmyz.trackon.entities.jpa.MPickupStatusEntity;

/**
 * Repository : MDocketStatus.
 */
public interface MDocketStatusJpaRepository extends PagingAndSortingRepository<MDocketStatusEntity, String> {

	List<MDocketStatusEntity> findByActiveFlag(int i);

}
