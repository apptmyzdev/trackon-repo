package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PaperWorkMasterEntity;

/**
 * Repository : PaperWorkMaster.
 */
public interface PaperWorkMasterJpaRepository extends PagingAndSortingRepository<PaperWorkMasterEntity, Integer> {

}
