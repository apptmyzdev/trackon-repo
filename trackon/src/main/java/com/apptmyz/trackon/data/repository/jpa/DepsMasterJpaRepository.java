package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.DepsMasterEntity;

/**
 * Repository : DepsMaster.
 */
public interface DepsMasterJpaRepository extends PagingAndSortingRepository<DepsMasterEntity, Integer> {
	List<DepsMasterEntity> findByActiveFlag(int activeFlag);
	List<DepsMasterEntity> findByVersionGreaterThan(int maxVersion);
}
