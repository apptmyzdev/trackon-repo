package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MConsigneeRetailEntity;

/**
 * Repository : MConsigneeRetail.
 */
public interface MConsigneeRetailJpaRepository extends PagingAndSortingRepository<MConsigneeRetailEntity, Integer> {

}
