package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;

/**
 * Repository : TrackonDrs.
 */
public interface TrackonDrsJpaRepository extends PagingAndSortingRepository<TrackonDrsEntity, Integer> {

	TrackonDrsEntity findTopByOrderByDrsIdDesc();
	
	TrackonDrsEntity findTopByApiTypeOrderByDrsIdDesc(String apiType);

//	List<TrackonDrsEntity> findByCreatedTimestampGreaterThanEqualAndAssignedToUser(Date fromDate, String userId);
	
	List<TrackonDrsEntity> findByCreatedTimestampAfterAndAssignedToUser(Date fromDate, String userId);
	
	List<TrackonDrsEntity> findByCreatedTimestampBetween(Date from, Date to);
	
	List<TrackonDrsEntity> findByCreatedTimestampBetweenAndAssignedToUser(Date from, Date to, String assignedUser);

	List<TrackonDrsEntity> findByIdIn(List<Integer> ids);
	
	List<TrackonDrsEntity> findByCreatedTimestampAfter(Date fromDate);
	
//	TrackonDrsEntity findTop1ByDrsNoAndAwbNo(String drs, String dktNo);
	
	TrackonDrsEntity findFirstByDrsNoAndAwbNo(String drsno, String dktno);
	
	List<TrackonDrsEntity> findByCreatedTimestampAfterAndFranchiseBranchCode(Date fromDate, String franchiseeBranchCode);
	
	List<TrackonDrsEntity> findByCreatedTimestampAfterAndFranchiseBranchCodeAndAckRequired(Date fromDate, String franchiseeBranchCode, Integer ackRequired);

	List<TrackonDrsEntity> findByCreatedTimestampAfterAndAssignedToUserAndAckRequired(Date fromDate, String userId, Integer ackRequired);

	@Modifying
	@Query("update TrackonDrsEntity u set u.acknowledged = :ackStatus where u.drsNo = :drsNo")
	int updateDrsAck(@Param("ackStatus") Integer ackStatus, @Param("drsNo")String drsNo);
}
