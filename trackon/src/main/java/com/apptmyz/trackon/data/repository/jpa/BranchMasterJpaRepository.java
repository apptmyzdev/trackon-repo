package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BranchMasterEntity;

/**
 * Repository : BranchMaster.
 */
public interface BranchMasterJpaRepository extends PagingAndSortingRepository<BranchMasterEntity, Integer> {



	List<BranchMasterEntity> findByBranchCode(String branchCode);
	
	List<BranchMasterEntity> findByZoneCode(String zoneCode);
	
	List<BranchMasterEntity> findByRegionCode(String regionCode);
	
	List<BranchMasterEntity> findByRegionCodeAndZoneCodeAndBranchCode(String region, String zone, String branch);
	
	List<BranchMasterEntity> findByRegionCodeAndZoneCode(String region, String zone);
	
	BranchMasterEntity findByErpIdEquals(int erpId);
	
	

	
}
