package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PincodeAreaMasterEntity;

/**
 * Repository : PincodeAreaMaster.
 */
public interface PincodeAreaMasterJpaRepository extends PagingAndSortingRepository<PincodeAreaMasterEntity, Integer> {

}
