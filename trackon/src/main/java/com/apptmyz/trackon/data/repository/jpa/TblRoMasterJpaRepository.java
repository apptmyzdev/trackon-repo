package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblRoMasterEntity;

/**
 * Repository : TblRoMaster.
 */
public interface TblRoMasterJpaRepository extends PagingAndSortingRepository<TblRoMasterEntity, Integer> {

}
