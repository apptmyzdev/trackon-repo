package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MChargeTypesEntity;

/**
 * Repository : MChargeTypes.
 */
public interface MChargeTypesJpaRepository extends PagingAndSortingRepository<MChargeTypesEntity, Integer> {

	List<MChargeTypesEntity> findByActiveFlag(int i);

}
