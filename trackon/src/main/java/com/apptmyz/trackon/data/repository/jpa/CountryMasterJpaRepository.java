package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.CountryMasterEntity;

/**
 * Repository : CountryMaster.
 */
public interface CountryMasterJpaRepository extends PagingAndSortingRepository<CountryMasterEntity, Integer> {

}
