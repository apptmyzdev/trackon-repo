package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsNextTrackMasterEntity;

/**
 * Repository : OpsNextTrackMaster.
 */
public interface OpsNextTrackMasterJpaRepository extends PagingAndSortingRepository<OpsNextTrackMasterEntity, Integer> {

}
