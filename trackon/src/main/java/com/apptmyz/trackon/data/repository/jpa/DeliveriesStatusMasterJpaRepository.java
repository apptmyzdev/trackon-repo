package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DeliveriesStatusMasterEntity;

/**
 * Repository : DeliveriesStatusMaster.
 */
public interface DeliveriesStatusMasterJpaRepository extends PagingAndSortingRepository<DeliveriesStatusMasterEntity, Integer> {

	DeliveriesStatusMasterEntity findByStatus(String status);
}
