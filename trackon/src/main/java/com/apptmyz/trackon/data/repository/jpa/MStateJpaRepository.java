package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MStateEntity;

/**
 * Repository : MState.
 */
public interface MStateJpaRepository extends PagingAndSortingRepository<MStateEntity, String> {
}
