package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.IssueDetailsEntity;

/**
 * Repository : IssueDetails.
 */
public interface IssueDetailsJpaRepository extends PagingAndSortingRepository<IssueDetailsEntity, Integer> {

}
