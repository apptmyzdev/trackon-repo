package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsAirlineVehicleMasterEntity;

/**
 * Repository : OpsAirlineVehicleMaster.
 */
public interface OpsAirlineVehicleMasterJpaRepository extends PagingAndSortingRepository<OpsAirlineVehicleMasterEntity, Integer> {

}
