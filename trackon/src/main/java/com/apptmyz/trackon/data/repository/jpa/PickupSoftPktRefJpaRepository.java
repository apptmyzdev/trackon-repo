package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupSoftPktRefEntity;

/**
 * Repository : PickupSoftPktRef.
 */
public interface PickupSoftPktRefJpaRepository extends PagingAndSortingRepository<PickupSoftPktRefEntity, Integer> {

}
