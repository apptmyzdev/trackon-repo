package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsModeMasterEntity;

/**
 * Repository : OpsModeMaster.
 */
public interface OpsModeMasterJpaRepository extends PagingAndSortingRepository<OpsModeMasterEntity, Integer> {

}
