package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingPkupScanSummaryEntity;

/**
 * Repository : BookingPkupScanSummary.
 */
public interface BookingPkupScanSummaryJpaRepository extends PagingAndSortingRepository<BookingPkupScanSummaryEntity, Integer> {

}
