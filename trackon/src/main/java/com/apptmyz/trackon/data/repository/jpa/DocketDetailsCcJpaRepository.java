package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.DocketDetailsCcEntity;

/**
 * Repository : DocketDetailsCc.
 */
public interface DocketDetailsCcJpaRepository extends PagingAndSortingRepository<DocketDetailsCcEntity, String> {

}
