package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblUserMasterEntity;

/**
 * Repository : TblUserMaster.
 */
public interface TblUserMasterJpaRepository extends PagingAndSortingRepository<TblUserMasterEntity, Integer> {

}
