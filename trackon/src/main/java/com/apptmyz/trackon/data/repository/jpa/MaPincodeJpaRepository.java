package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;

/**
 * Repository : MaPincode.
 */
public interface MaPincodeJpaRepository extends PagingAndSortingRepository<MaPincodeEntity, Integer> {

	List<MaPincodeEntity> findByPincodeAndActiveFlag(Integer pincode, Integer activeFlg);

	List<MaPincodeEntity> findByPincodeAndOda(Integer pincode, Integer odaFlag);

	List<MaPincodeEntity> findByActiveFlagAndOda(Integer activeFlag, Integer odaFlag);

	MaPincodeEntity findByPincode(Integer pincode);
	
	@Query(value="select office_code from ma_pincode where pincode = :pinCode", nativeQuery=true)
	String getBranchCodeByPincode(@Param("pinCode") Integer pincode);
}
