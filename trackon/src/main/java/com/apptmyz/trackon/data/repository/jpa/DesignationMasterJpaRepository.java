package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DesignationMasterEntity;

/**
 * Repository : DesignationMaster.
 */
public interface DesignationMasterJpaRepository extends PagingAndSortingRepository<DesignationMasterEntity, Integer> {

}
