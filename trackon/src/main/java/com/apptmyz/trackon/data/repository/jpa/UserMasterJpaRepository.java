package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.UserMasterEntity;

/**
 * Repository : UserMaster.
 */
public interface UserMasterJpaRepository extends PagingAndSortingRepository<UserMasterEntity, Integer> {

	UserMasterEntity findByUserIdAndUserPassword(String userId, String password);

	UserMasterEntity findByUserId(String userId);

	List<UserMasterEntity> findByActiveFlagEquals(int i);
	
	List<UserMasterEntity> findByActiveFlagEqualsAndBranchCode(int activeFlag, String branchCode);

	long countByActiveFlagEquals(int activeFlag);

	List<UserMasterEntity> findByBranchCode(String branch);
	
	List<UserMasterEntity> findByBranchCodeAndUserTypeAndActiveFlag(String branch, String userType, Integer activeFlg);
	
	@Query(value="select distinct(user_id) from user_master where franchise_branch_code = :franchiseBranchCode and active_flag = 1", nativeQuery=true)
	Set<String> getFranchiseDeliveryUsers(@Param("franchiseBranchCode") String franchiseBranchCode);
	
	List<UserMasterEntity> findByFranchiseBranchCodeAndActiveFlag(String franchiseBranchCode, Integer activeFlg);
}
