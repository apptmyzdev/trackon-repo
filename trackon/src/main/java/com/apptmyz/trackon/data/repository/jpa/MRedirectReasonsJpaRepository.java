package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MPackagingMasterEntity;
import com.apptmyz.trackon.entities.jpa.MRedirectReasonsEntity;

/**
 * Repository : MRedirectReasons.
 */
public interface MRedirectReasonsJpaRepository extends PagingAndSortingRepository<MRedirectReasonsEntity, String> {

	List<MRedirectReasonsEntity> findByVersionGreaterThanAndActiveFlag(Integer version, int acriveFlag);

	List<MRedirectReasonsEntity> findByActiveFlag(int activeFlag);

}
