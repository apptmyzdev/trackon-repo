package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupSoftInvEwbEntity;

/**
 * Repository : PickupSoftInvEwb.
 */
public interface PickupSoftInvEwbJpaRepository extends PagingAndSortingRepository<PickupSoftInvEwbEntity, Integer> {

}
