package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupSoftLbhDataEntity;

/**
 * Repository : PickupSoftLbhData.
 */
public interface PickupSoftLbhDataJpaRepository extends PagingAndSortingRepository<PickupSoftLbhDataEntity, Integer> {

}
