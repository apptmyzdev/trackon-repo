package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MUserEntity;

/**
 * Repository : MUser.
 */
public interface MUserJpaRepository extends PagingAndSortingRepository<MUserEntity, Integer> {

}
