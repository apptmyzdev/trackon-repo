package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsColoaderTypeMasterEntity;

/**
 * Repository : OpsColoaderTypeMaster.
 */
public interface OpsColoaderTypeMasterJpaRepository extends PagingAndSortingRepository<OpsColoaderTypeMasterEntity, Integer> {

}
