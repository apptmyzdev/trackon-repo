package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.MaAreasEntity;

/**
 * Repository : MaAreas.
 */
public interface MaAreasJpaRepository extends PagingAndSortingRepository<MaAreasEntity, Integer> {

	List<MaAreasEntity> findByPincode(Integer pincode);
	
	@Query(value="select area_name from ma_areas where pincode = :pinCode", nativeQuery=true)
	List<String> getAreasByPincode(@Param("pinCode") Integer pincode);
	
}
