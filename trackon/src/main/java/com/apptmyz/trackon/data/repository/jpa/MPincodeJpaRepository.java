//package com.apptmyz.trackon.data.repository.jpa;
//
//import java.util.List;
//
//import org.springframework.data.repository.PagingAndSortingRepository;
//
//import com.apptmyz.trackon.entities.jpa.MPincodeEntity;
//
///**
// * Repository : MPincode.
// */
//public interface MPincodeJpaRepository extends PagingAndSortingRepository<MPincodeEntity, Integer> {
//	List<MPincodeEntity> findByPincodeAndOdaFlag(Integer pincode, Integer odaFlag);
//
//	List<MPincodeEntity> findByPincodeAndActiveFlag(Integer pincode, Integer active);
//	
//	List<MPincodeEntity> findByAreaAndActiveFlag(String area, Integer active);
//	
//	List<MPincodeEntity> findByMBranchBranchCodeAndActiveFlag(String branch, Integer active);
//
//	List<MPincodeEntity> findByActiveFlagAndOdaFlag(Integer active, Integer odaFlag);
//
//}
