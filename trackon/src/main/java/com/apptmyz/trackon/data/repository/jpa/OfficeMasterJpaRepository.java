package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OfficeMasterEntity;

/**
 * Repository : OfficeMaster.
 */
public interface OfficeMasterJpaRepository extends PagingAndSortingRepository<OfficeMasterEntity, Integer> {

}
