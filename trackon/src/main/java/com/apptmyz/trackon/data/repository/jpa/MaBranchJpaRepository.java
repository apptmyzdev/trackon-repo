package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MaBranchEntity;

/**
 * Repository : MaBranch.
 */
public interface MaBranchJpaRepository extends PagingAndSortingRepository<MaBranchEntity, Integer> {

	List<MaBranchEntity> findByActiveFlag(Integer activeFlag);

}
