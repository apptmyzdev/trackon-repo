package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsAirlineVehicleNameMasterEntity;

/**
 * Repository : OpsAirlineVehicleNameMaster.
 */
public interface OpsAirlineVehicleNameMasterJpaRepository extends PagingAndSortingRepository<OpsAirlineVehicleNameMasterEntity, Integer> {

}
