package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MRecurringPickupEntity;

/**
 * Repository : MRecurringPickup.
 */
public interface MRecurringPickupJpaRepository extends PagingAndSortingRepository<MRecurringPickupEntity, Integer> {
	List<MRecurringPickupEntity> findByActiveFlag(Integer activeFlag);
	
	List<MRecurringPickupEntity> findByVersionGreaterThanAndActiveFlag(Integer version, Integer activeFlag);
}
