package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MUomEntity;

/**
 * Repository : MUom.
 */
public interface MUomJpaRepository extends PagingAndSortingRepository<MUomEntity, Integer> {
	List<MUomEntity> findByActiveFlag(Integer activeFlag);
	List<MUomEntity> findByUomCatAndActiveFlag(String uomCat, Integer activeFlag);
	List<MUomEntity> findByVersionGreaterThanAndActiveFlag(Integer version, Integer activeFlag);
	List<MUomEntity> findByVersionGreaterThanAndUomCatAndActiveFlag(Integer version, String uomCat, Integer activeFlag);
}
