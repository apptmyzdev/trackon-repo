package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPktMasterEntity;

/**
 * Repository : MPktMaster.
 */
public interface MPktMasterJpaRepository extends PagingAndSortingRepository<MPktMasterEntity, Integer> {

	List<MPktMasterEntity> findByCompleteFlag(Integer completeFlag);

	MPktMasterEntity findByStartPktNoLessThanEqualAndEndPktNoGreaterThanEqual(Long lastUsedPktNo,
			Long lastUsedPktNo2);

	List<MPktMasterEntity> findByUserIdAndCompleteFlag(String userId, int i);
	
	
	MPktMasterEntity findByUserId(String userId);
}
