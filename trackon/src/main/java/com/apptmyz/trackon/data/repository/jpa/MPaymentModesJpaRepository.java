package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPaymentModesEntity;

/**
 * Repository : MPaymentModes.
 */
public interface MPaymentModesJpaRepository extends PagingAndSortingRepository<MPaymentModesEntity, String> {

	List<MPaymentModesEntity> findByVersionGreaterThan(int version);

}
