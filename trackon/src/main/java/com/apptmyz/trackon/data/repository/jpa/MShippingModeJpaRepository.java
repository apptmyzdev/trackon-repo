package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MShippingModeEntity;

/**
 * Repository : MShippingMode.
 */
public interface MShippingModeJpaRepository extends PagingAndSortingRepository<MShippingModeEntity, String> {

	List<MShippingModeEntity> findByActiveFlag(int activeFlag);
	
	List<MShippingModeEntity> findByVersionGreaterThanAndActiveFlag(Integer version, Integer activeFlag);

}
