package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPickupStatusEntity;

/**
 * Repository : MPickupStatus.
 */
public interface MPickupStatusJpaRepository extends PagingAndSortingRepository<MPickupStatusEntity, String> {

	List<MPickupStatusEntity> findByActiveFlag(int activeFlag);

}
