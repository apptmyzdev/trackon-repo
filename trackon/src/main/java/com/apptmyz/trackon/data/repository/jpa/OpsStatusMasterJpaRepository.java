package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsStatusMasterEntity;

/**
 * Repository : OpsStatusMaster.
 */
public interface OpsStatusMasterJpaRepository extends PagingAndSortingRepository<OpsStatusMasterEntity, Integer> {

}
