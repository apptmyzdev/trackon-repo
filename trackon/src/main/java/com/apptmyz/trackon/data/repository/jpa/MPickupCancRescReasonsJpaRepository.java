package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MPickupAssignTypeEntity;
import com.apptmyz.trackon.entities.jpa.MPickupCancRescReasonsEntity;

/**
 * Repository : MPickupCancRescReasons.
 */
public interface MPickupCancRescReasonsJpaRepository extends PagingAndSortingRepository<MPickupCancRescReasonsEntity, String> {

	List<MPickupCancRescReasonsEntity> findByActiveFlag(int activeFlag);

	List<MPickupCancRescReasonsEntity> findByVersionGreaterThanAndActiveFlag(Integer integer, int i);

}
