package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MRelationshipEntity;

/**
 * Repository : MRelationship.
 */
public interface MRelationshipJpaRepository extends PagingAndSortingRepository<MRelationshipEntity, String> {

	List<MRelationshipEntity> findByVersionGreaterThan(Integer version);
}
