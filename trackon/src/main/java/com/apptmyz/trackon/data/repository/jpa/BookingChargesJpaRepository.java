package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingChargesEntity;

/**
 * Repository : BookingCharges.
 */
public interface BookingChargesJpaRepository extends PagingAndSortingRepository<BookingChargesEntity, Integer> {
	BookingChargesEntity findTopByDocketNoOrderByCreatedTimestampDesc(String docketNo);  

	List<BookingChargesEntity> findByBookingData_Id(Integer bookingId);
}
