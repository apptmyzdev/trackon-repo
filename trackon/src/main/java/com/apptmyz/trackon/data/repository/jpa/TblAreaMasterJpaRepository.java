package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblAreaMasterEntity;

/**
 * Repository : TblAreaMaster.
 */
public interface TblAreaMasterJpaRepository extends PagingAndSortingRepository<TblAreaMasterEntity, Integer> {

}
