package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsHubCutoffMasterEntity;

/**
 * Repository : OpsHubCutoffMaster.
 */
public interface OpsHubCutoffMasterJpaRepository extends PagingAndSortingRepository<OpsHubCutoffMasterEntity, Integer> {

}
