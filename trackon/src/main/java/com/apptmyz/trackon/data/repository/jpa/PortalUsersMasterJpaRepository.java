package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PortalUsersMasterEntity;

/**
 * Repository : PortalUsersMaster.
 */
public interface PortalUsersMasterJpaRepository extends PagingAndSortingRepository<PortalUsersMasterEntity, Integer> {

	PortalUsersMasterEntity findByUserIdAndPassword(String  userId, String password);
	
	PortalUsersMasterEntity findByUserId(String userId);
}
