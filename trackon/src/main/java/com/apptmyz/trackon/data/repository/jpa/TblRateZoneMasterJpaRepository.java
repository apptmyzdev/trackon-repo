package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblRateZoneMasterEntity;

/**
 * Repository : TblRateZoneMaster.
 */
public interface TblRateZoneMasterJpaRepository extends PagingAndSortingRepository<TblRateZoneMasterEntity, Integer> {

}
