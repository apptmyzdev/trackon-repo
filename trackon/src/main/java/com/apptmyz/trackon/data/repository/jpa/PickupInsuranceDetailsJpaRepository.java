package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupInsuranceDetailsEntity;

/**
 * Repository : PickupInsuranceDetails.
 */
public interface PickupInsuranceDetailsJpaRepository extends PagingAndSortingRepository<PickupInsuranceDetailsEntity, Long> {

}
