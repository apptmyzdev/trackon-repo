package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingInvoicesEntity;

/**
 * Repository : BookingInvoices.
 */
public interface BookingInvoicesJpaRepository extends PagingAndSortingRepository<BookingInvoicesEntity, Integer> {
	BookingInvoicesEntity findTopByDocketNoOrderByCreatedTimestampDesc(String docketNo);

//	List<BookingInvoicesEntity> findByBookingData_DocketNo(String dktno);
	
	List<BookingInvoicesEntity> findByBookingData_Id(Integer bookingId);
}
