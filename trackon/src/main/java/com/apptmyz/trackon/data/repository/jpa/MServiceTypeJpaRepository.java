package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MServiceTypeEntity;

/**
 * Repository : MServiceType.
 */
public interface MServiceTypeJpaRepository extends PagingAndSortingRepository<MServiceTypeEntity, Integer> {

	List<MServiceTypeEntity> findByVersionGreaterThan(Integer version);
}
