//package com.apptmyz.trackon.data.repository.jpa;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.PagingAndSortingRepository;
//import org.springframework.data.repository.query.Param;
//
//import com.apptmyz.trackon.entities.jpa.MCustomerEntity;
//
///**
// * Repository : MCustomer.
// */
//public interface MCustomerJpaRepository extends PagingAndSortingRepository<MCustomerEntity, Integer> {
//	
//	@Query(value = "Select c.cust_code, c.cust_name from m_customer c where c.cust_code like :custCode%" , nativeQuery = true)
//	List<Object[]> findByCustCodeContaining(@Param("custCode") String custCode );
//	
//	MCustomerEntity findByCustCode(String custCode);
//	
//}
