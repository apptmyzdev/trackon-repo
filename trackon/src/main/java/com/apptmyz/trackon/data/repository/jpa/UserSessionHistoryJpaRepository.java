package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.UserSessionHistoryEntity;

/**
 * Repository : UserSessionHistory.
 */
public interface UserSessionHistoryJpaRepository extends PagingAndSortingRepository<UserSessionHistoryEntity, Integer> {
	List<UserSessionHistoryEntity> findByLoggedInTimestampBetween(Date from, Date to);
}
