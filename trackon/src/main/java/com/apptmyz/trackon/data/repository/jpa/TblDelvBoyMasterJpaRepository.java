package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblDelvBoyMasterEntity;

/**
 * Repository : TblDelvBoyMaster.
 */
public interface TblDelvBoyMasterJpaRepository extends PagingAndSortingRepository<TblDelvBoyMasterEntity, Integer> {

}
