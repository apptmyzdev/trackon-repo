package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.NdrStatusEntity;

/**
 * Repository : NdrStatus.
 */
public interface NdrStatusJpaRepository extends PagingAndSortingRepository<NdrStatusEntity, Integer> {

}
