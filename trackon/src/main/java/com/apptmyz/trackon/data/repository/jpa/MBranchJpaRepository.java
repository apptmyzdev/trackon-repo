//package com.apptmyz.trackon.data.repository.jpa;
//
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.data.repository.PagingAndSortingRepository;
//import com.apptmyz.trackon.entities.jpa.MBranchEntity;
//
///**
// * Repository : MBranch.
// */
//public interface MBranchJpaRepository extends PagingAndSortingRepository<MBranchEntity, Integer> {
//
//	MBranchEntity findByBranchCode(String branchCode);
//
//	List<MBranchEntity> findByActiveFlag(int activeFlag);
//}
