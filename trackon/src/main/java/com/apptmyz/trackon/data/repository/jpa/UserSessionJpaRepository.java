package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;

/**
 * Repository : UserSession.
 */
public interface UserSessionJpaRepository extends PagingAndSortingRepository<UserSessionEntity, Integer> {

	UserSessionEntity findByUserId(String userId);

	UserSessionEntity findByUserIdAndActiveFlag(String userId, int i);

	long countByActiveFlag(Integer activeFlag);
	
	List<UserSessionEntity> findByActiveFlag(Integer activeFlag);
}
