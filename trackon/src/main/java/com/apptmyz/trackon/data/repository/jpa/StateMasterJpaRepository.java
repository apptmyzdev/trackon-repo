package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.StateMasterEntity;

/**
 * Repository : StateMaster.
 */
public interface StateMasterJpaRepository extends PagingAndSortingRepository<StateMasterEntity, Integer> {

	StateMasterEntity findByStateId(Integer stateId);

}
