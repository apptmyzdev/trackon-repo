package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.UserAppVersionsEntity;

/**
 * Repository : UserAppVersions.
 */
public interface UserAppVersionsJpaRepository extends PagingAndSortingRepository<UserAppVersionsEntity, Integer> {

	UserAppVersionsEntity findByUserIdAndActiveFlagEquals(String userId, int activeFlag);
	
	UserAppVersionsEntity findFirst1ByBranchCodeAndActiveFlagEquals(String branchCode, int activeFlag);
}
