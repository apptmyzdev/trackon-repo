package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.ItemMasterEntity;

/**
 * Repository : ItemMaster.
 */
public interface ItemMasterJpaRepository extends PagingAndSortingRepository<ItemMasterEntity, Integer> {

}
