package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PickupSoftdataDetailsEntity;

/**
 * Repository : PickupSoftdataDetails.
 */
public interface PickupSoftdataDetailsJpaRepository extends PagingAndSortingRepository<PickupSoftdataDetailsEntity, Integer> {

}
