package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.AuditLogEntity;

/**
 * Repository : AuditLog.
 */
public interface AuditLogJpaRepository extends PagingAndSortingRepository<AuditLogEntity, Integer> {

	List<AuditLogEntity> findByTimestampBetween(Date from, Date to);
}
