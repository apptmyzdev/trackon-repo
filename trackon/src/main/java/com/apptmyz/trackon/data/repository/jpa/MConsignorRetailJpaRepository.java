package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MConsignorRetailEntity;

/**
 * Repository : MConsignorRetail.
 */
public interface MConsignorRetailJpaRepository extends PagingAndSortingRepository<MConsignorRetailEntity, Integer> {

}
