package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.BookingPickupScanDepsImagesEntity;

/**
 * Repository : BookingPickupScanDepsImages.
 */
public interface BookingPickupScanDepsImagesJpaRepository extends PagingAndSortingRepository<BookingPickupScanDepsImagesEntity, Integer> {

}
