package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblRoleMasterEntity;

/**
 * Repository : TblRoleMaster.
 */
public interface TblRoleMasterJpaRepository extends PagingAndSortingRepository<TblRoleMasterEntity, Integer> {

}
