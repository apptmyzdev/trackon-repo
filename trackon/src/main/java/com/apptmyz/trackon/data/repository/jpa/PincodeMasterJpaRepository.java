package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PincodeMasterEntity;

/**
 * Repository : PincodeMaster.
 */
public interface PincodeMasterJpaRepository extends PagingAndSortingRepository<PincodeMasterEntity, Integer> {

	PincodeMasterEntity findByPincode(String originPinCode);

}
