package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MPaperworkTypeEntity;

/**
 * Repository : MPaperworkType.
 */
public interface MPaperworkTypeJpaRepository extends PagingAndSortingRepository<MPaperworkTypeEntity, Integer> {

	List<MPaperworkTypeEntity> findByActiveFlag(int i);
	
	List<MPaperworkTypeEntity> findByVersionGreaterThanAndActiveFlag(Integer version, Integer actievFlag);

}
