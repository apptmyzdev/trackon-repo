package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PortalUserRolesEntity;

/**
 * Repository : PortalUserRoles.
 */
public interface PortalUserRolesJpaRepository extends PagingAndSortingRepository<PortalUserRolesEntity, Integer> {

	PortalUserRolesEntity findByRole(String userRole);

}
