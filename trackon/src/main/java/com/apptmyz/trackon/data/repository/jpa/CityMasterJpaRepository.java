package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.CityMasterEntity;

/**
 * Repository : CityMaster.
 */
public interface CityMasterJpaRepository extends PagingAndSortingRepository<CityMasterEntity, Integer> {

	CityMasterEntity findByCityId(Integer cityId);

}
