package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.VendorMasterEntity;

/**
 * Repository : VendorMaster.
 */
public interface VendorMasterJpaRepository extends PagingAndSortingRepository<VendorMasterEntity, Integer> {

}
