package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.CustomerMasterEntity;

/**
 * Repository : CustomerMaster.
 */
public interface CustomerMasterJpaRepository extends PagingAndSortingRepository<CustomerMasterEntity, Integer> {

}
