package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.AppVersionsEntity;

/**
 * Repository : AppVersions.
 */
public interface AppVersionsJpaRepository extends PagingAndSortingRepository<AppVersionsEntity, Integer> {

	AppVersionsEntity findTopByOrderByVersionDesc();
}
