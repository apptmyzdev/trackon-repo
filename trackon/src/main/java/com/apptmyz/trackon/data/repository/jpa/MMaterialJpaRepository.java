package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MMaterialEntity;

/**
 * Repository : MMaterial.
 */
public interface MMaterialJpaRepository extends PagingAndSortingRepository<MMaterialEntity, String> {

	List<MMaterialEntity> findByVersionGreaterThan(int version);

}
