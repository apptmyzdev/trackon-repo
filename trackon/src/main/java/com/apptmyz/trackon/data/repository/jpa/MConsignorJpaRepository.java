package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MConsignorEntity;

/**
 * Repository : MConsignor.
 */
public interface MConsignorJpaRepository extends PagingAndSortingRepository<MConsignorEntity, Integer> {
	List<MConsignorEntity> findByPincode(Integer pincode);
	List<MConsignorEntity> findByConsignorCode(String consignorCode); 
	List<MConsignorEntity> findByBranchCode(String branch);

	List<MConsignorEntity> findByBranchCodeAndVersionGreaterThan(String branch, Integer integer);

	MConsignorEntity findTopByOrderByVersionDesc();
	
	List<MConsignorEntity> findByPincodeAndConsignorNameLike(Integer pincode, String word);

}
