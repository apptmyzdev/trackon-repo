package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.PrimeCountryMasterEntity;

/**
 * Repository : PrimeCountryMaster.
 */
public interface PrimeCountryMasterJpaRepository extends PagingAndSortingRepository<PrimeCountryMasterEntity, Integer> {

}
