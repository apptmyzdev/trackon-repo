package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.PickupAssignmentEntity;
import com.apptmyz.trackon.entities.jpa.PickupRegistrationEntity;

/**
 * Repository : PickupAssignment.
 */
public interface PickupAssignmentJpaRepository extends PagingAndSortingRepository<PickupAssignmentEntity, Integer> {
	// PickupAssignmentEntity findByPickupId(Integer pickupId);
	PickupAssignmentEntity findTopByPickupRegistrationAndActiveFlagOrderByIdDesc(
			PickupRegistrationEntity pickupRegistration, int activeFlag);

	List<PickupAssignmentEntity> findByAssignedToAndPickupDateAndActiveFlag(String userId, Date pickupDate, int i);

	PickupAssignmentEntity findByPickupRegistration(PickupRegistrationEntity pickupRegistration);

	PickupAssignmentEntity findByPickupRegistrationAndActiveFlag(PickupRegistrationEntity pickupRegistration,
			int activeFlag);

	List<PickupAssignmentEntity> findByAssignedToAndPickupDateAndActiveFlagAndPickupType(String userId, Date pickupDate,
			int i, String pickupType);

	List<PickupAssignmentEntity> findByAssignedToAndAssUsrTyp(String userId, String userType);
	
	List<PickupAssignmentEntity> findByAssignedToAndActiveFlag(String userId, Integer activeFlag);
}
