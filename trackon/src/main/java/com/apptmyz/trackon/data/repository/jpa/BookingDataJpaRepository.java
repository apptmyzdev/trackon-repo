package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.BookingDataEntity;

/**
 * Repository : BookingData.
 */
public interface BookingDataJpaRepository extends PagingAndSortingRepository<BookingDataEntity, Integer> {

	List<BookingDataEntity> findByPickupTimestampBetween(Date from, Date to);

	List<BookingDataEntity> findByDocketNo(String dktNo);

	List<BookingDataEntity> findByCreatedTimestampBetweenAndDoxFlagAndErpStatus(Date from, Date to, String bookingType,
			Integer erpStatus);

	List<BookingDataEntity> findByCreatedTimestampBetweenAndErpStatus(Date from, Date to, Integer activeFlg);

	BookingDataEntity findTopByDocketNoOrderByCreatedTimestampDesc(String docketNo);
	
	BookingDataEntity findTopByDocketNoAndErpStatusIsNullOrderByCreatedTimestampDesc(String docketNo);
	
	BookingDataEntity findTopByXbAwbNo(String xbdocketNo);
	
	List<BookingDataEntity> findByXbAwbNo(String xbdocketNo);
	
	List<BookingDataEntity> findByCreatedTimestampBetweenAndBookingBranchAndErpStatus(Date from, Date to, String bookingBranch,Integer erpStatus);

	List<BookingDataEntity> findByDocketNoAndErpStatus(String dktNo, Integer erpStatus);
	
	@Query(value="select docket_no from booking_data where created_timestamp >= :fromdate and created_timestamp <= :todate and erp_status = 1", nativeQuery=true)
	Set<String> getSuccessDktNos(@Param("fromdate") Date fromDate, @Param("todate") Date toDate);
	
	@Query(value="select docket_no from booking_data where created_timestamp >= :fromdate and created_timestamp <= :todate and booking_branch = :branchCode and erp_status = 1", nativeQuery=true)
	Set<String> getSuccessDktNosByBranch(@Param("fromdate") Date fromDate, @Param("todate") Date toDate, @Param("branchCode") String branchCode);


	List<BookingDataEntity> findByCreatedTimestampBetweenAndErpStatusIsNullAndRepushCountIsNull(Date from, Date to);
	

	@Query(value="select docket_no from booking_data where xb_awb_no = :xbdktno", nativeQuery=true)
	List<String> getxbdktrows(@Param("xbdktno") String xbdktno);
}
