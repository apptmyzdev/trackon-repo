package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MPermissionTypeEntity;

/**
 * Repository : MPermissionType.
 */
public interface MPermissionTypeJpaRepository extends PagingAndSortingRepository<MPermissionTypeEntity, Integer> {

}
