package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MCashFreightEntity;

/**
 * Repository : MCashFreight.
 */
public interface MCashFreightJpaRepository extends PagingAndSortingRepository<MCashFreightEntity, Integer> {

	MCashFreightEntity findByCategoryAndDoxNondoxAndShipMode(String category, String doxNondox, String shipMode);
	MCashFreightEntity findByCategoryAndDoxNondoxAndShipModeAndMinWtLessThanEqualAndMaxWtGreaterThanEqual(String category, String doxNondox, String shipMode, Integer minWt, Integer maxWt);
}
