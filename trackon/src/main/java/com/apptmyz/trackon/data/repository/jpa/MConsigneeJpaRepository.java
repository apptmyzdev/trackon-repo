package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;

/**
 * Repository : MConsignee.
 */
public interface MConsigneeJpaRepository extends PagingAndSortingRepository<MConsigneeEntity, Integer> {
	
	List<MConsigneeEntity> findByPincode(Integer pincode);

	List<MConsigneeEntity> findByPincodeAndConsigneeNameLike(Integer pincode, String word);
}
