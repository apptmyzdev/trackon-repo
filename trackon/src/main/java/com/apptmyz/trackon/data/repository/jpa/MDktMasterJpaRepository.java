package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MDktMasterEntity;

/**
 * Repository : MDktMaster.
 */
public interface MDktMasterJpaRepository extends PagingAndSortingRepository<MDktMasterEntity, Integer> {

	List<MDktMasterEntity> findByConsumed(int consumed);

	MDktMasterEntity findByDktNo(String docketNo);
	
	List<MDktMasterEntity> findByUserIdAndConsumed(String userId, int consumed);

}
