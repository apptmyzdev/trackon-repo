package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MUserPermissionEntity;

/**
 * Repository : MUserPermission.
 */
public interface MUserPermissionJpaRepository extends PagingAndSortingRepository<MUserPermissionEntity, Integer> {

}
