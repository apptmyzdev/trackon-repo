package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.DrAcknowledgementEntity;

/**
 * Repository : DrAcknowledgement.
 */
public interface DrAcknowledgementJpaRepository extends PagingAndSortingRepository<DrAcknowledgementEntity, Integer> {

}
