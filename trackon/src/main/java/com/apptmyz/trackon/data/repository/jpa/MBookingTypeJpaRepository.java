package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.MBookingTypeEntity;

/**
 * Repository : MBookingType.
 */
public interface MBookingTypeJpaRepository extends PagingAndSortingRepository<MBookingTypeEntity, String> {

	List<MBookingTypeEntity> findByVersionGreaterThan(int version);

}
