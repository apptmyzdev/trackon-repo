package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.DlyDataEntity;

/**
 * Repository : DlyData.
 */
public interface DlyDataJpaRepository extends PagingAndSortingRepository<DlyDataEntity, Integer> {
	List<DlyDataEntity> findByDeliveredTimeBetween(Date from, Date to);
	
	@Query(value="select dkt_no from dly_data where created_timestamp >= DATE_SUB(CURDATE(), INTERVAL 3 DAY) and dly_status = 'D'", nativeQuery=true)
	Set<String> getUpdatedDeliveredDkts();
	
	@Query(value = "select pdc_no, dkt_no, dly_status from dly_data where created_timestamp >= DATE_SUB(CURDATE(), INTERVAL 3 DAY) and (agent_user_name = :userId or agent_user_name = :franchiseBranchCode)", nativeQuery = true)
	List<Object[]> getUpdatedDkts(@Param("userId") String userId,
			@Param("franchiseBranchCode") String franchiseBranchCode);
	
	@Query(value="select count(1) from dly_data", nativeQuery=true)
	Long getDeliveriesCount();
	
	@Query(value="select created_timestamp from dly_data where created_timestamp >= :fromdate and created_timestamp <= :todate", nativeQuery=true)
	List<Date> getUpdatedDeliveriesBetween(@Param("fromdate") Date from, @Param("todate") Date todate);

	DlyDataEntity findByDktNoAndErpStatus(String dktNo, Integer erpStatus);
	
	DlyDataEntity findFirstByPdcNoAndDktNo(String pdc, String dktno);
	
	List<DlyDataEntity> findByCreatedTimestampBetweenAndAgentUserName(Date fdate, Date todate, String userId);
	
	DlyDataEntity findTopByDktNoAndDlyStatusOrderByCreatedTimestampDesc(String dktNo, String dlyStatus);
	
	List<DlyDataEntity> findByCreatedTimestampBetweenAndErpStatusAndAttemptCount(Date from, Date to, Integer erpStatus, Integer attemptCount);

}
