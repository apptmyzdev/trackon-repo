package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.OpsColoaderMasterEntity;

/**
 * Repository : OpsColoaderMaster.
 */
public interface OpsColoaderMasterJpaRepository extends PagingAndSortingRepository<OpsColoaderMasterEntity, Integer> {

}
