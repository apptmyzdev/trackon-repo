package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.BulkInterchangePickupsEntity;

/**
 * Repository : BulkInterchangePickups.
 */
public interface BulkInterchangePickupsJpaRepository extends PagingAndSortingRepository<BulkInterchangePickupsEntity, Integer> {

}
