package com.apptmyz.trackon.data.repository.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.PickupRegistrationEntity;

/**
 * Repository : PickupRegistration.
 */
public interface PickupRegistrationJpaRepository extends PagingAndSortingRepository<PickupRegistrationEntity, Integer> {
	
	List<PickupRegistrationEntity> findByPickupDateBetween(Date from, Date to);

	List<PickupRegistrationEntity> findByUserIdAndIsFranchise(String userId, Integer isFranchise);
	
	List<PickupRegistrationEntity> findByUserIdAndCustCode(String userId, String custCode);
	
	List<PickupRegistrationEntity> findByUserIdAndCustCodeAndActiveFlag(String userId, String custCode, Integer activeFlg);
	
	PickupRegistrationEntity findByErpPickupOrderId(String erpPickupOrderId);
}
