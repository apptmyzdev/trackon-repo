package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.BookingXbPktsEntity;

/**
 * Repository : BookingXbPkts.
 */
public interface BookingXbPktsJpaRepository extends PagingAndSortingRepository<BookingXbPktsEntity, Integer> {

	List<BookingXbPktsEntity> findByBookingId(Integer bookingId);
}
