package com.apptmyz.trackon.data.repository.jpa;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.MDktRangeMasterEntity;

/**
 * Repository : MDktRangeMaster.
 */
public interface MDktRangeMasterJpaRepository extends PagingAndSortingRepository<MDktRangeMasterEntity, Integer> {

	@Query("SELECT r from MDktRangeMasterEntity r WHERE :num BETWEEN r.fromSeries AND r.toSeries AND r.userId = 'XB' AND r.activeFlag = 1 ")
	MDktRangeMasterEntity checkNumInActiveRangeForXb(@Param("num") BigInteger seriesNo);
}
