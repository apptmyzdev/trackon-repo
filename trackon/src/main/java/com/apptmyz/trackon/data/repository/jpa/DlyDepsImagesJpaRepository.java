package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.DlyDepsImagesEntity;

/**
 * Repository : DlyDepsImages.
 */
public interface DlyDepsImagesJpaRepository extends PagingAndSortingRepository<DlyDepsImagesEntity, Integer> {

	
	List<DlyDepsImagesEntity> findByDktNo(String dktno);
}
