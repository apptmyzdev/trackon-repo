package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.UndlyImagesEntity;

/**
 * Repository : UndlyImages.
 */
public interface UndlyImagesJpaRepository extends PagingAndSortingRepository<UndlyImagesEntity, Integer> {
	List<UndlyImagesEntity> findByPdcNoAndDktNo(String pdcNo, String dktno);
}
