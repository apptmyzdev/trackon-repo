package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblUserInRoleEntity;

/**
 * Repository : TblUserInRole.
 */
public interface TblUserInRoleJpaRepository extends PagingAndSortingRepository<TblUserInRoleEntity, Integer> {

}
