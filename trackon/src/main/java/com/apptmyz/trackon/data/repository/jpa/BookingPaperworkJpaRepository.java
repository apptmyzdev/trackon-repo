package com.apptmyz.trackon.data.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.apptmyz.trackon.entities.jpa.BookingPaperworkEntity;

/**
 * Repository : BookingPaperwork.
 */
public interface BookingPaperworkJpaRepository extends PagingAndSortingRepository<BookingPaperworkEntity, Integer> {
//	BookingPaperworkEntity findTopByBookingDataDocketNoOrderByCreatedTimestampDesc(String docketNo);

	List<BookingPaperworkEntity> findByBookingId(Integer bookingId);
	
//	@Query("select b from BookingPaperworkEntity b where b.docketNo = :docketNo and order by b.createdTimestamp desc limit 1")
//	List<BookingPaperworkEntity> getBookingPaperWork(@Param("docketNo") String docketNo);
}
