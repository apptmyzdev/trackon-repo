package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.ItemGroupMasterEntity;

/**
 * Repository : ItemGroupMaster.
 */
public interface ItemGroupMasterJpaRepository extends PagingAndSortingRepository<ItemGroupMasterEntity, Integer> {

}
