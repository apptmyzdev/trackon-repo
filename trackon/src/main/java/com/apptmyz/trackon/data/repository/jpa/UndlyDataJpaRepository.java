package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.entities.jpa.UndlyDataEntity;

/**
 * Repository : UndlyData.
 */
public interface UndlyDataJpaRepository extends PagingAndSortingRepository<UndlyDataEntity, Integer> {

}
