package com.apptmyz.trackon.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.entities.jpa.TblHolidayMasterEntity;

/**
 * Repository : TblHolidayMaster.
 */
public interface TblHolidayMasterJpaRepository extends PagingAndSortingRepository<TblHolidayMasterEntity, Integer> {

}
