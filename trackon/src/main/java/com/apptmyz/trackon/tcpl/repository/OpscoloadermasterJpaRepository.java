package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpscoloadermasterEntity;

/**
 * Repository : Opscoloadermaster.
 */
public interface OpscoloadermasterJpaRepository extends PagingAndSortingRepository<OpscoloadermasterEntity, Integer> {

}
