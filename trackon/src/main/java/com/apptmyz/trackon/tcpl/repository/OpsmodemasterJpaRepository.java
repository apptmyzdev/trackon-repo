package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpsmodemasterEntity;

/**
 * Repository : Opsmodemaster.
 */
public interface OpsmodemasterJpaRepository extends PagingAndSortingRepository<OpsmodemasterEntity, Integer> {

}
