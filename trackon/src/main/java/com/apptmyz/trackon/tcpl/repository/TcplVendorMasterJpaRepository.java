package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.VendormasterEntity;

public interface TcplVendorMasterJpaRepository extends PagingAndSortingRepository<VendormasterEntity, Integer> {

}


