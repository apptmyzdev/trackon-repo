package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.OpsnexttrackmasterEntity;

/**
 * Repository : Opsnexttrackmaster.
 */
public interface OpsnexttrackmasterJpaRepository extends PagingAndSortingRepository<OpsnexttrackmasterEntity, String> {

}
