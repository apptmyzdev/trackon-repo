package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.MtbranchEntity;



public interface MtbranchJpaRepository extends PagingAndSortingRepository<MtbranchEntity, Integer>{

}
