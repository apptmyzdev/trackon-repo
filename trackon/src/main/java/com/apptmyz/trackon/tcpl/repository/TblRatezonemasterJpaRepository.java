package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.TblRatezonemasterEntity;

/**
 * Repository : TblRatezonemaster.
 */
public interface TblRatezonemasterJpaRepository extends PagingAndSortingRepository<TblRatezonemasterEntity, String> {

}
