package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.TblUsermasterEntity;

/**
 * Repository : TblUsermaster.
 */
public interface TblUsermasterJpaRepository extends PagingAndSortingRepository<TblUsermasterEntity, String> {

}
