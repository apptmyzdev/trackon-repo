package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.TbldelvboymasterEntity;

/**
 * Repository : Tbldelvboymaster.
 */
public interface TbldelvboymasterJpaRepository extends PagingAndSortingRepository<TbldelvboymasterEntity, String> {

}
