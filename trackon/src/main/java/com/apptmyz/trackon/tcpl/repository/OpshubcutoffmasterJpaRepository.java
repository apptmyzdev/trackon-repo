package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpshubcutoffmasterEntity;

/**
 * Repository : Opshubcutoffmaster.
 */
public interface OpshubcutoffmasterJpaRepository extends PagingAndSortingRepository<OpshubcutoffmasterEntity, Integer> {

}
