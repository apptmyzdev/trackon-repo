package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.PaperworkmasterEntity;

public interface TcplPaperWorkMasterJpaRepository extends PagingAndSortingRepository<PaperworkmasterEntity, Integer> {

}
