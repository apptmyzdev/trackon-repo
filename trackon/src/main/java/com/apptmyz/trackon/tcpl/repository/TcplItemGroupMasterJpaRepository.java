package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.ItemgroupmasterEntity;


public interface TcplItemGroupMasterJpaRepository extends PagingAndSortingRepository<ItemgroupmasterEntity, Integer>{

}
