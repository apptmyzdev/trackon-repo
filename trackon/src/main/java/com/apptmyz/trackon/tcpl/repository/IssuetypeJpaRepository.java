package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.IssuetypeEntity;;

public interface IssuetypeJpaRepository extends PagingAndSortingRepository<IssuetypeEntity, String> {

}
