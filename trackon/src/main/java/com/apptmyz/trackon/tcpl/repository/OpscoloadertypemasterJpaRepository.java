package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpscoloadertypemasterEntity;

/**
 * Repository : Opscoloadertypemaster.
 */
public interface OpscoloadertypemasterJpaRepository extends PagingAndSortingRepository<OpscoloadertypemasterEntity, Integer> {

}
