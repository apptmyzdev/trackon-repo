package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.CountrymasterEntity;

/**
 * Repository : Countrymaster.
 */
public interface CountrymasterJpaRepository extends PagingAndSortingRepository<CountrymasterEntity, String> {

//	CountrymasterEntity findByCountrycode(String code);
	
}
