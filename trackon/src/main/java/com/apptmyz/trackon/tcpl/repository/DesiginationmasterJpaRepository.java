package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.DesiginationmasterEntity;


/**
 * Repository : Desiginationmaster.
 */
public interface DesiginationmasterJpaRepository extends PagingAndSortingRepository<DesiginationmasterEntity, String> {

}
