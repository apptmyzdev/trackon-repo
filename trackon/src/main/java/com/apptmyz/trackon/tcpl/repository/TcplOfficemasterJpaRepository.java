package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OfficemasterEntity;

/**
 * Repository : Officemaster.
 */
public interface TcplOfficemasterJpaRepository extends PagingAndSortingRepository<OfficemasterEntity, String> {

}
