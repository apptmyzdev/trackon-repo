package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.OpsstatusmasterEntity;



public interface OpsstatusmasterJpaRepository extends PagingAndSortingRepository<OpsstatusmasterEntity, String> {

}
