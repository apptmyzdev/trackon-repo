package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.ReleasemasterEntity;


public interface ReleasemasterJpaRepository extends PagingAndSortingRepository<ReleasemasterEntity, String>{

}
