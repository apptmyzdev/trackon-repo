package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.ItemmasterEntity;

/**
 * Repository : Itemmaster.
 */
public interface ItemmasterJpaRepository extends PagingAndSortingRepository<ItemmasterEntity, String> {

}
