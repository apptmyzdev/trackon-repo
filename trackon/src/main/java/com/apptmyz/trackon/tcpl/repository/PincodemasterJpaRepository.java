package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.PincodemasterEntity;

/**
 * Repository : Pincodemaster.
 */
public interface PincodemasterJpaRepository extends PagingAndSortingRepository<PincodemasterEntity, String> {

}
