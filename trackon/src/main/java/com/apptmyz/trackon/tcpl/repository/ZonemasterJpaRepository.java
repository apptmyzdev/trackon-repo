package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.ZonemasterEntity;

/**
 * Repository : Zonemaster.
 */
public interface ZonemasterJpaRepository extends PagingAndSortingRepository<ZonemasterEntity, Integer> {

}
