/*
 * Created on 15 Jul 2022 ( Time 11:37:05 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.tcpl.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "ReleaseMaster"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="ReleaseMaster", schema="dbo", catalog="tcplMaster" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="ReleasemasterEntity.countAll", query="SELECT COUNT(x) FROM ReleasemasterEntity x" )
} )
public class ReleasemasterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="Id", nullable=false)
    private Integer    id           ;

    @Column(name="Category", length=5)
    private String     category     ;

    @Column(name="Code", length=5)
    private String     code         ;

    @Column(name="Description", length=100)
    private String     description  ;

    @Column(name="ActiveStatus", length=1)
    private String     activestatus ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public ReleasemasterEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : Id ( int identity ) 
    public void setId( Integer id ) {
        this.id = id;
    }
    public Integer getId() {
        return this.id;
    }

    //--- DATABASE MAPPING : Category ( varchar ) 
    public void setCategory( String category ) {
        this.category = category;
    }
    public String getCategory() {
        return this.category;
    }

    //--- DATABASE MAPPING : Code ( varchar ) 
    public void setCode( String code ) {
        this.code = code;
    }
    public String getCode() {
        return this.code;
    }

    //--- DATABASE MAPPING : Description ( varchar ) 
    public void setDescription( String description ) {
        this.description = description;
    }
    public String getDescription() {
        return this.description;
    }

    //--- DATABASE MAPPING : ActiveStatus ( char ) 
    public void setActivestatus( String activestatus ) {
        this.activestatus = activestatus;
    }
    public String getActivestatus() {
        return this.activestatus;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append("]:"); 
        sb.append(id);
        sb.append("|");
        sb.append(category);
        sb.append("|");
        sb.append(code);
        sb.append("|");
        sb.append(description);
        sb.append("|");
        sb.append(activestatus);
        return sb.toString(); 
    } 

}
