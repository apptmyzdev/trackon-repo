package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.CustomermasterEntity;


/**
 * Repository : Customermaster.
 */
public interface CustomermasterJpaRepository extends PagingAndSortingRepository<CustomermasterEntity, String> {

}
