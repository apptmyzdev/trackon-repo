package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.TblareamasterEntity;

/**
 * Repository : Tblareamaster.
 */
public interface TblareamasterJpaRepository extends PagingAndSortingRepository<TblareamasterEntity, Integer> {

}
