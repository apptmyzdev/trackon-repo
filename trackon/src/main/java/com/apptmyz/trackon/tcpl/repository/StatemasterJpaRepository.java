package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.StatemasterEntity;

/**
 * Repository : Statemaster.
 */
public interface StatemasterJpaRepository extends PagingAndSortingRepository<StatemasterEntity, String> {

}
