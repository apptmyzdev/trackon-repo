package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.NdrsmasterEntity;

public interface TcplNdrsMasterJpaRepository extends PagingAndSortingRepository<NdrsmasterEntity, Integer> {

}
