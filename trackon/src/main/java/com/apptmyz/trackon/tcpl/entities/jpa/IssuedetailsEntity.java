/*
 * Created on 13 Jul 2022 ( Time 13:36:36 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.tcpl.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "IssueDetails"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="IssueDetails", schema="dbo", catalog="tcplMaster" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="IssuedetailsEntity.countAll", query="SELECT COUNT(x) FROM IssuedetailsEntity x" )
} )
public class IssuedetailsEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //---------------------------------------------------------------------- 
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="IssueDetail_Id", nullable=false)
    private Integer    issuedetailId ;

    @Column(name="Issue_Details", nullable=false, length=250)
    private String     issueDetails ;

    @Column(name="Priotity", nullable=false, length=10)
    private String     priotity     ;

    @Column(name="Issue_Id", nullable=false)
    private Integer    issueId      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public IssuedetailsEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : IssueDetail_Id ( int identity ) 
    public void setIssuedetailId( Integer issuedetailId ) {
        this.issuedetailId = issuedetailId;
    }
    public Integer getIssuedetailId() {
        return this.issuedetailId;
    }

    //--- DATABASE MAPPING : Issue_Details ( varchar ) 
    public void setIssueDetails( String issueDetails ) {
        this.issueDetails = issueDetails;
    }
    public String getIssueDetails() {
        return this.issueDetails;
    }

    //--- DATABASE MAPPING : Priotity ( varchar ) 
    public void setPriotity( String priotity ) {
        this.priotity = priotity;
    }
    public String getPriotity() {
        return this.priotity;
    }

    //--- DATABASE MAPPING : Issue_Id ( int ) 
    public void setIssueId( Integer issueId ) {
        this.issueId = issueId;
    }
    public Integer getIssueId() {
        return this.issueId;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append("]:"); 
        sb.append(issuedetailId);
        sb.append("|");
        sb.append(issueDetails);
        sb.append("|");
        sb.append(priotity);
        sb.append("|");
        sb.append(issueId);
        return sb.toString(); 
    } 

}
