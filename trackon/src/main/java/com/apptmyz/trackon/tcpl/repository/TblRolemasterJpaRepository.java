package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.TblRolemasterEntity;

/**
 * Repository : TblRolemaster.
 */
public interface TblRolemasterJpaRepository extends PagingAndSortingRepository<TblRolemasterEntity, String> {

}
