package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpsairlinevehiclenamemasterEntity;

/**
 * Repository : Opsairlinevehiclenamemaster.
 */
public interface OpsairlinevehiclenamemasterJpaRepository extends PagingAndSortingRepository<OpsairlinevehiclenamemasterEntity, Integer> {

}
