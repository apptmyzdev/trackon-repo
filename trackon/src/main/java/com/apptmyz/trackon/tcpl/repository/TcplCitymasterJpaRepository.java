package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.CitymasterEntity;
/**
 * Repository : Citymaster.
 */
public interface TcplCitymasterJpaRepository extends PagingAndSortingRepository<CitymasterEntity, Integer> {

}
