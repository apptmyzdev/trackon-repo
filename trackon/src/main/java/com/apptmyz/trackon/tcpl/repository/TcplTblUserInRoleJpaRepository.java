package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.TblUserinroleEntity;


public interface TcplTblUserInRoleJpaRepository extends PagingAndSortingRepository<TblUserinroleEntity, Integer>{

}
