package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import com.apptmyz.trackon.tcpl.entities.jpa.OpsairlinevehiclemasterEntity;

/**
 * Repository : Opsairlinevehiclemaster.
 */
public interface OpsairlinevehiclemasterJpaRepository extends PagingAndSortingRepository<OpsairlinevehiclemasterEntity, Integer> {

}
