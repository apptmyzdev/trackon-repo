package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.TblRomasterEntity;

public interface TcplTblRoMasterJpaRepository extends PagingAndSortingRepository<TblRomasterEntity, String> {

}
