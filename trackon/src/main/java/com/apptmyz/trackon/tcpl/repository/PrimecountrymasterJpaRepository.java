package com.apptmyz.trackon.tcpl.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.apptmyz.trackon.tcpl.entities.jpa.PrimecountrymasterEntity;

public interface PrimecountrymasterJpaRepository extends PagingAndSortingRepository<PrimecountrymasterEntity, String> {

}
