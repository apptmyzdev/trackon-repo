package com.apptmyz.trackon;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.FilesUtil;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Configuration
@EnableJpaRepositories(basePackages={"com.apptmyz.trackon.data.repository.jpa"})
@EntityScan(basePackages={"com.apptmyz.trackon.entities.jpa"})
@ComponentScan(basePackages={"com.apptmyz.trackon.services","com.apptmyz.trackon.utils", "com.apptmyz.trackon.controller", "com.apptmyz.trackon.security"})
@EnableWebMvc
@EnableTransactionManagement
@SpringBootApplication
public class TrackonApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(TrackonApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(TrackonApplication.class, args);
	}

	@Bean
	FirebaseApp getFireBaseApp() throws IOException{
		FileInputStream serviceAccount = new FileInputStream(FilesUtil.getProperty("fcmServiceAccountFile"));

		GoogleCredentials googleCredentials = GoogleCredentials.fromStream(serviceAccount)
				.createScoped(Collections.singleton(FilesUtil.getProperty("googleCredsUrl")));

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(googleCredentials)
				.setDatabaseUrl(FilesUtil.getProperty("firebaseDbUrl"))
				.build();

		return FirebaseApp.initializeApp(options, Constants.APP_NAME);
	}
}
