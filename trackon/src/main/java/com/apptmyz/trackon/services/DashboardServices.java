package com.apptmyz.trackon.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apptmyz.trackon.data.repository.jpa.DlyDataJpaRepository;
import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.model.TrendChartModel;

@Service
public class DashboardServices {
	
	@Autowired
	private DlyDataJpaRepository dlyDataJpaRepository;

	public TrendChartModel getTrendGraphData() throws Exception {
		
		SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		Calendar fcal = Calendar.getInstance();
		fcal.add(Calendar.MONTH, -6);
		Date today = new Date();
		Date from = fcal.getTime();
		long fromsec = f.parse(f.format(fcal.getTime())+" 00:00:00").getTime();
		Set<Long> dateset = new TreeSet<>();
		Map<Long, Integer> bookingsData = new TreeMap<>();
		Map<Long, Integer> directBookingsData = new TreeMap<>();
		Map<Long, Integer> deliveriesData = new TreeMap<>();
		Map<Long, Integer> undeliveriesData = new TreeMap<>();
		
		while (fromsec <= today.getTime()) {
			if (!dateset.contains(fromsec)) {
				dateset.add(fromsec);
				bookingsData.put(fromsec, 0);
				directBookingsData.put(fromsec, 0);
				deliveriesData.put(fromsec, 0);
				undeliveriesData.put(fromsec, 0);
			}
			fromsec = fromsec + 86400000;
		}
		
		List<Date> dlydata = dlyDataJpaRepository.getUpdatedDeliveriesBetween(from, today);
		if(dlydata != null && !dlydata.isEmpty()){
			long deliverieskey = 0;
			for(Date o : dlydata){
				deliverieskey = f.parse(f.format(o)+" 00:00:00").getTime();
				if(deliveriesData.containsKey(deliverieskey)){
					deliveriesData.put(deliverieskey, (deliveriesData.get(deliverieskey) + 1));
				}
				
			}
		}
		
		TrendChartModel data = new TrendChartModel();
		data.setPointStart(fromsec);
		data.setPointInterval(86400000);
		data.setDeliveries(deliveriesData.values());
		
		return data;
	}
	
	
	
}
