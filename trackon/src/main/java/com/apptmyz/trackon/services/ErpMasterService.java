package com.apptmyz.trackon.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.EntityToModels;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.apptmyz.trackon.data.repository.jpa.MaBranchJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaContentJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaCustomerDetailsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPaperWorkJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPincodeJpaRepository;
import com.apptmyz.trackon.entities.jpa.MaBranchEntity;
import com.apptmyz.trackon.entities.jpa.MaContentEntity;
import com.apptmyz.trackon.entities.jpa.MaCustomerDetailsEntity;
import com.apptmyz.trackon.entities.jpa.MaPaperWorkEntity;
import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.MaBranchModel;
import com.apptmyz.trackon.model.MaContentTypeModel;
import com.apptmyz.trackon.model.MaCustomerDetailsModel;
import com.apptmyz.trackon.model.MaPaperWorkModel;
import com.apptmyz.trackon.model.MaPincodeModel;
import com.apptmyz.trackon.model.TrackonUserMasterModel;
import com.apptmyz.trackon.model.TrackonUserMasterResponseModel;

@Service
public class ErpMasterService {
	@Autowired
	MaPincodeJpaRepository maPincodeJpaRepository;

	@Autowired
	MaCustomerDetailsJpaRepository maCustomerDetailsJpaRepository;

	@Autowired
	MaBranchJpaRepository maBranchJpaRepository;

	@Autowired
	MaPaperWorkJpaRepository maPaperWorkJpaRepository;

	@Autowired
	MaContentJpaRepository maContentJpaRepository;

	public ResponseEntity<AppGeneralResponse> getPincodeDetails(Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		String erpDetails = FilesUtil.getProperty("erpPincode");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {
			WSResponseObjectModel response = CommonUtility.doWSRequest(erpDetails, "", Constants.HTTP_GET, logger);
			logger.info("data:" + response.getResponse());
			if (response.getResponseCode() == Constants.HTTP_OK) {
				TypeToken<List<MaPincodeModel>> datasetType = new TypeToken<List<MaPincodeModel>>() {};
				List<MaPincodeModel> erpPincodes = gson.fromJson(response.getResponse(), datasetType.getType());
				if (erpPincodes != null && !erpPincodes.isEmpty()) {
					List<MaPincodeEntity> existingPincodes = (List<MaPincodeEntity>) maPincodeJpaRepository.findAll();

					HashMap<Integer, MaPincodeEntity> epincodemap = new HashMap<>();
					HashMap<Integer, MaPincodeEntity> newepincodemap = new HashMap<>();


					if (existingPincodes != null && !existingPincodes.isEmpty()) {
						for (MaPincodeEntity u : existingPincodes) {
							epincodemap.put(u.getPincode(), u);
						}
					}
					for (MaPincodeModel t : erpPincodes) {
						if (epincodemap.containsKey(Integer.parseInt(t.getPincode()))) {
							MaPincodeEntity u = EntityToModels.convertMaPincodeModelToEntity(t,
									epincodemap.get(Integer.parseInt(t.getPincode())));
							newepincodemap.put(u.getPincode(), u);
							epincodemap.remove(u.getPincode());													
						} else {
							// epincodemap.put(Integer.parseInt(t.getPincode()),EntityToModels.convertMaPincodeModelToEntity(t, null));
							newepincodemap.put(Integer.parseInt(t.getPincode()),
									EntityToModels.convertMaPincodeModelToEntity(t, null));
						}
					}
//					logger.info("scds"+epincodemap);
//					logger.info("ccsdcs"+newepincodemap);
					maPincodeJpaRepository.save(newepincodemap.values());

					if(!epincodemap.isEmpty()){
					for(Map.Entry<Integer, MaPincodeEntity> map : epincodemap.entrySet() )
					{
						MaPincodeEntity entity = map.getValue();
						entity.setActiveFlag(0);
						epincodemap.put(map.getKey(), entity);
					}
					maPincodeJpaRepository.save(epincodemap.values());
					}
					generalResponse= new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
				}
			} else {
				logger.info("Error in Webservice: " + response.getErrorString());
				generalResponse=new  ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getcustomerDetails(Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		String erpDetails = FilesUtil.getProperty("erpcustomerDetails");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {

			WSResponseObjectModel response = CommonUtility.doWSRequest(erpDetails, "", Constants.HTTP_GET, logger);
			logger.info("Data:" + response.getResponse());
			if (response.getResponseCode() == Constants.HTTP_OK) {
				TypeToken<List<MaCustomerDetailsModel>> datasetType = new TypeToken<List<MaCustomerDetailsModel>>(){};
				List<MaCustomerDetailsModel> erpCustomers = gson.fromJson(response.getResponse(), datasetType.getType());
				if (erpCustomers != null && !erpCustomers.isEmpty()) {
					List<MaCustomerDetailsEntity> existingCustomers = (List<MaCustomerDetailsEntity>) maCustomerDetailsJpaRepository.findAll();
					HashMap<String, MaCustomerDetailsEntity> ecustomermap = new HashMap<>();
					HashMap<String, MaCustomerDetailsEntity> newCustomerMap = new HashMap<>();
					if(existingCustomers != null && !existingCustomers.isEmpty()){
						for(MaCustomerDetailsEntity u : existingCustomers){
							ecustomermap.put(u.getCustCode(), u);
						}
					}

					for(MaCustomerDetailsModel t : erpCustomers){
						if(ecustomermap.containsKey(t.getCustCode())){
							MaCustomerDetailsEntity u = EntityToModels.convertMaCustomerModelToEntity(t, ecustomermap.get(t.getCustCode()));
							newCustomerMap.put(t.getCustCode(), u);
							ecustomermap.remove(t.getCustCode());		
						}
						else{
							if(!newCustomerMap.containsKey(t.getCustCode())){								
								newCustomerMap.put(t.getCustCode(), EntityToModels.convertMaCustomerModelToEntity(t, null));
							}
						}
					}
					maCustomerDetailsJpaRepository.save(newCustomerMap.values());
					if(!ecustomermap.isEmpty()){
						for(Map.Entry<String, MaCustomerDetailsEntity> map : ecustomermap.entrySet() )
						{
							MaCustomerDetailsEntity entity = map.getValue();
							entity.setActiveFlag(0);
							ecustomermap.put(map.getKey(), entity);
						}
						maCustomerDetailsJpaRepository.save(ecustomermap.values());
					}



					generalResponse= new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);

				}
			}
			else {
				logger.info("Error in Webservice: " + response.getErrorString());
				generalResponse=new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}

		}
		catch(Exception e){
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getContentTypes(Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		String erpDetails = FilesUtil.getProperty("erpContentTypes");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {

			WSResponseObjectModel response = CommonUtility.doWSRequest(erpDetails, "", Constants.HTTP_GET, logger);
			logger.info("Data:" + response.getResponse());
			if (response.getResponseCode() == Constants.HTTP_OK) {
				TypeToken<List<MaContentTypeModel>> datasetType = new TypeToken<List<MaContentTypeModel>>(){};
				List<MaContentTypeModel> erpContent = gson.fromJson(response.getResponse(), datasetType.getType());
				if (erpContent != null && !erpContent.isEmpty()) {

					List<MaContentEntity> existingConetents = (List<MaContentEntity>) maContentJpaRepository.findAll();
					HashMap<Integer, MaContentEntity> eContentsMap = new HashMap<>();
					HashMap<Integer, MaContentEntity> newContentsMap = new HashMap<>();
					if(existingConetents != null && !existingConetents.isEmpty()){
						for(MaContentEntity u : existingConetents){
							eContentsMap.put(u.getErpId(), u);
						}
					}

					for(MaContentTypeModel t : erpContent){
						if(eContentsMap.containsKey(Integer.parseInt(t.getId()))){
							MaContentEntity u = EntityToModels.convertMaContentModelToEntity(t, eContentsMap.get(Integer.parseInt(t.getId())));
							newContentsMap.put(u.getErpId(), u);
							eContentsMap.remove(u.getErpId());	
						}
						else{
							newContentsMap.put(Integer.parseInt(t.getId()), EntityToModels.convertMaContentModelToEntity(t, null));
						}
					}

					maContentJpaRepository.save(newContentsMap.values());
					if(!eContentsMap.isEmpty()){
						for(Map.Entry<Integer, MaContentEntity> map : eContentsMap.entrySet() )
						{
							MaContentEntity entity = map.getValue();
							entity.setActiveFlag(0);
							eContentsMap.put(map.getKey(), entity);
						}
						maContentJpaRepository.save(eContentsMap.values());
					}
					


					generalResponse= new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);


				}
			}
			else {
				logger.info("Error in Webservice: " + response.getErrorString());
				generalResponse=new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}

		}
		catch(Exception e){
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getBranchDetails(Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		String erpDetails = FilesUtil.getProperty("erpBranchDetails");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {

			WSResponseObjectModel response = CommonUtility.doWSRequest(erpDetails, "", Constants.HTTP_GET, logger);
			logger.info("Data:" + response.getResponse());
			if (response.getResponseCode() == Constants.HTTP_OK) {
				TypeToken<List<MaBranchModel>> datasetType = new TypeToken<List<MaBranchModel>>(){};
				List<MaBranchModel> erpBranch = gson.fromJson(response.getResponse(), datasetType.getType());
				if (erpBranch != null && !erpBranch.isEmpty()) {

					List<MaBranchEntity> existingBranches = (List<MaBranchEntity>) maBranchJpaRepository.findAll();
					HashMap<String, MaBranchEntity> eBranchMap = new HashMap<>();
					HashMap<String, MaBranchEntity> newBranchMap = new HashMap<>();
					
					if(existingBranches != null && !existingBranches.isEmpty()){
						for(MaBranchEntity u : existingBranches){
							eBranchMap.put(u.getOfficeCode(), u);
						}
					}

					for(MaBranchModel t : erpBranch){
						if(eBranchMap.containsKey(t.getOfficeCode())){
							MaBranchEntity u = EntityToModels.convertMaBranchModelToEntity(t, eBranchMap.get(t.getOfficeCode()));
							newBranchMap.put(u.getOfficeCode(), u);
							eBranchMap.remove(u.getOfficeCode());
						}
						else{
							newBranchMap.put(t.getOfficeCode(), EntityToModels.convertMaBranchModelToEntity(t, null));
						}
					}

					maBranchJpaRepository.save(newBranchMap.values());
					if(!eBranchMap.isEmpty()){
					for(Map.Entry<String, MaBranchEntity> map : eBranchMap.entrySet() )
					{
						MaBranchEntity entity = map.getValue();
						entity.setActiveFlag(0);
						eBranchMap.put(map.getKey(), entity);
					}
					}
					maBranchJpaRepository.save(eBranchMap.values());
					generalResponse= new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);


				}
			}
			else {
				logger.info("Error in Webservice: " + response.getErrorString());
				generalResponse=	new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}

		}
		catch(Exception e){
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getPaperWorkDetails(Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		String erpDetails = FilesUtil.getProperty("erpPaperWorkDEtails");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {

			WSResponseObjectModel response = CommonUtility.doWSRequest(erpDetails, "", Constants.HTTP_GET, logger);
			logger.info("Data:" + response.getResponse());
			if (response.getResponseCode() == Constants.HTTP_OK) {
				TypeToken<List<MaPaperWorkModel>> datasetType = new TypeToken<List<MaPaperWorkModel>>(){};
				List<MaPaperWorkModel> erpPaperwork = gson.fromJson(response.getResponse(), datasetType.getType());
				if (erpPaperwork != null && !erpPaperwork.isEmpty()) {
					List<MaPaperWorkEntity> existingPaperwork = (List<MaPaperWorkEntity>) maPaperWorkJpaRepository.findAll();
					HashMap<Integer, MaPaperWorkEntity> epapermap = new HashMap<>();
					HashMap<Integer, MaPaperWorkEntity> newpapermap = new HashMap<>();
					if(existingPaperwork != null && !existingPaperwork.isEmpty()){
						for(MaPaperWorkEntity u : existingPaperwork){
							epapermap.put(u.getErpId(), u);
						}
					}

					for(MaPaperWorkModel t : erpPaperwork){
						if(epapermap.containsKey(Integer.parseInt(t.getID()))){
							MaPaperWorkEntity u = EntityToModels.convertMaPaperWorkModelToEntity(t, epapermap.get(Integer.parseInt(t.getID())));
							newpapermap.put(u.getErpId(), u);
							epapermap.remove(u.getErpId());
						}
						else{
							newpapermap.put(Integer.parseInt(t.getID()), EntityToModels.convertMaPaperWorkModelToEntity(t, null));
						}
					}
					
					maPaperWorkJpaRepository.save(newpapermap.values());
					if(!epapermap.isEmpty()){
					for(Map.Entry<Integer, MaPaperWorkEntity> map : epapermap.entrySet() )
					{
						MaPaperWorkEntity entity = map.getValue();
						entity.setActiveFlag(0);
						epapermap.put(map.getKey(), entity);
						
					}
					maPaperWorkJpaRepository.save(epapermap.values());
					}
				}

					generalResponse= new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
				}
			
			else {
				logger.info("Error in Webservice: " + response.getErrorString());
				generalResponse=new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}

		}
		catch(Exception e){
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

}
