package com.apptmyz.trackon.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.apptmyz.trackon.data.repository.jpa.BookingChargesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingInvoicesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingLbhJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPaperworkJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPkupScanDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingXbPktsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DeviceInfoJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DlyDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DlyDepsImagesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsigneeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsignorJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MServiceTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaBranchJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPincodeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PdcAssignedJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupRegistrationJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.TrackonDrsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UndlyImagesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionHistoryJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.BookingChargesEntity;
import com.apptmyz.trackon.entities.jpa.BookingDataEntity;
import com.apptmyz.trackon.entities.jpa.BookingInvoicesEntity;
import com.apptmyz.trackon.entities.jpa.BookingLbhEntity;
import com.apptmyz.trackon.entities.jpa.BookingPaperworkEntity;
import com.apptmyz.trackon.entities.jpa.BookingXbPktsEntity;
import com.apptmyz.trackon.entities.jpa.DeviceInfoEntity;
import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.entities.jpa.DlyDepsImagesEntity;
import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;
import com.apptmyz.trackon.entities.jpa.MConsignorEntity;
import com.apptmyz.trackon.entities.jpa.MaBranchEntity;
import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;
import com.apptmyz.trackon.entities.jpa.PdcAssignedEntity;
import com.apptmyz.trackon.entities.jpa.PickupRegistrationEntity;
import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;
import com.apptmyz.trackon.entities.jpa.UndlyImagesEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionHistoryEntity;
import com.apptmyz.trackon.model.BookingData;
import com.apptmyz.trackon.model.BookingDataModel;
import com.apptmyz.trackon.model.BookingDataQuery;
import com.apptmyz.trackon.model.BookingDetails;
import com.apptmyz.trackon.model.BookingDocketDataModel;
import com.apptmyz.trackon.model.BookingReportModel;
import com.apptmyz.trackon.model.ChargesModel;
import com.apptmyz.trackon.model.Consignee;
import com.apptmyz.trackon.model.Consignor;
import com.apptmyz.trackon.model.DRSResponseModel;
import com.apptmyz.trackon.model.DeliveriesData;
import com.apptmyz.trackon.model.DeliveriesDataQuery;
import com.apptmyz.trackon.model.DeliveryDataResponseModel;
import com.apptmyz.trackon.model.DeliveryReportRequestModel;
import com.apptmyz.trackon.model.DeliveryPostModel;
import com.apptmyz.trackon.model.DeliveryStatusModel;
import com.apptmyz.trackon.model.ErpPickupBookingResponse;
import com.apptmyz.trackon.model.ImageData;
import com.apptmyz.trackon.model.ErpResponseModel;
import com.apptmyz.trackon.model.LbhData;
import com.apptmyz.trackon.model.LoginAuthenticationModel;
import com.apptmyz.trackon.model.PDCModel;
import com.apptmyz.trackon.model.PaperWork;
import com.apptmyz.trackon.model.PickupInvoiceEwb;
import com.apptmyz.trackon.model.PickupScanDataModel;
import com.apptmyz.trackon.model.PincodeModel;
import com.apptmyz.trackon.model.TaxTypeAmount;
import com.apptmyz.trackon.model.TrackonDRSDetailsModel;
import com.apptmyz.trackon.model.TrackonDRSDetailsResponseModel;
import com.apptmyz.trackon.model.UndlyPostModel;
import com.apptmyz.trackon.model.UserSessionData;
import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.apptmyz.trackon.model.XbPcsDataModel;
import com.apptmyz.trackon.model.XbPcsReprintModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.EntityToModels;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

@Component
public class ReportsServices {

	@Autowired
	private BookingDataJpaRepository bookingDataJpaRepository;

	@Autowired
	private MServiceTypeJpaRepository mServiceTypeJpaRepository;

	@Autowired
	private DlyDataJpaRepository dlyDataJpaRepository;
	
	@Autowired
	private CodeDescriptionServices codeDescriptionServices;
	
	@Autowired
	private PdcAssignedJpaRepository pdcAssignedJpaRepository;


	@Autowired
	private TrackonDrsJpaRepository trackonDrsJpaRepository;
	
	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	
	@Autowired
	private UserSessionJpaRepository userSessionJpaRepository;
	
	@Autowired
	private MaBranchJpaRepository maBranchJpaRepository;
	
	@Autowired
	private MConsigneeJpaRepository mConsigneeJpaRepository;
	
	@Autowired
	private MConsignorJpaRepository mConsignorJpaRepository;
	
	@Autowired
	private BookingPkupScanDataJpaRepository bookingPkupScanDataJpaRepository;
	
	@Autowired
	private BookingChargesJpaRepository bookingChargesJpaRepository;

	@Autowired
	private BookingInvoicesJpaRepository bookingInvoicesJpaRepository;

	@Autowired
	private BookingLbhJpaRepository bookingLbhJpaRepository;

	@Autowired
	private BookingPaperworkJpaRepository bookingPaperworkJpaRepository;
	
	@Autowired
	private BookingXbPktsJpaRepository bookingXbPktsJpaRepository;
	
	@Autowired
	private PickupRegistrationJpaRepository pickupRegistrationJpaRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private DlyDepsImagesJpaRepository dlyDepsImagesJpaRepository ;
	
	@Autowired
	private DeliveryServices deliveryServices;
	
	@Autowired
	private UndlyImagesJpaRepository undlyImagesJpaRepository;
	
	@Autowired
	private MaPincodeJpaRepository maPincodeJpaRepository;
	
	@Autowired
	private FirebaseApp fireBaseApp;
//	
//	@Autowired
//	private DeviceInfoJpaRepository deviceInfoJpaRepository;
//	
//	@Autowired
//	private UserSessionHistoryJpaRepository userSessionHistoryJpaRepository;
	
	private SimpleDateFormat longformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private SimpleDateFormat shortformat = new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	Gson gson = new GsonBuilder().serializeNulls().create();
	
	public List<BookingData> getBookings(BookingDataQuery req, Logger logger) {
		List<BookingData> bookingData = new ArrayList<>();
		
		Date from = null;
		Date to = null;
		try {
			if (CommonUtility.check(req.getBookingDateFrom())) {
				from = longformat.parse(req.getBookingDateFrom()+" 00:00:00");
			}
			if (CommonUtility.check(req.getBookingDateTo())) {
				to = longformat.parse(req.getBookingDateTo()+" 23:59:59");
			}

			List<BookingDataEntity> bookingList = null;
			Set<String> successList = null;
			
			if(CommonUtility.check(req.getShowFailed()) && req.getShowFailed().equalsIgnoreCase("Y")){
				if(CommonUtility.check(req.getBookingBranch())){
					bookingList = bookingDataJpaRepository.findByCreatedTimestampBetweenAndBookingBranchAndErpStatus(from, to, req.getBookingBranch(), null);
					successList = bookingDataJpaRepository.getSuccessDktNosByBranch(from, to, req.getBookingBranch());
				}else{					
					bookingList = bookingDataJpaRepository.findByCreatedTimestampBetweenAndErpStatus(from, to, null);
					successList = bookingDataJpaRepository.getSuccessDktNos(from, to);
				}
				
			}else{
				if(CommonUtility.check(req.getBookingBranch())){
					bookingList = bookingDataJpaRepository.findByCreatedTimestampBetweenAndBookingBranchAndErpStatus(from, to, req.getBookingBranch(), 1);
				}else{
					
					bookingList = bookingDataJpaRepository.findByCreatedTimestampBetweenAndErpStatus(from, to, 1);
				}
			}
			
			logger.info("Data from DB: " + bookingList.size() + ", " + shortformat.format(from) + ","
					+ shortformat.format(to));

			if (!bookingList.isEmpty()) {
				
				Map<String, String> shippingModesMap = codeDescriptionServices.getShippingModes();
				Map<String, String> serviceTypesMap = codeDescriptionServices.getServiceTypes();
				Map<String, String> uomMap = codeDescriptionServices.getUomValues();
				
				for (BookingDataEntity bkData : bookingList) {
					if(req.getShowFailed().equalsIgnoreCase("Y") && successList != null && successList.contains(bkData.getDocketNo())){
						continue;
					}
					BookingData data = new BookingData();
					data.setActualWt(bkData.getActualWt());
					data.setBookingDate(bkData.getPickupTimestamp() != null ? longformat.format(bkData.getPickupTimestamp()) : "-");
					data.setCustCode(bkData.getCustCode());
					data.setDestPincode(bkData.getDeliveryPincode());
					data.setBookingBranch(bkData.getBookingBranch());
					data.setDeliveryBranch(bkData.getDeliveryBranch());
					data.setDocketNo(bkData.getDocketNo());
					data.setNoPkts(bkData.getPktCount());
					data.setOriginPincode(bkData.getBookingPincode());
					data.setPickupId(bkData.getPickupId());
					String svcType = bkData.getSvcType();
					if (CommonUtility.check(svcType)) {
						data.setServiceTypeDesc(serviceTypesMap.get(svcType));
					}
					data.setStatus(bkData.getDocketStatus());
					String shipMode = bkData.getShipMode();
					if (CommonUtility.check(shipMode)) {
						data.setShipModeDesc(shippingModesMap.get(shipMode));
					}
					
					String uom = bkData.getUom();
					if (CommonUtility.check(uom)) {
						data.setUomDesc(uomMap.get(uom));
					}
					if (bkData.getDoxFlag() != null && bkData.getDoxFlag().equals("D")) {
						data.setDoxFlagDesc(Constants.DOX);
					} else {
						data.setDoxFlagDesc(Constants.NONDOX);
					}
					data.setErpMessage(bkData.getErpMessage());
					bookingData.add(data);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bookingData;
	}

	public ResponseEntity<AppGeneralResponse> getDeliveries(DeliveriesDataQuery requestData, Logger logger) {

		ResponseEntity<AppGeneralResponse> response = null;
		List<DeliveriesData> deliveryData = new ArrayList<DeliveriesData>();
		
		Date from = null;
		Date to = null;
		boolean isFranchiseUser = false;
		try {

			UserMasterEntity user = userMasterJpaRepository.findByUserId(requestData.getAgentId());

			if (user != null) {
				
				Set<String> userset = new HashSet<>();
				if(user.getUserType().equals(Constants.FRANCHISE_USER)){
					isFranchiseUser = true;
					userset = userMasterJpaRepository.getFranchiseDeliveryUsers(user.getBranchCode());
				}
				
				if (CommonUtility.check(requestData.getDeliveriesDateFrom())) {
					from = longformat.parse(requestData.getDeliveriesDateFrom()+" 00:00:00");
				}
				if (CommonUtility.check(requestData.getDeliveriesDateTo())) {
					to = longformat.parse(requestData.getDeliveriesDateTo()+" 23:59:59");
				}

				List<DlyDataEntity> deliveriesList = null;
				
				
				deliveriesList = dlyDataJpaRepository.findByDeliveredTimeBetween(from, to);
				
				logger.info("Data from DB: " + deliveriesList.size() + ", " + shortformat.format(from) + ","
						+ shortformat.format(to));

				if (deliveriesList != null && !deliveriesList.isEmpty()) {
					if (isFranchiseUser) {
						for (DlyDataEntity dbRec : deliveriesList) {
							if(userset.contains(dbRec.getAgentUserName())){
								deliveryData.add(EntityToModels.getDeliveryReportModel(dbRec));
							}
						}
					}
					else{
						for (DlyDataEntity dbRec : deliveriesList) {
							deliveryData.add(EntityToModels.getDeliveryReportModel(dbRec));
						}
					}
					
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, deliveryData, null), HttpStatus.OK);
				}
			} else {
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "User not found", null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;

	}
	
	public List<PDCModel> getPDCsData(DeliveriesDataQuery requestData) throws Exception{
		
		List<PDCModel> data = null;
		Date from = null;
		Date to = null;
		List<PdcAssignedEntity> pdcs = null;
		
		if (CommonUtility.check(requestData.getDeliveriesDateFrom())) {
			from = shortformat.parse(requestData.getDeliveriesDateFrom());
		}
		if (CommonUtility.check(requestData.getDeliveriesDateTo())) {
			to = shortformat.parse(requestData.getDeliveriesDateTo());
		}
		
		if(CommonUtility.check(requestData.getDeliveryBranch(), requestData.getAgentId())){
			pdcs = pdcAssignedJpaRepository.findByPdcDateBetweenAndBranchCodeAndAgentUserName(from, to, requestData.getDeliveryBranch(), requestData.getAgentId());
		}
		else if(CommonUtility.check(requestData.getDeliveryBranch())){
			pdcs = pdcAssignedJpaRepository.findByPdcDateBetweenAndBranchCode(from, to, requestData.getDeliveryBranch());
		}
		else if(CommonUtility.check(requestData.getAgentId())){
			pdcs = pdcAssignedJpaRepository.findByPdcDateBetweenAndAgentUserName(from, to, requestData.getAgentId());
		}
		else{
			pdcs = pdcAssignedJpaRepository.findByPdcDateBetween(from, to);
		}
		
		if(pdcs != null && !pdcs.isEmpty()){
			data = EntityToModels.convertAndGetPdcModelList(pdcs);
		}
		
		return data;
		
	}
	
	public List<TrackonDRSDetailsModel> getDrsDocketsData(DeliveriesDataQuery requestData) throws Exception {
		
		List<TrackonDRSDetailsModel> drsDockets = null;
		Date from = null;
		Date to = null;
		List<TrackonDrsEntity> dockets = null;
		
		if (CommonUtility.check(requestData.getDeliveriesDateFrom())) {
			from = shortformat.parse(requestData.getDeliveriesDateFrom());
		}
		if (CommonUtility.check(requestData.getDeliveriesDateTo())) {
			to = shortformat.parse(requestData.getDeliveriesDateTo());
		}
		
		if(CommonUtility.check(requestData.getAgentId())){
			dockets = trackonDrsJpaRepository.findByCreatedTimestampBetweenAndAssignedToUser(from, to, requestData.getAgentId());
		}
		else{
			dockets = trackonDrsJpaRepository.findByCreatedTimestampBetween(from, to);
		}
		
		if(dockets != null && !dockets.isEmpty()){
			drsDockets = EntityToModels.getTrackonDrsDetailsModelList(dockets);
		}
		
		return drsDockets;
	}
	
	public List<UserSessionData> getUserSessionDetails(Integer flg) throws Exception{
		List<UserSessionData> sessionData = new ArrayList<UserSessionData>();
		List<UserMasterEntity> masterList = (List<UserMasterEntity>) userMasterJpaRepository.findAll();
		List<UserSessionEntity> sessionList = (List<UserSessionEntity>) userSessionJpaRepository.findAll();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Map<String,UserSessionEntity> hm = new HashMap<>();
		UserSessionEntity use = null;
		
		
		
		if(sessionList != null && !sessionList.isEmpty()){
			for(UserSessionEntity us : sessionList){
				UserSessionEntity e = us;
				hm.put(e.getUserId(), us);
			}
		}
		
		for(UserMasterEntity um:masterList){
			
			
			UserSessionData usd = new UserSessionData();
			usd.setUserId(um.getUserId());
			usd.setBranchCode(um.getBranchCode());
			
			if(hm.containsKey(um.getUserId())){
				use = hm.get(um.getUserId());
				usd.setCurrentAppVersion(use.getCurrentAppVersion());
				usd.setDeviceImei(use.getDeviceImei());
				usd.setActiveFlag(use.getActiveFlag());
				usd.setLoginTimestamp(df.format(use.getLoginTimestamp()));
				usd.setLogoutTimestamp(df.format(use.getLogoutTimestamp()));
			}
			
			if(hm.containsKey(um.getUserId()) && flg == 1){				
				sessionData.add(usd);
			}
			else if(!hm.containsKey(um.getUserId()) && flg == 0){
				sessionData.add(usd);
			}
			else if(flg == 2){
				sessionData.add(usd);
			}
			
		}
		return sessionData;
	}

	public Map<String, BookingReportModel> getBookingsReport(Date from, Date to, String bookingType) throws Exception {
		Map<String, BookingReportModel> bookingReport = new HashMap<>();
		List<BookingDataEntity> list = null;
		BookingReportModel model = null;
		if (CommonUtility.check(bookingType) && !bookingType.equals("A")) {
			list = bookingDataJpaRepository.findByCreatedTimestampBetweenAndDoxFlagAndErpStatus(from, to, bookingType,
					1);
		} else {
			list = bookingDataJpaRepository.findByCreatedTimestampBetweenAndErpStatus(from, to, 1);
		}
		Map<String, MaBranchEntity> hm = new HashMap<>();
		List<MaBranchEntity> branchMaster = (List<MaBranchEntity>) maBranchJpaRepository.findAll();
		for (MaBranchEntity bm : branchMaster) {
			hm.put(bm.getOfficeCode(), bm);
		}
		if (list != null && !list.isEmpty()) {
			for (BookingDataEntity bd : list) {

				// if(CommonUtility.check(bookingType)){
				// model.setTotaldockets(bookingDataJpaRepository.getTotalDockets(bd.getBookingAgentId(),bookingType,from,to));
				// }
				// else{
				// model.setTotaldockets(bookingDataJpaRepository.getTotalDocketsnobookingtype(bd.getBookingAgentId(),from,to));
				// }
				if (bookingReport.containsKey(bd.getBookingAgentId())) {

					bookingReport.get(bd.getBookingAgentId())
							.setTotaldockets(bookingReport.get(bd.getBookingAgentId()).getTotaldockets() + 1);
				} else {
					model = new BookingReportModel();
					model.setBookingAgentId(bd.getBookingAgentId());
					if (hm.containsKey(bd.getBookingBranch())) {
						MaBranchEntity bme = hm.get(bd.getBookingBranch());
						model.setRegionCode(bme.getRoCode());
						model.setBranchCode(bme.getOfficeCode());
						model.setBranchName(bme.getOfficeName());
					}
					model.setTotaldockets(1);
					bookingReport.put(bd.getBookingAgentId(), model);
				}
			}
		}
		return bookingReport;
	}

	public ResponseEntity<AppGeneralResponse> getBookingDetails(String docketNo){
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("ReprintBookingData");
		log.info("DKT : "+docketNo);
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{
			
			BookingDetails model = null;
			BookingDataEntity en = bookingDataJpaRepository.findTopByDocketNoOrderByCreatedTimestampDesc(docketNo);
			if(en != null){
				Set<String> pktList = new HashSet<>();
//			List<BookingPkupScanDataEntity> pkt =  bookingPkupScanDataJpaRepository.findByDocketNo(docketNo);
//			Set<String> pktList = new HashSet<>();
//			for(BookingPkupScanDataEntity en1 : pkt){
//				if(!pktList.contains(en1.getPktNo())){
//					pktList.add(en1.getPktNo());
//				}
//			}
				if(en.getManualPktSeries()==1){
					response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "This operation is not valid for manual series", null, null), HttpStatus.OK);
					log.info("This operation is not valid for manual series");
					return response;
				}
				else{
					model = new BookingDetails();
					if(en.getConsigneeId()!=null){
						MConsigneeEntity con = mConsigneeJpaRepository.findOne(en.getConsigneeId());
						if (con != null) {
							Consignee consignee = new Consignee();
							consignee.setAddress1(con.getConsigneeAddress1());
							consignee.setAddress2(con.getConsigneeAddress2());
							consignee.setCity(con.getCity());
							consignee.setConsigneeCode(con.getConsigneeCode());
							consignee.setConsigneeName(con.getConsigneeName());
							consignee.setConsigneePincode(con.getPincode());
							consignee.setState(con.getState());
							consignee.setCustCode(con.getCustCode());
							consignee.setContactName(con.getContactName());
							consignee.setContactPhoneno(con.getContactPhoneno());
							consignee.setConsigneeId(con.getId());
							consignee.setCustomerId(con.getCustomerId());
							consignee.setGstin(con.getGstin());
							consignee.setMobileno(con.getPhoneNum());
							consignee.setEmail(con.getEmail());
							consignee.setPan(con.getPan());
							model.setConsignee(consignee);
						}
					}
					if(en.getConsignorId()!=null){
						MConsignorEntity ce = mConsignorJpaRepository.findOne(en.getConsignorId());
						if (ce != null) {
							Consignor consignor = new Consignor();
							consignor.setConsignorCode(ce.getConsignorCode());
							consignor.setConsignorName(ce.getConsignorName());
							consignor.setConsignorPincode(ce.getPincode());
							consignor.setAddress1(ce.getConsignorAddress1());
							consignor.setAddress2(ce.getConsignorAddress2());
							consignor.setCity(ce.getCity());
							consignor.setState(ce.getState());
							consignor.setCustCode(ce.getCustCode());
							consignor.setContactName(ce.getContactName());
							consignor.setContactEmailid(ce.getContactEmailid());
							consignor.setContactPhoneno(ce.getContactPhoneno());
							consignor.setConsignorId(ce.getId());
							consignor.setGstin(ce.getGstin());
							consignor.setMobileno(ce.getPhoneNum());
							consignor.setEmail(ce.getEmail());
							consignor.setPan(ce.getPan());
							model.setConsignor(consignor);
						}
					}
					model.setDocketNo(en.getDocketNo());
					model.setXbAwbNo(en.getXbAwbNo());
					model.setPktCount(en.getPktCount());
					model.setCustomerRefNo(en.getCustomerRefNo());
					model.setWeight(en.getActualWt());
					model.setInvoiceValue(en.getConsignmentValue());
					model.setShipmentMode(en.getShipMode());
					model.setXbDest(en.getXbDest());
					model.setXbOrigin(en.getXbOrigin());
					pktList.add(en.getPktNoStart());
					model.setPackList(pktList);
					
					List<BookingXbPktsEntity> xbpktsdata = bookingXbPktsJpaRepository.findByBookingId(en.getId());
					if(xbpktsdata != null && !xbpktsdata.isEmpty()){
						List<XbPcsReprintModel> xbpkts = new ArrayList<>();
						XbPcsReprintModel xpkt = null;
						for(BookingXbPktsEntity x : xbpktsdata){
							xpkt = new XbPcsReprintModel();
							xpkt.setAwbNum(x.getDocketNo());
							xpkt.setRefNo(x.getPktNo());
							xpkt.setXbAwbNum(x.getXbAwbNo());
							xpkt.setXbRefNo(x.getXbPktNo());
							xbpkts.add(xpkt);
						}
						model.setPcsList(xbpkts);
					}
					
					response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, model, null), HttpStatus.OK);
				}
			}
			 else {
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		log.info("Response : "+gson.toJson(response));
		return response;
	} 

	public String getImgLink(String dktno, String imgPath) {
		String s3Baseurl = "https://trackon-efs-to-s3.s3.ap-south-1.amazonaws.com/UAT/";
		String efsBaserUrl = "https://mobility.trackon.in/trackon/reports/view/image?imgPath=";
		String url = "";
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
		try{
			if(CommonUtility.check(imgPath)){
				if(imgPath.startsWith("Pickups")){
					BookingDataEntity dkt = bookingDataJpaRepository.findTopByDocketNoOrderByCreatedTimestampDesc(dktno);
					if(dkt != null){
						if(sf.parse(sf.format(dkt.getCreatedTimestamp())).compareTo(sf.parse(sf.format(new Date()))) == 0){
							url = url.concat(efsBaserUrl).concat(imgPath);
						}else{
							url = url.concat(s3Baseurl).concat(imgPath);
						}
					}					
				}else if(imgPath.startsWith("Deliveries") || imgPath.startsWith("Undeliveries")){
					DlyDataEntity dlydata = null;
					if(imgPath.startsWith("Deliveries")){
						dlydata = dlyDataJpaRepository.findTopByDktNoAndDlyStatusOrderByCreatedTimestampDesc(dktno, Constants.DELIVERY);
					}
					if(imgPath.startsWith("Undeliveries")){
						dlydata = dlyDataJpaRepository.findTopByDktNoAndDlyStatusOrderByCreatedTimestampDesc(dktno, Constants.UNDELIVERY);
					}
					if(dlydata != null){						
						if(sf.parse(sf.format(dlydata.getCreatedTimestamp())).compareTo(sf.parse(sf.format(new Date()))) == 0){
							url = url.concat(efsBaserUrl).concat(imgPath);
						}else{
							url = url.concat(s3Baseurl).concat(imgPath);
						}
					}
				}else {
					DlyDataEntity dlydata = null;
					if(imgPath.startsWith("POD")){
						dlydata = dlyDataJpaRepository.findTopByDktNoAndDlyStatusOrderByCreatedTimestampDesc(dktno, Constants.DELIVERY);
					}
					if(imgPath.startsWith("NDR")){
						dlydata = dlyDataJpaRepository.findTopByDktNoAndDlyStatusOrderByCreatedTimestampDesc(dktno, Constants.UNDELIVERY);
					}
					if(dlydata != null){
						if(!imgPath.endsWith(".png")){
							imgPath = imgPath.concat(".png");
						}
						if(sf.parse(sf.format(dlydata.getCreatedTimestamp())).compareTo(sf.parse(sf.format(new Date()))) == 0){
							url = url.concat(efsBaserUrl).concat(imgPath);
						}else{
							url = url.concat(s3Baseurl).concat(imgPath);
						}
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return url;
	}
	
	public ResponseEntity<AppGeneralResponse> getDeliveryStatusV2(String userId, String date){
		try {
			DeliveryStatusModel model = new DeliveryStatusModel();
			
			Date fdate = yyyyMMddHHmmss.parse(date+" 00:00:00");
			boolean isParentUser = false;
			UserMasterEntity user = userMasterJpaRepository.findByUserId(userId);
			Map<String, DRSResponseModel> pdcResponseList = new HashMap<>();
			
			if(user != null){
				
				Map<String, DlyDataEntity> deliveredMap = new HashMap<>();
				Map<String, DlyDataEntity> undeliveredMap = new HashMap<>();
				
				List<Object []> updatedDktsList = dlyDataJpaRepository.getUpdatedDkts(user.getUserId(), user.getFranchiseBranchCode());
//				Set<String> updatedDkts = new HashSet<>();
				if(updatedDktsList != null && !updatedDktsList.isEmpty()){
					for(Object [] o : updatedDktsList){
						String dlystatus = (String) o[2];
						String conno = (String) o[1];
						String drsNumber = (String) o[0];
						String checkkey = (drsNumber).concat("-").concat(conno);
//						updatedDkts.add(checkkey);
						
						DlyDataEntity e = new DlyDataEntity();
						e.setPdcNo(drsNumber);
						e.setDktNo(conno);
						if(CommonUtility.check(dlystatus) && dlystatus.equalsIgnoreCase(Constants.DELIVERY)){
							deliveredMap.put(conno, e);
						}else if(CommonUtility.check(dlystatus) && dlystatus.equalsIgnoreCase(Constants.UNDELIVERY)){
							undeliveredMap.put(checkkey, e);
						}
					}
				}
				
				if(CommonUtility.check(user.getUserId(), user.getFranchiseBranchCode()) && user.getUserId().equalsIgnoreCase(user.getFranchiseBranchCode()))
					isParentUser = true;
				List<TrackonDrsEntity> pdcList = null;
				if(isParentUser){
					pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndFranchiseBranchCode(fdate, user.getFranchiseBranchCode());
				}
				else{
					pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndAssignedToUser(fdate, user.getUserId());
				}
				
				if(pdcList.isEmpty()){
					return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}else{
					String key = null;
					for(TrackonDrsEntity t : pdcList){
						key = t.getDrsNo()+"-"+t.getAwbNo();
						if((t.getAckRequired() != null && t.getAckRequired() == 1 && t.getAcknowledged() != null && t.getAcknowledged() ==1) || (t.getAckRequired() == null) || t.getAckRequired() == 0){
							pdcResponseList.put(key, EntityToModels.getAssignedDrsModel(t));
						}
					}
					
					model.setDelivered(new ArrayList<>());
					model.setUnDelivered(new ArrayList<>());
					List<DRSResponseModel> finaldata = new ArrayList<>();
					for(DRSResponseModel r : pdcResponseList.values()){
						String checkkey = r.getDrsNo().concat("-").concat(r.getAwbNo());
						if(deliveredMap.containsKey(r.getAwbNo()) && deliveredMap.get(r.getAwbNo()).getPdcNo().equalsIgnoreCase(r.getDrsNo())){
							model.getDelivered().add(r);
						}else if(undeliveredMap.containsKey(checkkey)){
//							DlyDataEntity del = undeliveredMap.get(checkkey);
							model.getUnDelivered().add(r);
						}else{
							if(!deliveredMap.containsKey(r.getAwbNo()))
								finaldata.add(r);
						}
					}
					
					model.setPending(finaldata);
					return new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, model, null), HttpStatus.OK);
				}
				
			}else{
				return new ResponseEntity<>(new AppGeneralResponse(false, "Invalid user", null, null), HttpStatus.OK);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
	}
	
	public ResponseEntity<AppGeneralResponse> getDeliveryStatus(String userId, String date){
		ResponseEntity<AppGeneralResponse> response = null;
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat lf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		try{
			Date fdate = yyyyMMddHHmmss.parse(date+" 00:00:00");
			Date todate = yyyyMMddHHmmss.parse(date+ " 23:59:59");
			
			Calendar cal = Calendar.getInstance();
			Date toDate = lf.parse(sf.format(new Date()).concat(" 23:59:59"));
//			cal.add(Calendar.DATE, -4);
			
			
			List<DRSResponseModel> deliveredList = new ArrayList<>();
//			List<DRSResponseModel> unDeliveredList = new ArrayList<>();
			Map<String,DRSResponseModel> undeliveredMap = new HashMap<>();
			Set<String> undeliveredSet = new TreeSet<>();
			Map<String, DRSResponseModel> pendingList = new HashMap<>();
			Set<String> deliveredSet = new HashSet<>();
			DeliveryStatusModel model = new DeliveryStatusModel();
			Map<String,DeliveriesData> hm = new HashMap<>();
			Map<String,DRSResponseModel> drs = new HashMap<>();
			DRSResponseModel drsModel = null;
			DeliveriesData delModel = null;
			
			List<TrackonDrsEntity> drsList = trackonDrsJpaRepository.findByCreatedTimestampBetweenAndAssignedToUser(fdate, todate, userId);
			if(drsList!=null && !drsList.isEmpty()){
				for(TrackonDrsEntity t : drsList){
//					String tkey = t.getDrsNo()+"-"+t.getAwbNo();
					String tkey = t.getAwbNo();
					drs.put(tkey, EntityToModels.getAssignedDrsModel(t));
				}
				List<DlyDataEntity> dlydata = dlyDataJpaRepository.findByCreatedTimestampBetweenAndAgentUserName(fdate, toDate, userId);
				if(dlydata!=null && !dlydata.isEmpty()){ 
					for(DlyDataEntity t : dlydata){
//						String key = t.getPdcNo()+"-"+t.getDktNo();
						String key = t.getDktNo();
						hm.put(key, EntityToModels.getDeliveryReportModel(t));
						
						if(CommonUtility.check(t.getDlyStatus()) && t.getDlyStatus().equalsIgnoreCase(Constants.UNDELIVERY)){
							undeliveredSet.add(t.getPdcNo().concat("-").concat(t.getDktNo()));
						}
					}
				}
				for(TrackonDrsEntity en : drsList){
					String undelkeyval = en.getDrsNo()+"-"+en.getAwbNo();
					String keyval = en.getAwbNo();
					drsModel = drs.get(keyval);
					if(hm.containsKey(keyval)){
						delModel = hm.get(keyval);
						if(delModel.getStatus()!=null && delModel.getStatus().equals("D")){
							drsModel.setStatus("Delivered");
							deliveredList.add(drsModel);
							if(undeliveredMap.containsKey(delModel.getDocketNo())){
								undeliveredMap.remove(delModel.getDocketNo());
							}
						}
						if(delModel.getStatus()!=null && delModel.getStatus().equals("U")){

							if(undeliveredSet.contains(undelkeyval)){
								drsModel.setStatus("UnDelivered");
								undeliveredMap.put(delModel.getDocketNo(), drsModel);								
							}else{
								if((en.getAckRequired() != null && en.getAckRequired() == 1 && en.getAcknowledged() != null && en.getAcknowledged() ==1) || en.getAckRequired() == null || en.getAckRequired() == 0){							
									drsModel.setStatus("Pending");
									pendingList.put(en.getAwbNo(),drsModel);
								}
							}
							
						}
					}else{
						if((en.getAckRequired() != null && en.getAckRequired() == 1 && en.getAcknowledged() != null && en.getAcknowledged() ==1) || en.getAckRequired() == null || en.getAckRequired() == 0){							
							drsModel.setStatus("Pending");
							pendingList.put(en.getAwbNo(), drsModel);
						}
					}
				}
				List<DRSResponseModel> pendingDeliveries = new ArrayList<>(pendingList.values());
				pendingList.clear();
				Collections.sort(pendingDeliveries, Comparator.comparingInt(DRSResponseModel::getAwbSerialNo));
				model.setPending(pendingDeliveries);
				model.setUnDelivered(new ArrayList<>(undeliveredMap.values()));
				model.setDelivered(deliveredList);
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, model, null), HttpStatus.OK);
				
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	public ResponseEntity<AppGeneralResponse> docketRepush(String dktno,Logger logger, BookingDataEntity bookingdata){
		ResponseEntity<AppGeneralResponse> response = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		BookingDataModel datamodel = null;
		List<BookingDocketDataModel> dockets = new ArrayList<>();
		List<ErpPickupBookingResponse> erpresponselist = new ArrayList<>();
		Set<String> successDkts = new HashSet<>();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		logger.info("REPUSH DKT : "+dktno);
		try{
			if(CommonUtility.check(dktno)){
				if(bookingdata == null){
					bookingdata = bookingDataJpaRepository.findTopByDocketNoAndErpStatusIsNullOrderByCreatedTimestampDesc(dktno);
				}
				if(bookingdata != null && (bookingdata.getErpStatus() == null || bookingdata.getErpStatus() == 0)){
					datamodel = new BookingDataModel();
					if(bookingdata.getPickupId() != null && bookingdata.getPickupId() > 0){
						PickupRegistrationEntity registration = pickupRegistrationJpaRepository.findOne(bookingdata.getPickupId());
						if(registration != null){
							datamodel.setBookingPincode(registration.getPickupPincode());
							datamodel.setPickupId(Integer.parseInt(registration.getErpPickupOrderId()));
						}
						else{
							return new ResponseEntity<>(new AppGeneralResponse(false, "Pickup registration not found", null, null), HttpStatus.OK);
						}
					}
					
					
					datamodel.setBookingAgentId(bookingdata.getBookingAgentId());
					datamodel.setBookingBranchCode(bookingdata.getBookingBranch());
					datamodel.setBookingPincode(bookingdata.getBookingPincode());
					datamodel.setImei(bookingdata.getImei());
					BookingDocketDataModel dktModel = new BookingDocketDataModel();
					dktModel.setCustId(bookingdata.getCustId());
					if(bookingdata.getConsignorId() != null){
						dktModel.setConsignorId(bookingdata.getConsignorId());
						MConsignorEntity consignorEntity = mConsignorJpaRepository.findOne(bookingdata.getConsignorId());
						if(consignorEntity != null){
							Consignor consignor = new Consignor();
							consignor.setAddress1(consignorEntity.getConsignorAddress1());
							consignor.setAddress2(consignorEntity.getConsignorAddress2());
							consignor.setBranchCode(consignorEntity.getBranchCode());
							consignor.setCity(consignorEntity.getCity());
							consignor.setConsignorCode(consignorEntity.getConsignorCode());
							consignor.setConsignorId(consignorEntity.getId());
							consignor.setConsignorName(consignorEntity.getConsignorName());
							consignor.setConsignorPincode(consignorEntity.getPincode());
							consignor.setContactEmailid(consignorEntity.getContactEmailid());
							consignor.setContactName(consignorEntity.getContactName());
							consignor.setContactPhoneno(consignorEntity.getContactPhoneno());
//							c2.setContractCustomer(c1.get);
							consignor.setCustCode(consignorEntity.getCustCode());
							consignor.setCustomerId(consignorEntity.getMaCustomerDetails().getId());
							consignor.setEmail(consignorEntity.getEmail());
							consignor.setGstin(consignorEntity.getGstin());
							consignor.setMobileno(consignorEntity.getPhoneNum());
							consignor.setPan(consignorEntity.getPan());
							consignor.setState(consignorEntity.getStateCode());
							dktModel.setConsignor(consignor);
						}
					}
					if(bookingdata.getConsigneeId() != null){
						dktModel.setConsigneeId(bookingdata.getConsigneeId());
						MConsigneeEntity consigneeEntity = mConsigneeJpaRepository.findOne(bookingdata.getConsigneeId());
						if(consigneeEntity != null){
							Consignee consignee = new Consignee();
							consignee.setAddress1(consigneeEntity.getConsigneeAddress1());
							consignee.setAddress2(consigneeEntity.getConsigneeAddress2());
							consignee.setCity(consigneeEntity.getCity());
							consignee.setConsigneeCode(consigneeEntity.getConsigneeCode());
							consignee.setConsigneeId(consigneeEntity.getId());
							consignee.setConsigneeName(consigneeEntity.getConsigneeName());
							consignee.setConsigneePincode(consigneeEntity.getPincode());
							consignee.setContactName(consigneeEntity.getContactName());
							consignee.setContactPhoneno(consigneeEntity.getContactPhoneno());
							consignee.setCustCode(consigneeEntity.getCustCode());
							consignee.setCustomerId(consigneeEntity.getCustomerId());
							consignee.setEmail(consigneeEntity.getEmail());
							consignee.setGstin(consigneeEntity.getGstin());
							consignee.setPan(consigneeEntity.getPan());
							consignee.setState(consigneeEntity.getStateCode());
							dktModel.setConsignee(consignee);
						}
					}
					
					dktModel.setDocketNo(bookingdata.getDocketNo());
					dktModel.setCustCode(bookingdata.getCustCode());
					dktModel.setDestinationBranch(bookingdata.getDeliveryBranch());//destination branch -del branch
					dktModel.setPickupTimestamp(bookingdata.getPickupTimestamp() != null ? sf.format(bookingdata.getPickupTimestamp()) : null);
					dktModel.setShipMode(bookingdata.getShipMode());
					dktModel.setDoxFlag(bookingdata.getDoxFlag());
					dktModel.setServiceType(bookingdata.getSvcType());//svc service 
					dktModel.setActualWt(bookingdata.getActualWt());
					dktModel.setChargedWt(bookingdata.getChargedWt());
					dktModel.setUom(bookingdata.getUom());
					dktModel.setPktCount(bookingdata.getPktCount());
					dktModel.setStartPktNo(bookingdata.getPktNoStart());
					dktModel.setEndPktNo(bookingdata.getPktNoEnd());
					dktModel.setMaterial(bookingdata.getMaterial());
					dktModel.setCodAmount(bookingdata.getCodAmount());
					dktModel.setConsignmentValue(bookingdata.getConsignmentValue());
					dktModel.setIsAppointmentDelivery(bookingdata.getIsAppointmentDelivery()==0?false:true);//check
					dktModel.setDeliveryPincode(bookingdata.getDeliveryPincode());
					dktModel.setVehicleNo(bookingdata.getVehicleNo());
					
					List<LbhData> lbhDataList = new ArrayList<>();
					List<BookingLbhEntity> lbhEntityList = bookingLbhJpaRepository.findByBookingData_Id(bookingdata.getId());
					if(lbhEntityList != null && !lbhEntityList.isEmpty()){
						for(BookingLbhEntity lbhEntity : lbhEntityList){
							
							LbhData lbhDatamodel = new LbhData();
							lbhDatamodel.setPktCount(lbhEntity.getPktCount());
							lbhDatamodel.setUom(lbhEntity.getUom());
							lbhDatamodel.setPktLen(lbhEntity.getPktLen()); 
							lbhDatamodel.setPktWidth(lbhEntity.getPktWidth());
							lbhDatamodel.setPktHt(lbhEntity.getPktHeight());
							lbhDatamodel.setMaterial(lbhEntity.getMaterial());
							lbhDatamodel.setPktNo(lbhEntity.getPktNo());
							lbhDatamodel.setCustomerPktRefNo(lbhEntity.getCustPktRefNo());
							lbhDatamodel.setPackagingType(lbhEntity.getPackagingType());
							lbhDatamodel.setActualWt(lbhEntity.getActualWeight());
							lbhDatamodel.setChargedWt(lbhEntity.getChargedWeight());
							lbhDataList.add(lbhDatamodel);
						}
					}
					
					dktModel.setLbhData(lbhDataList);
					
					List<PickupInvoiceEwb> invoiceDetailsList = new ArrayList<>();
					List<BookingInvoicesEntity> invoiceEntityList = bookingInvoicesJpaRepository.findByBookingData_Id(bookingdata.getId());
					if(invoiceEntityList != null && !invoiceEntityList.isEmpty()){
						for(BookingInvoicesEntity invoiceEntity : invoiceEntityList){							
							PickupInvoiceEwb invoiceModel = new PickupInvoiceEwb();
							invoiceModel.setInvoiceNo(invoiceEntity.getInvNo());
							invoiceModel.setInvoiceDate(invoiceEntity.getInvDate()!=null ?invoiceEntity.getInvDate().toString() :null);
							invoiceModel.setInvoiceAmount(invoiceEntity.getInvAmout());
							invoiceModel.setEwbNo(invoiceEntity.getEwbNo());
							invoiceModel.setEwbValidTill(invoiceEntity.getEwbValidTill() != null ? sf.format(invoiceEntity.getEwbValidTill()) : null);
							invoiceModel.setEwbDate(invoiceEntity.getEwbDate() != null ? sf.format(invoiceEntity.getEwbDate()) : null);
							invoiceModel.setVehicleNo(invoiceEntity.getVehicleNo());
							invoiceModel.setBase64Data(invoiceEntity.getInvoiceImage());//img 
							invoiceModel.setInvoiceImageURL(invoiceEntity.getInvoiceImagePath());
							invoiceDetailsList.add(invoiceModel);
						}
					}
					dktModel.setInvoiceDetails(invoiceDetailsList);  
					
					List<PaperWork> paperWorkList = new ArrayList<>();
					List<BookingPaperworkEntity> bpwList = bookingPaperworkJpaRepository.findByBookingId(bookingdata.getId());
					if(bpwList != null && !bpwList.isEmpty()){
						for(BookingPaperworkEntity bpw : bpwList){							
							PaperWork pwm = new PaperWork();
							pwm.setName(bpw.getPaperWorkName());//img or paperwork
//						pwm.setDocumentNo(bpw.get);
							pwm.setRefNo(bpw.getPaperWorkRefNo());              
							pwm.setBase64Data(bpw.getImageName());
							pwm.setPaperWorkImageUrl(bpw.getImagePath());
							paperWorkList.add(pwm);
						}
					}
					
					dktModel.setPaperWork(paperWorkList);
					
					dktModel.setPaymentMode(bookingdata.getPaymentMode());
					dktModel.setPaymentType(bookingdata.getPaymentType());
					dktModel.setManualPktSeries(bookingdata.getManualPktSeries()==0);
					
					List<ChargesModel> chargesList = null;
					List<BookingChargesEntity> charges = bookingChargesJpaRepository.findByBookingData_Id(bookingdata.getId());
					if(charges != null && !charges.isEmpty()){
						for(BookingChargesEntity charge : charges){							
							chargesList = new ArrayList<>();
							ChargesModel model = new ChargesModel();
							model.setType(charge.getChargeType());
							model.setAmount(charge.getChargeAmount());
							
							List<TaxTypeAmount> taxesList = new ArrayList<>();
							TaxTypeAmount taxModel = new TaxTypeAmount();
							taxModel.setTaxType(charge.getTaxDetails().split("-")[0]);          
							taxModel.setTaxAmount(charge.getTaxAmount());
							taxesList.add(taxModel);  
							model.setTaxes(taxesList);  
						}
						
					}
					
					dktModel.setCharges(chargesList);
					
					dktModel.setTotalCharges(bookingdata.getTotalCharges());
					
					List<PickupScanDataModel> pickupScanData = null;
//					BookingPkupScanDataEntity pkscanEntity =  bookingPkupScanDataJpaRepository.findTopByDocketNoOrderByCreatedTimestampDesc(dktno);
//					if(pkscanEntity != null){
//						pickupScanData = new ArrayList<>();
//						PickupScanDataModel pkscan = new PickupScanDataModel();
//						pkscan.setDocketNo(pkscanEntity.getDocketNo());
//						pkscan.setPktNo(pkscanEntity.getPktNo());                       
//						pkscan.setCustomerPktNo(pkscanEntity.getRefNo());
//						pkscan.setSerialNo(pkscanEntity.getSerialNo());       
//						pkscan.setManualScanFlag(pkscanEntity.getManualScanFlag());
//						pkscan.setHasDeps(pkscanEntity.getHasDeps()==0 ? false : true);                         
//						pkscan.setDepsReason(pkscanEntity.getDepsReason());
//						pkscan.setScanTime(pkscanEntity.getScanTime() != null ? sf.format(pkscanEntity.getScanTime()) : null);
//						pickupScanData.add(pkscan);
//					}
					
					dktModel.setPickupScanData(pickupScanData);                               
					
					dktModel.setCustomerRefNo(bookingdata.getCustomerRefNo()); 
					dktModel.setRemarks(bookingdata.getRemarks());
					
					List<BookingXbPktsEntity> xbpktsdata = bookingXbPktsJpaRepository.findByBookingId(bookingdata.getId());
					if(xbpktsdata != null && xbpktsdata.isEmpty()){
						List<XbPcsDataModel> xbpkts = new ArrayList<>();
						XbPcsDataModel xpkt = null;
						for(BookingXbPktsEntity x : xbpktsdata){
							xpkt = new XbPcsDataModel();
							xpkt.setAWBnum(x.getDocketNo());
							xpkt.setRefNo(x.getPktNo());
							xpkt.setXBAWBnum(x.getXbAwbNo());
							xpkt.setXBrefNo(x.getXbPktNo());
							xbpkts.add(xpkt);
						}
						dktModel.setPcsList(xbpkts);
					}

					
					dockets.add(dktModel);
					
					datamodel.setDockets(dockets);
				}else{
					return new ResponseEntity<>(new AppGeneralResponse(false, "Docket already booked", null, null), HttpStatus.OK);
				}
				String url = null;
				if(CommonUtility.check(bookingdata.getXbAwbNo())){
					url = FilesUtil.getProperty("erpPickupBookingNew");
				}else{
					url = FilesUtil.getProperty("erpPickupBooking");
				}
				String erpResponseString = CommonUtility.postDataToErp(gson.toJson(datamodel), url,logger);
//				System.out.println(erpResponseString);
				try{
					TypeToken<List<ErpPickupBookingResponse>> datasetType = new TypeToken<List<ErpPickupBookingResponse>>() {};
					erpresponselist = gson.fromJson(erpResponseString, datasetType.getType());
					if(erpresponselist != null && !erpresponselist.isEmpty()){
						try{
							for(ErpPickupBookingResponse r : erpresponselist){
								if(r.getResult()){
									successDkts.add(r.getAWBNumber());
									if(dktno.equals(r.getAWBNumber())){
										bookingdata.setErpStatus(1);
										
										
									}
								}else{
									bookingdata.setErpStatus(0);
								}
								if(CommonUtility.check(r.getMessege())){
									if(r.getMessege().length() > 125){
										bookingdata.setErpMessage(r.getMessege().substring(0, 120));
									}
									else{
										bookingdata.setErpMessage(r.getMessege());
									}
								}
								if(bookingdata.getRepushCount() != null){
									bookingdata.setRepushCount(bookingdata.getRepushCount() + 1);
								}else{
									bookingdata.setRepushCount(1);
								}
								bookingDataJpaRepository.save(bookingdata);
							}
							response = new ResponseEntity<>(
									new AppGeneralResponse(true, "DONE", erpresponselist, null), HttpStatus.OK);
						}
						catch(Exception e){
							e.printStackTrace();
							logger.info("!!! FAILED TO SAVE ERP RESPONSE !!!");
							response = new ResponseEntity<>(
									new AppGeneralResponse(false, "!!! FAILED TO SAVE ERP RESPONSE !!!", null, null), HttpStatus.OK);
						}
					}
					else{
						logger.info("!!! FAILED TO PARSE ERP RESPONSE !!!");
						response = new ResponseEntity<>(
								new AppGeneralResponse(false, "!!! FAILED TO PARSE ERP RESPONSE !!!", null, null), HttpStatus.OK);
					}
				}
				catch(Exception e){
					e.printStackTrace();
					logger.info("EXCEPTION : SENDING TO ERP"+ e.getMessage(), e);
					JsonParser parser = new JsonParser();
					JsonObject rsp = (JsonObject) parser.parse(erpResponseString);
					String decryptedresponse = rsp.get("Message").toString();
					if(bookingdata.getRepushCount() != null){
						bookingdata.setRepushCount(bookingdata.getRepushCount() + 1);
					}else{
						bookingdata.setRepushCount(1);
					}
					bookingdata.setErpStatus(0);
					bookingdata.setErpMessage(decryptedresponse);
					bookingDataJpaRepository.save(bookingdata);
					response = new ResponseEntity<>(
							new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
				}

				
			}else{
				response = new ResponseEntity<>(
						new AppGeneralResponse(false, "Docket no. not sent", null, null), HttpStatus.OK);
			}
		}catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	public ResponseEntity<AppGeneralResponse> getDeliveriesReport(DeliveryReportRequestModel model) {
		ResponseEntity<AppGeneralResponse> response = null;
		SimpleDateFormat lf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		List<DeliveryDataResponseModel> resList = new ArrayList<>();
		try{
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<DlyDataEntity> criteriaQuery = criteriaBuilder.createQuery(DlyDataEntity.class);
			Root<DlyDataEntity> itemRoot = criteriaQuery.from(DlyDataEntity.class);
			Date from  = lf.parse(model.getFromDate().concat(" 00:00:00"));
			Date to = lf.parse(model.getToDate().concat(" 23:59:59"));
			Predicate predicateForGT = null;
			if (from != null) {
				predicateForGT = criteriaBuilder.greaterThanOrEqualTo(
						criteriaBuilder.function("DATE", java.sql.Date.class, itemRoot.get("deliveredTime")),
						criteriaBuilder.function("DATE", java.sql.Date.class, criteriaBuilder.literal(from)));
			}

			Predicate predicateForLT = null;
			if (to != null) {
				predicateForLT = criteriaBuilder.lessThanOrEqualTo(
						criteriaBuilder.function("DATE", java.sql.Date.class, itemRoot.get("deliveredTime")),
						criteriaBuilder.function("DATE", java.sql.Date.class, criteriaBuilder.literal(to)));
			} 
			Predicate predicateForBranch = null;
			if(model.getBranchCode() != null){
				predicateForBranch = criteriaBuilder.equal(itemRoot.get("dlyBranchCode"), model.getBranchCode());
			}
			
			Predicate predicateForDlyStatus = null;
			if(model.getDlyStatus() != null && !model.getDlyStatus().equals("All")){
				predicateForDlyStatus = criteriaBuilder.equal(itemRoot.get("dlyStatus"), model.getDlyStatus());
			}
			Predicate finalPredicate = null;
			if (predicateForGT != null && predicateForLT != null) {
				finalPredicate = criteriaBuilder.and(predicateForGT, predicateForLT);
			}
			if (predicateForBranch != null) {
				finalPredicate = criteriaBuilder.and(finalPredicate, predicateForBranch);
			}
			if(predicateForDlyStatus != null){
				finalPredicate = criteriaBuilder.and(finalPredicate, predicateForDlyStatus);
			}
			
			criteriaQuery.where(finalPredicate);
			List<DlyDataEntity> entities = entityManager.createQuery(criteriaQuery.select(itemRoot))
					.getResultList();
			
			if(entities != null && !entities.isEmpty()){
				for(DlyDataEntity entity : entities){
					DeliveryDataResponseModel dataModel = new DeliveryDataResponseModel();
					dataModel.setConsigneeName(entity.getRecieverName());
					dataModel.setDktNo(entity.getDktNo());
					dataModel.setDrsDate(entity.getDeliveredTime().toString());
					dataModel.setDrsNo(entity.getPdcNo());
					ErpResponseModel erpModel = gson.fromJson(entity.getErpMessage(), ErpResponseModel.class);
					if(erpModel != null && !erpModel.getResult()){
						dataModel.setErpMessage(erpModel.getErrMsg());
					}else{
						dataModel.setErpMessage(null);
					}
					dataModel.setImg(entity.getPodImage());
					TrackonDrsEntity drsEntity = trackonDrsJpaRepository.findFirstByDrsNoAndAwbNo(entity.getPdcNo(), entity.getDktNo());
					if(drsEntity != null){
						dataModel.setPktCount(drsEntity.getNoPkts());
					}
					dataModel.setStatus(entity.getDlyStatus());
					List<DlyDepsImagesEntity> deps = dlyDepsImagesJpaRepository.findByDktNo(entity.getDktNo());
					if(deps != null && deps.size() > 0){
						dataModel.setDepsImg(deps.get(0).getImage());
					}
					dataModel.setDeliveredBy(entity.getAgentUserName());
					resList.add(dataModel);
				}
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, resList, null), HttpStatus.OK);
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	public ResponseEntity<AppGeneralResponse> repushBookings() {
		Logger log = Logger.getLogger("RepushBookings");
		ResponseEntity<AppGeneralResponse> response = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat lf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		try{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			Date from  = lf.parse(sf.format(cal.getTime()).concat(" 00:00:00"));
			Date to = new Date();
			log.info("REQUEST FROM : "+from.toString()+ " TO : "+to.toString());
			List<BookingDataEntity> bookings = bookingDataJpaRepository.findByCreatedTimestampBetweenAndErpStatusIsNullAndRepushCountIsNull(from, to);
			if(bookings != null && !bookings.isEmpty()){
				
				for(BookingDataEntity b : bookings){
					ResponseEntity<AppGeneralResponse> dktResponse = docketRepush(b.getDocketNo(), log, b);
					log.info("DKT : "+b.getDocketNo()+ " : RESPONSE : "+gson.toJson(dktResponse));
				}
				response = new ResponseEntity<>(new AppGeneralResponse(true, "DONE", null, null), HttpStatus.OK);
			}
			else{
				response = new ResponseEntity<>(new AppGeneralResponse(false, "NO BOOKINGS TO REPUSH", null, null), HttpStatus.OK);
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			response = new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return response;
	}
	
	DeliveryPostModel getErpDeliveryModelFromEntity(DlyDataEntity e){
		SimpleDateFormat longDateReverseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<ImageData> erpImagesData = new ArrayList<>();
		try{
			DeliveryPostModel m = new DeliveryPostModel();
			m.setAppVersion(e.getAppVersion());
			m.setAmountCollected(e.getAmountCollected());
			m.setAmountToBeCollected(e.getAmountToBeCollected());
			m.setBankName(e.getBankName());
			m.setBranchCode(e.getDlyBranchCode());
			m.setDeliveredContact(e.getRecieverContactNo());
			m.setDeliveredTime(e.getDeliveredTime() != null ? longDateReverseFormat.format(e.getDeliveredTime()) : null);
//			m.setDepsImages(depsImages);
			m.setDepsReason(e.getDepsReason());
			m.setDocketNo(e.getDktNo());
			m.setHasDeps(e.getDeps() != null && e.getDeps() == 1);
			m.setImei(e.getImei());
			m.setIsBadDelivery(e.getBadDelivery() != null && e.getBadDelivery() == 1);
			m.setIsPaymentCollected(e.getPaymentCollected() != null && e.getPaymentCollected() == 1);
			m.setLatitude(e.getLatitude());
			m.setLongitude(e.getLongitude());
			m.setPaymentMethod(e.getPaymentMethod());
			m.setPaymentReferenceId(e.getPaymentRefId());
//			m.setPaymentProofImages(paymentProofImages);
			m.setPdcNo(e.getPdcNo());
//			m.setPodImages(podImages);
			m.setRecieverName(e.getRecieverName());
			m.setRelationship(e.getRelationship());
			m.setRemarks(e.getRemarks());
//			m.setSignatureImages(signatureImages);
			m.setTxnDate(e.getTxnDate() != null ? longDateReverseFormat.format(e.getTxnDate()) : null);
			m.setTxnId(e.getId());
			m.setUserId(e.getAgentUserName());
			erpImagesData.add(new ImageData(e.getPaymentProofImage(), Constants.ERP_PAYMENT_IMG));
			erpImagesData.add(new ImageData(e.getPaymentProofImage2(), Constants.ERP_PAYMENT_IMG));
			List<DlyDepsImagesEntity> deps = dlyDepsImagesJpaRepository.findByDktNo(e.getDktNo());
			if(deps != null && !deps.isEmpty()){
				for(DlyDepsImagesEntity i : deps){
					erpImagesData.add(new ImageData(i.getImage(), Constants.ERP_DEPS_IMG));
				}
			}
			m.setErpImagesList(erpImagesData);
			return m;
		}catch(Exception a){
			a.printStackTrace();
			return null;
		}
	}
	
	UndlyPostModel getErpUndeliveryModelFromEntity(DlyDataEntity e){
		SimpleDateFormat longDateReverseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<ImageData> erpImagesData = new ArrayList<>();
		try{
			UndlyPostModel m = new UndlyPostModel();
			m.setAppVersion(e.getAppVersion());
			m.setBranchCode(e.getDlyBranchCode());
			m.setDocketNo(e.getDktNo());
//			m.setImages(images);
//			m.setImei(imei);
			m.setLatitude(e.getLatitude());
			m.setLongitude(e.getLongitude());
			m.setPdcNo(e.getPdcNo());
			m.setReason(e.getUndeliveryReason());
			m.setRemarks(e.getRemarks());
			m.setTransactionId(e.getId());
			m.setUndlyTime(e.getTxnDate() != null ? longDateReverseFormat.format(e.getTxnDate()) : null);
			m.setUserId(e.getAgentUserName());
			m.setImei(e.getImei());
			List<UndlyImagesEntity> images = undlyImagesJpaRepository.findByPdcNoAndDktNo(e.getPdcNo(), e.getDktNo());
			if(images != null && !images.isEmpty()){				
				for(UndlyImagesEntity  u : images){
					erpImagesData.add(new ImageData(u.getImage(), Constants.ERP_UNDELIVERY_IMG));
				}
				m.setErpImagesList(erpImagesData);
			}
			return m;
		}catch(Exception a){
			a.printStackTrace();
			return null;
		}
	}

	public ResponseEntity<AppGeneralResponse> repushFailedDeliveries(Integer finalPush) {
		
		Logger log = Logger.getLogger("RepushDeliveries");
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat lf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		int pastDays = Integer.parseInt(FilesUtil.getProperty("repushDelFromLastDays"));
		Set<String> pushedDkts = new HashSet<>();
		try{
			Calendar cal = Calendar.getInstance();
			if(finalPush != null && finalPush == 1){
				log.info("FINAL PUSH");
				cal.add(Calendar.DATE, -1);
			}else{				
				cal.add(Calendar.DATE, pastDays);
			}
			Date from  = lf.parse(sf.format(cal.getTime()).concat(" 00:00:00"));
			Date to = new Date();
			log.info("REQUEST FROM : "+from.toString()+ " TO : "+to.toString());
			List<DlyDataEntity> updatedList = new ArrayList<>();

			List<DlyDataEntity> deliveries = null;
			if(finalPush != null && finalPush == 1){
				deliveries = dlyDataJpaRepository.findByCreatedTimestampBetweenAndErpStatusAndAttemptCount(from, to, 0, 2);
			}else{				
				deliveries = dlyDataJpaRepository.findByCreatedTimestampBetweenAndErpStatusAndAttemptCount(from, to, 0, 1);
			}
			
			Set<String> deliveredDkts = new HashSet<>();
			if(deliveries != null && !deliveries.isEmpty()){
				
				for(DlyDataEntity d : deliveries){
					String pdcdktno = d.getPdcNo().concat("-").concat(d.getDktNo());
					if(!pushedDkts.contains(pdcdktno)){
						
						if(CommonUtility.check(d.getDlyStatus()) && d.getDlyStatus().equalsIgnoreCase(Constants.DELIVERY)){
							DeliveryPostModel data = getErpDeliveryModelFromEntity(d);
							if(data != null){
								WSResponseObjectModel erpResponse = deliveryServices.pushDlyToErp(data, log, d, data.getErpImagesList());
								if(erpResponse != null){
									if(erpResponse.getResponseCode() == 200){
										ErpResponseModel resdata = gson.fromJson(erpResponse.getResponse(), ErpResponseModel.class);
										if(resdata.getResult()){						
											d.setErpStatus(1);
											deliveredDkts.add(data.getDocketNo());
										}else{
											d.setErpStatus(0);
										}
										if(erpResponse.getResponse() != null)
											d.setErpMessage(erpResponse.getResponse().length() > 45 ? erpResponse.getResponse().substring(0, 45) : erpResponse.getResponse());
										
									}
									else{
										d.setErpStatus(0);
										if(erpResponse.getErrorString() != null)
											d.setErpMessage(erpResponse.getErrorString().length() > 45  ? erpResponse.getErrorString().substring(0,45) : erpResponse.getErrorString());
									}
								}
								else{
									d.setErpStatus(0);
								}
							}else{
								log.info("MODEL FAILED : "+d.getDktNo());
							}
						}else{
							if(!deliveredDkts.contains(d.getDktNo())){								
								UndlyPostModel data = getErpUndeliveryModelFromEntity(d);
								if(data != null){
									WSResponseObjectModel erpResponse = deliveryServices.pushUndlyToErp(data, data.getErpImagesList(),log);
									if(erpResponse != null){
										if(erpResponse.getResponseCode() == 200){
											ErpResponseModel resdata = gson.fromJson(erpResponse.getResponse(), ErpResponseModel.class);
											if(resdata.getResult()){						
												d.setErpStatus(1);
											}else{
												d.setErpStatus(0);
											}
											if(erpResponse.getResponse() != null)
												d.setErpMessage(erpResponse.getResponse().length() > 45 ? erpResponse.getResponse().substring(0, 45) : erpResponse.getResponse());
											
										}
										else{
											d.setErpStatus(0);
											if(erpResponse.getErrorString() != null)
												d.setErpMessage(erpResponse.getErrorString().length() > 45  ? erpResponse.getErrorString().substring(0,45) : erpResponse.getErrorString());
										}
										
									}
									else{
										d.setErpStatus(0);
									}
								}else{
									log.info("MODEL FAILED : "+d.getDktNo());
								}
							}else{
								log.info("ALREADY DELIVERED : "+d.getDktNo());
							}
						}
						if(d.getAttemptCount() != null){
							d.setAttemptCount(d.getAttemptCount() + 1);
						}else{
							d.setAttemptCount(2);
						}
						log.info("DKT : "+d.getDktNo()+" ERP-RESPONSE: "+d.getErpStatus()+":"+d.getErpMessage());
						updatedList.add(d);
						pushedDkts.add(pdcdktno);
						if(updatedList.size() == 500){
							dlyDataJpaRepository.save(updatedList);
							log.info("UPDATED DKTS : "+updatedList.size());
							updatedList.clear();
						}
						
					}
				}
				if(!updatedList.isEmpty()){
					
					dlyDataJpaRepository.save(updatedList);
					log.info("UPDATED DKTS : "+updatedList.size());
					updatedList.clear();
				}
				return new ResponseEntity<>(new AppGeneralResponse(true, "Repush Completed", null, null), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new AppGeneralResponse(false, "No data to re-push", null, null), HttpStatus.OK);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
				
	}

	public ResponseEntity<AppGeneralResponse> saveDrs(String model) {
		Logger log = Logger.getLogger("SaveDrsFromErp");
		log.info("REQUEST : "+model);
		if(CommonUtility.check(model)){
			try{
				TrackonDRSDetailsResponseModel drsResponse = gson.fromJson(model, TrackonDRSDetailsResponseModel.class);
				List<TrackonDRSDetailsModel> drsList = drsResponse.getDrsList();
				List<TrackonDrsEntity> dbDrsList = null;
				TrackonDrsEntity te = null;
				if (drsList != null && !drsList.isEmpty()) {
					dbDrsList = new ArrayList<>();
					int i = 0;
					int limit = 500;
					int totalRecords = 0;
					for(TrackonDRSDetailsModel t : drsList){
						i = i+1;
						totalRecords = totalRecords + 1;
						te = EntityToModels.convertTrackonDRSDetailModelToEntity(t, Constants.DRS_STD_TYPE);
						dbDrsList.add(te);
						if(i == limit){
							log.info("** SAVING BATCH **");
							trackonDrsJpaRepository.save(dbDrsList);
							log.info("** SAVING BATCH FINISHED **");
							dbDrsList.clear();
							log.info("** LIST SIZE AFTER SAVE : "+dbDrsList.size());
							i = 0;
						}
					}
					if(dbDrsList.size() < limit){						
						trackonDrsJpaRepository.save(dbDrsList);
					}
					log.info("** SAVED RECORDS : "+totalRecords);
					return new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);

				} else {
					log.info("!!!!Empty STD data!!!!");
					return new ResponseEntity<>(new AppGeneralResponse(false, "DRS data not sent", null, null), HttpStatus.OK);
				}
			}catch(Exception e){
				log.info(e.getMessage(), e);
				e.printStackTrace();
				return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			
		}else{
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_FROM_ERP, null, null), HttpStatus.OK); 
		}
	}

	public ResponseEntity<AppGeneralResponse> getPincodeDetails(Integer pincodeParam) {
		
		if(pincodeParam != null){
			MaPincodeEntity pincode = maPincodeJpaRepository.findByPincode(pincodeParam);
			if(pincode != null){				
				PincodeModel pin = new PincodeModel();
				pin.setActiveFlag(pincode.getActiveFlag());
				pin.setCityName(pincode.getCityName());
				pin.setCreatedTimestamp(pincode.getCreatedTimestamp());
				pin.setDox(pincode.getDox());
				pin.setIsServicable(pincode.getIsServicable());
				pin.setNonDox(pincode.getNonDox());
				pin.setOda(pincode.getOda());
				pin.setOfficeCode(pincode.getOfficeCode());
				pin.setOfficeName(pincode.getOfficeName());
				pin.setPincode(pincode.getPincode());
				pin.setPrimeTrack(pincode.getPrimeTrack());
				pin.setReversePickup(pincode.getReversePickup());
				pin.setRoadExpress(pincode.getRoadExpress());
				pin.setRoda(pincode.getRoda());
				pin.setStateExp(pincode.getStateExp());
				pin.setStateName(pincode.getStateName());
				pin.setToPay(pincode.getToPay());
				pin.setId(pincode.getId());
				return new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, pin, null), HttpStatus.OK);
			}else{
				return new ResponseEntity<>(new AppGeneralResponse(false, "Pincode not found", null, null), HttpStatus.OK);
			}
		}else{
			return new ResponseEntity<>(new AppGeneralResponse(false, "Pincode not sent", null, null), HttpStatus.OK);
		}
	}

	public boolean multiplePushNotifications(List<String> registrationTokens,Logger log) throws FirebaseMessagingException, IOException{
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{

		FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(fireBaseApp);

		Notification notification = Notification.builder()
				.setTitle("Force Logout")
				.setBody("Log out user")
				.build();


		log.info("PUSHING FOR : "+ registrationTokens);
		MulticastMessage message = MulticastMessage.builder()
				.putData("id", Constants.PUSH_FOR_LOGOUT)
				.setNotification(notification)
				.addAllTokens(registrationTokens)
				.build();

		log.info("message req : "+ gson.toJson(message));
		BatchResponse response = firebaseMessaging.sendMulticast(message);
		log.info(response.getSuccessCount() + " messages were sent successfully");
		return response.getSuccessCount() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage(),e);
			return false; 
		}
	}

//	public ResponseEntity<AppGeneralResponse> logoutUser(LoginAuthenticationModel model, Logger logger) {
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try{
//			if (model.getUserId() != null && !model.getUserId().isEmpty()) {
//				UserMasterEntity user = userMasterJpaRepository.findByUserId(model.getUserId());
//				if (user != null) {
//					UserSessionEntity session = userSessionJpaRepository.findByUserId(model.getUserId());
//					if (session != null) {
//						session.setActiveFlag(0);
//						session.setLogoutTimestamp(new Date());
//						userSessionJpaRepository.save(session);
//						
//						UserSessionHistoryEntity sessionHistory = new UserSessionHistoryEntity();
//						sessionHistory.setDeviceImei(session.getDeviceImei());
//						sessionHistory.setUserId(session.getUserId());
//						sessionHistory.setAppVersion(model.getAppVersion());
//						sessionHistory.setLoggedInTimestamp(session.getLoginTimestamp());
//						sessionHistory.setLoggedOutTimestamp(new Date());
//						sessionHistory.setCreatedTimestamp(new Date());
//						sessionHistory.setAndroidVersion(model.getAndroidVersion());
//						sessionHistory.setDeviceBrand(model.getDeviceBrand());
//						sessionHistory.setDeviceModel(model.getDeviceModel());
//						userSessionHistoryJpaRepository.save(sessionHistory);
//						
//						DeviceInfoEntity device = deviceInfoJpaRepository.findByDeviceImeiAndActiveFlagAndUserId(model.getImei(), 1 , model.getUserId());
//								
//						if(device!=null){
//							device.setActiveFlag(false);
//							deviceInfoJpaRepository.save(device);
//						}
//						
//						generalResponse = new ResponseEntity<AppGeneralResponse>(
//								new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//					} else {
//						generalResponse = new ResponseEntity<AppGeneralResponse>(
//								new AppGeneralResponse(false, "User is not signed in", null, null), HttpStatus.OK);
//					}
//				} else {
//					generalResponse = new ResponseEntity<AppGeneralResponse>(
//							new AppGeneralResponse(false, "Not an existing user", null, null), HttpStatus.OK);
//				}
//			} else {
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, "Enter user ID", null, null), HttpStatus.OK);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.info("Exception ", e);
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}
}
