package com.apptmyz.trackon.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.LoginAuthenticationModel;


@Service
@EnableCaching(proxyTargetClass = true)
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserMasterJpaRepository userMasterJpaRepository;
	

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Logger logger=Logger.getLogger("loadUserByUsername");
		UserMasterEntity user = userMasterJpaRepository.findByUserId(username);
//		System.out.println(user);
		if(user == null) {
			throw new UsernameNotFoundException("User Not Found with username: " + username);
		}
		if(user.getActiveFlag() != null && user.getActiveFlag().intValue() == 0) {
			throw new UsernameNotFoundException("User " + username + " is Inactive " );
		}
		
		LoginAuthenticationModel userDetailsModel = new LoginAuthenticationModel();
		userDetailsModel.setUserId(username);

		return UserDetailsImpl.build(userDetailsModel, user.getUserPassword());
	}

}
