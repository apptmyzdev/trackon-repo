package com.apptmyz.trackon.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.apptmyz.trackon.data.repository.jpa.MPickupStatusJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MServiceTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MShipTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MShippingModeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MUomJpaRepository;
import com.apptmyz.trackon.entities.jpa.MPickupStatusEntity;
import com.apptmyz.trackon.entities.jpa.MServiceTypeEntity;
import com.apptmyz.trackon.entities.jpa.MShippingModeEntity;
import com.apptmyz.trackon.entities.jpa.MUomEntity;

@Component
public class CodeDescriptionServices {

	@Autowired
	private MServiceTypeJpaRepository mServiceTypeJpaRepository;

	@Autowired
	private MUomJpaRepository mUomJpaRepository;
	
	@Autowired
	private MShippingModeJpaRepository mShippingModeJpaRepository;

	@Autowired
	private MShipTypeJpaRepository mShipTypeJpaRepository;
	
	@Autowired
	private MPickupStatusJpaRepository mPickupStatusJpaRepository;

	public  Map<String, String> getShippingModes() {
		Map<String, String> map = new HashMap<String, String>();
		List<MShippingModeEntity> shippingModes = (List<MShippingModeEntity>) mShippingModeJpaRepository.findAll();
		if (shippingModes != null && !shippingModes.isEmpty()) {
			for (MShippingModeEntity item : shippingModes) {
				map.put(item.getCode(), item.getDesc());
			}
		}
		return map;
	}

	public  Map<String, String> getServiceTypes() {
		Map<String, String> map = new HashMap<String, String>();
		List<MServiceTypeEntity> serviceTypes = (List<MServiceTypeEntity>) mServiceTypeJpaRepository.findAll();
		if (serviceTypes != null && !serviceTypes.isEmpty()) {
			for (MServiceTypeEntity item : serviceTypes) {
				map.put(item.getCode(), item.getServiceType());
			}
		}
		return map;
	}

	public   Map<String, String> getPickupStatus() {
		Map<String, String> map = new HashMap<String, String>();
		List<MPickupStatusEntity> statusTypes = (List<MPickupStatusEntity>) mPickupStatusJpaRepository.findAll();
		if (statusTypes != null && !statusTypes.isEmpty()) {
			for (MPickupStatusEntity item : statusTypes) {
				map.put(item.getCode(), item.getDesc());
			}
		}
		return map;
	}

	public  Map<String, String> getUomValues() {
		Map<String, String> map = new HashMap<String, String>();
		List<MUomEntity> uomVals = (List<MUomEntity>) mUomJpaRepository.findAll();
		if (uomVals != null && !uomVals.isEmpty()) {
			for (MUomEntity item : uomVals) {
				map.put(item.getUomValue(), item.getUomDisplayValue());
			}
		}
		return map;
	}

}
