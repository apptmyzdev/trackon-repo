package com.apptmyz.trackon.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.apptmyz.trackon.data.repository.jpa.CityMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DepsMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MBookingTypeJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.MBranchJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MCashFreightJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MChargeTypesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsigneeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsignorJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.MCustomerJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MDktMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MDocketStatusJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MMaterialJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPackagingMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPaperworkTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPaymentModesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPickupAssignTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPickupCancRescReasonsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPickupSlotJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPickupStatusJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPickupTypesJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.MPincodeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPktMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MRecurringPickupJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MRedirectReasonsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MRelationshipJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MServiceTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MShipTypeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MShippingModeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MStateJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MUomJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaAreasJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaBranchJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaContentJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaCustomerDetailsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPincodeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PincodeMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.ReasonsMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.StateMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.entities.jpa.CityMasterEntity;
import com.apptmyz.trackon.entities.jpa.DepsMasterEntity;
import com.apptmyz.trackon.entities.jpa.MBookingTypeEntity;
//import com.apptmyz.trackon.entities.jpa.MBranchEntity;
import com.apptmyz.trackon.entities.jpa.MCashFreightEntity;
import com.apptmyz.trackon.entities.jpa.MChargeTypesEntity;
import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;
import com.apptmyz.trackon.entities.jpa.MConsignorEntity;
//import com.apptmyz.trackon.entities.jpa.MCustomerEntity;
import com.apptmyz.trackon.entities.jpa.MDktMasterEntity;
import com.apptmyz.trackon.entities.jpa.MDocketStatusEntity;
import com.apptmyz.trackon.entities.jpa.MMaterialEntity;
import com.apptmyz.trackon.entities.jpa.MPackagingMasterEntity;
import com.apptmyz.trackon.entities.jpa.MPaperworkTypeEntity;
import com.apptmyz.trackon.entities.jpa.MPaymentModesEntity;
import com.apptmyz.trackon.entities.jpa.MPickupAssignTypeEntity;
import com.apptmyz.trackon.entities.jpa.MPickupCancRescReasonsEntity;
import com.apptmyz.trackon.entities.jpa.MPickupSlotEntity;
import com.apptmyz.trackon.entities.jpa.MPickupStatusEntity;
import com.apptmyz.trackon.entities.jpa.MPickupTypesEntity;
//import com.apptmyz.trackon.entities.jpa.MPincodeEntity;
import com.apptmyz.trackon.entities.jpa.MPktMasterEntity;
import com.apptmyz.trackon.entities.jpa.MRecurringPickupEntity;
import com.apptmyz.trackon.entities.jpa.MRedirectReasonsEntity;
import com.apptmyz.trackon.entities.jpa.MRelationshipEntity;
import com.apptmyz.trackon.entities.jpa.MServiceTypeEntity;
import com.apptmyz.trackon.entities.jpa.MShipTypeEntity;
import com.apptmyz.trackon.entities.jpa.MShippingModeEntity;
import com.apptmyz.trackon.entities.jpa.MStateEntity;
import com.apptmyz.trackon.entities.jpa.MUomEntity;
import com.apptmyz.trackon.entities.jpa.MaAreasEntity;
import com.apptmyz.trackon.entities.jpa.MaBranchEntity;
import com.apptmyz.trackon.entities.jpa.MaContentEntity;
import com.apptmyz.trackon.entities.jpa.MaCustomerDetailsEntity;
import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;
import com.apptmyz.trackon.entities.jpa.PincodeMasterEntity;
import com.apptmyz.trackon.entities.jpa.ReasonsMasterEntity;
import com.apptmyz.trackon.entities.jpa.StateMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.AgentModel;
import com.apptmyz.trackon.model.AssignedToModel;
import com.apptmyz.trackon.model.Branch;
import com.apptmyz.trackon.model.CashFreightRequestModel;
import com.apptmyz.trackon.model.Consignee;
import com.apptmyz.trackon.model.Consignor;
import com.apptmyz.trackon.model.ConsignorMasterByPincode;
import com.apptmyz.trackon.model.CustomerMasterByCustCode;
import com.apptmyz.trackon.model.DepsMaster;
import com.apptmyz.trackon.model.DocketMasterModel;
import com.apptmyz.trackon.model.DocketStatusModel;
import com.apptmyz.trackon.model.GenericMasterModel;
import com.apptmyz.trackon.model.OdaPincodeDetails;
import com.apptmyz.trackon.model.PacketMasterModel;
import com.apptmyz.trackon.model.PickupAssignmentType;
import com.apptmyz.trackon.model.PickupCancelRescheduleReasonsModel;
import com.apptmyz.trackon.model.PickupSlots;
import com.apptmyz.trackon.model.PickupStatusModel;
import com.apptmyz.trackon.model.PincodeOdaDetails;
import com.apptmyz.trackon.model.ReasonsDataModel;
import com.apptmyz.trackon.model.RecurringFrequency;
import com.apptmyz.trackon.model.ServiceType;
import com.apptmyz.trackon.model.ShipMode;
import com.apptmyz.trackon.model.ShipmentType;
import com.apptmyz.trackon.model.State;
import com.apptmyz.trackon.model.TrackonUserMasterModel;
import com.apptmyz.trackon.model.TrackonUserMasterResponseModel;
import com.apptmyz.trackon.model.Uom;
import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.EntityToModels;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class MasterDataServices {
	@Autowired
	private DepsMasterJpaRepository depsMasterJpaRepository;

	@Autowired
	private ReasonsMasterJpaRepository reasonsMasterJpaRepository;

	@Autowired
	private MUomJpaRepository mUomJpaRepository;

	@Autowired
	private MPickupSlotJpaRepository mPickupSlotJpaRepository;

//	@Autowired
//	private MCustomerJpaRepository mCustomerJpaRepository;

	@Autowired
	private MConsignorJpaRepository mConsignorJpaRepository;

//	@Autowired
//	private MPincodeJpaRepository mPincodeJpaRepository;

	@Autowired
	private MServiceTypeJpaRepository mServiceTypeJpaRepository;

	@Autowired
	private MConsigneeJpaRepository mConsigneeJpaRepository;

	@Autowired
	private MRecurringPickupJpaRepository mRecurringPickupJpaRepository;

	@Autowired
	private MStateJpaRepository mStateJpaRepository;

	@Autowired
	private MShippingModeJpaRepository mShippingModeJpaRepository;

	@Autowired
	private MShipTypeJpaRepository mShipTypeJpaRepository;

	@Autowired
	private MPickupStatusJpaRepository mPickupStatusJpaRepository;

	@Autowired
	private MDocketStatusJpaRepository mDocketStatusJpaRepository;

	@Autowired
	private MPickupAssignTypeJpaRepository mPickupAssignTypeJpaRepository;

	@Autowired
	private MPickupCancRescReasonsJpaRepository mPickupCancRescReasonsJpaRepository;

	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;

//	@Autowired
//	private MBranchJpaRepository mBranchJpaRepository;

	@Autowired
	private MPackagingMasterJpaRepository mPackagingMasterJpaRepository;

	@Autowired
	private MMaterialJpaRepository mMaterialJpaRepository;

	@Autowired
	private MPaymentModesJpaRepository mPaymentModesJpaRepository;

	@Autowired
	private MBookingTypeJpaRepository mBookingTypeJpaRepository;

	@Autowired
	private MPickupTypesJpaRepository mPickupTypesJpaRepository;

	@Autowired
	private MPaperworkTypeJpaRepository mPaperworkTypeJpaRepository;

	@Autowired
	private MChargeTypesJpaRepository mChargeTypesJpaRepository;

	@Autowired
	private MDktMasterJpaRepository mDktMasterJpaRepository;

	@Autowired
	private MPktMasterJpaRepository mPktMasterJpaRepository;

	@Autowired
	private MRelationshipJpaRepository mRelationshipJpaRepository;

	@Autowired
	private MRedirectReasonsJpaRepository mRedirectReasonsJpaRepository;

	@Autowired
	private MCashFreightJpaRepository mCashFreightJpaRepository;

	@Autowired
	private PincodeMasterJpaRepository pincodeMasterJpaRepository;

	@Autowired
	private StateMasterJpaRepository stateMasterJpaRepository;

	@Autowired
	private CityMasterJpaRepository cityMasterJpaRepository;

	@Autowired
	private MaPincodeJpaRepository maPincodeJpaRepository;
	
	@Autowired
	private MaAreasJpaRepository maAreasJpaRepository;
	
	@Autowired
	private MaCustomerDetailsJpaRepository maCustomerDetailsJpaRepository;
	
	@Autowired
	private MaBranchJpaRepository maBranchJpaRepository;
	
	@Autowired
	private MaContentJpaRepository maContentJpaRepository;
	
	public ResponseEntity<AppGeneralResponse> getDeps(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<DepsMasterEntity> depsMasterList = null;
			int version = 0;
			if (maxVersion.isPresent()) {
				version = maxVersion.get();
			}
			if (version == 0) {
				depsMasterList = depsMasterJpaRepository.findByActiveFlag(1);
			} else {
				depsMasterList = depsMasterJpaRepository.findByVersionGreaterThan(version);
			}
			if (depsMasterList != null && !depsMasterList.isEmpty()) {
				List<DepsMaster> depsList = new ArrayList<DepsMaster>();
				for (DepsMasterEntity dbDeps : depsMasterList) {
					DepsMaster deps = new DepsMaster();
					deps.setDepsCategory(dbDeps.getDepsCategory());
					deps.setDepsCode(dbDeps.getDepsCode());
					deps.setDepsDesc(dbDeps.getDepsDesc());
					deps.setSortOrder(dbDeps.getSortOrder());
					deps.setVersion(dbDeps.getVersion());
					depsList.add(deps);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, depsList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getReasons(Optional<Integer> maxVersion) {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<ReasonsMasterEntity> reasonsdata = null;
		int version = 0;
		if (maxVersion.isPresent()) {
			version = maxVersion.get();
		}

		if (version == 0) {
			reasonsdata = (List<ReasonsMasterEntity>) reasonsMasterJpaRepository.findAll();
		} else {
			reasonsdata = reasonsMasterJpaRepository.findByVersionGreaterThan(version);
		}
		ReasonsDataModel re = null;
		List<ReasonsDataModel> master = new ArrayList<>();
		if (reasonsdata != null && !reasonsdata.isEmpty()) {
			for (ReasonsMasterEntity r : reasonsdata) {
				re = new ReasonsDataModel();
				re.setId(r.getReasonId());
				re.setReason(r.getReason());
				if (r.getIsPickup() != null)
					re.setPickupFlag(r.getIsPickup() ? 1 : 0);
				else
					re.setPickupFlag(0);
				if (r.getIsDelivery() != null)
					re.setDeliveryFlag(r.getIsDelivery() ? 1 : 0);
				else
					re.setDeliveryFlag(0);

				re.setSortOrder(r.getSortOrder() == null ? 0 : r.getSortOrder());

				re.setVersion(r.getVersion());
				master.add(re);
			}

			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, master, null), HttpStatus.OK);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

//	public ResponseEntity<AppGeneralResponse> getMaterials(Optional<Integer> maxVersion) {
//
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		List<MMaterialEntity> materials = null;
//		int version = 0;
//		if (maxVersion.isPresent()) {
//			version = maxVersion.get();
//		}
//
//		if (version == 0) {
//			materials = (List<MMaterialEntity>) mMaterialJpaRepository.findAll();
//		} else {
//			materials = mMaterialJpaRepository.findByVersionGreaterThan(version);
//		}
//		List<GenericMasterModel> data = new ArrayList<>();
//		if (materials != null && !materials.isEmpty()) {
//			for (MMaterialEntity dbRecord : materials) {
//				GenericMasterModel item = new GenericMasterModel();
//				item.setCode(dbRecord.getCode());
//				item.setDescription(dbRecord.getDescription());
//				item.setSortOrder(dbRecord.getSortOrder());
//				item.setVersion(dbRecord.getVersion());
//				data.add(item);
//			}
//
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
//		} else {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}

	public ResponseEntity<AppGeneralResponse> getPaymentModes(Optional<Integer> maxVersion) {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<MPaymentModesEntity> paymentmodes = null;
		int version = 0;
		if (maxVersion.isPresent()) {
			version = maxVersion.get();
		}

		if (version == 0) {
			paymentmodes = (List<MPaymentModesEntity>) mPaymentModesJpaRepository.findAll();
		} else {
			paymentmodes = mPaymentModesJpaRepository.findByVersionGreaterThan(version);
		}
		List<GenericMasterModel> data = new ArrayList<>();
		if (paymentmodes != null && !paymentmodes.isEmpty()) {
			for (MPaymentModesEntity dbRecord : paymentmodes) {
				GenericMasterModel item = new GenericMasterModel();
				item.setCode(dbRecord.getCode());
				item.setDescription(dbRecord.getDescription());
				item.setSortOrder(dbRecord.getSortOrder());
				item.setVersion(dbRecord.getVersion());
				data.add(item);
			}

			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getBookingTypes(Optional<Integer> maxVersion) {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<MBookingTypeEntity> bookingTypes = null;
		int version = 0;
		if (maxVersion.isPresent()) {
			version = maxVersion.get();
		}

		if (version == 0) {
			bookingTypes = (List<MBookingTypeEntity>) mBookingTypeJpaRepository.findAll();
		} else {
			bookingTypes = mBookingTypeJpaRepository.findByVersionGreaterThan(version);
		}
		List<GenericMasterModel> data = new ArrayList<>();
		if (bookingTypes != null && !bookingTypes.isEmpty()) {
			for (MBookingTypeEntity dbRecord : bookingTypes) {
				GenericMasterModel item = new GenericMasterModel();
				item.setCode(dbRecord.getCode());
				item.setDescription(dbRecord.getDescription());
				item.setSortOrder(dbRecord.getSortOrder());
				item.setVersion(dbRecord.getVersion());
				data.add(item);
			}

			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getUoms(String cat, Optional<Integer> version) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<MUomEntity> uomData = null;
		List<Uom> uomResponseList = null;
		try {
			if (!version.isPresent() || version.get() == 0) {
				if (!CommonUtility.check(cat) || cat.equals(Constants.ALL)) {
					uomData = (List<MUomEntity>) mUomJpaRepository.findByActiveFlag(1);
				} else {
					uomData = (List<MUomEntity>) mUomJpaRepository.findByUomCatAndActiveFlag(cat, 1);
				}
			} else {
				if (!CommonUtility.check(cat) || cat.equals(Constants.ALL)) {
					uomData = (List<MUomEntity>) mUomJpaRepository.findByVersionGreaterThanAndActiveFlag(version.get(),
							1);
				} else {
					uomData = (List<MUomEntity>) mUomJpaRepository
							.findByVersionGreaterThanAndUomCatAndActiveFlag(version.get(), cat, 1);
				}
			}
			if (uomData != null && !uomData.isEmpty()) {
				uomResponseList = new ArrayList<Uom>();
				for (MUomEntity dbRecord : uomData) {
					Uom uom = new Uom();
					uom.setUomCat(dbRecord.getUomCat());
					uom.setUomDisplayValue(dbRecord.getUomDisplayValue());
					uom.setUomValue(dbRecord.getUomValue());
					uom.setVersion(dbRecord.getVersion());
					uomResponseList.add(uom);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, uomResponseList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPackageTypes(Optional<Integer> version) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<GenericMasterModel> data = null;
		List<MPackagingMasterEntity> dbList = null;
		try {
			if (!version.isPresent() || version.get() == 0) {
				dbList = (List<MPackagingMasterEntity>) mPackagingMasterJpaRepository.findByActiveFlag(1);
			} else {
				dbList = (List<MPackagingMasterEntity>) mPackagingMasterJpaRepository
						.findByVersionGreaterThanAndActiveFlag(version.get(), 1);
			}
			if (dbList != null && !dbList.isEmpty()) {
				data = new ArrayList<GenericMasterModel>();
				for (MPackagingMasterEntity dbRecord : dbList) {
					GenericMasterModel item = new GenericMasterModel();
					item.setCode(dbRecord.getCode());
					item.setDescription(dbRecord.getDescription());
					item.setSortOrder(dbRecord.getSortOrder());
					item.setVersion(dbRecord.getVersion());
					data.add(item);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getRecurringFrequency(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<RecurringFrequency> frequencyList = null;
		try {
			List<MRecurringPickupEntity> dbFrequencyList = null; 
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				dbFrequencyList = mRecurringPickupJpaRepository.findByActiveFlag(1);
			}else{
				dbFrequencyList = mRecurringPickupJpaRepository.findByVersionGreaterThanAndActiveFlag(maxVersion.get(), 1);
			}
			if (dbFrequencyList != null && !dbFrequencyList.isEmpty()) {
				frequencyList = new ArrayList<RecurringFrequency>();
				for (MRecurringPickupEntity dbFrequency : dbFrequencyList) {
					RecurringFrequency freq = new RecurringFrequency();
					freq.setCode(dbFrequency.getCode());
					freq.setSortOrder(dbFrequency.getSortOrder());
					freq.setValue(dbFrequency.getValue());
					freq.setVersion(dbFrequency.getVersion());
					frequencyList.add(freq);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, frequencyList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPickupSlots(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<MPickupSlotEntity> pickupSlots = null;
		List<PickupSlots> responsePickupSlots = null;
		try {
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				pickupSlots = mPickupSlotJpaRepository.findByActiveFlag(1);				
			}else{
				pickupSlots = mPickupSlotJpaRepository.findByVersionGreaterThanAndActiveFlag(maxVersion.get(), 1);
			}
			if (pickupSlots != null && !pickupSlots.isEmpty()) {
				responsePickupSlots = new ArrayList<PickupSlots>();
				for (MPickupSlotEntity dbPickup : pickupSlots) {
					PickupSlots pickup = new PickupSlots();
					pickup.setPickupSlotDisplay(dbPickup.getPickupSlotDisplay());
					pickup.setPickupSlotValue(dbPickup.getPickupSlotValue());
					pickup.setVersion(dbPickup.getVersion());
					responsePickupSlots.add(pickup);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, responsePickupSlots, null),
						HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {

		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getCustomersByCustomerCode(String customerCode) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
//		List<MCustomerEntity> customers = mCustomerJpaRepository.findByCustCodeContaining(customerCode);
			List<Object[]> customers = maCustomerDetailsJpaRepository.findByCustCodeContaining(customerCode);

			List<CustomerMasterByCustCode> customerList = null;
			if (customers != null && !customers.isEmpty()) {
				customerList = new ArrayList<CustomerMasterByCustCode>();
				for (Object[] customer : customers) {
					CustomerMasterByCustCode responseCustomer = new CustomerMasterByCustCode();
//				responseCustomer.setCustCode(customer.getCustCode());
//				responseCustomer.setCustName(customer.getCustName());
					responseCustomer.setCustCode((String) customer[0]);
					responseCustomer.setCustName((String) customer[1]);

//				List<CustomerCodeConsignor> consignorList = new ArrayList<CustomerCodeConsignor>();
//				List<MConsignorEntity> dbConsignors = customer.getListOfMConsignor();
//				if (dbConsignors != null && !dbConsignors.isEmpty()) {
//					for (MConsignorEntity dbConsignor : dbConsignors) {
//						CustomerCodeConsignor consignor = new CustomerCodeConsignor();
//						consignor.setAddress1(dbConsignor.getConsignorAddress1());
//						consignor.setAddress2(dbConsignor.getConsignorAddress2());
//						consignor.setBranchCode(dbConsignor.getMBranch().getBranchCode());
//						consignor.setCity(dbConsignor.getCity());
//						consignor.setConsignorCode(dbConsignor.getConsignorCode());
//						consignor.setConsignorName(dbConsignor.getConsignorName());
//						consignor.setConsignorPincode(Integer.toString(dbConsignor.getMPincode().getPincode()));
//						consignor.setState(dbConsignor.getStateCode());
//						consignorList.add(consignor);
//					}
//				}
//				responseCustomer.setConsignors(consignorList);
					customerList.add(responseCustomer);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, customerList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getConsignorsByCustCode(String customerCode) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
//			MCustomerEntity customer = mCustomerJpaRepository.findByCustCode(customerCode);
			MaCustomerDetailsEntity customer = maCustomerDetailsJpaRepository.findByCustCode(customerCode);
			
			if (customer != null) {
				CustomerMasterByCustCode customerMaster = new CustomerMasterByCustCode();

				List<MConsignorEntity> dbConsignorList = customer.getListOfMConsignor();

				if (dbConsignorList != null && !dbConsignorList.isEmpty()) {
					List<Consignor> consignorList = new ArrayList<Consignor>();
					for (MConsignorEntity dbConsignor : dbConsignorList) {
						Consignor consignor = new Consignor();
						consignor.setAddress1(dbConsignor.getConsignorAddress1());
						consignor.setAddress2(dbConsignor.getConsignorAddress2());
						if (dbConsignor.getBranchCode() != null) {
							consignor.setBranchCode(dbConsignor.getBranchCode());
						}
						consignor.setCity(dbConsignor.getCity());
						consignor.setConsignorCode(dbConsignor.getConsignorCode());
						consignor.setConsignorName(dbConsignor.getConsignorName());
						consignor.setConsignorPincode(dbConsignor.getPincode());
						consignor.setState(dbConsignor.getStateCode());
						consignor.setCustCode(dbConsignor.getCustCode());
						consignor.setConsignorCode(dbConsignor.getConsignorCode());
						consignor.setContactEmailid(dbConsignor.getContactEmailid());
						consignor.setContactName(dbConsignor.getContactName());
						consignor.setContactPhoneno(dbConsignor.getContactPhoneno());
						consignor.setContractCustomer(true);
						consignor.setCustomerId(dbConsignor.getMaCustomerDetails().getId());
						consignor.setConsignorId(dbConsignor.getId());
						consignor.setEmail(dbConsignor.getEmail());
						consignorList.add(consignor);
					}
					customerMaster.setConsignors(consignorList);
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, customerMaster, null),
							HttpStatus.OK);

				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
				}
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getConsignorsByPincode(Integer pincode) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MConsignorEntity> dbConsignorList = mConsignorJpaRepository.findByPincode(pincode);

			// need to read from adhoc / retail consignee table as well

			if (dbConsignorList != null && !dbConsignorList.isEmpty()) {
				ConsignorMasterByPincode consignorMaster = new ConsignorMasterByPincode();
				List<Consignor> consignorList = new ArrayList<Consignor>();
				for (MConsignorEntity dbConsignor : dbConsignorList) {
					Consignor consignor = new Consignor();
					consignor.setAddress1(dbConsignor.getConsignorAddress1());
					consignor.setAddress2(dbConsignor.getConsignorAddress2());
					consignor.setBranchCode(dbConsignor.getBranchCode());
					consignor.setCity(dbConsignor.getCity());
					consignor.setConsignorCode(dbConsignor.getConsignorCode());
					consignor.setConsignorName(dbConsignor.getConsignorName());
					consignor.setConsignorPincode(dbConsignor.getPincode());
					consignor.setState(dbConsignor.getStateCode());
					consignor.setCustCode(dbConsignor.getCustCode());
					consignor.setConsignorCode(dbConsignor.getConsignorCode());
					consignor.setContactEmailid(dbConsignor.getContactEmailid());
					consignor.setContactName(dbConsignor.getContactName());
					consignor.setContactPhoneno(dbConsignor.getContactPhoneno());
					consignor.setContractCustomer(true);
					if (dbConsignor.getMaCustomerDetails() != null) {
						consignor.setCustomerId(dbConsignor.getMaCustomerDetails().getId());
					}
					consignor.setConsignorId(dbConsignor.getId());
					consignorList.add(consignor);
				}
				consignorMaster.setPincode(pincode);
				consignorMaster.setConsignors(consignorList);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, consignorMaster, null), HttpStatus.OK);

			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_CONSIGNORS_AVLBL, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getConsigneeByPincode(Integer pincode) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MConsigneeEntity> dbConsigneeList = mConsigneeJpaRepository.findByPincode(pincode);

			if (dbConsigneeList != null && !dbConsigneeList.isEmpty()) {
				List<Consignee> consigneeList = new ArrayList<Consignee>();
				for (MConsigneeEntity dbConsignee : dbConsigneeList) {
					Consignee consignee = new Consignee();
					consignee.setAddress1(dbConsignee.getConsigneeAddress1());
					consignee.setAddress2(dbConsignee.getConsigneeAddress2());
					consignee.setCity(dbConsignee.getCity());
					consignee.setConsigneeCode(dbConsignee.getConsigneeCode());
					consignee.setConsigneeId(dbConsignee.getId());
					consignee.setConsigneeName(dbConsignee.getConsigneeName());
					consignee.setConsigneePincode(dbConsignee.getPincode());
					consignee.setContactName(dbConsignee.getContactName());
					consignee.setContactPhoneno(dbConsignee.getContactPhoneno());
					consignee.setCustCode(dbConsignee.getCustCode());
					consignee.setCustomerId(dbConsignee.getCustomerId());
					consignee.setEmail(dbConsignee.getEmail());
					consignee.setGstin(dbConsignee.getGstin());
//						consignee.setMobileno(dbConsignee.getPhoneNum());
					consignee.setPan(dbConsignee.getPan());
					consignee.setState(dbConsignee.getState());
					consigneeList.add(consignee);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, consigneeList, null), HttpStatus.OK);

			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_CONSIGNEE_AVLBL, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

//	public ResponseEntity<AppGeneralResponse> getOdaByPin(Integer pincode) {
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			List<MaAreasEntity> areasdata = maAreasJpaRepository.findByPincode(pincode);
//			PincodeOdaDetails pinOdaDetails = new PincodeOdaDetails();
//			pinOdaDetails.setPincode(pincode);
//			if (areasdata != null && !areasdata.isEmpty()) {
//				pinOdaDetails.setOdaFlag(true);
//				List<String> areaList = new ArrayList<String>();
//				for (MaAreasEntity dbPinEntity : areasdata) {
//					areaList.add(dbPinEntity.getAreaName());
//				}
//				pinOdaDetails.setArea(areaList);
//			} else {
//				pinOdaDetails.setOdaFlag(false);
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pinOdaDetails, null), HttpStatus.OK);
//		} catch (Exception e) {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//			e.printStackTrace();
//		}
//		return generalResponse;
//	}

//	public ResponseEntity<AppGeneralResponse> getAllOdaPinCodeData() {
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			List<MaPincodeEntity> pinEntityList = maPincodeJpaRepository.findByActiveFlagAndOda(1, 1);
//
//			Map<Integer, List<String>> data = new HashMap<Integer, List<String>>();
//
//			List<OdaPincodeDetails> odaData = new ArrayList<OdaPincodeDetails>();
//
//			if (pinEntityList != null && !pinEntityList.isEmpty()) {
//
//				List<String> areaList = new ArrayList<String>();
//				for (MPincodeEntity dbPinEntity : pinEntityList) {
//					if (data.containsKey(dbPinEntity.getPincode())) {
//						List<String> aList = data.get(dbPinEntity.getPincode());
//						aList.add(dbPinEntity.getArea());
//						data.put(dbPinEntity.getPincode(), aList);
//					} else {
//						List<String> aList = new ArrayList<String>();
//						aList.add(dbPinEntity.getArea());
//						data.put(dbPinEntity.getPincode(), aList);
//					}
//				}
//				for (Integer pin : data.keySet()) {
//					OdaPincodeDetails oda = new OdaPincodeDetails();
//					oda.setPincode(pin);
//					List<String> areaLst = new ArrayList<String>();
//					oda.setArea(areaLst);
//					for (String area : data.get(pin)) {
//						oda.getArea().add(area);
//					}
//					odaData.add(oda);
//				}
//
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, odaData, null), HttpStatus.OK);
//		} catch (Exception e) {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//			e.printStackTrace();
//		}
//		return generalResponse;
//	}

	public ResponseEntity<AppGeneralResponse> getServiceTypes(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MServiceTypeEntity> pinEntityList = null;
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				pinEntityList = (List<MServiceTypeEntity>) mServiceTypeJpaRepository.findAll();
			}else{
				pinEntityList = mServiceTypeJpaRepository.findByVersionGreaterThan(maxVersion.get());
			}
			if (pinEntityList != null && !pinEntityList.isEmpty()) {
				List<ServiceType> svcs = new ArrayList<ServiceType>();
				for (MServiceTypeEntity entity : pinEntityList) {
					ServiceType svc = new ServiceType();
					svc.setCode(entity.getCode());
					svc.setServiceType(entity.getServiceType());
					svc.setSortOrder(entity.getSortOrder());
					svc.setSeriesLength(entity.getSeriesLength());
					svc.setShipmentType(entity.getShipmentType());
					svc.setStartSeries(entity.getStartSeries());
					svc.setVersion(entity.getVersion());
					svcs.add(svc);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, svcs, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getShipModes(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MShippingModeEntity> shipModes = null;
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				shipModes = (List<MShippingModeEntity>) mShippingModeJpaRepository
						.findByActiveFlag(1);
			}else{
				shipModes = (List<MShippingModeEntity>) mShippingModeJpaRepository.findByVersionGreaterThanAndActiveFlag(maxVersion.get(), 1);
						
			}
			
			if (shipModes != null && !shipModes.isEmpty()) {
				List<ShipMode> shippingModes = new ArrayList<ShipMode>();
				for (MShippingModeEntity entity : shipModes) {
					ShipMode mode = new ShipMode();
					mode.setCode(entity.getCode());
					mode.setShipMode(entity.getDesc());
					mode.setSortOrder(entity.getSortOrder());
					mode.setVersion(entity.getVersion());
					shippingModes.add(mode);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, shippingModes, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getShipmentType() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MShipTypeEntity> shipModes = (List<MShipTypeEntity>) mShipTypeJpaRepository.findByActiveFlag(1);
			if (shipModes != null && !shipModes.isEmpty()) {
				List<ShipmentType> shipTypeList = new ArrayList<ShipmentType>();
				for (MShipTypeEntity entity : shipModes) {
					ShipmentType shipType = new ShipmentType();
					shipType.setCode(entity.getCode());
					shipType.setDescription(entity.getDesc());
					shipType.setSortOrder(entity.getSortOrder());
					shipTypeList.add(shipType);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, shipTypeList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getStateMaster() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MStateEntity> stateList = (List<MStateEntity>) mStateJpaRepository.findAll();
			if (stateList != null && !stateList.isEmpty()) {
				List<State> svcs = new ArrayList<State>();
				for (MStateEntity entity : stateList) {
					State state = new State();
					state.setCode(entity.getStateCode());
					state.setName(entity.getStateName());
					state.setStateGstCode(entity.getGstStateCode());
					state.setType(entity.getStateType());
					svcs.add(state);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, svcs, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPickupStatus() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupStatusModel> pickupStatuses = null;
			List<MPickupStatusEntity> pickups = (List<MPickupStatusEntity>) mPickupStatusJpaRepository
					.findByActiveFlag(1);
			if (pickups != null && !pickups.isEmpty()) {
				pickupStatuses = new ArrayList<PickupStatusModel>();
				for (MPickupStatusEntity p : pickups) {
					PickupStatusModel ps = new PickupStatusModel();
					ps.setCode(p.getCode());
					ps.setDesc(p.getDesc());
					ps.setSortOrder(p.getSortOrder());
					pickupStatuses.add(ps);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, pickupStatuses, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPickupAssigmentType() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupAssignmentType> assTypes = null;
			List<MPickupAssignTypeEntity> dbAssTypes = (List<MPickupAssignTypeEntity>) mPickupAssignTypeJpaRepository
					.findByActiveFlag(1);
			if (dbAssTypes != null && !dbAssTypes.isEmpty()) {
				assTypes = new ArrayList<PickupAssignmentType>();
				for (MPickupAssignTypeEntity p : dbAssTypes) {
					PickupAssignmentType assType = new PickupAssignmentType();
					assType.setCode(p.getCode());
					assType.setDesc(p.getDesc());
					assType.setSortOrder(p.getSortOrder());
					assTypes.add(assType);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, assTypes, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPickupCancelRescheduleReasonCodes(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupCancelRescheduleReasonsModel> reasons = null;
			List<MPickupCancRescReasonsEntity> dbReasons = null;
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				dbReasons = (List<MPickupCancRescReasonsEntity>) mPickupCancRescReasonsJpaRepository
						.findByActiveFlag(1);
			} else {
				dbReasons = (List<MPickupCancRescReasonsEntity>) mPickupCancRescReasonsJpaRepository
						.findByVersionGreaterThanAndActiveFlag(maxVersion.get(), 1);
			}

			if (dbReasons != null && !dbReasons.isEmpty()) {
				reasons = new ArrayList<PickupCancelRescheduleReasonsModel>();
				for (MPickupCancRescReasonsEntity p : dbReasons) {
					PickupCancelRescheduleReasonsModel res = new PickupCancelRescheduleReasonsModel();
					res.setCode(p.getCode());
					res.setDesc(p.getDesc());
					res.setCancelRescheduleFlag(p.getCancelReschFlag());
					res.setSortOrder(p.getSortOrder());
					res.setVersion(p.getVersion());
					reasons.add(res);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, reasons, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public List<AssignedToModel> getAssignedToList() {
		List<AssignedToModel> assignedToList = new ArrayList<AssignedToModel>();
		try {
			List<UserMasterEntity> allUsers = userMasterJpaRepository.findByActiveFlagEquals(1);
			List<MaBranchEntity> allBranches = maBranchJpaRepository.findByActiveFlag(1);

			for (UserMasterEntity user : allUsers) {
				AssignedToModel rec = new AssignedToModel();
				rec.setName(user.getUserId());
				rec.setType(Constants.AGENT);
				assignedToList.add(rec);
			}

			for (MaBranchEntity branch : allBranches) {
				AssignedToModel rec = new AssignedToModel();
				rec.setName(branch.getOfficeCode());
				if (branch.getOfficeType().equals(Constants.FRANCHISEE)) {
					rec.setType(Constants.FRANCHISEE);
				} else {
					rec.setType(Constants.BRANCH);
				}
				assignedToList.add(rec);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return assignedToList;
	}

	public ResponseEntity<AppGeneralResponse> getPickupTypes(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupAssignmentType> assTypes = null;
			List<MPickupTypesEntity> dbAssTypes = null;
			if(!maxVersion.isPresent() || maxVersion.get() == 0){
				dbAssTypes = mPickupTypesJpaRepository.findByActiveFlag(1);
			}else{
				dbAssTypes = mPickupTypesJpaRepository.findByVersionGreaterThanAndActiveFlag(maxVersion.get(),1);
			}
		
			if (dbAssTypes != null && !dbAssTypes.isEmpty()) {
				assTypes = new ArrayList<PickupAssignmentType>();
				for (MPickupTypesEntity p : dbAssTypes) {
					PickupAssignmentType assType = new PickupAssignmentType();
					assType.setCode(p.getCode());
					assType.setDesc(p.getDesc());
					assType.setVersion(p.getVersion());
					assTypes.add(assType);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, assTypes, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPaperWorkTypes(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupAssignmentType> assTypes = null;
			List<MPaperworkTypeEntity> dbAssTypes = null;
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				dbAssTypes = mPaperworkTypeJpaRepository.findByActiveFlag(1);
			}else{
				dbAssTypes = mPaperworkTypeJpaRepository.findByVersionGreaterThanAndActiveFlag(maxVersion.get(), 1);
			}
			if (dbAssTypes != null && !dbAssTypes.isEmpty()) {
				assTypes = new ArrayList<PickupAssignmentType>();
				for (MPaperworkTypeEntity p : dbAssTypes) {
					PickupAssignmentType assType = new PickupAssignmentType();
					assType.setId(p.getId());
					assType.setCode(p.getCode());
					assType.setDesc(p.getPaperworkType());
					assType.setVersion(p.getVersion());
					assTypes.add(assType);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, assTypes, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getChargeTypes() {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupAssignmentType> assTypes = null;
			List<MChargeTypesEntity> dbAssTypes = mChargeTypesJpaRepository.findByActiveFlag(1);
			if (dbAssTypes != null && !dbAssTypes.isEmpty()) {
				assTypes = new ArrayList<PickupAssignmentType>();
				for (MChargeTypesEntity p : dbAssTypes) {
					PickupAssignmentType assType = new PickupAssignmentType();
					assType.setId(p.getId());
					assType.setCode(p.getCode());
					assType.setDesc(p.getChargeType());
					assTypes.add(assType);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, assTypes, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getConsignorsByBranch(String branch, Optional<Integer> maxVersion) {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MConsignorEntity> dbConsignorList = null;
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				dbConsignorList = mConsignorJpaRepository.findByBranchCode(branch);
			} else {
				dbConsignorList = mConsignorJpaRepository.findByBranchCodeAndVersionGreaterThan(branch,
						maxVersion.get());
			}

			// need to read from adhoc / retail consignee table as well

			if (dbConsignorList != null && !dbConsignorList.isEmpty()) {
				ConsignorMasterByPincode consignorMaster = new ConsignorMasterByPincode();
				List<Consignor> consignorList = new ArrayList<Consignor>();
				for (MConsignorEntity dbConsignor : dbConsignorList) {
					Consignor consignor = new Consignor();
					consignor.setAddress1(dbConsignor.getConsignorAddress1());
					consignor.setAddress2(dbConsignor.getConsignorAddress2());
					consignor.setBranchCode(dbConsignor.getBranchCode());
					consignor.setCity(dbConsignor.getCity());
					consignor.setConsignorCode(dbConsignor.getConsignorCode());
					consignor.setConsignorName(dbConsignor.getConsignorName());
					consignor.setConsignorPincode(dbConsignor.getPincode());
					consignor.setState(dbConsignor.getStateCode());
					consignor.setCustCode(dbConsignor.getCustCode());
					consignor.setConsignorCode(dbConsignor.getConsignorCode());
					consignor.setContactEmailid(dbConsignor.getContactEmailid());
					consignor.setContactName(dbConsignor.getContactName());
					consignor.setContactPhoneno(dbConsignor.getContactPhoneno());
					consignor.setContractCustomer(true);
					if (dbConsignor.getMaCustomerDetails() != null) {
						consignor.setCustomerId(dbConsignor.getMaCustomerDetails().getId());
					}
					consignor.setConsignorId(dbConsignor.getId());
					consignorList.add(consignor);
				}
				consignorMaster.setConsignors(consignorList);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, consignorMaster, null), HttpStatus.OK);

			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;

	}

	public ResponseEntity<AppGeneralResponse> getAllConsignees(Optional<Integer> version) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MConsigneeEntity> dbConsigneeList = null;
//			dbConsigneeList = (List<MConsigneeEntity>) mConsigneeJpaRepository.findAll();

			if (dbConsigneeList != null && !dbConsigneeList.isEmpty()) {
				List<Consignee> consigneeList = new ArrayList<Consignee>();
				for (MConsigneeEntity dbConsignee : dbConsigneeList) {
					Consignee consignee = new Consignee();
					consignee.setAddress1(dbConsignee.getConsigneeAddress1());
					consignee.setAddress2(dbConsignee.getConsigneeAddress2());
					consignee.setCity(dbConsignee.getCity());
					consignee.setConsigneeCode(dbConsignee.getConsigneeCode());
					consignee.setConsigneeId(dbConsignee.getId());
					consignee.setConsigneeName(dbConsignee.getConsigneeName());
					consignee.setConsigneePincode(dbConsignee.getPincode());
					consignee.setContactName(dbConsignee.getContactName());
					consignee.setContactPhoneno(dbConsignee.getContactPhoneno());
					consignee.setCustCode(dbConsignee.getCustCode());
					consignee.setCustomerId(dbConsignee.getCustomerId());
					consignee.setEmail(dbConsignee.getEmail());
					consignee.setGstin(dbConsignee.getGstin());
//						consignee.setMobileno(dbConsignee.getPhoneNum());
					consignee.setPan(dbConsignee.getPan());
					consignee.setState(dbConsignee.getState());
					consigneeList.add(consignee);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, consigneeList, null), HttpStatus.OK);

			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getAllBranches() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<MaBranchEntity> dbBranchList = (List<MaBranchEntity>) maBranchJpaRepository.findAll();
			if (dbBranchList != null && !dbBranchList.isEmpty()) {
				List<Branch> branchList = new ArrayList<Branch>();
				for (MaBranchEntity branch : dbBranchList) {
					Branch br = new Branch();
					br.setBranchCode(branch.getOfficeCode());
					br.setBranchName(branch.getOfficeName());
					branchList.add(br);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, branchList, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getDocketStatus() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<DocketStatusModel> docketStatuses = null;
			List<MDocketStatusEntity> docketStatus = (List<MDocketStatusEntity>) mDocketStatusJpaRepository
					.findByActiveFlag(1);
			if (docketStatus != null && !docketStatus.isEmpty()) {
				docketStatuses = new ArrayList<DocketStatusModel>();
				for (MDocketStatusEntity p : docketStatus) {
					DocketStatusModel ds = new DocketStatusModel();
					ds.setCode(p.getCode());
					ds.setDesc(p.getDesc());
					ds.setSortOrder(p.getSortOrder());
					docketStatuses.add(ds);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, docketStatuses, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getDocketMaster(String userId, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			logger.info("UserId: " + userId);
			List<DocketMasterModel> docketMasterList = null;
			List<MDktMasterEntity> dbDkts = (List<MDktMasterEntity>) mDktMasterJpaRepository
					.findByUserIdAndConsumed(userId, 0);
			if (dbDkts != null && !dbDkts.isEmpty()) {
				docketMasterList = new ArrayList<DocketMasterModel>();
				for (MDktMasterEntity rec : dbDkts) {
					DocketMasterModel item = new DocketMasterModel();
					item.setDktNo(rec.getDktNo());
					item.setDktType(rec.getDktTypeFlag());
					docketMasterList.add(item);
					logger.info("Dkt: " + item.getDktNo());
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, docketMasterList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		logger.info("******* END ****** ");
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getPacketMaster(String userId) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PacketMasterModel> packetMasterList = null;
			List<MPktMasterEntity> dbPkts = (List<MPktMasterEntity>) mPktMasterJpaRepository.findByUserIdAndCompleteFlag(userId, 0);
			if (dbPkts != null && !dbPkts.isEmpty()) {
				packetMasterList = new ArrayList<PacketMasterModel>();
				for (MPktMasterEntity rec : dbPkts) {
					PacketMasterModel item = new PacketMasterModel();
					item.setStartPktNo(rec.getStartPktNo());
					item.setEndPktNo(rec.getEndPktNo());
					item.setLastPktUsed(rec.getLastPktUsed());
					item.setPktType(rec.getPktTypeFlag());
					packetMasterList.add(item);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, packetMasterList, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.EMPTY_DATA_SET, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getRelationships(Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<MRelationshipEntity> relationships = null;
		if (!maxVersion.isPresent() || maxVersion.get() == 0) {
		relationships = (List<MRelationshipEntity>) mRelationshipJpaRepository.findAll();
		}else{
			relationships = mRelationshipJpaRepository.findByVersionGreaterThan(maxVersion.get());
		}
		List<GenericMasterModel> data = new ArrayList<>();
		if (relationships != null && !relationships.isEmpty()) {
			for (MRelationshipEntity dbRecord : relationships) {
				GenericMasterModel item = new GenericMasterModel();
				item.setCode(dbRecord.getCode());
				item.setDescription(dbRecord.getDesc());
				item.setSortOrder(dbRecord.getSortOrder());
				item.setVersion(dbRecord.getVersion());
				data.add(item);
			}

			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getAgentsByBranch(String branch, String userId) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		UserMasterEntity user = userMasterJpaRepository.findByUserId(userId);
		if(user == null){
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, "User not found", null, null), HttpStatus.OK);
		}
		List<UserMasterEntity> userList = null;
		if(CommonUtility.check(user.getUserType()) && user.getUserType().equalsIgnoreCase(Constants.FRANCHISE_USER)){
			userList = userMasterJpaRepository.findByFranchiseBranchCodeAndActiveFlag(branch, 1);
		}else{
			
			userList = userMasterJpaRepository.findByBranchCode(branch);
		}

		List<AgentModel> data = new ArrayList<>();
		if (userList != null && !userList.isEmpty()) {
			for (UserMasterEntity dbRecord : userList) {
				AgentModel item = new AgentModel();
				item.setBranch(branch);
				item.setFullName(dbRecord.getFullName());
				item.setUserId(dbRecord.getUserId());
				if(dbRecord.getAllowDelivery() != null)
					item.setIsDeliveryAllowed(dbRecord.getAllowDelivery() == 0 ? false : true);
				else
					item.setIsDeliveryAllowed(false);
				if(dbRecord.getAllowPickupRegistration() != null)
					item.setIsRegistrationAllowed(dbRecord.getAllowPickupRegistration() == 0 ? false : true);
				else
					item.setIsRegistrationAllowed(false);
				
				if(dbRecord.getAllowPickupBooking() != null)
					item.setIsBookingAllowed(dbRecord.getAllowPickupBooking() == 0 ? false : true);
				else
					item.setIsBookingAllowed(false);
				
				item.setUserType(dbRecord.getUserType());
				data.add(item);
			}

			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.SYNC_UPTODATE, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getRedirectReasons(Optional<Integer> version) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<GenericMasterModel> data = null;
		List<MRedirectReasonsEntity> dbList = null;
		try {
			if (!version.isPresent() || version.get() == 0) {
				dbList = (List<MRedirectReasonsEntity>) mRedirectReasonsJpaRepository.findByActiveFlag(1);
			} else {
				dbList = (List<MRedirectReasonsEntity>) mRedirectReasonsJpaRepository
						.findByVersionGreaterThanAndActiveFlag(version.get(), 1);
			}
			if (dbList != null && !dbList.isEmpty()) {
				data = new ArrayList<GenericMasterModel>();
				for (MRedirectReasonsEntity dbRecord : dbList) {
					GenericMasterModel item = new GenericMasterModel();
					item.setCode(dbRecord.getCode());
					item.setDescription(dbRecord.getDescription());
					item.setSortOrder(dbRecord.getSortOrder());
					item.setVersion(dbRecord.getVersion());
					data.add(item);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> getCashFreight(CashFreightRequestModel data) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			String originPin = data.getOriginPin();
			String destPin = data.getDestinationPin();

			String category = getCategory(originPin, destPin);
			String uom = data.getUom();
			Integer wt = data.getWeight();
			if (uom != null && uom.equals("KG")) {
				wt = wt * 1000;
			} else if (uom != null && uom.equals("TON")) {
				wt = wt * 1000 * 1000;
			}
			String shipMode = "-";
			if (data.getDoxNondox() != null && data.getDoxNondox().equals("N")) {
				shipMode = data.getShipMode();
			}
			MCashFreightEntity frt = mCashFreightJpaRepository
					.findByCategoryAndDoxNondoxAndShipModeAndMinWtLessThanEqualAndMaxWtGreaterThanEqual(category,
							data.getDoxNondox(), shipMode, wt, wt);
			Integer addWtInKG = 0;
			Integer additionalWtInGrams = wt - frt.getMinWt();
			if (additionalWtInGrams > 0) {
				addWtInKG = (int) (additionalWtInGrams / 1000);
				if (additionalWtInGrams % 1000 > 0) {
					addWtInKG++;
				}
			}
			Integer freight = frt.getRate() + (frt.getRateAddlKg() * addWtInKG);
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, freight, null), HttpStatus.OK);
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}

	private String getCategory(String originPin, String destPin) {
		PincodeMasterEntity originPinEntity = pincodeMasterJpaRepository.findByPincode(originPin);
		PincodeMasterEntity destPinEntity = pincodeMasterJpaRepository.findByPincode(destPin);

		if (originPinEntity.getCityId().intValue() == destPinEntity.getCityId().intValue()) {
			return Constants.CATEGORY_FREIGHT_SAME_CITY;
		} else if (originPinEntity.getStateId().intValue() == destPinEntity.getStateId().intValue()) {
			return Constants.CATEGORY_FREIGHT_SAME_STATE;
		} else {
			StateMasterEntity originStateEntity = stateMasterJpaRepository.findByStateId(originPinEntity.getStateId());
			StateMasterEntity destStateEntity = stateMasterJpaRepository.findByStateId(destPinEntity.getStateId());

			if (originStateEntity.getZoneId().intValue() == destStateEntity.getZoneId().intValue()) {
				return Constants.CATEGORY_FREIGHT_SAME_ZONE;
			} else {
				CityMasterEntity originCityEntity = cityMasterJpaRepository.findByCityId(originPinEntity.getCityId());
				CityMasterEntity destCityEntity = cityMasterJpaRepository.findByCityId(destPinEntity.getCityId());

				if (originCityEntity.getCategory() != null && destCityEntity.getCategory() != null) {
					if (originCityEntity.getCategory().toUpperCase().equals(Constants.CATEGORY_FREIGHT_METRO)
							&& destCityEntity.getCategory().toUpperCase().equals(Constants.CATEGORY_FREIGHT_METRO)) {
						return Constants.CATEGORY_FREIGHT_METRO;
					} else if (!originCityEntity.getCategory().equals(Constants.CATEGORY_FREIGHT_SPL_REMOTE)
							&& destCityEntity.getCategory().equals(Constants.CATEGORY_FREIGHT_ROI_A)) {
						return Constants.CATEGORY_FREIGHT_ROI_A;
					} else if (!originCityEntity.getCategory().equals(Constants.CATEGORY_FREIGHT_SPL_REMOTE)
							&& destCityEntity.getCategory().equals(Constants.CATEGORY_FREIGHT_ROI_B)) {
						return Constants.CATEGORY_FREIGHT_ROI_B;
					}
				}
				return Constants.CATEGORY_FREIGHT_SPL_REMOTE;
			}
		}
	}

	public ResponseEntity<AppGeneralResponse> getTrackonUserMaster(Logger logger) {
		String usermasterUrl = FilesUtil.getProperty("usermaster");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {
			WSResponseObjectModel data = CommonUtility.doWSRequest(usermasterUrl, "", Constants.HTTP_GET, logger);
			logger.info("Data: " + data.getResponse());
			List<TrackonUserMasterModel> userList = null;
			if (data.getResponseCode() == Constants.HTTP_OK) {
				Set<String> incomingUserSet = new HashSet<>();
				TrackonUserMasterResponseModel userMasterResponse = gson.fromJson(data.getResponse(), TrackonUserMasterResponseModel.class);
				userList = userMasterResponse.getUserDetails();
				if (userList != null && !userList.isEmpty()) {
					
					List<UserMasterEntity> existingUsers = (List<UserMasterEntity>) userMasterJpaRepository.findAll();
					HashMap<String, UserMasterEntity> eusersmap = new HashMap<>();
					if(existingUsers != null && !existingUsers.isEmpty()){
						for(UserMasterEntity u : existingUsers){
							eusersmap.put(u.getUserId(), u);
						}
					}
					
					for(TrackonUserMasterModel t : userList){
						incomingUserSet.add(t.getUserName());
						if(eusersmap.containsKey(t.getUserName())){
							UserMasterEntity u = EntityToModels.convertTrackonUserMasterModelToEntity(t, eusersmap.get(t.getUserName()));
							eusersmap.put(t.getUserName(), u);
						}
						else{
							eusersmap.put(t.getUserName(), EntityToModels.convertTrackonUserMasterModelToEntity(t, null));
						}
					}
					
					userMasterJpaRepository.save(eusersmap.values());
					List<UserMasterEntity> inactiveUsers = new ArrayList<>();
					for(UserMasterEntity u : eusersmap.values()){
						if(!incomingUserSet.contains(u.getUserId())){
							u.setActiveFlag(0);
							u.setUpdatedTimestamp(new Date());
							inactiveUsers.add(u);
						}
					}
					if(!inactiveUsers.isEmpty()){
						userMasterJpaRepository.save(inactiveUsers);
					}
//					List<UserMasterEntity> dbEntities = EntityToModels.convertTrackonUserModelsToEntity(userList);
//					userMasterJpaRepository.save(dbEntities);
					return new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
				}else {
					return new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}
			} else {
				logger.info("Error in Webservice: " + data.getErrorString());
				new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.FAILED_ERP_RESPONSE, null, null), HttpStatus.OK);
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage(), e);
			new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, e.getLocalizedMessage(), null), HttpStatus.OK);
		}
		return null;
	}
	
	public ResponseEntity<AppGeneralResponse> getAllOdaPinCodeData() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			
			
			
			List<MaPincodeEntity> pincodes = maPincodeJpaRepository.findByActiveFlagAndOda(1, 1);

			Map<Integer, List<String>> data = new HashMap<Integer, List<String>>();

			//response
			Map<Integer, OdaPincodeDetails> odaAreas = new HashMap<>();
			OdaPincodeDetails pincode = null;
 			if (pincodes != null && !pincodes.isEmpty()) {

				for(MaPincodeEntity p : pincodes){
					pincode = new OdaPincodeDetails();
					pincode.setPincode(p.getPincode());
					pincode.setArea(new ArrayList<>());
					odaAreas.put(p.getPincode(), pincode);
				}
				
				List<MaAreasEntity> areaslist = (List<MaAreasEntity>) maAreasJpaRepository.findAll();
				if(areaslist != null && !areaslist.isEmpty()){
					for(MaAreasEntity a : areaslist){
						if(odaAreas.containsKey(a.getPincode())){
							odaAreas.get(a.getPincode()).getArea().add(a.getAreaName());
						}
					}
				}

			}
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, odaAreas.values(), null), HttpStatus.OK);
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			e.printStackTrace();
		}
		return generalResponse;
	}
	
	public ResponseEntity<AppGeneralResponse> getMaterials(Optional<Integer> maxVersion) {

		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try{
			List<GenericMasterModel> data = new ArrayList<>();
			GenericMasterModel c = null;
			List<MaContentEntity>  materials = null; 
			if (!maxVersion.isPresent() || maxVersion.get() == 0) {
				materials = (List<MaContentEntity>) maContentJpaRepository.findAll();
			}else{
				materials = (List<MaContentEntity>) maContentJpaRepository.findByVersionGreaterThan(maxVersion.get());
			}
		
			if (materials != null && !materials.isEmpty()) {

				for(MaContentEntity m : materials){
					c = new GenericMasterModel();
					c.setCode(m.getSkuCode());
					c.setDescription(m.getSkuCategory());
					c.setVersion(m.getVersion());
					data.add(c);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
	}
}
