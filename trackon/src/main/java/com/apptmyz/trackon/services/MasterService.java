package com.apptmyz.trackon.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.apptmyz.trackon.data.repository.jpa.BookingDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsigneeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsignorJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MDktRangeMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPktMasterJpaRepository;
import com.apptmyz.trackon.entities.jpa.BookingDataEntity;
import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;
import com.apptmyz.trackon.entities.jpa.MConsignorEntity;
import com.apptmyz.trackon.entities.jpa.MDktRangeMasterEntity;
import com.apptmyz.trackon.entities.jpa.MPktMasterEntity;
import com.apptmyz.trackon.model.Consignee;
import com.apptmyz.trackon.model.Consignor;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.ResponseMessages;

@Service
public class MasterService {
	
	@Autowired
	private MPktMasterJpaRepository mPktMasterJpaRepository;
	
	@Autowired
	private MConsignorJpaRepository mConsignorJpaRepository;

	@Autowired
	private MConsigneeJpaRepository mConsigneeJpaRepository;
	
	@Autowired
	private BookingDataJpaRepository bookingDataJpaRepository;
	
	@Autowired
	private MDktRangeMasterJpaRepository mDktRangeMasterJpaRepository;
	
	public synchronized List<?> getPktSeries(Integer count, Logger log, String isxb) throws Exception {
		

		BigInteger bigOne = BigInteger.valueOf(1);
		BigInteger requestCount = new BigInteger(String.valueOf(count));
		List<Object> packets = new ArrayList<>();
		
		MPktMasterEntity pkt = null;
		
		if(CommonUtility.check(isxb) && isxb.equalsIgnoreCase("Y")){
			pkt = mPktMasterJpaRepository.findByUserId("XB");
		}else{
			
			pkt = mPktMasterJpaRepository.findByUserId("TRACKON");
		}
		
		if(pkt != null){
			if (pkt.getRemainingCount() != null && pkt.getRemainingCount() >= count) {
				BigInteger startPkt = new BigInteger(pkt.getLastPktUsed().toString()).add(bigOne);
				BigInteger endPkt = new BigInteger(pkt.getLastPktUsed().toString()).add(requestCount);
				pkt.setLastPktUsed(endPkt.longValueExact());
				pkt.setRemainingCount(pkt.getRemainingCount() - count);
				mPktMasterJpaRepository.save(pkt);
				while (!startPkt.equals(endPkt)) {
					if(CommonUtility.check(isxb) && isxb.equalsIgnoreCase("Y")){
						packets.add(pkt.getPktPrefix().concat(startPkt.toString()));
					}else{						
						packets.add(startPkt);
					}
					startPkt = startPkt.add(bigOne);
				}
				if(CommonUtility.check(isxb) && isxb.equalsIgnoreCase("Y")){
					packets.add(pkt.getPktPrefix().concat(endPkt.toString()));
				}else{						
					packets.add(endPkt);
				}
				
			}
		}
		else{
			log.info("pkt master not found");
		}
		return packets;
	}

	public ResponseEntity<AppGeneralResponse> getMConsignorDetails(Integer pincode,String word){
		ResponseEntity<AppGeneralResponse> response = null;
		List<Consignor> model = new ArrayList<>();
		try{
			List<MConsignorEntity> list = mConsignorJpaRepository.findByPincodeAndConsignorNameLike(pincode,word);
			Consignor mcon = null;
			if(list!=null && !list.isEmpty()){
				for(MConsignorEntity en : list){
					mcon = new Consignor();
					mcon.setBranchCode(en.getBranchCode());
					mcon.setCity(en.getCity());
					mcon.setAddress1(en.getConsignorAddress1());
					mcon.setContactEmailid(en.getContactEmailid());
					mcon.setAddress2(en.getConsignorAddress2());
					mcon.setConsignorCode(en.getConsignorCode());
					mcon.setConsignorName(en.getConsignorName());
					mcon.setContactName(en.getContactName());
					mcon.setContactPhoneno(en.getContactPhoneno());
					mcon.setConsignorId(en.getId());
					if(en.getMaCustomerDetails()!=null){
						mcon.setCustomerId(en.getMaCustomerDetails().getId());
					}
					mcon.setCustCode(en.getCustCode());
					mcon.setEmail(en.getEmail());
					mcon.setGstin(en.getGstin());
					mcon.setPan(en.getPan());
					mcon.setMobileno(en.getPhoneNum());
					mcon.setConsignorPincode(en.getPincode());
					mcon.setState(en.getState()); 
					mcon.setConsignorId(en.getId());
					mcon.setContractCustomer(true);
					model.add(mcon);
				}
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, model, null), HttpStatus.OK);
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "No consignors", null, null), HttpStatus.OK);
			}
		
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	public ResponseEntity<AppGeneralResponse> getMConsigneeDetails(Integer pincode,String word){
		ResponseEntity<AppGeneralResponse> response = null;
		List<Consignee> model = new ArrayList<>();
		try{
			List<MConsigneeEntity> list =  mConsigneeJpaRepository.findByPincodeAndConsigneeNameLike(pincode,word);
			Consignee mcon = null;
			if(list!=null && !list.isEmpty()){
				for(MConsigneeEntity en : list){
					mcon = new Consignee();
					mcon.setConsigneeId(en.getId());
					mcon.setCity(en.getCity());
					mcon.setConsigneeCode(en.getConsigneeCode());
					mcon.setConsigneeName(en.getConsigneeName());
					mcon.setContactName(en.getContactName());
					mcon.setContactPhoneno(en.getContactPhoneno());
					mcon.setCustCode(en.getCustCode());
					mcon.setCustomerId(en.getCustomerId());
					mcon.setEmail(en.getEmail());
					mcon.setGstin(en.getGstin());
					mcon.setPan(en.getPan());     
					mcon.setState(en.getState());
					mcon.setAddress1(en.getConsigneeAddress1());
					mcon.setAddress2(en.getConsigneeAddress2());
					mcon.setConsigneeId(en.getId());
					mcon.setConsigneePincode(en.getPincode());
					mcon.setMobileno(en.getPhoneNum());
					model.add(mcon);
				}
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, model, null), HttpStatus.OK);
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "No consignees", null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	public ResponseEntity<AppGeneralResponse> validateXbDktNo(String xbDktno) throws Exception {
		Logger log = Logger.getLogger("ValidateXbDktNo");
		ResponseEntity<AppGeneralResponse> response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "XB docket already used", null, null), HttpStatus.OK);
		log.info("Query start : "+xbDktno);
		List<String> booking = bookingDataJpaRepository.getxbdktrows(xbDktno);
		log.info("Query end");
		if(booking != null && booking.size() > 0){
			log.info("already exists");
			response = new ResponseEntity<>(new AppGeneralResponse(false, "XB docket already used", null, null), HttpStatus.OK);
		}
		else{
			log.info("not exists");
			MDktRangeMasterEntity range = mDktRangeMasterJpaRepository.checkNumInActiveRangeForXb(new BigInteger(xbDktno));
			
			if(range != null){
				log.info("found in valid range");
				response = new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
			}else{
				log.info("invalid");
				response = new ResponseEntity<>(new AppGeneralResponse(false, "Invalid XB docket no.", null, null), HttpStatus.OK);
			}
		}
		return response;
	}
	
}
