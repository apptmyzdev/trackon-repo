//package com.apptmyz.trackon.services;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//
//import com.apptmyz.trackon.data.repository.jpa.CityMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.ContentMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.CountryMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.CustomerMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.DesignationMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.IssueDetailsJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.IssueTypeJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.ItemGroupMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.ItemMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.MtBranchJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.NdrStatusJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.NdrsMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OfficeMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OfficeTypeMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsAirlineVehicleMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsAirlineVehicleNameMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsColoaderMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsColoaderTypeMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsHubCutoffMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsModeMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsNextTrackMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.OpsStatusMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.PaperWorkMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.PincodeAreaMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.PincodeMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.PrimeCountryMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.ReleaseMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.StateMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblAreaMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblDelvBoyMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblHolidayMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblRateZoneMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblRoMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblRoleMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblUserInRoleJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.TblUserMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.VendorMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.ZoneMasterJpaRepository;
//import com.apptmyz.trackon.entities.jpa.CityMasterEntity;
//import com.apptmyz.trackon.entities.jpa.ContentMasterEntity;
//import com.apptmyz.trackon.entities.jpa.CountryMasterEntity;
//import com.apptmyz.trackon.entities.jpa.CustomerMasterEntity;
//import com.apptmyz.trackon.entities.jpa.DesignationMasterEntity;
//import com.apptmyz.trackon.entities.jpa.IssueDetailsEntity;
//import com.apptmyz.trackon.entities.jpa.IssueTypeEntity;
//import com.apptmyz.trackon.entities.jpa.ItemGroupMasterEntity;
//import com.apptmyz.trackon.entities.jpa.ItemMasterEntity;
//import com.apptmyz.trackon.entities.jpa.MtBranchEntity;
//import com.apptmyz.trackon.entities.jpa.NdrStatusEntity;
//import com.apptmyz.trackon.entities.jpa.NdrsMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OfficeMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OfficeTypeMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsAirlineVehicleMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsAirlineVehicleNameMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsColoaderMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsColoaderTypeMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsHubCutoffMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsModeMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsNextTrackMasterEntity;
//import com.apptmyz.trackon.entities.jpa.OpsStatusMasterEntity;
//import com.apptmyz.trackon.entities.jpa.PaperWorkMasterEntity;
//import com.apptmyz.trackon.entities.jpa.PincodeAreaMasterEntity;
//import com.apptmyz.trackon.entities.jpa.PincodeMasterEntity;
//import com.apptmyz.trackon.entities.jpa.PrimeCountryMasterEntity;
//import com.apptmyz.trackon.entities.jpa.ReleaseMasterEntity;
//import com.apptmyz.trackon.entities.jpa.StateMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblAreaMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblDelvBoyMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblHolidayMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblRateZoneMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblRoMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblRoleMasterEntity;
//import com.apptmyz.trackon.entities.jpa.TblUserInRoleEntity;
//import com.apptmyz.trackon.entities.jpa.TblUserMasterEntity;
//import com.apptmyz.trackon.entities.jpa.VendorMasterEntity;
//import com.apptmyz.trackon.entities.jpa.ZoneMasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.CitymasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.ContentmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.CountrymasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.CustomermasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.DesiginationmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.IssuedetailsEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.IssuetypeEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.ItemgroupmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.ItemmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.MtbranchEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.NdrsmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.NdrstatusEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OfficemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OfficetypemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpsairlinevehiclemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpsairlinevehiclenamemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpscoloadermasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpscoloadertypemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpshubcutoffmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpsmodemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpsnexttrackmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.OpsstatusmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.PaperworkmasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.PincodeareamasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.PincodemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.PrimecountrymasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.ReleasemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.StatemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblRatezonemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblRolemasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblRomasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblUserinroleEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblUsermasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblareamasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TbldelvboymasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.TblholidaymasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.VendormasterEntity;
//import com.apptmyz.trackon.tcpl.entities.jpa.ZonemasterEntity;
//import com.apptmyz.trackon.tcpl.repository.ContentmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.CountrymasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.CustomermasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.DesiginationmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.IssuedetailsJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.IssuetypeJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.ItemmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.MtbranchJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.NdrsStatusJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OfficetypemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpsairlinevehiclemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpsairlinevehiclenamemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpscoloadermasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpscoloadertypemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpshubcutoffmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpsmodemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpsnexttrackmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.OpsstatusmasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.PincodemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.PrimecountrymasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.ReleasemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.StatemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TblRatezonemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TblRolemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TblUsermasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TblareamasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TbldelvboymasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TblholidaymasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplCitymasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplItemGroupMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplNdrsMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplOfficemasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplPaperWorkMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplPincodeAreaMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplTblRoMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplTblUserInRoleJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.TcplVendorMasterJpaRepository;
//import com.apptmyz.trackon.tcpl.repository.ZonemasterJpaRepository;
//import com.apptmyz.trackon.utils.AppGeneralResponse;
//import com.apptmyz.trackon.utils.ResponseMessages;
//
//@Service
//public class TcplMastersService {
//	
//	@Autowired
//	private CountrymasterJpaRepository countriesrepo;
//	
//	@Autowired
//	private CountryMasterJpaRepository mcountriesrepo;
//	
//	@Autowired
//	private ItemGroupMasterJpaRepository mItemGroupMasterRepo;
//	
//	@Autowired
//	private TcplItemGroupMasterJpaRepository itemGroupMasterRepo;
//	
//	@Autowired
//	private ItemMasterJpaRepository mItemMasterRepo;
//	
//	@Autowired
//	private ItemmasterJpaRepository itemMasterRepo;
//	
//	@Autowired
//	private NdrsMasterJpaRepository mNdrsMasterRepo;
//	
//	@Autowired
//	private TcplNdrsMasterJpaRepository ndrsMasterRepo;
//	
//	@Autowired
//	private NdrStatusJpaRepository mNdrStatusMasterRepo;
//	
//	@Autowired
//	private NdrsStatusJpaRepository ndrStatusMasterRepo;
//	
//	@Autowired
//	private OpsAirlineVehicleMasterJpaRepository mOpsAirlineVehicleMasterRepo;
//	
//	@Autowired
//	private OpsairlinevehiclemasterJpaRepository opsAirlineVehicleMasterRepo;
//	
//	@Autowired
//	private OpsAirlineVehicleNameMasterJpaRepository mOpsAirlineVehicleNameMasterRepo;
//	
//	@Autowired
//	private OpsairlinevehiclenamemasterJpaRepository opsAirlineVehicleNameMasterRepo;
//	
//	@Autowired
//	private OpsColoaderMasterJpaRepository mOpsColoaderMasterRepo;
//	
//	@Autowired
//	private OpscoloadermasterJpaRepository opsColoaderMasterRepo;
//	
//	@Autowired
//	private OpsColoaderTypeMasterJpaRepository mOpsColoaderTypeMasterRepo;
//	
//	@Autowired
//	private OpscoloadertypemasterJpaRepository opsColoaderTypeMasterRepo;
//	
//	@Autowired
//	private PaperWorkMasterJpaRepository mPaperworkMasterRepo;
//	
//	@Autowired
//	private TcplPaperWorkMasterJpaRepository paperworkMasterRepo;
//	
//	@Autowired
//	private PincodeAreaMasterJpaRepository mPincodeAreaMasterRepo;
//	
//	@Autowired
//	private TcplPincodeAreaMasterJpaRepository pincodeAreaMasterRepo;
//	
//	@Autowired
//	private PincodeMasterJpaRepository mPincodeMasterRepo;
//	
//	@Autowired
//	private PincodemasterJpaRepository pincodeMasterRepo;
//	
//	@Autowired
//	private StateMasterJpaRepository mStateMasterRepo;
//	
//	@Autowired
//	private StatemasterJpaRepository stateMasterRepo;
//	
//	@Autowired
//	private TblRoleMasterJpaRepository mTblRoleMasterRepo;
//	
//	@Autowired
//	private TblRolemasterJpaRepository tblRoleMasterRepo;
//	
//	@Autowired
//	private TblUserInRoleJpaRepository mTblUserInRoleMasterRepo;
//	
//	@Autowired
//	private TcplTblUserInRoleJpaRepository tblUserInRoleMasterRepo;
//	
//	@Autowired
//	private TblUserMasterJpaRepository mTblUserMasterRepo;
//	
//	@Autowired
//	private TblUsermasterJpaRepository tblUserMasterRepo;
//	
//	@Autowired
//	private VendorMasterJpaRepository mVendorMasterRepo;
//	
//	@Autowired
//	private TcplVendorMasterJpaRepository vendorMasterRepo;
//	
//	@Autowired
//	private ZoneMasterJpaRepository mZoneMasterRepo;
//	
//	@Autowired
//	private ZonemasterJpaRepository zoneMasterRepo;
//	
//	@Autowired
//	private CityMasterJpaRepository mCityMasterRepo;
//	
//	@Autowired
//	private TcplCitymasterJpaRepository cityMasterRepo;
//	
//	@Autowired
//	private ContentMasterJpaRepository mContentMasterRepo;
//	
//	@Autowired
//	private ContentmasterJpaRepository contentMasterRepo;
//	
//	@Autowired
//	private CustomerMasterJpaRepository mCustomerMasterRepo;
//	
//	@Autowired
//	private CustomermasterJpaRepository customerMasterRepo;
//	
//	@Autowired
//	private DesignationMasterJpaRepository mDesignationMasterRepo;
//	
//	@Autowired
//	private DesiginationmasterJpaRepository designationMasterRepo;
//	
//	@Autowired
//	private IssueDetailsJpaRepository mIssueDetailsMasterRepo;
//	
//	@Autowired
//	private IssuedetailsJpaRepository issueDetailsMasterRepo;
//	
//	@Autowired
//	private IssuetypeJpaRepository issueTypeMasterRepo;
//	
//	@Autowired
//	private IssueTypeJpaRepository mIssueTypeMasterRepo;
//	
//	@Autowired
//	private MtBranchJpaRepository mMtBranchMasterRepo;
//	
//	@Autowired
//	private MtbranchJpaRepository mtBranchMasterRepo;
//	
//	@Autowired
//	private OfficeMasterJpaRepository mOfficeMasterRepo;
//	
//	@Autowired
//	private TcplOfficemasterJpaRepository officeMasterRepo;
//	
//	@Autowired
//	private OfficeTypeMasterJpaRepository mOfficeTypeMasterRepo;
//	
//	@Autowired
//	private OfficetypemasterJpaRepository officeTypeMasterRepo;
//	
//	@Autowired
//	private PrimeCountryMasterJpaRepository mPrimeCountryMasterRepo;
//	
//	@Autowired
//	private PrimecountrymasterJpaRepository primeCountryMasterRepo;
//	
//	@Autowired
//	private TblRoMasterJpaRepository mTblRoMasterRepo;
//	
//	@Autowired
//	private TcplTblRoMasterJpaRepository tblRoMasterRepo;
//	
//	@Autowired
//	private TblAreaMasterJpaRepository mTblAreaMasterRepo;
//	
//	@Autowired
//	private TblareamasterJpaRepository tblAreaMasterRepo;
//	
//	@Autowired
//	private TblDelvBoyMasterJpaRepository mTblDelvBoyMasterRepo;
//	
//	@Autowired
//	private TbldelvboymasterJpaRepository tblDelvBoyMasterRepo;
//	
//	@Autowired
//	private OpsHubCutoffMasterJpaRepository mOpsHubCutoffMasterRepo;
//
//	@Autowired
//	private OpshubcutoffmasterJpaRepository opsHubCutoffMasterRepo;
//	
//	@Autowired
//	private TblRateZoneMasterJpaRepository mTblRateZoneMasterRepo;
//
//	@Autowired
//	private TblRatezonemasterJpaRepository tblRateZoneMasterRepo;
//	
//	@Autowired
//	private TblHolidayMasterJpaRepository mTblHolidayMasterRepo;
//
//	@Autowired
//	private TblholidaymasterJpaRepository tblHolidayMasterRepo;
//	
//	@Autowired
//	private ReleaseMasterJpaRepository mReleaseMasterRepo;
//
//	@Autowired
//	private ReleasemasterJpaRepository releaseMasterRepo;
//	
//	@Autowired
//	private OpsModeMasterJpaRepository mOpsModeMasterRepo;
//
//	@Autowired
//	private OpsmodemasterJpaRepository opsModeMasterRepo;
//	
//	@Autowired
//	private OpsStatusMasterJpaRepository mOpsStatusMasterRepo;
//
//	@Autowired
//	private OpsstatusmasterJpaRepository opsStatusMasterRepo;
//	
//	@Autowired
//	private OpsNextTrackMasterJpaRepository mOpsNextTrackMasterRepo;
//
//	@Autowired
//	private OpsnexttrackmasterJpaRepository opsNextTrackMasterRepo;
//
//
//	public ResponseEntity<AppGeneralResponse> updateCountriesMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		
//		try{
//			//fetching data from mssql
//			List<CountrymasterEntity> countries = (List<CountrymasterEntity>) countriesrepo.findAll();
//			
//			if(countries != null && !countries.isEmpty()){
//				
//				CountryMasterEntity c = null;
//				List<CountryMasterEntity> ucs = new ArrayList<>();
//				
//				//creating mysql record objects by iterating previous countries list
//				for(CountrymasterEntity m : countries){
//					c = new CountryMasterEntity();
//					c.setCountryCode(m.getCountrycode());
//					c.setCountryId(m.getCountryid());
//					c.setCountryName(m.getCountryname());
//					c.setCreatedBy(m.getCreatedby());
//					c.setCreatedOn(new Date());
//					c.setIsActive(m.getIsactive());
//					c.setModifiedBy(m.getModifiedby());
//					c.setModifiedOn(new Date());
//					c.setVersion(0);
//					ucs.add(c);
//				}
//				
//				mcountriesrepo.save(ucs);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e) {
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	public ResponseEntity<AppGeneralResponse> updateItemGroupMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<ItemgroupmasterEntity> itemGroupMasters = (List<ItemgroupmasterEntity>) itemGroupMasterRepo.findAll();
//			
//			if(itemGroupMasters != null && !itemGroupMasters.isEmpty()){
//				
//				ItemGroupMasterEntity igm = null;
//				List<ItemGroupMasterEntity> itemGroupMasterList = new ArrayList<>();
//				
//				for(ItemgroupmasterEntity i : itemGroupMasters){
//					
//					igm = new ItemGroupMasterEntity();
//					
//					igm.setItemCode(i.getItemcode());
//					igm.setItemDesc(i.getItemdesc());
//					igm.setItemGroup(i.getItemgroup());
//					igm.setVersion(0);
//
//					itemGroupMasterList.add(igm);
//				}
//				
//				mItemGroupMasterRepo.save(itemGroupMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//
//	public ResponseEntity<AppGeneralResponse> updateItemMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<ItemmasterEntity> itemMasters = (List<ItemmasterEntity>) itemMasterRepo.findAll();
//			
//			if(itemMasters != null && !itemMasters.isEmpty()){
//				
//				ItemMasterEntity item = null;
//				List<ItemMasterEntity> itemMasterList = new ArrayList<>();
//				
//				for(ItemmasterEntity i : itemMasters){
//					
//					item = new ItemMasterEntity();
//					
//					item.setActive(i.getActive());
//					item.setExpireDays(i.getExpiredays());
//					item.setItemCategory(i.getItemcategory());
//					item.setItemCode(i.getItemcode());
//					item.setItemDesc(i.getItemdesc());
//					item.setLogicalType(i.getLogicaltype());
//					item.setMaxOrderQty(i.getMaxorderqty());
//					item.setMinimumQty(i.getMinimumqty());
//					item.setNoLength(i.getNolength());
//					item.setReorderQty(i.getReorderqty());
//					item.setSetQty(i.getSetqty());
//					item.setStartSeries(i.getStartseries());
//					item.setUseFor(i.getUsefor());
//					item.setVersion(0);
//
//					itemMasterList.add(item);
//				}
//				
//				mItemMasterRepo.save(itemMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	public ResponseEntity<AppGeneralResponse> updateNdrsMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<NdrsmasterEntity> ndrsMasters = (List<NdrsmasterEntity>) ndrsMasterRepo.findAll();
//			
//			if(ndrsMasters != null && !ndrsMasters.isEmpty()){
//				
//				NdrsMasterEntity ndrs = null;
//				List<NdrsMasterEntity> ndrsMasterList = new ArrayList<>();
//				
//				for(NdrsmasterEntity i : ndrsMasters){
//					
//					ndrs = new NdrsMasterEntity();
//					
//					ndrs.setAllowHeldup(i.getAllowheldup());
//					ndrs.setAllowNdr(i.getAllowndr());
//					ndrs.setAllowRto(i.getAllowrto());
//					ndrs.setAutoMailBcc(i.getAutomailbcc());
//					ndrs.setAutoMailCc(i.getAutomailcc());
//					ndrs.setAutoMailTo(i.getAutomailto());
//					ndrs.setDescription(i.getDescription());
//					ndrs.setFailureCategory(i.getFailurecategory());
//					ndrs.setHeldupTillDate(i.getHelduptilldate());
//					ndrs.setIsAutoMail(i.getIsauotmail());
//					ndrs.setIsReattempt(i.getIsreattempt());
//					ndrs.setIsSmsOnRto(i.getIssmsonrto());
//					ndrs.setIsSmsOnHeldup(i.getIssmsonheldup());
//					ndrs.setIsSmsOnNdr(i.getIssmsonndr());
//					ndrs.setIsWoAtmtRto(i.getIswoatmtrto());
//					ndrs.setReasonCode(i.getReasoncode());
//					ndrs.setRtoHold(i.getRtohold());
//					ndrs.setErpId(i.getId());
//					ndrs.setVersion(0);
//
//					ndrsMasterList.add(ndrs);
//				}
//				
//				mNdrsMasterRepo.save(ndrsMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateNdrStatusMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<NdrstatusEntity> ndrStatusMasters = (List<NdrstatusEntity>) ndrStatusMasterRepo.findAll();
//			
//			if(ndrStatusMasters != null && !ndrStatusMasters.isEmpty()){
//				
//				NdrStatusEntity ndrStatus = null;
//				List<NdrStatusEntity> ndrStatusList = new ArrayList<>();
//				
//				for(NdrstatusEntity i : ndrStatusMasters){
//					
//					ndrStatus = new NdrStatusEntity();
//					
//					ndrStatus.setRemarks(i.getRemarks());
//					ndrStatus.setStatus(i.getStatus());
//					ndrStatus.setStCode(i.getStCode().intValue());
//					ndrStatus.setVersion(0);
//
//					ndrStatusList.add(ndrStatus);
//				}
//				
//				mNdrStatusMasterRepo.save(ndrStatusList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsAirlineVehicleMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<OpsairlinevehiclemasterEntity> opsAirlineVehicleMasters = (List<OpsairlinevehiclemasterEntity>) opsAirlineVehicleMasterRepo.findAll();
//			
//			if(opsAirlineVehicleMasters != null && !opsAirlineVehicleMasters.isEmpty()){
//				
//				OpsAirlineVehicleMasterEntity ops = null;
//				List<OpsAirlineVehicleMasterEntity> opsAirlineVehicleMasterList = new ArrayList<>();
//				
//				for(OpsairlinevehiclemasterEntity i : opsAirlineVehicleMasters){
//					
//					ops = new OpsAirlineVehicleMasterEntity();
//					
//					ops.setAirlineVehicleId(i.getAirlinevehicleid());
//					ops.setArrivalTime(i.getArrivaltime());
//					ops.setCreatedBy(i.getCreatedby());
//					ops.setCreatedOn(new Date());
//					ops.setDepartureTime(i.getDeparturetime());
//					ops.setFromCityCode(i.getFromcitycode());
//					ops.setIpAddress(i.getIpaddress());
//					ops.setIsActive(i.getIsactive());
//					ops.setMode(i.getMode());
//					ops.setModeCompany(i.getModecompany());
//					ops.setModifiedBy(i.getModifiedby());
//					ops.setModifiedOn(new Date());
//					ops.setName(i.getName());
//					ops.setOperationInDays(i.getOperationindays());
//					ops.setRemarks(i.getRemarks());
//					ops.setToCityCode(i.getTocitycode());
//					ops.setVersion(0);
//
//					opsAirlineVehicleMasterList.add(ops);
//				}
//				
//				mOpsAirlineVehicleMasterRepo.save(opsAirlineVehicleMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsAirlineVehicleNameMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<OpsairlinevehiclenamemasterEntity> opsAirlineVehicleNameMasters = (List<OpsairlinevehiclenamemasterEntity>) opsAirlineVehicleNameMasterRepo.findAll();
//			
//			if(opsAirlineVehicleNameMasters != null && !opsAirlineVehicleNameMasters.isEmpty()){
//				
//				OpsAirlineVehicleNameMasterEntity opsVehicleName = null;
//				List<OpsAirlineVehicleNameMasterEntity> opsAirlineVehicleNameMasterList = new ArrayList<>();
//				
//				for(OpsairlinevehiclenamemasterEntity i : opsAirlineVehicleNameMasters){
//					
//					opsVehicleName = new OpsAirlineVehicleNameMasterEntity();
//					
//					opsVehicleName.setErpId(i.getId());
//					opsVehicleName.setIsActive(i.getIsactive());
//					opsVehicleName.setMode(i.getMode());
//					opsVehicleName.setName(i.getName());
//					opsVehicleName.setVersion(0);
//
//					opsAirlineVehicleNameMasterList.add(opsVehicleName);
//				}
//				
//				mOpsAirlineVehicleNameMasterRepo.save(opsAirlineVehicleNameMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsColoaderMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<OpscoloadermasterEntity> opsColoaderMasters = (List<OpscoloadermasterEntity>) opsColoaderMasterRepo.findAll();
//			
//			if(opsColoaderMasters != null && !opsColoaderMasters.isEmpty()){
//				
//				OpsColoaderMasterEntity opsColoader = null;
//				List<OpsColoaderMasterEntity> opsColoaderMasterList = new ArrayList<>();
//				
//				for(OpscoloadermasterEntity i : opsColoaderMasters){
//					
//					opsColoader = new OpsColoaderMasterEntity();
//					
//					opsColoader.setCoAddress(i.getCoaddress());
//					opsColoader.setCoCity(i.getCocity());
//					opsColoader.setCoCode(i.getCocode());
//					opsColoader.setCoContactNo(i.getCocontactno());
//					opsColoader.setCoEmail(i.getCoemail());
//					opsColoader.setCoGstNo(i.getCogstno());
//					opsColoader.setColoaderId(i.getColoaderid());
//					opsColoader.setColoaderTypeId(i.getColoadertypeid());
//					opsColoader.setCoName(i.getConame());
//					opsColoader.setCoPanNo(i.getCopanno());
//					opsColoader.setCoPincode(i.getCopincode());
//					opsColoader.setCreatedBy(i.getCreatedby());
//					opsColoader.setCreatedOn(new Date());
//					opsColoader.setIpAddress(i.getIpaddress());
//					opsColoader.setIsActive(i.getIsactive());
//					opsColoader.setIsApi(i.getIsapi());
//					opsColoader.setIsBlackListed(i.getIsblacklisted());
//					opsColoader.setRemarks(i.getRemarks());
//					opsColoader.setModifiedBy(i.getModifiedby());
//					opsColoader.setModifiedOn(new Date());				
//					opsColoader.setVersion(0);
//
//					opsColoaderMasterList.add(opsColoader);
//				}
//				
//				mOpsColoaderMasterRepo.save(opsColoaderMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateGetOpsColoaderTypeMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<OpscoloadertypemasterEntity> opsColoaderTypeMasters = (List<OpscoloadertypemasterEntity>) opsColoaderTypeMasterRepo.findAll();
//			
//			if(opsColoaderTypeMasters != null && !opsColoaderTypeMasters.isEmpty()){
//				
//				OpsColoaderTypeMasterEntity opsColoaderType = null;
//				List<OpsColoaderTypeMasterEntity> opsColoaderTypeMasterList = new ArrayList<>();
//				
//				for(OpscoloadertypemasterEntity i : opsColoaderTypeMasters){
//					
//					opsColoaderType = new OpsColoaderTypeMasterEntity();
//					
//					opsColoaderType.setColoaderTypeId(i.getColoadertypeid());
//					opsColoaderType.setCreatedBy(i.getCreatedby());
//					opsColoaderType.setCreatedOn(new Date());
//					opsColoaderType.setIpAddress(i.getIpaddress());
//					opsColoaderType.setIsActive(i.getIsactive());
//					opsColoaderType.setMode(i.getMode());
//					opsColoaderType.setModifiedBy(i.getModifyeby());
//					opsColoaderType.setModifiedOn(new Date());
//					opsColoaderType.setRemarks(i.getRemarks());
//					opsColoaderType.setTypeName(i.getTypename());
//					opsColoaderType.setVersion(0);
//
//					opsColoaderTypeMasterList.add(opsColoaderType);
//				}
//				
//				mOpsColoaderTypeMasterRepo.save(opsColoaderTypeMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updatePaperworkMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<PaperworkmasterEntity> paperworkMasters = (List<PaperworkmasterEntity>) paperworkMasterRepo.findAll();
//			
//			if(paperworkMasters != null && !paperworkMasters.isEmpty()){
//				
//				PaperWorkMasterEntity paperwork = null;
//				List<PaperWorkMasterEntity> paperworkMasterList = new ArrayList<>();
//				
//				for(PaperworkmasterEntity i : paperworkMasters){
//					
//					paperwork = new PaperWorkMasterEntity();
//					paperwork.setErpId(i.getId());
//					paperwork.setPwName(i.getPwname());
//					paperwork.setVersion(0);
//
//					paperworkMasterList.add(paperwork);
//				}
//				
//				mPaperworkMasterRepo.save(paperworkMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updatePincodeAreaMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<PincodeareamasterEntity> pincodeAreaMasters = (List<PincodeareamasterEntity>) pincodeAreaMasterRepo.findAll();
//			
//			if(pincodeAreaMasters != null && !pincodeAreaMasters.isEmpty()){
//				
//				PincodeAreaMasterEntity pincodeArea = null;
//				List<PincodeAreaMasterEntity> pincodeAreaMasterList = new ArrayList<>();
//				
//				for(PincodeareamasterEntity i : pincodeAreaMasters){
//					
//					pincodeArea = new PincodeAreaMasterEntity();
//					
//					pincodeArea.setAreaName(i.getAreaname());
//					pincodeArea.setControllingOfficeId(i.getControllingofficeid());
//					pincodeArea.setCreatedBy(i.getCreatedby());
//					pincodeArea.setCreatedOn(new Date());
//					pincodeArea.setIsActive(i.getIsactive());
//					pincodeArea.setModifiedBy(i.getModifiedby());
//					pincodeArea.setModifiedOn(new Date());
//					pincodeArea.setPincode(i.getPincode());
//					pincodeArea.setPincodeAreaId(i.getPincodeareaid());
//					pincodeArea.setRemarks(i.getRemarks());	
//					pincodeArea.setVersion(0);
//
//					pincodeAreaMasterList.add(pincodeArea);
//				}
//				
//				mPincodeAreaMasterRepo.save(pincodeAreaMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updatePincodeMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<PincodemasterEntity> pincodeMasters = (List<PincodemasterEntity>) pincodeMasterRepo.findAll();
//			
//			if(pincodeMasters != null && !pincodeMasters.isEmpty()){
//				
//				PincodeMasterEntity pincode = null;
//				List<PincodeMasterEntity> pincodeMasterList = new ArrayList<>();
//				
//				for(PincodemasterEntity i : pincodeMasters){
//					
//					pincode = new PincodeMasterEntity();
//					
//					pincode.setCityId(i.getCityid());
//					pincode.setControllingOfficeId(i.getControllingofficeid());
//					pincode.setCountryId(i.getCountryid());
//					pincode.setCreatedBy(i.getCreatedby());
//					pincode.setCreatedOn(new Date());
//					pincode.setDiplomatic(i.getDiplomatic());
//					pincode.setDox(i.getDox());
//					pincode.setIsActive(i.getIsactive());
//					pincode.setIsServiceable(i.getIsserviceable());
//					pincode.setLaneType(i.getLanetype());
//					pincode.setModifiedBy(i.getModifiedby());
//					pincode.setModifiedOn(new Date());
//					pincode.setNonDox(i.getNondox());
//					pincode.setOda(i.getOda());
//					pincode.setPes(i.getPes());
//					pincode.setPincode(i.getPincode());
//					pincode.setPinId(i.getPinid());
//					pincode.setPinType(i.getPintype());
//					pincode.setPrimeTrack(i.getPrimetrack());
//					pincode.setRemarks(i.getRemarks());
//					pincode.setReversePickup(i.getReversepickup());
//					pincode.setStateExp(i.getStateexp());
//					pincode.setStateId(i.getStateid());
//					pincode.setToPay(i.getTopay());
//					pincode.setVersion(0);
//
//					pincodeMasterList.add(pincode);
//				}
//				
//				mPincodeMasterRepo.save(pincodeMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateStateMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<StatemasterEntity> stateMasters = (List<StatemasterEntity>) stateMasterRepo.findAll();
//			
//			if(stateMasters != null && !stateMasters.isEmpty()){
//				
//				StateMasterEntity state = null;
//				List<StateMasterEntity> stateMasterList = new ArrayList<>();
//				
//				for(StatemasterEntity i : stateMasters){
//					
//					state = new StateMasterEntity();
//					
//					state.setCountryId(i.getCountryid());
//					state.setCreatedBy(i.getCreatedby());
//					state.setCreatedOn(new Date());
//					state.setGstStateCode(i.getGststatecode());
//					state.setModifiedBy(i.getModifiedby());
//					state.setModifiedOn(new Date());
//					state.setStateCode(i.getStatecode());
//					state.setStateId(i.getStateid());
//					state.setStateName(i.getStatename());
//					state.setTransporterId(i.getTransporterid());
//					state.setZoneId(i.getZoneid());
//					state.setVersion(0);
//
//					stateMasterList.add(state);
//				}
//				
//				mStateMasterRepo.save(stateMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateTblRoleMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<TblRolemasterEntity> tblRoleMasters = (List<TblRolemasterEntity>) tblRoleMasterRepo.findAll();
//			
//			if(tblRoleMasters != null && !tblRoleMasters.isEmpty()){
//				
//				TblRoleMasterEntity tblRole = null;
//				List<TblRoleMasterEntity> tblRoleMasterList = new ArrayList<>();
//				
//				for(TblRolemasterEntity i : tblRoleMasters){
//					
//					tblRole = new TblRoleMasterEntity();
//					
//					tblRole.setByPassIp(i.getBypassip());
//					tblRole.setCreatedBy(i.getCreatedby());
//					tblRole.setCreatedOn(new Date());
//					tblRole.setDescription(i.getDescription());
//					tblRole.setIsActive(i.getIsactive());
//					tblRole.setIsDefaultRole(i.getIsdefaultrole());
//					tblRole.setModifiedBy(i.getModifiedby());
//					tblRole.setModifiedOn(new Date());
//					tblRole.setRoleId(i.getRoleid());
//					tblRole.setRoleName(i.getRolename());
//					tblRole.setVersion(0);
//
//					tblRoleMasterList.add(tblRole);
//				}
//				
//				mTblRoleMasterRepo.save(tblRoleMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateTblUserInRoleMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<TblUserinroleEntity> tblUserInRoleMasters = (List<TblUserinroleEntity>) tblUserInRoleMasterRepo.findAll();
//			
//			if(tblUserInRoleMasters != null && !tblUserInRoleMasters.isEmpty()){
//				
//				TblUserInRoleEntity tblUser = null;
//				List<TblUserInRoleEntity> tblUserInRoleMasterList = new ArrayList<>();
//				
//				for(TblUserinroleEntity i : tblUserInRoleMasters){
//					
//					tblUser = new TblUserInRoleEntity();
//					
//					tblUser.setCreatedBy(i.getCreatedby());
//					tblUser.setCreatedOn(new Date());
//					tblUser.setIsActive(i.getIsactive());
//					tblUser.setModifiedBy(i.getModifiedby());
//					tblUser.setModifiedOn(new Date());
//					tblUser.setRoleId(i.getRoleid());
//					tblUser.setUserId(i.getUserid().intValue());
//					tblUser.setUserRoleId(i.getUserroleid());
//					tblUser.setVersion(0);
//
//					tblUserInRoleMasterList.add(tblUser);
//				}
//				
//				mTblUserInRoleMasterRepo.save(tblUserInRoleMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateTblUserMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<TblUsermasterEntity> tblUserMasters = (List<TblUsermasterEntity>) tblUserMasterRepo.findAll();
//			
//			if(tblUserMasters != null && !tblUserMasters.isEmpty()){
//				
//				TblUserMasterEntity tbluser = null;
//				List<TblUserMasterEntity> tblUserMasterList = new ArrayList<>();
//				
//				for(TblUsermasterEntity i : tblUserMasters){
//					
//					tbluser = new TblUserMasterEntity();
//					
//					tbluser.setBranchCode(i.getBranchcode());
//					tbluser.setCity(i.getCity());
//					tbluser.setCreatedBy(i.getCreatedby());
//					tbluser.setCreatedOn(new Date());
//					tbluser.setDrsNo(i.getDrsno() != null ? i.getDrsno().intValue() : null);
//					tbluser.setEmailId(i.getEmailid());
//					tbluser.setErpId(i.getId());
//					tbluser.setIsActive(i.getIsactive());
//					tbluser.setIsCrmUser(i.getIscrmuser());
//					tbluser.setMobileNo(i.getMobileno());
//					tbluser.setModifyBy(i.getModifyby());	
//					tbluser.setModifyOn(new Date());
//					tbluser.setPassword(i.getPassword());
//					tbluser.setPhone(i.getPhone());
//					tbluser.setPincode(i.getPincode());
//					tbluser.setRoCode(i.getRocode());
//					tbluser.setRoleId(i.getRoleid());
//					tbluser.setState(i.getState());
//					tbluser.setUserFullName(i.getUserfullname());
//					tbluser.setUserId(i.getUserid());
//					tbluser.setUserRole(i.getUserrole());
//					tbluser.setUserTypeId(i.getUsertypeid());
//					tbluser.setVersion(0);
//
//					tblUserMasterList.add(tbluser);
//				}
//				
//				mTblUserMasterRepo.save(tblUserMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateVendorMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<VendormasterEntity> vendorMasters = (List<VendormasterEntity>) vendorMasterRepo.findAll();
//			
//			if(vendorMasters != null && !vendorMasters.isEmpty()){
//				
//				VendorMasterEntity vendor = null;
//				List<VendorMasterEntity> vendorMasterList = new ArrayList<>();
//				
//				for(VendormasterEntity i : vendorMasters){
//					
//					vendor = new VendorMasterEntity();
//					
//					vendor.setIsActive(i.getIsactive());
//					vendor.setRemarks(i.getRemarks());
//					vendor.setVendorCode(i.getVendorcode());
//					vendor.setVendorName(i.getVendorname());
//					vendor.setVersion(0);
//
//					vendorMasterList.add(vendor);
//				}
//				
//				mVendorMasterRepo.save(vendorMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	
//	public ResponseEntity<AppGeneralResponse> updateCityMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<CitymasterEntity> cityMasters = (List<CitymasterEntity>) cityMasterRepo.findAll();
//			
//			if(cityMasters != null && !cityMasters.isEmpty()){
//				
//				CityMasterEntity city = null;
//				List<CityMasterEntity> cityMastersList = new ArrayList<>();
//				
//				for(CitymasterEntity i : cityMasters){
//					
//					city = new CityMasterEntity();
//					city.setAdditionalTat(i.getAdditionaltat());
//					city.setCategory(i.getCategory());
//					city.setCityCode(i.getCitycode());
//					city.setCityId(i.getCityid());
//					city.setCityName(i.getCityname());
//					city.setCreatedBy(i.getCreatedby());
//					city.setCreatedOn(new Date());
//					city.setIsActive(i.getIsactive());
//					city.setMasterCode(i.getMastercode());
//					city.setModifiedBy(i.getModifiedby());
//					city.setModifiedOn(new Date());
//					city.setRoCode(i.getRocode());
//					city.setStateId(i.getStateid());		
//					city.setVersion(0);
//
//					cityMastersList.add(city);
//				}
//				
//				mCityMasterRepo.save(cityMastersList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	public ResponseEntity<AppGeneralResponse> updateContentMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			List<ContentmasterEntity> contentMasters = (List<ContentmasterEntity>) contentMasterRepo.findAll();
//			
//			if(contentMasters != null && !contentMasters.isEmpty()){
//				
//				ContentMasterEntity content = null;
//				List<ContentMasterEntity> contentMasterList = new ArrayList<>();
//				
//				for(ContentmasterEntity i : contentMasters){
//					
//					content = new ContentMasterEntity();
//					
//					content.setErpId(i.getId());
//					content.setSkuCategory(i.getSkucategory());
//					content.setSkuCode(i.getSkucode());
//					content.setSkuDescription(i.getSkudescription());
//					content.setVersion(0);
//
//					contentMasterList.add(content);
//				}
//				
//				mContentMasterRepo.save(contentMasterList);
//				
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//			}
//			else{
//				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//			}
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//
//	public ResponseEntity<AppGeneralResponse> updateCustomerMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<CustomermasterEntity> customerMasters = (List<CustomermasterEntity>) customerMasterRepo.findAll();
//		
//		if(customerMasters != null && !customerMasters.isEmpty()){
//			
//			CustomerMasterEntity customer = null;
//			List<CustomerMasterEntity> customerMastersList = new ArrayList<>();
//			
//			for(CustomermasterEntity i : customerMasters){
//				
//				customer = new CustomerMasterEntity();
//				customer.setStateCode(i.getStatecode());
//				customer.setAccMgrCode(i.getAccmgrcode());
//				customer.setAddress1(i.getAddress1());
//				customer.setAddress2(i.getAddress2());
//				customer.setAllowNoService(i.getAllownoservice());
//				customer.setAutoMailer(i.getAutomailer());
//				customer.setBillBy(i.getBillby());
//				customer.setBillingCity(i.getBillingcity());
//				customer.setBillingAddress1(i.getBillingaddress1());
//				customer.setBillingAddress2(i.getBillingaddress2());
//				customer.setBillingCycle(i.getBillingcycle());
//				customer.setBillingEmail(i.getBillingemail());
//				customer.setBillingEmailCc(i.getBillingemailcc());
//				customer.setBillingMobile(i.getBillingmobile());
//				customer.setBillingName(i.getBillingname());
//				customer.setBillingPhone(i.getBillingphone());
//				customer.setBillingPincode(i.getBillingpincode());
//				customer.setBillingState(i.getBillingstate());
//				customer.setBillingType(i.getBillingtype());
//				customer.setBookingSms(i.getBookingsms());
//				customer.setByHub(i.getByhub());
//				customer.setByHubName(i.getByhubname());
//				customer.setCity(i.getCity());
//				customer.setContactPerson(i.getContactperson());
//				customer.setCorpCustomer(i.getCorpcustomer());
//				customer.setCreatedBy(i.getCreatedby());
//				customer.setCreatedOn(new Date());
//				customer.setCreditLimit(i.getCreditlimit().intValue());
//				customer.setCreditPeriod(i.getCreditperiod());
//				customer.setCrmCode(i.getCrmcode());
//				customer.setCustCode(i.getCustcode());
//				customer.setCustomerName(i.getCustomername());
//				customer.setCustomerType(i.getCustomertype());
//				customer.setCustType(i.getCusttype());
//				customer.setDestinationCode(i.getDestinationcode());
//				customer.setDiscount(i.getDiscount());
//				customer.setDownloadType(i.getDownloadtype());
//				customer.setDrsUpdateSms(i.getDrsupdatesms());
//				customer.setEmail(i.getEmail());
//				customer.setEmailBcc(i.getEmailbcc());
//				customer.setEmailCc(i.getEmailcc());
//				customer.setEntryDate(i.getEntrydate());
//				customer.setErpId(i.getId());
//				customer.setExpireDate(i.getExpiredate());
//				customer.setFrCode(i.getFrcode());
//				customer.setFuelCharge(i.getFulecharge().doubleValue());
//				customer.setGstIn(i.getGstin());
//				customer.setIndustryType(i.getIndustrytype());
//				customer.setInsurance(i.getInsurance().doubleValue());
//				customer.setIsActive(i.getIsactive());
//				customer.setIsReversePickup(i.getIsreversepickup());
//				customer.setIsRtoAllow(i.getIsrtoallow());
//				customer.setIsVirtualSeries(i.getIsvirtualseries());
//				customer.setLoginPassword(i.getLoginpassword());
//				customer.setMacAddress(i.getMacaddress());
//				customer.setMasterCode(i.getMastercode());
//				customer.setMobile(i.getMobile());
//				customer.setModifyBy(i.getModifyby());
//				customer.setModifyOn(new Date());
//				customer.setNonDelivered(i.getNondelivered());
//				customer.setOfdSms(i.getOfdsms());
//				customer.setOldCustCode(i.getOldcustcode());
//				customer.setOperationName(i.getOperationname());
//				customer.setPanNo(i.getPanno());
//				customer.setPercentageDiscount(i.getPercentagediscount().doubleValue());
//				customer.setPhone(i.getPhone());
//				customer.setPincode(i.getPincode());
//				customer.setRateCalculation(i.getRatecalculation());
//				customer.setRemarks(i.getRemarks());
//				customer.setRoCode(i.getRocode());
//				customer.setSalesTax(i.getSalestax().doubleValue());
//				customer.setServiceTax(i.getServicetax());
//				customer.setState(i.getState());
//				customer.setTallyCode(i.getTallycode());
//				customer.setTatAdjust(i.getTatadjust().intValue());
//				
//				customer.setVersion(0);
//
//				customerMastersList.add(customer);
//			}
//			
//			mCustomerMasterRepo.save(customerMastersList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateDesignationMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<DesiginationmasterEntity> designationMasters = (List<DesiginationmasterEntity>) designationMasterRepo.findAll();
//		
//		if(designationMasters != null && !designationMasters.isEmpty()){
//			
//			DesignationMasterEntity designation = null;
//			List<DesignationMasterEntity> designationMasterList = new ArrayList<>();
//			
//			for(DesiginationmasterEntity i : designationMasters){
//				
//				designation = new DesignationMasterEntity();
//				
//				designation.setCreatedBy(i.getCreatedby());
//				designation.setCreatedOn(new Date());
//				designation.setDesignationId(i.getDesiginationid());
//				designation.setDesignationName(i.getDesiginationname());
//				designation.setIsActive(i.getIsactive());
//				designation.setModifiedBy(i.getModifiedby());
//				designation.setModifiedOn(new Date());
//				designation.setVersion(0);
//
//				designationMasterList.add(designation);
//			}
//			
//			mDesignationMasterRepo.save(designationMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateIssueDetailsMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<IssuedetailsEntity> issueDetailsMasters = (List<IssuedetailsEntity>) issueDetailsMasterRepo.findAll();
//		
//		if(issueDetailsMasters != null && !issueDetailsMasters.isEmpty()){
//			
//			IssueDetailsEntity issueDetail = null;
//			List<IssueDetailsEntity> issueDetailMasterList = new ArrayList<>();
//			
//			for(IssuedetailsEntity i : issueDetailsMasters){
//				
//				issueDetail = new IssueDetailsEntity();
//				
//				issueDetail.setIssueDetailId(i.getIssuedetailId());
//				issueDetail.setIssueDetails(i.getIssueDetails());
//				issueDetail.setIssueId(i.getIssueId());
//				issueDetail.setPriority(i.getPriotity());
//				issueDetail.setVersion(0);
//
//				issueDetailMasterList.add(issueDetail);
//			}
//			
//			mIssueDetailsMasterRepo.save(issueDetailMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateIssueTypeMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<IssuetypeEntity> issueTypeMasters = (List<IssuetypeEntity>) issueTypeMasterRepo.findAll();
//		
//		if(issueTypeMasters != null && !issueTypeMasters.isEmpty()){
//			
//			IssueTypeEntity issueType = null;
//			List<IssueTypeEntity> issueTypeMasterList = new ArrayList<>();
//			
//			for(IssuetypeEntity i : issueTypeMasters){
//				
//				issueType = new IssueTypeEntity();
//				
//				issueType.setIssueId(i.getIssueId());
//				issueType.setIssueType(i.getIssueType());
//				issueType.setVersion(0);
//
//				issueTypeMasterList.add(issueType);
//			}
//			
//			mIssueTypeMasterRepo.save(issueTypeMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateMtBranchMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<MtbranchEntity> mtBranchMasters = (List<MtbranchEntity>) mtBranchMasterRepo.findAll();
//		
//		if(mtBranchMasters != null && !mtBranchMasters.isEmpty()){
//			
//			MtBranchEntity mtBranch = null;
//			List<MtBranchEntity> mtBranchMasterList = new ArrayList<>();
//			
//			for(MtbranchEntity i : mtBranchMasters){
//				
//				mtBranch = new MtBranchEntity();
//				
//				mtBranch.setAccManagerCode(i.getAccmanagercode());
//				mtBranch.setAddress1(i.getAddress1());
//				mtBranch.setAddress2(i.getAddress2());
//				mtBranch.setAllowBag(i.getAllowbag().floatValue());
//				mtBranch.setAllowBook(i.getAllowbook().floatValue());
//				mtBranch.setAllowConnectCServer(i.getAllowconnectcserver().floatValue());
//				mtBranch.setAllowDr(i.getAllowdr().floatValue());
//				mtBranch.setAllowLineHaul(i.getAllowlinehaul().floatValue());
//				mtBranch.setAllowVehicle(i.getAllowvehicle().floatValue());
//				mtBranch.setAllowZone(i.getAllowzone().floatValue());
//				mtBranch.setApexCodeSf(i.getApexcodesf());
//				mtBranch.setAreaCategory(i.getAreacategory());
//				mtBranch.setBillingName(i.getBillingname());
//				mtBranch.setBrConnectionOffice(i.getBrconnectionoffice());
//				mtBranch.setCity(i.getCity());
//				mtBranch.setContactPerson(i.getContactperson());
//				mtBranch.setContractEndDate(i.getContractenddate());
//				mtBranch.setContractStartDate(i.getContractstartdate());
//				mtBranch.setCreatedDate(new Date());
//				mtBranch.setCrmCode(i.getCrmcode());
//				mtBranch.setDelvChargeType(i.getDelvchargetype());
//				mtBranch.setDelvChargeValue(i.getDelvchargevalue());
//				mtBranch.setDiscountType(i.getDiscounttype());
//				mtBranch.setDiscountValue(i.getDiscountvalue());
//				mtBranch.setDisplayName(i.getDisplayname());
//				mtBranch.setDownloadStatus(i.getDownloadstatus().floatValue());
//				mtBranch.setDrCutoff1(i.getDrcutoff1());
//				mtBranch.setDrCutoff2(i.getDrcutoff2());
//				mtBranch.setEmailCc(i.getEmailcc());
//				mtBranch.setEmailTo(i.getEmailto());
//				mtBranch.setFrCode(i.getFrcode());
//				mtBranch.setFuelCharge(i.getFuelcharge());
//				mtBranch.setGstIn(i.getGstin());
//				mtBranch.setIsActivatedLoad(i.getIsactivatedload().floatValue());
//				mtBranch.setIsActivatedManifest(i.getIsactivatedmanifest().floatValue());
//				mtBranch.setIsBilling(i.getIsbilling());
//				mtBranch.setIsShipperIssue(i.getIsshipperissue().floatValue());
//				mtBranch.setIsVirtualAwb(i.getIsvirtualawb().floatValue());
//				mtBranch.setMobile(i.getMobile().floatValue());
//				mtBranch.setOfficeCategory(i.getOfficecategory());
//				mtBranch.setOfficeCode(i.getOfficecode());
//				mtBranch.setOfficeName(i.getOfficename());
//				mtBranch.setOfficeOwner(i.getOfficeowner());
//				mtBranch.setOfficeTypeCode(i.getOfficetypecode());
//				mtBranch.setOpsBased(i.getOpsbased());
//				mtBranch.setOpsroCode(i.getOpsrocode());
//				mtBranch.setOtcCharge(i.getOtccharge());
//				mtBranch.setOtcPaid(i.getOtcpaid());
//				mtBranch.setPanNo(i.getPanno());
//				mtBranch.setPhone(i.getPhone().floatValue());
//				mtBranch.setPincode(i.getPincode().floatValue());
//				mtBranch.setRateType(i.getRatetype());
//				mtBranch.setRateZone(i.getRatezone());
//				mtBranch.setReportingZone(i.getReportingzone());
//				mtBranch.setSelectedOfficeType(i.getSelectedofficetype());
//				mtBranch.setServiceablePincode(i.getServiceablepincode());
//				mtBranch.setServiceTax(i.getServicetax());
//				mtBranch.setServiceType(i.getServicetype());
//				mtBranch.setState(i.getState());
//				mtBranch.setTallyCode(i.getTallycode());
////				mtBranch.setApexCodeAir(i.getApexcodeair());
//				mtBranch.setVersion(0);
//
//				mtBranchMasterList.add(mtBranch);
//			}
//			
//			mMtBranchMasterRepo.save(mtBranchMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateOfficeMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OfficemasterEntity> officeMasters = (List<OfficemasterEntity>) officeMasterRepo.findAll();
//		
//		if(officeMasters != null && !officeMasters.isEmpty()){
//			
//			OfficeMasterEntity office = null;
//			List<OfficeMasterEntity> officeMasterList = new ArrayList<>();
//			
//			for(OfficemasterEntity i : officeMasters){
//				
//				office = new OfficeMasterEntity();
//				
//				office.setAccManagerCode(i.getAccmanagercode());
//				office.setAddress1(i.getAddress1());
//				office.setAddress2(i.getAddress2());
//				office.setAllowBag(i.getAllowbag());
//				office.setAllowBook(i.getAllowbook());
//				office.setAllowConnectCServer(i.getAllowconnectcserver());
//				office.setAllowDr(i.getAllowdr());
//				office.setAllowIssueMile(i.getAllowissuemile());
//				office.setAllowLineHaul(i.getAllowlinehaul());
//				office.setAllowShipperForBilling(i.getAllowshipperforbilling());
//				office.setAllowShipperIssue(i.getAllowshiperissue());
//				office.setAllowVehicle(i.getAllowvehicle());
//				office.setAllowZone(i.getAllowzone());
//				office.setApexCode(i.getApexcode());
//				office.setApexCodeAir(i.getApexcodeair());
//				office.setApexCodeSf(i.getApexcodesf());
//				office.setAreaCategory(i.getAreacategory());
//				office.setBillAddress1(i.getBilladdress1());
//				office.setBillAddress2(i.getBilladdress2());
//				office.setBillCity(i.getBillcity());
//				office.setBillContactPerson(i.getBillcontactperson());
//				office.setBillCreditLimit(i.getBillcreditlimit());
//				office.setBillEmailCc(i.getEmailcc());
//				office.setBillEmailTo(i.getEmailto());
//				office.setBillingCycle(i.getBillingcycle());
//				office.setBillingName(i.getBillingname());
//				office.setBillMobile(i.getBillmobile().intValue());
//				office.setBillPhone(i.getBillphone().intValue());
//				office.setBillPincode(i.getBillpincode().intValue());
//				office.setBillStateId(i.getStateid());
//				office.setBillTallyCode(i.getBilltallycode());
//				office.setBlockDate(i.getBlockdate());
//				office.setBlockedBy(i.getBlockedby());
//				office.setBlockType(i.getBlocktype());
//				office.setBrConnectionOffice(i.getBrconnectionoffice());
//				office.setCity(i.getCity());
//				office.setConnectiongApex(i.getConnectingapex());
//				office.setContactPerson(i.getContactperson());
//				office.setContractEndDate(i.getContractenddate());
//				office.setContractStartDate(i.getContractstartdate());
//				office.setCreatedBy(i.getCreatedby());
//				office.setCreatedDate(i.getCreateddate());
//				office.setCreatedOn(new Date());
//				office.setCrmCode(i.getCrmcode());
//				office.setDeactivatedBy(i.getDeactivatedby());
//				office.setDeactivatedOn(i.getDeactivatedon());
//				office.setDelvChargeType(i.getDelvchargetype());
//				office.setDelvChargeValue(i.getDelvchargevalue() != null ? i.getDelvchargevalue().doubleValue() : null);
//				office.setDevlpCharge(i.getDevlpcharge() != null ? i.getDevlpcharge().doubleValue() : null);
//				office.setDiscountType(i.getDiscounttype());
//				office.setDiscountValue(i.getDiscountvalue() != null ? i.getDiscountvalue().doubleValue() : null);
//				office.setDisplayName(i.getDisplayname());
//				office.setDownloadStatus(i.getDownloadstatus());
//				office.setDownloadType(i.getDownloadtype());
//				office.setDrCutoff1(i.getDrcutoff1());
//				office.setDrCutoff2(i.getDrcutoff2());
//				office.setDrsUpdateSms(i.getDrsupdatesms());
//				office.setEmailCc(i.getEmailcc());
//				office.setEmailTo(i.getEmailto());
//				office.setFrCode(i.getFrcode());
//				office.setFuelCharge(i.getFuelcharge() != null ? i.getFuelcharge().doubleValue() : null);
//				office.setGstIn(i.getGstin());
//				office.setGstStateCode(i.getGststatecode());
//				office.setIsActivatedLoad(i.getIsactivatedload());
//				office.setIpAddress(i.getIpaddress());
//				office.setIsActivatedManifest(i.getIsactivatedmanifest());
//				office.setIsActive(i.getIsactive());
//				office.setIsAuthorizedFr(i.getIsauthorizedfr());
//				office.setIsAutoMailer(i.getIsautomailer());
//				office.setIsBilling(i.getIsbilling());
//				office.setIsMapDisplay(i.getIsmapdisplay());
//				office.setIsMpls(i.getIsmpls());
//				office.setIsVirtualAwb(i.getIsvirtualawb());
//				office.setLocLat(i.getLoclat());
//				office.setLocLong(i.getLoclong());
//				office.setMobile(i.getMobile() != null ? i.getMobile().intValue() : null);
//				office.setModifiedBy(i.getModifyedby());
//				office.setModifiedOn(new Date());
//				office.setOfdsms(i.getOfdsms());
//				office.setOfficeCategory(i.getOfficecategory());
//				office.setOfficeCode(i.getOfficecode());
//				office.setOfficeId(i.getOfficeid());
//				office.setOfficeName(i.getOfficename());
//				office.setOfficeOwner(i.getOfficeowner());
//				office.setOfficeTypeId(i.getOfficetypeid());
//				office.setOldOfficeCode(i.getOldofficecode());
//				office.setOpsBased(i.getOpsbased());
//				office.setOpsroCode(i.getOpsrocode());
//				office.setOtcPaid(i.getOtcpaid());
//				office.setOtcCharge(i.getOtccharge() != null ? i.getOtccharge().doubleValue() : null);
//				office.setOwnerMobile(i.getOwnermobile() != null ? i.getOwnermobile().intValue() : null);
//				office.setPanNo(i.getPanno());
//				office.setPassword(i.getPassword());
//				office.setPhone(i.getPhone() != null ? i.getPhone().intValue() : null);
//				office.setPincode(i.getPincode() != null ? i.getPincode().intValue() : null);
//				office.setPrimeInvoiceAreaCd(i.getPrimeinvoiceareacd());
//				office.setProductType(i.getProducttype());
//				office.setRateZone(i.getRatezone());
//				office.setRateCalcType(i.getRatecalctype());
//				office.setRateType(i.getRatetype());
//				office.setRemarks(i.getRemarks());
//				office.setReportingZone(i.getReportingzone());
//				office.setSelectedOfficeType(i.getSelectedofficetype());
//				office.setServiceTax(i.getServicetax());
//				office.setServiceType(i.getServicetype());
//				office.setShipperRateGroup(i.getShipperrategroup());
//				office.setStateId(i.getStateid());
//				office.setVersion(0);
//
//				officeMasterList.add(office);
//			}
//			
//			mOfficeMasterRepo.save(officeMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updatePrimeCountryMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<PrimecountrymasterEntity> primeCountryMasters = (List<PrimecountrymasterEntity>) primeCountryMasterRepo.findAll();
//		
//		if(primeCountryMasters != null && !primeCountryMasters.isEmpty()){
//			
//			PrimeCountryMasterEntity primeCountry = null;
//			List<PrimeCountryMasterEntity> primeCountryMasterList = new ArrayList<>();
//			
//			for(PrimecountrymasterEntity i : primeCountryMasters){
//				
//				primeCountry = new PrimeCountryMasterEntity();
//				
//				primeCountry.setCountryCd(i.getCountryCd());
//				primeCountry.setCountryDesc(i.getCountryDesc());
//				primeCountry.setErpId(i.getId() != null ? i.getId().floatValue() : null);
//				primeCountry.setStatus(i.getStatus() != null ? i.getStatus().floatValue() : null);
//				primeCountry.setVersion(0);
//
//				primeCountryMasterList.add(primeCountry);
//			}
//			
//			mPrimeCountryMasterRepo.save(primeCountryMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateTblRoMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<TblRomasterEntity> tblRoMasters = (List<TblRomasterEntity>) tblRoMasterRepo.findAll();
//		
//		if(tblRoMasters != null && !tblRoMasters.isEmpty()){
//			
//			TblRoMasterEntity tblro = null;
//			List<TblRoMasterEntity> tblroMasterList = new ArrayList<>();
//			
//			for(TblRomasterEntity i : tblRoMasters){
//				
//				tblro = new TblRoMasterEntity();
//				
//				tblro.setCreatedBy(i.getCreatedby());
//				tblro.setCreatedOn(new Date());
//				tblro.setCurrentPending(i.getCurrentpending());
//				tblro.setErpId(i.getId());
//				tblro.setLastPending(i.getLastpending());
//				tblro.setModifyBy(i.getModifyby());
//				tblro.setModifyOn(new Date());
//				tblro.setRo(i.getRo());
//				tblro.setSecondLastPending(i.getSecondlastpending());
//				tblro.setVersion(0);
//
//				tblroMasterList.add(tblro);
//			}
//			
//			mTblRoMasterRepo.save(tblroMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateOfficeTypeMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OfficetypemasterEntity> officeTypeMasters = (List<OfficetypemasterEntity>) officeTypeMasterRepo.findAll();
//		
//		if(officeTypeMasters != null && !officeTypeMasters.isEmpty()){
//			
//			OfficeTypeMasterEntity officeType = null;
//			List<OfficeTypeMasterEntity> officeTypeMasterList = new ArrayList<>();
//			
//			for(OfficetypemasterEntity i : officeTypeMasters){
//				
//				officeType = new OfficeTypeMasterEntity();
//				
//				officeType.setCreatedBy(i.getCreatedby());
//				officeType.setCreatedOn(new Date());
//				officeType.setIsActive(i.getIsactive());
//				officeType.setIsSelfOffice(i.getIsselfoffice());
//				officeType.setModifiedBy(i.getModifiedby());
//				officeType.setModifiedOn(new Date());
//				officeType.setOfficeDescription(i.getOfficedescription());
//				officeType.setOfficeTypeCode(i.getOfficetypecode());
//				officeType.setOfficeTypeId(i.getOfficetypeid());		
//				officeType.setVersion(0);
//
//				officeTypeMasterList.add(officeType);
//			}
//			
//			mOfficeTypeMasterRepo.save(officeTypeMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateTblDelvBoyMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<TbldelvboymasterEntity> tblDelvBoyMasters = (List<TbldelvboymasterEntity>) tblDelvBoyMasterRepo.findAll();
//		
//		if(tblDelvBoyMasters != null && !tblDelvBoyMasters.isEmpty()){
//			
//			TblDelvBoyMasterEntity tblDelvBoy = null;
//			List<TblDelvBoyMasterEntity> tblDelvBoyMasterList = new ArrayList<>();
//			
//			for(TbldelvboymasterEntity i : tblDelvBoyMasters){
//				
//				tblDelvBoy = new TblDelvBoyMasterEntity();
//				
//				tblDelvBoy.setAccessType(i.getAccesstype());
//				tblDelvBoy.setCompany(i.getCompany());
//				tblDelvBoy.setCreatedBy(i.getCreatedby());
//				tblDelvBoy.setCreatedOn(new Date());
//				tblDelvBoy.setEmailId(i.getEmailid());
//				tblDelvBoy.setErpId(i.getId());
//				tblDelvBoy.setFullName(i.getFullname());
//				tblDelvBoy.setIsActive(i.getIsactive());
//				tblDelvBoy.setMobileImeiNo(i.getMobileimeino());
//				tblDelvBoy.setMobileNo(i.getMobileno());
//				tblDelvBoy.setModifyBy(i.getModifyby());
//				tblDelvBoy.setModifyOn(new Date());
//				tblDelvBoy.setOfficeCode(i.getOfficecode());
//				tblDelvBoy.setPass(i.getPass());
//				tblDelvBoy.setRo(i.getRo());
//				tblDelvBoy.setUserId(i.getUserid());
//				tblDelvBoy.setUserType(i.getUsertype());
//				tblDelvBoy.setDelvBoyCode(i.getDelvboycode());
//				tblDelvBoy.setVersion(0);
//
//				tblDelvBoyMasterList.add(tblDelvBoy);
//			}
//			
//			mTblDelvBoyMasterRepo.save(tblDelvBoyMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateTblAreaMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<TblareamasterEntity> tblAreaMasters = (List<TblareamasterEntity>) tblAreaMasterRepo.findAll();
//		
//		if(tblAreaMasters != null && !tblAreaMasters.isEmpty()){
//			
//			TblAreaMasterEntity tblArea = null;
//			List<TblAreaMasterEntity> tblAreaMasterList = new ArrayList<>();
//			
//			for(TblareamasterEntity i : tblAreaMasters){
//				
//				tblArea = new TblAreaMasterEntity();
//				
//				tblArea.setAreaCreatedDate(i.getAreacreateddate());
//				tblArea.setAreaName(i.getAreaname());
//				tblArea.setAreaRouteName(i.getArearoutename());
//				tblArea.setControllingOffice(i.getControllingoffice());
//				tblArea.setCreatedBy(i.getCreatedby());
//				tblArea.setCreatedOn(new Date());
//				tblArea.setErpId(i.getId());
//				tblArea.setIsActive(i.getIsactive());
//				tblArea.setModifiedBy(i.getModifiedby());
//				tblArea.setModifiedOn(new Date());
//				tblArea.setPincode(i.getPincode());
//				tblArea.setRemarks(i.getRemarks());
//				tblArea.setVersion(0);
//
//				tblAreaMasterList.add(tblArea);
//			}
//			
//			mTblAreaMasterRepo.save(tblAreaMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//
//	public ResponseEntity<AppGeneralResponse> updateZoneMaster(){
//	
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<ZonemasterEntity> zoneMasters = (List<ZonemasterEntity>) zoneMasterRepo.findAll();
//		
//		if(zoneMasters != null && !zoneMasters.isEmpty()){
//			
//			ZoneMasterEntity zone = null;
//			List<ZoneMasterEntity> zoneMasterList = new ArrayList<>();
//			
//			for(ZonemasterEntity i : zoneMasters){
//				
//				zone = new ZoneMasterEntity();
//				
//				zone.setCreatedBy(i.getCreatedby());
//				zone.setCreatedOn(new Date());
//				zone.setModifiedBy(i.getModifiedby());
//				zone.setModifiedOn(new Date());
//				zone.setRoCity(i.getRocity());
//				zone.setRoCode(i.getRocode());
//				zone.setZoneCode(i.getZonecode());
//				zone.setZoneId(i.getZoneid());
//				zone.setZoneName(i.getZonename());
//				zone.setZoneTypeId(i.getZonetypeid());
//				zone.setVersion(0);
//
//				zoneMasterList.add(zone);
//			}
//			
//			mZoneMasterRepo.save(zoneMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsModeMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OpsmodemasterEntity> opsModeMasters = (List<OpsmodemasterEntity>) opsModeMasterRepo.findAll();
//		
//		if(opsModeMasters != null && !opsModeMasters.isEmpty()){
//			
//			OpsModeMasterEntity opsMode = null;
//			List<OpsModeMasterEntity> opsModeMasterList = new ArrayList<>();
//			
//			for(OpsmodemasterEntity i : opsModeMasters){
//				
//				opsMode = new OpsModeMasterEntity();
//				
//				opsMode.setCreatedBy(i.getCreatedby());
//				opsMode.setCreatedOn(new Date());
//				opsMode.setIsActive(i.getIsactive());
//				opsMode.setModeDescription(i.getModedescription());
//				opsMode.setModeId(i.getModeid());
//				opsMode.setModeName(i.getModename());
//				opsMode.setModifiedBy(i.getModifiedby());
//				opsMode.setModifiedOn(new Date());
//				opsMode.setVersion(0);
//
//				opsModeMasterList.add(opsMode);
//			}
//			
//			mOpsModeMasterRepo.save(opsModeMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsHubCutoffMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OpshubcutoffmasterEntity> opsHubCutoffMasters = (List<OpshubcutoffmasterEntity>) opsHubCutoffMasterRepo.findAll();
//		
//		if(opsHubCutoffMasters != null && !opsHubCutoffMasters.isEmpty()){
//			
//			OpsHubCutoffMasterEntity opsHubCutoff = null;
//			List<OpsHubCutoffMasterEntity> opsHubCutoffMasterList = new ArrayList<>();
//			
//			for(OpshubcutoffmasterEntity i : opsHubCutoffMasters){
//				
//				opsHubCutoff = new OpsHubCutoffMasterEntity();
//				
//				opsHubCutoff.setCdInTime(i.getCdintime());
//				opsHubCutoff.setCdOutTime(i.getCdouttime());
//				opsHubCutoff.setCreatedBy(i.getCreatedby());
//				opsHubCutoff.setCreatedOn(new Date());
//				opsHubCutoff.setCutoffId(i.getCutoffid());
//				opsHubCutoff.setDoxCutoffBk(i.getDoxcutoffbk());
//				opsHubCutoff.setDoxCutoffDlv(i.getDoxcutoffdlv());
//				opsHubCutoff.setIpAddress(i.getIpaddress());
//				opsHubCutoff.setIsActive(i.getIsactive());
//				opsHubCutoff.setModifiedBy(i.getModifiedby());
//				opsHubCutoff.setModifiedOn(new Date());
//				opsHubCutoff.setNonDoxCutoffBk(i.getNondoxcutoffbk());
//				opsHubCutoff.setNonDoxCutoffDlv(i.getNondoxcutoffdlv());
//				opsHubCutoff.setOfficeId(i.getOfficeid());
//				opsHubCutoff.setProductType(i.getProducttype());
//				opsHubCutoff.setRemarks(i.getRemarks());
//				opsHubCutoff.setVersion(0);
//
//				opsHubCutoffMasterList.add(opsHubCutoff);
//			}
//			
//			mOpsHubCutoffMasterRepo.save(opsHubCutoffMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsNextTrackMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OpsnexttrackmasterEntity> opsNextTrackMasters = (List<OpsnexttrackmasterEntity>) opsNextTrackMasterRepo.findAll();
//		
//		if(opsNextTrackMasters != null && !opsNextTrackMasters.isEmpty()){
//			
//			OpsNextTrackMasterEntity opsNextTrack = null;
//			List<OpsNextTrackMasterEntity> opsNextTrackMasterList = new ArrayList<>();
//			
//			for(OpsnexttrackmasterEntity i : opsNextTrackMasters){
//				
//				opsNextTrack = new OpsNextTrackMasterEntity();
//				
//				opsNextTrack.setCrmLastSync(i.getCrmlastsync());
//				opsNextTrack.setFunctionalArea(i.getFunctionalarea());
//				opsNextTrack.setLastSyncDate(i.getLastsyncdate());
//				opsNextTrack.setTableName(i.getTablename());
//				opsNextTrack.setTrackId(i.getTrackid());
//				opsNextTrack.setVersion(0);
//
//				opsNextTrackMasterList.add(opsNextTrack);
//			}
//			
//			mOpsNextTrackMasterRepo.save(opsNextTrackMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateOpsStatusMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<OpsstatusmasterEntity> opsStatusMasters = (List<OpsstatusmasterEntity>) opsStatusMasterRepo.findAll();
//		
//		if(opsStatusMasters != null && !opsStatusMasters.isEmpty()){
//			
//			OpsStatusMasterEntity opsStatus = null;
//			List<OpsStatusMasterEntity> opsStatusMasterList = new ArrayList<>();
//			
//			for(OpsstatusmasterEntity i : opsStatusMasters){
//				
//				opsStatus = new OpsStatusMasterEntity();
//				
//				opsStatus.setCreatedOn(new Date());
//				opsStatus.setErpId(i.getId());
//				opsStatus.setNumericCode(i.getNumericcode());
//				opsStatus.setStatusCode(i.getStatuscode());
//				opsStatus.setStatusText(i.getStatustext());
//				opsStatus.setVersion(0);
//
//				opsStatusMasterList.add(opsStatus);
//			}
//			
//			mOpsStatusMasterRepo.save(opsStatusMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateReleaseMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<ReleasemasterEntity> releaseMasters = (List<ReleasemasterEntity>) releaseMasterRepo.findAll();
//		
//		if(releaseMasters != null && !releaseMasters.isEmpty()){
//			
//			ReleaseMasterEntity release = null;
//			List<ReleaseMasterEntity> releaseMasterList = new ArrayList<>();
//			
//			for(ReleasemasterEntity i : releaseMasters){
//				
//				release = new ReleaseMasterEntity();
//				
//				release.setActiveStatus(i.getActivestatus());
//				release.setCategory(i.getCategory());
//				release.setCode(i.getCode());
//				release.setDescription(i.getDescription());
//				release.setErpId(i.getId());
//				release.setVersion(0);
//
//				releaseMasterList.add(release);
//			}
//			
//			mReleaseMasterRepo.save(releaseMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateTblRateZoneMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<TblRatezonemasterEntity> tblRateZoneMasters = (List<TblRatezonemasterEntity>) tblRateZoneMasterRepo.findAll();
//		
//		if(tblRateZoneMasters != null && !tblRateZoneMasters.isEmpty()){
//			
//			TblRateZoneMasterEntity tblRateZone = null;
//			List<TblRateZoneMasterEntity> tblRateZoneMasterList = new ArrayList<>();
//			
//			for(TblRatezonemasterEntity i : tblRateZoneMasters){
//				
//				tblRateZone = new TblRateZoneMasterEntity();
//				
//				tblRateZone.setCreatedBy(i.getCreatedby());
//				tblRateZone.setCreatedOn(new Date());
//				tblRateZone.setModifiedBy(i.getModifiedby());
//				tblRateZone.setModifiedOn(new Date());
//				tblRateZone.setRateCalcType(i.getRatecalctype());
//				tblRateZone.setRateId(i.getRateid());
//				tblRateZone.setRateZone(i.getRatezone());
//				tblRateZone.setVersion(0);
//
//				tblRateZoneMasterList.add(tblRateZone);
//			}
//			
//			mTblRateZoneMasterRepo.save(tblRateZoneMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//	public ResponseEntity<AppGeneralResponse> updateTblHolidayMaster(){
//		
//	ResponseEntity<AppGeneralResponse> response = null;
//	try{
//		List<TblholidaymasterEntity> tblHolidayMasters = (List<TblholidaymasterEntity>) tblHolidayMasterRepo.findAll();
//		
//		if(tblHolidayMasters != null && !tblHolidayMasters.isEmpty()){
//			
//			TblHolidayMasterEntity tblHoliday = null;
//			List<TblHolidayMasterEntity> tblHolidayMasterList = new ArrayList<>();
//			
//			for(TblholidaymasterEntity i : tblHolidayMasters){
//				
//				tblHoliday = new TblHolidayMasterEntity();
//				
//				tblHoliday.setCityId(i.getCityid());
//				tblHoliday.setEntryType(i.getEntrytype());
//				tblHoliday.setErpId(i.getId());
//				tblHoliday.setHolidayDate(i.getHolidaydate());
//				tblHoliday.setHolidayName(i.getHolidayname());
//				tblHoliday.setIsActive(i.getIsactive());
//				tblHoliday.setStateId(i.getStateid());
//				tblHoliday.setStateName(i.getStatename());
//				tblHoliday.setVersion(0);
//
//				tblHolidayMasterList.add(tblHoliday);
//			}
//			
//			mTblHolidayMasterRepo.save(tblHolidayMasterList);
//			
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		}
//		else{
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
//		}
//	}
//	catch(Exception e){
//		e.printStackTrace();
//		response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//	}
//	
//	return response;
//	
//}
//	
//}
