package com.apptmyz.trackon.services;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.apptmyz.trackon.data.repository.jpa.BookingChargesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingInvoicesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingLbhJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPaperworkJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPickupScanDepsImagesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPkupScanDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingPkupScanSummaryJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BookingXbPktsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BulkInterchangePickupsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsigneeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MConsignorJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MDktMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MPktMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaCustomerDetailsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPincodeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupAssignmentJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupRegistrationJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupSoftInvEwbJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupSoftLbhDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupSoftPaperworkJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupSoftPktRefJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PickupSoftdataDetailsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.entities.jpa.BookingChargesEntity;
import com.apptmyz.trackon.entities.jpa.BookingDataEntity;
import com.apptmyz.trackon.entities.jpa.BookingInvoicesEntity;
import com.apptmyz.trackon.entities.jpa.BookingLbhEntity;
import com.apptmyz.trackon.entities.jpa.BookingPaperworkEntity;
import com.apptmyz.trackon.entities.jpa.BookingPickupScanDepsImagesEntity;
import com.apptmyz.trackon.entities.jpa.BookingPkupScanDataEntity;
import com.apptmyz.trackon.entities.jpa.BookingPkupScanSummaryEntity;
import com.apptmyz.trackon.entities.jpa.BookingXbPktsEntity;
import com.apptmyz.trackon.entities.jpa.BulkInterchangePickupsEntity;
import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;
import com.apptmyz.trackon.entities.jpa.MConsignorEntity;
import com.apptmyz.trackon.entities.jpa.MDktMasterEntity;
import com.apptmyz.trackon.entities.jpa.MPktMasterEntity;
import com.apptmyz.trackon.entities.jpa.MaCustomerDetailsEntity;
import com.apptmyz.trackon.entities.jpa.PickupAssignmentEntity;
import com.apptmyz.trackon.entities.jpa.PickupInsuranceDetailsEntity;
import com.apptmyz.trackon.entities.jpa.PickupRegistrationEntity;
import com.apptmyz.trackon.entities.jpa.PickupSoftInvEwbEntity;
import com.apptmyz.trackon.entities.jpa.PickupSoftLbhDataEntity;
import com.apptmyz.trackon.entities.jpa.PickupSoftPaperworkEntity;
import com.apptmyz.trackon.entities.jpa.PickupSoftPktRefEntity;
import com.apptmyz.trackon.entities.jpa.PickupSoftdataDetailsEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.BookingDataModel;
import com.apptmyz.trackon.model.BookingDocketDataModel;
import com.apptmyz.trackon.model.BulkInterchangePickupErpModel;
import com.apptmyz.trackon.model.BulkInterchangePickupModel;
import com.apptmyz.trackon.model.BulkInterchangeSubmitList;
import com.apptmyz.trackon.model.BulkInterchangeSubmitModel;
import com.apptmyz.trackon.model.CCPktModel;
import com.apptmyz.trackon.model.ChargesModel;
import com.apptmyz.trackon.model.Consignee;
import com.apptmyz.trackon.model.Consignor;
import com.apptmyz.trackon.model.DktValidationModel;
import com.apptmyz.trackon.model.ErpDktValidationModel;
import com.apptmyz.trackon.model.ErpPickupBookingResponse;
import com.apptmyz.trackon.model.ErpResponseModel;
import com.apptmyz.trackon.model.ErpResponsePickupRegistration;
import com.apptmyz.trackon.model.LbhData;
import com.apptmyz.trackon.model.MaPincodeModel;
import com.apptmyz.trackon.model.PaperWork;
import com.apptmyz.trackon.model.PickupAssignedDetailsModel;
import com.apptmyz.trackon.model.PickupAssignmentModel;
import com.apptmyz.trackon.model.PickupCancelModel;
import com.apptmyz.trackon.model.PickupInsuranceDetails;
import com.apptmyz.trackon.model.PickupInvoiceEwb;
import com.apptmyz.trackon.model.PickupRegistrationData;
import com.apptmyz.trackon.model.PickupRegistrationDataQuery;
import com.apptmyz.trackon.model.PickupRegistrationSubmit;
import com.apptmyz.trackon.model.PickupRegnSoftDataModel;
import com.apptmyz.trackon.model.PickupRescheduleModel;
import com.apptmyz.trackon.model.PickupScanDataModel;
import com.apptmyz.trackon.model.PickupScanSubmitDataModel;
import com.apptmyz.trackon.model.PrsRequestModel;
import com.apptmyz.trackon.model.TaxTypeAmount;
import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.apptmyz.trackon.model.XbPcsDataModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.EntityToModels;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mysql.fabric.xmlrpc.base.Array;

@Component
public class PickupServices {

	@Autowired
	private PickupRegistrationJpaRepository pickupRegistrationJpaRepository;

	@Autowired
	private MConsigneeJpaRepository mConsigneeJpaRepository;

	@Autowired
	private MConsignorJpaRepository mConsignorJpaRepository;

	@Autowired
	private PickupAssignmentJpaRepository pickupAssignmentJpaRepository;

	@Autowired
	private BookingDataJpaRepository bookingDataJpaRepository;

	@Autowired
	private BookingChargesJpaRepository bookingChargesJpaRepository;

	@Autowired
	private BookingInvoicesJpaRepository bookingInvoicesJpaRepository;

	@Autowired
	private BookingLbhJpaRepository bookingLbhJpaRepository;

	@Autowired
	private BookingPaperworkJpaRepository bookingPaperworkJpaRepository;

	@Autowired
	private BookingPkupScanDataJpaRepository bookingPkupScanDataJpaRepository;
	
	@Autowired
	private BookingPkupScanSummaryJpaRepository bookingPkupScanSummaryJpaRepository;

	@Autowired
	private PickupSoftdataDetailsJpaRepository pickupSoftdataDetailsJpaRepository;

	@Autowired
	private PickupSoftInvEwbJpaRepository pickupSoftInvEwbJpaRepository;

	@Autowired
	private PickupSoftPaperworkJpaRepository pickupSoftPaperworkJpaRepository;

	@Autowired
	private PickupSoftLbhDataJpaRepository pickupSoftLbhDataJpaRepository;

	@Autowired
	private CodeDescriptionServices codeDescriptionServices;

	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	
	@Autowired
	private BookingPickupScanDepsImagesJpaRepository bookingPickupScanDepsImagesJpaRepository;
	
	@Autowired
	private MDktMasterJpaRepository mDktMasterJpaRepository;
	
	@Autowired
	private MPktMasterJpaRepository mPktMasterJpaRepository;
	
	@Autowired
	private PickupSoftPktRefJpaRepository pickupSoftPktRefJpaRepository;
	
	@Autowired
	private MaPincodeJpaRepository maPincodeJpaRepository;

	@Autowired
	private MaCustomerDetailsJpaRepository maCustomerDetailsJpaRepository;
	
	@Autowired
	private BookingXbPktsJpaRepository bookingXbPktsJpaRepository;
	
	@Autowired
	private BulkInterchangePickupsJpaRepository bulkInterchangePickupsJpaRepository;
	
	private SimpleDateFormat longformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private SimpleDateFormat shortformat = new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat erpshortformat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat erplongformat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public ResponseEntity<AppGeneralResponse> register(PickupRegistrationSubmit data, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		PickupRegistrationEntity registerEntity = null;
		boolean isFranchiseReg = false;
		try {
			if(CommonUtility.check(data.getUserId())){
				if (CommonUtility.check(data.getCustCode())) {
					
					UserMasterEntity user = userMasterJpaRepository.findByUserId(data.getUserId());
					
					if(user != null){
//						if(user.getUserType().equals(Constants.FRANCHISE_USER) || (user.getUserType().equals(Constants.DELIVERY_USER) && CommonUtility.check(user.getFranchiseBranchCode()))){
//							List<PickupRegistrationEntity> registerList = pickupRegistrationJpaRepository.findByUserIdAndIsFranchise(user.getUserId(), 1);
//							if(registerList != null && !registerList.isEmpty()){
//								return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Frachise pickup registration already exists", null, null), HttpStatus.OK);
//							}
//						}
						
						if(user.getUserType().equals(Constants.FRANCHISE_USER) || user.getUserType().equals(Constants.DELIVERY_USER)){
							if(user.getUserId().equals(data.getCustCode()) || user.getFranchiseBranchCode().equals(data.getCustCode())){
								isFranchiseReg = true;
								List<PickupRegistrationEntity> registerList = pickupRegistrationJpaRepository.findByUserIdAndCustCodeAndActiveFlag(user.getUserId(), data.getCustCode(), 1);
								if(registerList != null && !registerList.isEmpty()){
									return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Pickup registration already exists", null, null), HttpStatus.OK);
								}
							}
						}
						
						MaCustomerDetailsEntity customer = maCustomerDetailsJpaRepository.findByCustCode(data.getCustCode());
						if (customer != null) {
							data.setCustomerId(customer.getId());
							registerEntity = new PickupRegistrationEntity();
							
							
							//consignee
							if(data.getConsignee() != null){
								Consignee consignee = data.getConsignee();
//								data.getConsignee().setCustomerId(customer.getId());
								if (consignee.getConsigneeId() != null
										&& consignee.getConsigneeId() > 0) {
									registerEntity.setConsigneeCode(consignee.getConsigneeCode());
									registerEntity.setConsigneeId(consignee.getConsigneeId());
								} else if(CommonUtility.check(consignee.getConsigneeName(), consignee.getAddress1()) && data.getConsignee().getConsigneePincode() != null) {
									MConsigneeEntity consigneeEntity = persistConsignee(data.getConsignee());
									registerEntity.setConsigneeId(consigneeEntity.getId());
								}
							}
							
							//consignor
							if(data.getConsignor() != null){
//								data.getConsignor().setCustomerId(customer.getId());
								if (data.getConsignor().getConsignorId() != null
										&& data.getConsignor().getConsignorId() > 0) {
									registerEntity.setConsignorCode(data.getConsignor().getConsignorCode());
									registerEntity.setConsignorId(data.getConsignor().getConsignorId());
								} else {
									
									MConsignorEntity topVersionConsignor = mConsignorJpaRepository.findTopByOrderByVersionDesc();
									Integer version = 0;
									if (topVersionConsignor != null) {
										version = topVersionConsignor.getVersion();
									}
									String consignorBranch = maPincodeJpaRepository.getBranchCodeByPincode(data.getConsignor().getConsignorPincode());
									MConsignorEntity consignor = null;
									try{
										consignor = persistConsignor(data.getConsignor(), consignorBranch, version, customer);
										if (consignor != null) {
											registerEntity.setConsignorId(consignor.getId());
											registerEntity.setConsignorCode(consignor.getConsignorCode());
											data.setConsignor(EntityToModels.convertConsignorEntityToModel(consignor));
										}
										else{
											return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Failed to save consignor details", null, null), HttpStatus.OK);
										}
									}
									catch(Exception e){
										logger.info(e.getMessage(), e);
										return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Failed to save consignor details", null, null), HttpStatus.OK);
									}
								}
							}
							registerEntity.setUserId(user.getUserId());
							registerEntity.setConsignorCode(data.getConsignor().getConsignorCode());
							registerEntity.setConsignorName(data.getConsignor().getConsignorName());
							registerEntity.setConsignorContactNo(data.getConsignor().getContactPhoneno());
							registerEntity.setCustomerName(data.getCustomerName());
							registerEntity.setPickupPincode(data.getOriginPincode());
							registerEntity.setDocketNo(data.getDocketNo());
							registerEntity.setCreatedTimestamp(new Date());
							registerEntity.setCustCode(data.getCustCode());
							registerEntity.setCustId(customer.getId());
							registerEntity.setDeliveryPincode(data.getDeliveryPincode());
							registerEntity.setDoxFlag(data.getDoxNonDox());
							registerEntity.setBillingFlag(data.getBillingFlag());
							registerEntity.setIsRecurring(data.getIsRecurring() == true ? 1 : 0);
							registerEntity.setOdaArea(data.getOdaArea());
							registerEntity.setOdaFlag(data.isOdaFlag() == true ? 1 : 0);
							if (CommonUtility.check(data.getPickupDate())) {
								registerEntity.setPickupDate(shortformat.parse(data.getPickupDate()));
							}
							registerEntity.setPickupStatus(Constants.PICKUP_STATUS_PENDING);
							registerEntity.setPickupPincode(data.getOriginPincode());
							registerEntity.setPickupSlot(data.getPickupSlot());
							registerEntity.setPktCount(data.getPktCount());
							if (CommonUtility.check(data.getRecurringEndDate())) {
								registerEntity.setRecurEndDate(shortformat.parse(data.getRecurringEndDate()));
							}
							if (CommonUtility.check(data.getRecurringStartDate())) {
								registerEntity.setRecurStartDate(shortformat.parse(data.getRecurringStartDate()));
							}
							registerEntity.setRecurFrequency(data.getRecurringFrequency());
							if (CommonUtility.check(data.getRecurringFrequency())) {
								if (data.getRecurringFrequency().equals(Constants.CUSTOM)) {
									registerEntity
											.setCustomRecurFreq(CommonUtility.getCsvFromArray(data.getCustomRecurFrequency()));
								}
							}
							registerEntity.setShipMode(data.getShipMode());
							registerEntity.setSvcType(data.getSvcType());
							registerEntity.setUom(data.getUom());
							registerEntity.setWt(data.getWt());
							if (data.getPickupSoftData() == null) {
								registerEntity.setPickupType("N"); // Normal
																			// pickup
							}

							if (data.getPickupSoftData() != null) {
								registerEntity.setHasSoftdata(1);
								PickupRegnSoftDataModel softData = data.getPickupSoftData();
								if (softData.getCustomerRefNo() != null
										|| (softData.getPktData() != null && !softData.getPktData().isEmpty())) {
									registerEntity.setPickupType("I"); // Interchange
								} else {
									registerEntity.setPickupType("B");// Bulk
								}
							}

							try {
								data.setPickupDate(registerEntity.getPickupDate() != null ? erpshortformat.format(registerEntity.getPickupDate()) : null);
								if (registerEntity.getRecurStartDate() != null) {
									data.setRecurringStartDate(erpshortformat.format(registerEntity.getRecurStartDate()));
								}
								if (registerEntity.getRecurEndDate() != null) {
									data.setRecurringEndDate(erpshortformat.format(registerEntity.getRecurEndDate()));
								}
								
								String erpRecFrequency = "";
								if(data.getCustomRecurFrequency() != null && !data.getCustomRecurFrequency().isEmpty()){
									for(int i = 0; i<data.getCustomRecurFrequency().size(); i++){
										if(i > 0){
											erpRecFrequency = erpRecFrequency+","+data.getCustomRecurFrequency().get(i);
										}else{
											erpRecFrequency = ""+data.getCustomRecurFrequency().get(i);
										}
									}
									data.setErpCustomRecurFrequency(erpRecFrequency);
								}
								
								String url = FilesUtil.getProperty("erpPickupRegistration");
								String erpResponseString = CommonUtility.postDataToErp(gson.toJson(data), url, logger);
								ErpResponsePickupRegistration responseObj = gson.fromJson(erpResponseString,
										ErpResponsePickupRegistration.class);
								registerEntity.setErpStatus(responseObj.getResult() ? 1 : 0);
								registerEntity.setErpMessage(responseObj.getMessege());
								registerEntity.setErpPickupOrderId(responseObj.getPickUpOrderId());

							} catch (Exception e) {
								e.printStackTrace();
								registerEntity.setErpStatus(0);
								registerEntity.setErpMessage(ResponseMessages.INTERNAL_SERVER_ERROR);
								logger.info("EXCEPTION : SENDING TO ERP");
								logger.info(e.getMessage(), e);
							}
							if(registerEntity.getErpStatus() != null && registerEntity.getErpStatus() == 1){
								registerEntity.setActiveFlag(1);
							}else{
								registerEntity.setActiveFlag(0);
							}
//							if(Constants.FRANCHISE_USER.equals(data.getUserType()) || CommonUtility.check(user.getFranchiseBranchCode())){
								registerEntity.setIsFranchise(isFranchiseReg ? 1 : 0);
//							}
//							else{
//								registerEntity.setIsFranchise(0);
//							}

							registerEntity = pickupRegistrationJpaRepository.save(registerEntity);

							if (data.getIsRecurring() && registerEntity.getErpStatus() != null && registerEntity.getErpStatus() == 1) {
								
								createRecurringPickupRegistrations(data, registerEntity, user);
							}

							if (data.getInsuranceDetails() != null) {
								PickupInsuranceDetails ins = data.getInsuranceDetails();
								PickupInsuranceDetailsEntity insurance = new PickupInsuranceDetailsEntity();
								insurance.setInsuranceCompany(ins.getInsuranceCompany());
								insurance.setInsuredPerson(ins.getInsuredPerson());
								insurance.setPickupRegistration(registerEntity);
								insurance.setPolicyNumber(ins.getPolicyNumber());
								insurance.setStartDate(shortformat.parse(ins.getStartDate()));
								insurance.setEndDate(shortformat.parse(ins.getEndDate()));
							}

							if (data.getPickupSoftData() != null) {
								PickupRegnSoftDataModel softData = data.getPickupSoftData();

								PickupSoftdataDetailsEntity psd = new PickupSoftdataDetailsEntity();
								psd.setCodAmount(softData.getCodAmount());
								psd.setMaterial(softData.getMaterial());
								psd.setPktNoStart(softData.getStartPktNo());
								psd.setPktNoEnd(softData.getEndPktNo());
								psd.setPickupDate(shortformat.parse((data.getPickupDate())));
								psd.setPickupRegistration(registerEntity);
								
								psd = pickupSoftdataDetailsJpaRepository.save(psd);

								if (softData.getPktData() != null && !softData.getPktData().isEmpty()) {
									psd.setCustPktDataAvlbl(1);
									for (CCPktModel ccPkt : softData.getPktData()) {
										PickupSoftPktRefEntity ccPktEntity = new PickupSoftPktRefEntity();
										ccPktEntity.setCustPktNo(ccPkt.getCustomerPktNo());
										ccPktEntity.setPktNo(ccPkt.getPktNo());
										ccPktEntity.setPickupRegistration(registerEntity);
										ccPktEntity.setCreatedTimestamp(new Date());
										ccPktEntity.setDocketNo(registerEntity.getDocketNo());
										pickupSoftPktRefJpaRepository.save(ccPktEntity);
									}
								}

								if (softData.getInvoiceDetails() != null && !softData.getInvoiceDetails().isEmpty()) {
									psd.setInvoiceEwbAvlbl(1);
									for (PickupInvoiceEwb inv : softData.getInvoiceDetails()) {
										PickupSoftInvEwbEntity invoice = new PickupSoftInvEwbEntity();
										if (inv.getEwbDate() != null) {
											invoice.setEwbDate(shortformat.parse(inv.getEwbDate()));
										}
										invoice.setEwbNo(inv.getEwbNo());
										if (inv.getEwbValidTill() != null) {
											invoice.setEwbValidTill(shortformat.parse(inv.getEwbValidTill()));
										}
										if (inv.getInvoiceDate() != null) {
											invoice.setInvDate(shortformat.parse(inv.getInvoiceDate()));
										}
										invoice.setInvNo(inv.getInvoiceNo());
										invoice.setPickupDate(shortformat.parse((data.getPickupDate())));
										invoice.setPickupRegistration(registerEntity);
										pickupSoftInvEwbJpaRepository.save(invoice);
									}
								}

								if (softData.getPaperWork() != null && !softData.getPaperWork().isEmpty()) {
									psd.setPaperWorkAvlbl(1);
									for (PaperWork pw : softData.getPaperWork()) {
										PickupSoftPaperworkEntity paper = new PickupSoftPaperworkEntity();
										paper.setPaperWorkDocumentName(pw.getName());
										paper.setPaperWorkName(pw.getName());
										paper.setPaperWorkRefNo(pw.getRefNo());
										paper.setPickupDate(shortformat.parse((data.getPickupDate())));

										String imgData = pw.getBase64Data();
										if (CommonUtility.check(imgData)) {
											String filename = CommonUtility.toImgFromBase64(imgData,
													data.getDocketNo() + "-PR-", "png", 2, "PW");
											paper.setDocPath(filename);
										}
										paper.setPickupRegistration(registerEntity);
										pickupSoftPaperworkJpaRepository.save(paper);
									}
								}

								if (softData.getLbhData() != null && !softData.getLbhData().isEmpty()) {
									psd.setLbhValuesAvlbl(1);
									for (LbhData lbh : softData.getLbhData()) {
										PickupSoftLbhDataEntity lbhData = new PickupSoftLbhDataEntity();
										lbhData.setActualWeight(lbh.getActualWt());
										lbhData.setCustPktRefNo(lbh.getCustomerPktRefNo());
										lbhData.setMaterial(lbh.getMaterial());
										lbhData.setPickupDate(shortformat.parse((data.getPickupDate())));
										lbhData.setPktCount(lbh.getPktCount());
										lbhData.setPktHeight(lbh.getPktHt());
										lbhData.setPktLen(lbh.getPktLen());
										lbhData.setPktWidth(lbh.getPktWidth());
										lbhData.setPktNo(lbh.getPktNo());
										lbhData.setUom(lbh.getUom());
										lbhData.setPickupRegistration(registerEntity);
										pickupSoftLbhDataJpaRepository.save(lbhData);
									}
								}
								pickupSoftdataDetailsJpaRepository.save(psd);
							}

//							UserMasterEntity user = userMasterJpaRepository.findByUserId(data.getUserId());
							if (registerEntity != null && registerEntity.getPickupType() != null
									&& registerEntity.getPickupType().equals("N") && registerEntity.getErpStatus() != null && registerEntity.getErpStatus() == 1) {

								PickupAssignmentEntity dbPickupAssigned = new PickupAssignmentEntity();
								dbPickupAssigned.setAssignedTo(user.getUserId());
								dbPickupAssigned.setAssUsrTyp(user.getUserType());
								dbPickupAssigned.setCreatedTimestamp(new Date());
								dbPickupAssigned.setPickupRegistration(registerEntity);
								dbPickupAssigned.setActiveFlag(1);
								// if (registeredPickup.getHasSoftdata() != null &&
								// registeredPickup.getHasSoftdata() == 1) {
								// if (registeredPickup.getCustRefNo() != null) {
								// dbPickupAssigned.setPickupType("I"); // Interchange
								// } else {
								// dbPickupAssigned.setPickupType("B");// Bulk
								// }
								// } else {
								// dbPickupAssigned.setPickupType("N");
								// }

								dbPickupAssigned.setAssignedBy(user.getUserId());
								dbPickupAssigned.setPickupType(registerEntity.getPickupType());

								try {
									dbPickupAssigned.setPickupDate(registerEntity.getPickupDate());
								} catch (Exception e) {
									e.printStackTrace();
								}
								pickupAssignmentJpaRepository.save(dbPickupAssigned);

								registerEntity.setPickupStatus(Constants.PICKUP_STATUS_ASSIGNED);
								pickupRegistrationJpaRepository.save(registerEntity);
							}

							if (registerEntity.getErpStatus() == 1) {
								if (CommonUtility.check(user.getUserId(), user.getFranchiseBranchCode())
										&& user.getUserId().equalsIgnoreCase(user.getFranchiseBranchCode())
										&& user.getUserType().equalsIgnoreCase(Constants.FRANCHISE_USER)){									
									generatePickupRegistrations(registerEntity, user.getFranchiseBranchCode(), data);
								}
								generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true,
										registerEntity.getErpMessage(), registerEntity.getId(), null),
										HttpStatus.OK);
							} else {
								generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false,
										registerEntity.getErpMessage() + " / failed at ERP", null, null), HttpStatus.OK);
							}
						} else {
							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(false, "Customer not found", null, null), HttpStatus.OK);
						}
					}
					else{
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, "User not found", null, null), HttpStatus.OK);
					}

				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, "Customer code not sent", null, null), HttpStatus.OK);
				}
			}
			else{
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "User ID not sent", null, null), HttpStatus.OK);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	private PickupAssignmentEntity buildPickupAssignmentEntity(PickupRegistrationEntity entity, UserMasterEntity user){
		PickupAssignmentEntity assignEntity = new PickupAssignmentEntity();
		assignEntity.setAssignedTo(entity.getUserId());
		assignEntity.setAssUsrTyp(user.getUserType());
		assignEntity.setCreatedTimestamp(new Date());
		assignEntity.setPickupRegistration(entity);
		assignEntity.setActiveFlag(1);
		assignEntity.setAssignedBy(entity.getUserId());
		assignEntity.setPickupType(entity.getPickupType());
		assignEntity.setPickupDate(entity.getPickupDate());
		return assignEntity;
	}

	private void createRecurringPickupRegistrations(PickupRegistrationSubmit data, PickupRegistrationEntity entity, UserMasterEntity user) {
		try {
			List<PickupRegistrationEntity> registrations = new ArrayList<>();
			List<PickupAssignmentEntity> assignments = new ArrayList<>();
			LocalDate startDate = entity.getRecurStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate endDate = entity.getRecurEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			String recur = data.getRecurringFrequency();
			if (CommonUtility.check(data.getRecurringFrequency())) {
				List<DayOfWeek> daysOfWk = new ArrayList<DayOfWeek>();
				if (recur.equals(Constants.RECUR_FREQ_MTOF)) {
					daysOfWk.add(DayOfWeek.MONDAY);
					daysOfWk.add(DayOfWeek.TUESDAY);
					daysOfWk.add(DayOfWeek.WEDNESDAY);
					daysOfWk.add(DayOfWeek.THURSDAY);
					daysOfWk.add(DayOfWeek.FRIDAY);
				} else if (recur.equals(Constants.RECUR_FREQ_MTOS)) {
					daysOfWk.add(DayOfWeek.MONDAY);
					daysOfWk.add(DayOfWeek.TUESDAY);
					daysOfWk.add(DayOfWeek.WEDNESDAY);
					daysOfWk.add(DayOfWeek.THURSDAY);
					daysOfWk.add(DayOfWeek.FRIDAY);
					daysOfWk.add(DayOfWeek.SATURDAY);
				} else if (recur.equals(Constants.RECUR_FREQ_EVERYDAY)) {
					daysOfWk.add(DayOfWeek.MONDAY);
					daysOfWk.add(DayOfWeek.TUESDAY);
					daysOfWk.add(DayOfWeek.WEDNESDAY);
					daysOfWk.add(DayOfWeek.THURSDAY);
					daysOfWk.add(DayOfWeek.FRIDAY);
					daysOfWk.add(DayOfWeek.SATURDAY);
					daysOfWk.add(DayOfWeek.SUNDAY);
				} else if (data.getRecurringFrequency().equals(Constants.CUSTOM)) {
					List<Integer> daysOfRecur = data.getCustomRecurFrequency();
					for (Integer dayOfRecur : daysOfRecur) {
						switch (dayOfRecur) {
						case 1:
							daysOfWk.add(DayOfWeek.MONDAY);
							break;
						case 2:
							daysOfWk.add(DayOfWeek.TUESDAY);
							break;
						case 3:
							daysOfWk.add(DayOfWeek.WEDNESDAY);
							break;
						case 4:
							daysOfWk.add(DayOfWeek.THURSDAY);
							break;
						case 5:
							daysOfWk.add(DayOfWeek.FRIDAY);
							break;
						case 6:
							daysOfWk.add(DayOfWeek.SATURDAY);
							break;
						case 7:
							daysOfWk.add(DayOfWeek.SUNDAY);
							break;
						default:
							break;
						}
					}
				}

				boolean found = false;
				int index = 0;
				int startDay = startDate.getDayOfWeek().getValue();

				for (int i = 0; i < daysOfWk.size(); i++) {
					if (startDay == daysOfWk.get(i).getValue()) {
						index = i;
						PickupRegistrationEntity recReg = persistPickupRegistration(data, java.sql.Date.valueOf(startDate), entity);
						registrations.add(recReg);
						found = true;
					}
				}
				if (!found) {
					LocalDate firstDate = startDate.with(TemporalAdjusters.next(daysOfWk.get(0)));
					for (int i = 0; i < daysOfWk.size(); i++) {
						if (firstDate.isAfter(startDate.with(TemporalAdjusters.next(daysOfWk.get(i))))
								&& firstDate.isAfter(startDate)) {
							index = i;
							firstDate = startDate.with(TemporalAdjusters.next(daysOfWk.get(i)));
						}
					}
					PickupRegistrationEntity recReg = persistPickupRegistration(data, java.sql.Date.valueOf(firstDate), entity);
					registrations.add(recReg);
					startDate = firstDate;
				}

				while (true) {
					index = (index + 1) % daysOfWk.size();
					startDate = startDate.with(TemporalAdjusters.next(daysOfWk.get(index)));
					if (!startDate.isAfter(endDate)) {
						PickupRegistrationEntity recReg = persistPickupRegistration(data, java.sql.Date.valueOf(startDate), entity);
						registrations.add(recReg);
					} else {
						break;
					}
				}
				
				if(registrations != null && !registrations.isEmpty()){
					for(PickupRegistrationEntity r : registrations){
						assignments.add(buildPickupAssignmentEntity(r, user));
					}
					pickupAssignmentJpaRepository.save(assignments);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public PickupRegistrationEntity persistPickupRegistration(PickupRegistrationSubmit data, Date pickupDate, PickupRegistrationEntity entity) throws Exception {
		
		if (CommonUtility.check(data.getCustCode())) {
			MaCustomerDetailsEntity customer = maCustomerDetailsJpaRepository.findByCustCode(data.getCustCode());
			if (customer != null) {
				
				PickupRegistrationEntity pickupRegisterEntity = new PickupRegistrationEntity();
				pickupRegisterEntity.setActiveFlag(1);
				//consignee
				if(data.getConsignee() != null){
//					data.getConsignee().setCustomerId(customer.getId());
					if (data.getConsignee().getConsigneeId() != null
							&& data.getConsignee().getConsigneeId() > 0) {
						pickupRegisterEntity.setConsigneeCode(data.getConsignee().getConsigneeCode());
						pickupRegisterEntity.setConsigneeId(data.getConsignee().getConsigneeId());
					} else {
						MConsigneeEntity consignee = persistConsignee(data.getConsignee());
						pickupRegisterEntity.setConsigneeId(consignee.getId());
					}
				}
				
				//consignor
				if(data.getConsignor() != null){
//					data.getConsignor().setCustomerId(customer.getId());
					if (data.getConsignor().getConsignorId() != null
							&& data.getConsignor().getConsignorId() > 0) {
						pickupRegisterEntity.setConsignorCode(data.getConsignor().getConsignorCode());
						pickupRegisterEntity.setConsignorId(data.getConsignor().getConsignorId());
					} else {
						
						MConsignorEntity topVersionConsignor = mConsignorJpaRepository.findTopByOrderByVersionDesc();
						Integer version = 0;
						if (topVersionConsignor != null) {
							version = topVersionConsignor.getVersion();
						}
						String consignorBranch = maPincodeJpaRepository.getBranchCodeByPincode(data.getConsignor().getConsignorPincode());
						MConsignorEntity consignor = persistConsignor(data.getConsignor(), consignorBranch, version, customer);
						if (consignor != null) {
							pickupRegisterEntity.setConsignorId(consignor.getId());
							pickupRegisterEntity.setConsignorCode(consignor.getConsignorCode());
							data.setConsignor(EntityToModels.convertConsignorEntityToModel(consignor));
						}
					}
				}
				pickupRegisterEntity.setUserId(entity.getUserId());
				pickupRegisterEntity.setConsignorCode(data.getConsignor().getConsignorCode());
				pickupRegisterEntity.setConsignorName(data.getConsignor().getConsignorName());
				pickupRegisterEntity.setConsignorContactNo(data.getConsignor().getContactPhoneno());
				pickupRegisterEntity.setCustomerName(data.getCustomerName());
				pickupRegisterEntity.setPickupPincode(data.getOriginPincode());
				pickupRegisterEntity.setDocketNo(data.getDocketNo());
				pickupRegisterEntity.setCreatedTimestamp(new Date());
				pickupRegisterEntity.setCustCode(data.getCustCode());
				pickupRegisterEntity.setCustId(data.getCustomerId());
				pickupRegisterEntity.setDeliveryPincode(data.getDeliveryPincode());
				pickupRegisterEntity.setDoxFlag(data.getDoxNonDox());
				pickupRegisterEntity.setBillingFlag(data.getBillingFlag());
				pickupRegisterEntity.setOdaArea(data.getOdaArea());
				pickupRegisterEntity.setOdaFlag(data.isOdaFlag() ? 1 : 0);
				pickupRegisterEntity.setPickupDate(pickupDate);
				pickupRegisterEntity.setPickupStatus(Constants.PICKUP_STATUS_PENDING);
				pickupRegisterEntity.setPickupPincode(data.getOriginPincode());
				pickupRegisterEntity.setPickupSlot(data.getPickupSlot());
				pickupRegisterEntity.setPktCount(data.getPktCount());
				pickupRegisterEntity.setShipMode(data.getShipMode());
				pickupRegisterEntity.setSvcType(data.getSvcType());
				pickupRegisterEntity.setUom(data.getUom());
				pickupRegisterEntity.setWt(data.getWt());
				if (data.getPickupSoftData() == null) {
					pickupRegisterEntity.setPickupType("N"); // Normal pickup
				}
				pickupRegisterEntity.setRecurringPickupId(entity.getId());
				pickupRegisterEntity.setErpStatus(entity.getErpStatus());
				pickupRegisterEntity.setErpMessage(entity.getErpMessage());
				pickupRegisterEntity.setErpPickupOrderId(entity.getErpPickupOrderId());
				pickupRegisterEntity.setErpPickupOrderRef(entity.getErpPickupOrderRef());
				pickupRegisterEntity.setIsFranchise(entity.getIsFranchise());
				
				return pickupRegistrationJpaRepository.save(pickupRegisterEntity);
				
			}else{
				return null;
			}
		}else{
			return null;
		}
		
		
	}

	public List<PickupRegistrationData> PickupList(PickupRegistrationDataQuery requestData, Logger logger) {
		List<PickupRegistrationData> pickupData = new ArrayList<PickupRegistrationData>();
		Date from = null;
		Date to = null;
		try {
			if (CommonUtility.check(requestData.getPickupDateFrom())) {
				from = shortformat.parse(requestData.getPickupDateFrom());
			}
			if (CommonUtility.check(requestData.getPickupDateTo())) {
				to = shortformat.parse(requestData.getPickupDateTo());
			}
			if (CommonUtility.check(requestData.getCustCode())) {

			}
			if (CommonUtility.check(requestData.getPickupPincode())) {

			}

			List<PickupRegistrationEntity> pickupList = pickupRegistrationJpaRepository.findByPickupDateBetween(from,
					to);
			logger.info("Data from DB: " + pickupList.size() + ", " + shortformat.format(from) + ","
					+ shortformat.format(to));
			if (pickupList != null && pickupList.size() > 0) {
				Map<String, String> shippingModesMap = codeDescriptionServices.getShippingModes();
				Map<String, String> serviceTypesMap = codeDescriptionServices.getServiceTypes();
				Map<String, String> pickupStatusMap = codeDescriptionServices.getPickupStatus();
				Map<String, String> uomMap = codeDescriptionServices.getUomValues();

				for (PickupRegistrationEntity pickup : pickupList) {
					PickupRegistrationData pd = new PickupRegistrationData();
					pd.setPickupId(pickup.getId());
//					if (CommonUtility.check(pickup.getAssignedToAgent())) {
//						pd.setAssignedTo(pickup.getAssignedToAgent());
//					} else if (CommonUtility.check(pickup.getAssignedToFranchisee())) {
//						pd.setAssignedTo(pickup.getAssignedToFranchisee());
//					} else if (CommonUtility.check(pickup.getAssignedToBranch())) {
//						pd.setAssignedTo(pickup.getAssignedToBranch());
//					}
					pd.setConsignorCode(pickup.getConsignorCode());
					pd.setConsignorName(pickup.getConsignorName());
					pd.setContactNumber(pickup.getConsignorContactNo());
//					pd.setCreatedBy(createdBy);
//					pd.setCreatedTime(pickup.getCreatedTimestamp());
					pd.setCustCode(pickup.getCustCode());
					pd.setCustName(pickup.getCustomerName());
					if (pickup.getDeliveryPincode() != null) {
						pd.setDeliveryPincode(pickup.getDeliveryPincode());
					}
					pd.setDocketNo(pickup.getDocketNo());
					pd.setDoxFlag(pickup.getDoxFlag());
					if (pickup.getDoxFlag() != null && pickup.getDoxFlag().equals(Constants.DOX_FLG)) {
						pd.setDoxFlagDesc(Constants.DOX_FLG);
					} else if(pickup.getDoxFlag() != null && pickup.getDoxFlag().equals(Constants.NON_DOX_FLG)) {
						pd.setDoxFlagDesc(Constants.NON_DOX_FLG);
					}
					
					pd.setPickupDate(shortformat.format(pickup.getPickupDate()));
					pd.setPickupPincode(pickup.getPickupPincode());
					pd.setPickupSlot(pickup.getPickupSlot());
					pd.setPickupStatus(pickup.getPickupStatus());
					if (CommonUtility.check(pickup.getPickupStatus())) {
						pd.setPickupStatusDesc(pickupStatusMap.get(pickup.getPickupStatus()));
					}
					if (pickup.getPktCount() != null) {
						pd.setPktCount(pickup.getPktCount());
					}
					pd.setServiceType(pickup.getSvcType());
					if (CommonUtility.check(pickup.getSvcType())) {
						pd.setServiceTypeDesc(serviceTypesMap.get(pickup.getSvcType()));
					}
					pd.setShipMode(pickup.getShipMode());
					if (CommonUtility.check(pickup.getShipMode())) {
						pd.setShipModeDesc(shippingModesMap.get(pickup.getShipMode()));
					}
					pd.setUom(pickup.getUom());
					if (CommonUtility.check(pickup.getUom())) {
						pd.setUomDesc(uomMap.get(pickup.getUom()));
					}
					if (pickup.getWt() != null) {
						pd.setWt(pickup.getWt());
					}
					pd.setPickupType(pickup.getPickupType());
					pickupData.add(pd);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pickupData;
	}

	private MConsigneeEntity persistConsignee(Consignee consignee) {
		try {
			MConsigneeEntity cEntity = new MConsigneeEntity();
			cEntity.setActiveFlag(1);
			cEntity.setCity(consignee.getCity());
			cEntity.setConsigneeAddress1(consignee.getAddress1());
			cEntity.setConsigneeAddress2(consignee.getAddress2());
//			cEntity.setConsigneeCode(consigneeCode);
			cEntity.setConsigneeName(consignee.getConsigneeName());
			cEntity.setContactName(consignee.getContactName());
			cEntity.setContactPhoneno(consignee.getContactPhoneno());
			cEntity.setCreatedTimestamp(new Date());
			if (CommonUtility.check(consignee.getCustCode())) {
				cEntity.setCustomerId(consignee.getCustomerId());
				cEntity.setCustCode(consignee.getCustCode());
			}
			cEntity.setEmail(consignee.getEmail());
			cEntity.setGstin(consignee.getGstin());
//			cEntity.setBranchCode(consignee.getb);
			cEntity.setPan(consignee.getPan());
			cEntity.setPhoneNum(consignee.getMobileno());
			cEntity.setPincode(consignee.getConsigneePincode());
			cEntity.setState(consignee.getState());
			cEntity = mConsigneeJpaRepository.save(cEntity);
			cEntity.setConsigneeCode(String.format("%08d", cEntity.getId()));
			mConsigneeJpaRepository.save(cEntity);
			return cEntity;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private MConsignorEntity persistConsignor(Consignor consignor, String branchCode, Integer version, MaCustomerDetailsEntity customer) throws Exception {
		
			MConsignorEntity cEntity = new MConsignorEntity();
			cEntity.setActiveFlag(1);
			cEntity.setCity(consignor.getCity());
			cEntity.setConsignorAddress1(consignor.getAddress1());
			cEntity.setConsignorAddress2(consignor.getAddress2());
			cEntity.setConsignorName(consignor.getConsignorName());
			cEntity.setContactName(consignor.getContactName());
			cEntity.setContactPhoneno(consignor.getContactPhoneno());
			cEntity.setCreatedTimestamp(new Date());
			if (CommonUtility.check(consignor.getCustCode())) {
				cEntity.setCustCode(consignor.getCustCode());
			}else {
				cEntity.setCustCode(Constants.RETAIL_CUSTCODE);
			}
			cEntity.setEmail(consignor.getEmail());
			cEntity.setGstin(consignor.getGstin());
			cEntity.setBranchCode(branchCode);
			cEntity.setPan(consignor.getPan());
			cEntity.setVersion(version != null ? version + 1 : 0);
			cEntity.setPhoneNum(consignor.getMobileno());
			cEntity.setPincode(consignor.getConsignorPincode());
			cEntity.setState(consignor.getState());
			cEntity.setMaCustomerDetails(customer);
			cEntity = mConsignorJpaRepository.save(cEntity);
			cEntity.setConsignorCode(String.format("%08d", cEntity.getId()));
			cEntity = mConsignorJpaRepository.save(cEntity);
			return cEntity;
		
		
	}

	public ResponseEntity<AppGeneralResponse> assign(PickupAssignmentModel data, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			String assignedto = data.getAssignedto();
			String assignedToType = data.getAssignedToType();
			String userId = data.getUserId();
			List<PickupAssignedDetailsModel> pickups = data.getPickupdetails();
			PickupRegistrationEntity newreg = null;
			if (pickups != null && !pickups.isEmpty()) {
				for (PickupAssignedDetailsModel pickup : pickups) {
					PickupAssignmentEntity dbPickupAssigned = null;
					PickupRegistrationEntity pickupEntity = pickupRegistrationJpaRepository
							.findOne(pickup.getPickupId());

					if (data.getIsRedirected() == 1) {
						dbPickupAssigned = pickupAssignmentJpaRepository
								.findTopByPickupRegistrationAndActiveFlagOrderByIdDesc(pickupEntity, 1);
						if (dbPickupAssigned != null) {
							dbPickupAssigned.setActiveFlag(0);
							dbPickupAssigned.setRedirectReasonCode(data.getReasonCode());
							dbPickupAssigned.setRemarks(data.getRemarks());
							dbPickupAssigned.setRedirectedOn(new Date());
							dbPickupAssigned.setRedirectedBy(userId);
							pickupAssignmentJpaRepository.save(dbPickupAssigned);
						}
						
						newreg = new PickupRegistrationEntity();
						newreg.setActiveFlag(1);
						newreg.setBillingFlag(pickupEntity.getBillingFlag());
						newreg.setConsigneeCode(pickupEntity.getConsigneeCode());
						newreg.setConsigneeId(pickupEntity.getConsigneeId());
						newreg.setConsignorCode(pickupEntity.getConsignorCode());
						newreg.setConsignorContactNo(pickupEntity.getConsignorContactNo());
						newreg.setConsignorId(pickupEntity.getConsignorId());
						newreg.setConsignorName(pickupEntity.getConsignorName());
						newreg.setCreatedTimestamp(new Date());
						newreg.setCustCode(pickupEntity.getCustCode());
						newreg.setCustId(pickupEntity.getCustId());
						newreg.setCustomerName(pickupEntity.getCustomerName());
						newreg.setCustomRecurFreq(pickupEntity.getCustomRecurFreq());
						newreg.setCustRefNo(pickupEntity.getCustRefNo());
						newreg.setDeliveryPincode(pickupEntity.getDeliveryPincode());
						newreg.setDocketNo(pickupEntity.getDocketNo());
						newreg.setDoxFlag(pickupEntity.getDoxFlag());
						newreg.setErpMessage(pickupEntity.getErpMessage());
						newreg.setErpPickupOrderId(pickupEntity.getErpPickupOrderId());
						newreg.setErpStatus(pickupEntity.getErpStatus());
						newreg.setHasSoftdata(pickupEntity.getHasSoftdata());
						newreg.setIsFranchise(pickupEntity.getIsFranchise());
						newreg.setIsRecurring(pickupEntity.getIsRecurring());
						newreg.setOdaArea(pickupEntity.getOdaArea());
						newreg.setOdaFlag(pickupEntity.getOdaFlag());
						newreg.setPickupDate(pickupEntity.getPickupDate());
						newreg.setPickupPincode(pickupEntity.getPickupPincode());
						newreg.setPickupSlot(pickupEntity.getPickupSlot());
						newreg.setPickupStatus(Constants.PICKUP_STATUS_PENDING);
						newreg.setPickupType(pickupEntity.getPickupType());
						newreg.setPktCount(pickupEntity.getPktCount());
						newreg.setRecurEndDate(pickupEntity.getRecurEndDate());
						newreg.setRecurFrequency(pickupEntity.getRecurFrequency());
						newreg.setRecurringPickupId(pickupEntity.getRecurringPickupId());
						newreg.setRecurStartDate(pickupEntity.getRecurStartDate());
						newreg.setShipMode(pickupEntity.getShipMode());
						newreg.setSvcType(pickupEntity.getSvcType());
						newreg.setUom(pickupEntity.getUom());
						newreg.setUserId(assignedto);
						newreg.setWt(pickupEntity.getWt());
						
						pickupEntity.setActiveFlag(0);
						pickupRegistrationJpaRepository.save(newreg);
					}
						
						dbPickupAssigned = new PickupAssignmentEntity();
						dbPickupAssigned.setAssignedTo(assignedto);
						dbPickupAssigned.setAssUsrTyp(assignedToType);
						dbPickupAssigned.setCreatedTimestamp(new Date());
						if(newreg != null && data.getIsRedirected() == 1){
							dbPickupAssigned.setPickupRegistration(newreg);
						}else{
							dbPickupAssigned.setPickupRegistration(pickupEntity);
						}
						dbPickupAssigned.setActiveFlag(1);
						dbPickupAssigned.setPickupType(pickupEntity.getPickupType());
						dbPickupAssigned.setAssignedBy(userId);


					try {
						dbPickupAssigned.setPickupDate(shortformat.parse(pickup.getPickupDate()));
					} catch (Exception e) {
						e.printStackTrace();
					}
					pickupAssignmentJpaRepository.save(dbPickupAssigned);

					pickupEntity.setPickupStatus(Constants.PICKUP_STATUS_ASSIGNED);
					pickupRegistrationJpaRepository.save(pickupEntity);
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
			} else {
				new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> cancel(PickupCancelModel data, Logger logger) {
		// TODO Auto-generated method stub
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			PickupRegistrationEntity dbRecord = pickupRegistrationJpaRepository.findOne(data.getPickupId());
			dbRecord.setActiveFlag(0);
			dbRecord.setPickupStatus(Constants.PICKUP_STATUS_CANCELLED);
			dbRecord.setUpdatedTimestamp(new Date());
			pickupRegistrationJpaRepository.save(dbRecord);
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data.getPickupId(), null), HttpStatus.OK);

			PickupAssignmentEntity pickupAssigned = pickupAssignmentJpaRepository
					.findTopByPickupRegistrationAndActiveFlagOrderByIdDesc(dbRecord, 1);
			if (pickupAssigned != null) {
//				pickupAssignmentJpaRepository.delete(pickupAssigned.getId());
				pickupAssigned.setActiveFlag(0);
				pickupAssignmentJpaRepository.save(pickupAssigned);
				logger.info("Inactivated assigned pickup : " + pickupAssigned.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> reschedule(PickupRescheduleModel data, Logger logger) {
		// TODO Auto-generated method stub
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			PickupRegistrationEntity dbRecord = pickupRegistrationJpaRepository.findOne(data.getPickupId());
			dbRecord.setPickupDate(shortformat.parse(data.getReschDate()));
			dbRecord.setUpdatedTimestamp(new Date());
			pickupRegistrationJpaRepository.save(dbRecord);
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, data.getPickupId(), null), HttpStatus.OK);
			PickupAssignmentEntity pickupAssigned = pickupAssignmentJpaRepository
					.findTopByPickupRegistrationAndActiveFlagOrderByIdDesc(dbRecord, 1);
			if (pickupAssigned != null) {
//				pickupAssignmentJpaRepository.delete(pickupAssigned.getId());
				pickupAssigned.setActiveFlag(0);
				logger.info("Inactivetd assigned pickup : " + pickupAssigned.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

//	Map<String, String> getShippingModes() {
//		Map<String, String> map = new HashMap<String, String>();
//		List<MShippingModeEntity> shippingModes = (List<MShippingModeEntity>) mShippingModeJpaRepository.findAll();
//		if (shippingModes != null && !shippingModes.isEmpty()) {
//			for (MShippingModeEntity item : shippingModes) {
//				map.put(item.getCode(), item.getDesc());
//			}
//		}
//		return map;
//	}
//
//	Map<String, String> getServiceTypes() {
//		Map<String, String> map = new HashMap<String, String>();
//		List<MServiceTypeEntity> serviceTypes = (List<MServiceTypeEntity>) mServiceTypeJpaRepository.findAll();
//		if (serviceTypes != null && !serviceTypes.isEmpty()) {
//			for (MServiceTypeEntity item : serviceTypes) {
//				map.put(item.getCode(), item.getServiceType());
//			}
//		}
//		return map;
//	}
//
//	private Map<String, String> getPickupStatus() {
//		Map<String, String> map = new HashMap<String, String>();
//		List<MPickupStatusEntity> statusTypes = (List<MPickupStatusEntity>) mPickupStatusJpaRepository.findAll();
//		if (statusTypes != null && !statusTypes.isEmpty()) {
//			for (MPickupStatusEntity item : statusTypes) {
//				map.put(item.getCode(), item.getDesc());
//			}
//		}
//		return map;
//	}
//
//	private Map<String, String> getUomValues() {
//		Map<String, String> map = new HashMap<String, String>();
//		List<MUomEntity> uomVals = (List<MUomEntity>) mUomJpaRepository.findAll();
//		if (uomVals != null && !uomVals.isEmpty()) {
//			for (MUomEntity item : uomVals) {
//				map.put(item.getUomValue(), item.getUomDisplayValue());
//			}
//		}
//		return map;
//	}

	public ResponseEntity<AppGeneralResponse> getPrsData(PrsRequestModel data, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		UserMasterEntity user  = userMasterJpaRepository.findByUserId(data.getUserId());
		Set<Integer> regset = new HashSet<>();
		if(user != null){
			
			try {
				List<PickupAssignmentEntity> assignedPickups = null;
				if (data.getPickupType() != null) {
					assignedPickups = pickupAssignmentJpaRepository.findByAssignedToAndPickupDateAndActiveFlagAndPickupType(
							data.getUserId(), shortformat.parse(data.getPickupDate()), 1, data.getPickupType());
				} else {
					assignedPickups = pickupAssignmentJpaRepository.findByAssignedToAndPickupDateAndActiveFlag(
							data.getUserId(), shortformat.parse(data.getPickupDate()), 1);
				}
				
				if(CommonUtility.check(user.getFranchiseBranchCode())){
					assignedPickups.addAll(pickupAssignmentJpaRepository.findByAssignedToAndActiveFlag(data.getUserId(),1));
				}
				
				if (assignedPickups != null && !assignedPickups.isEmpty()) {
					List<PickupRegistrationData> pickupList = new ArrayList<PickupRegistrationData>();

					Map<String, String> svcTypes = codeDescriptionServices.getServiceTypes();
					Map<String, String> shipModes = codeDescriptionServices.getShippingModes();
					Map<String, String> uomTypes = codeDescriptionServices.getUomValues();
					PickupRegistrationEntity registration = null;
					List<MConsignorEntity> dbConsignors = null;
					MConsignorEntity dbConsignor = null;
					for (PickupAssignmentEntity aPickup : assignedPickups) {
						
						try{
							
							registration = aPickup.getPickupRegistration();
							
							
							if(!regset.contains(registration.getId())){
								
								PickupRegistrationData pickup = new PickupRegistrationData();
								pickup.setAssignedTo(data.getUserId());
								pickup.setConsignorCode(registration.getConsignorCode());
								pickup.setCustId(registration.getCustId());
								dbConsignors = mConsignorJpaRepository.findByConsignorCode(registration.getConsignorCode());
								
								dbConsignor = null;
								
								if (dbConsignors != null && dbConsignors.size() > 0) {
									dbConsignor = dbConsignors.get(0);
								}
								
								if (dbConsignor != null) {
									Consignor consignor = new Consignor();
									consignor.setAddress1(dbConsignor.getConsignorAddress1());
									consignor.setAddress2(dbConsignor.getConsignorAddress2());
									consignor.setBranchCode(dbConsignor.getBranchCode());
									consignor.setCity(dbConsignor.getCity());
									consignor.setConsignorCode(dbConsignor.getConsignorCode());
									consignor.setConsignorName(dbConsignor.getConsignorName());
									consignor.setConsignorPincode(dbConsignor.getPincode());
									consignor.setState(dbConsignor.getStateCode());
									consignor.setCustCode(dbConsignor.getCustCode());
									consignor.setConsignorCode(dbConsignor.getConsignorCode());
									consignor.setContactEmailid(dbConsignor.getContactEmailid());
									consignor.setContactName(dbConsignor.getContactName());
									consignor.setContactPhoneno(dbConsignor.getContactPhoneno());
									consignor.setCustCode(dbConsignor.getMaCustomerDetails().getCustCode());
//							if (dbConsignor.getMCustomer() != null) {
//								if (dbConsignor.getMCustomer().getIsTbb() != null) {
//									consignor.setContractCustomer(
//											dbConsignor.getMCustomer().getIsTbb().intValue() == 1 ? true : false);
//								}
//							} else {
//								consignor.setContractCustomer(false);
//							}
//							if (dbConsignor.getMCustomer() != null) {
//								consignor.setCustomerId(dbConsignor.getMCustomer().getId());
//							}
									consignor.setConsignorId(dbConsignor.getId());
									
									pickup.setConsignor(consignor);
								}
								
								pickup.setConsignorName(registration.getConsignorName());
								pickup.setContactNumber(registration.getConsignorContactNo());
								pickup.setCustCode(registration.getCustCode());
								pickup.setCustName(registration.getCustomerName());
								pickup.setDeliveryPincode(registration.getDeliveryPincode());
								pickup.setDocketNo(registration.getDocketNo());
								pickup.setDoxFlag(registration.getDoxFlag());
//						pickup.setDoxFlagDesc(
//								registration.getDoxFlag() != null
//										? (registration.getDoxFlag().equals("D") ? Constants.DOX
//												: Constants.NONDOX)
//										: null);
								if(registration.getDoxFlag().equals(Constants.DOX_FLG)){
									pickup.setDoxFlagDesc(Constants.DOX_FLG);
								}
								else if(registration.getDoxFlag().equals(Constants.NON_DOX_FLG)){
									pickup.setDoxFlagDesc(Constants.NON_DOX_FLG);
								}
								
								pickup.setPickupDate(shortformat.format(registration.getPickupDate()));
								pickup.setPktCount(registration.getPktCount());
								pickup.setServiceType(registration.getSvcType());
								pickup.setServiceTypeDesc(svcTypes.get(registration.getSvcType()));
								pickup.setShipMode(registration.getShipMode());
								pickup.setShipModeDesc(shipModes.get(registration.getShipMode()));
								pickup.setUom(registration.getUom());
								pickup.setUomDesc(uomTypes.get(registration.getUom()));
								pickup.setPickupPincode(registration.getPickupPincode());
								pickup.setWt(registration.getWt());
								pickup.setPickupId(registration.getId());
								pickup.setPickupType(aPickup.getPickupType());
								pickup.setIsFranchise(registration.getIsFranchise());
								
								if(registration.getConsigneeId() != null){
									MConsigneeEntity mconsignee = mConsigneeJpaRepository.findOne(registration.getConsigneeId());
									if(mconsignee != null){
										Consignee consignee = new Consignee();
										consignee.setAddress1(mconsignee.getConsigneeAddress1());
										consignee.setAddress2(mconsignee.getConsigneeAddress2());
										consignee.setCity(mconsignee.getCity());
										consignee.setConsigneeName(mconsignee.getConsigneeName());
										consignee.setConsigneePincode(mconsignee.getPincode());
										consignee.setContactName(mconsignee.getContactName());
										consignee.setContactPhoneno(mconsignee.getContactPhoneno());
										consignee.setState(mconsignee.getStateCode());
										if(!CommonUtility.check(consignee.getState())){
											consignee.setState(mconsignee.getState());
										}
										pickup.setConsignee(consignee);
									}
								}
								
								if (registration.getHasSoftdata() != null
										&& registration.getHasSoftdata() == 1) {
									PickupRegnSoftDataModel softData = new PickupRegnSoftDataModel();
									PickupSoftdataDetailsEntity dbSoftData = registration
											.getListOfPickupSoftdataDetails().get(0);
									softData.setConsignmentValue(dbSoftData.getConsignmentValue());
									softData.setConsignorGstin(dbSoftData.getConsignorGstin());
									softData.setConsineeGstin(dbSoftData.getConsigneeGstin());
									softData.setStartPktNo(dbSoftData.getPktNoStart());
									softData.setEndPktNo(dbSoftData.getPktNoEnd());
									softData.setCodAmount(dbSoftData.getCodAmount());
									softData.setMaterial(dbSoftData.getMaterial());
									softData.setCustomerRefNo(dbSoftData.getPickupRegistration().getCustCode());
									if (dbSoftData.getInvoiceEwbAvlbl() != null && dbSoftData.getInvoiceEwbAvlbl() == 1) {
										List<PickupSoftInvEwbEntity> invList = registration
												.getListOfPickupSoftInvEwb();
										if (invList != null && !invList.isEmpty()) {
											List<PickupInvoiceEwb> invoiceList = new ArrayList<PickupInvoiceEwb>();
											for (PickupSoftInvEwbEntity inv : invList) {
												PickupInvoiceEwb invoice = new PickupInvoiceEwb();
												invoice.setEwbDate(shortformat.format(inv.getEwbDate()));
												invoice.setEwbNo(inv.getEwbNo());
												invoice.setEwbValidTill(shortformat.format(inv.getEwbValidTill()));
												invoice.setInvoiceDate(shortformat.format(inv.getInvDate()));
												invoice.setInvoiceNo(inv.getInvNo());
												invoiceList.add(invoice);
											}
											softData.setInvoiceDetails(invoiceList);
										}
									}
									if (dbSoftData.getCustPktDataAvlbl() != null && dbSoftData.getCustPktDataAvlbl() == 1) {
										List<PickupSoftPktRefEntity> ccPkts = registration.getListOfCustPkts();
										if (ccPkts != null && !ccPkts.isEmpty()) {
											List<CCPktModel> customerPktData = new ArrayList<CCPktModel>();
											for (PickupSoftPktRefEntity cPkt : ccPkts) {
												CCPktModel cp = new CCPktModel();
												cp.setCustomerPktNo(cPkt.getCustPktNo());
												cp.setPktNo(cPkt.getPktNo());
												customerPktData.add(cp);
											}
											softData.setPktData(customerPktData);
											
										}
									}
									if (dbSoftData.getLbhValuesAvlbl() != null && dbSoftData.getLbhValuesAvlbl() == 1) {
										List<PickupSoftLbhDataEntity> lbhList = registration
												.getListOfPickupSoftLbhData();
										if (lbhList != null && !lbhList.isEmpty()) {
											List<LbhData> lbhValues = new ArrayList<LbhData>();
											for (PickupSoftLbhDataEntity lbh : lbhList) {
												LbhData lbhData = new LbhData();
												lbhData.setCustomerPktRefNo(lbh.getCustPktRefNo());
												lbhData.setMaterial(lbh.getMaterial());
												lbhData.setPktCount(lbh.getPktCount());
												lbhData.setPktHt(lbh.getPktHeight());
												lbhData.setPktLen(lbh.getPktLen());
												lbhData.setPktWidth(lbh.getPktWidth());
												lbhData.setPktNo(lbh.getPktNo());
												lbhValues.add(lbhData);
											}
											softData.setLbhData(lbhValues);
										}
									}
									if (dbSoftData.getPaperWorkAvlbl() != null && dbSoftData.getPaperWorkAvlbl() == 1) {
										List<PickupSoftPaperworkEntity> paperworkList = registration
												.getListOfPickupSoftPaperwork();
										if (paperworkList != null && !paperworkList.isEmpty()) {
											List<PaperWork> paperworkData = new ArrayList<PaperWork>();
											for (PickupSoftPaperworkEntity pw : paperworkList) {
												PaperWork item = new PaperWork();
												item.setName(pw.getPaperWorkName());
												item.setRefNo(pw.getPaperWorkRefNo());
												item.setDocumentNo(pw.getPaperWorkDocumentName());
												paperworkData.add(item);
											}
											softData.setPaperWork(paperworkData);
										}
									}
									
									pickup.setSoftData(softData);
								}
								pickupList.add(pickup);
								if(registration != null){
									regset.add(registration.getId());
								}
							}
						}
						catch(Exception e){
							logger.info(e.getMessage(), e);
						}
						
					}
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, pickupList, null), HttpStatus.OK);
				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.info(e.getMessage(), e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
		}
		else{
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, "User not found", null, null), HttpStatus.OK);
		}
		

		return generalResponse;
	}

	public ResponseEntity<AppGeneralResponse> bookingSubmit(BookingDataModel data, Logger logger, String userId, String branchCode) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		MConsignorEntity consignor = null;
		MConsigneeEntity consignee = null;
		PickupRegistrationEntity registration = null;
		List<ErpPickupBookingResponse> erpresponselist = new ArrayList<>();
		Set<String> successDkts = new HashSet<>();
		String baseUrl = FilesUtil.getProperty("viewImageUrl");
		Map<String, BookingDataEntity> bookingmap = new HashMap<>();
		try {
			MaCustomerDetailsEntity customer = null;
			Map<Integer, MaCustomerDetailsEntity> customers = new HashMap<Integer, MaCustomerDetailsEntity>();
			List<BookingDocketDataModel> dockets = data.getDockets();
			
			if (dockets != null && !dockets.isEmpty()) {
				UserMasterEntity user = userMasterJpaRepository.findByUserId(userId);
				branchCode = user.getBranchCode();
				data.setBookingUserType(user.getUserType());
				
				if(data.getPickupId() != null && data.getPickupId() > 0){
					registration = pickupRegistrationJpaRepository.findOne(data.getPickupId());
					if(registration != null){
						data.setBookingPincode(registration.getPickupPincode());
					}
					else{
						return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Pickup registration not found", null, null), HttpStatus.OK);
					}
				}
				for (BookingDocketDataModel dkt : dockets) {
					
					List<BookingDataEntity> existingBooking = bookingDataJpaRepository.findByDocketNoAndErpStatus(dkt.getDocketNo(), 1);
					if(existingBooking != null && !existingBooking.isEmpty()){
						logger.info("DKT NO : "+dkt.getDocketNo()+" Booking with this AWB already exists. Please use different AWB for this booking.");
						return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Booking with this AWB already exists. Please use different AWB for this booking.", null, null),HttpStatus.OK);
					}
					
					if(customers.containsKey(dkt.getCustId())){
						customer = customers.get(dkt.getCustId());
					}
					else{
						customer = maCustomerDetailsJpaRepository.findOne(dkt.getCustId());
					}
						
					
//					Date bookingDate = longformat.parse(dkt.getPickupTimestamp());
					BookingDataEntity bookingEntity = new BookingDataEntity();
					
					bookingEntity.setPickupId(data.getPickupId());
					bookingEntity.setBookingAgentId(data.getBookingAgentId());
					bookingEntity.setBookingBranch(data.getBookingBranchCode());
					bookingEntity.setBookingPincode(data.getBookingPincode());
					bookingEntity.setImei(data.getImei());

					bookingEntity.setActualWt(dkt.getActualWt());
					bookingEntity.setChargedWt(dkt.getChargedWt());

					bookingEntity.setCodAmount(dkt.getCodAmount());

					if (dkt.getConsigneeId() == null || dkt.getConsigneeId() == 0) {
						consignee = persistConsignee(dkt.getConsignee());
						bookingEntity.setConsigneeId(consignee.getId());
					} else {
						consignee = mConsigneeJpaRepository.findOne(dkt.getConsigneeId());
						bookingEntity.setConsigneeId(dkt.getConsigneeId());
						dkt.setConsignee(EntityToModels.convertConsigneeEntityToModel(consignee));
					}

					if (dkt.getConsignorId() == null || dkt.getConsignorId() == 0) {
						MConsignorEntity topVersionConsignor = mConsignorJpaRepository.findTopByOrderByVersionDesc();
						Integer version = 0;
						if (topVersionConsignor != null) {
							version = topVersionConsignor.getVersion();
						}
						try{
							consignor = persistConsignor(dkt.getConsignor(), branchCode, version, customer);
							bookingEntity.setConsignorId(consignor.getId());
						}
						catch(Exception e){
							e.printStackTrace();
							logger.info(e.getMessage(), e);
							return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Failed to save consignor details", null, null), HttpStatus.OK);
						}
					} else {
						consignor = mConsignorJpaRepository.findOne(dkt.getConsignorId());
						consignor = EntityToModels.convertConsignorModelToEntity(dkt.getConsignor(), consignor);
						dkt.setConsignor(EntityToModels.convertConsignorEntityToModel(consignor));
						bookingEntity.setConsignorId(dkt.getConsignorId());
						
					}

					bookingEntity.setConsignorGstin(dkt.getConsignor().getGstin());
					bookingEntity.setConsigneeGstin(dkt.getConsignee().getGstin());
					
					bookingEntity.setConsignorId(dkt.getConsignorId());

					bookingEntity.setConsignmentValue(dkt.getConsignmentValue());

					bookingEntity.setCreatedTimestamp(new Date());
					bookingEntity.setCustCode(dkt.getCustCode());
					bookingEntity.setCustId(dkt.getCustId());
					bookingEntity.setCustomerRefNo(dkt.getCustomerRefNo());
					bookingEntity.setDeliveryBranch(dkt.getDestinationBranch());
					bookingEntity.setDeliveryPincode(dkt.getDeliveryPincode());
					bookingEntity.setDocketNo(dkt.getDocketNo());
					if(CommonUtility.check(dkt.getXBAwbNo())){
						bookingEntity.setXbAwbNo(dkt.getXBAwbNo());
					}
					bookingEntity.setDocketStatus(Constants.PICKED);
					bookingEntity.setDoxFlag(dkt.getDoxFlag());
					bookingEntity.setIsAppointmentDelivery(dkt.getIsAppointmentDelivery() == true ? 1 : 0);
					bookingEntity.setMaterial(dkt.getMaterial());
					bookingEntity.setPaymentMode(dkt.getPaymentMode());
					bookingEntity.setPickupId(data.getPickupId());
					if (dkt.getPickupTimestamp() != null && !dkt.getPickupTimestamp().trim().isEmpty()) {
						bookingEntity.setPickupTimestamp(longformat.parse(dkt.getPickupTimestamp()));
						dkt.setPickupTimestamp(erplongformat.format(bookingEntity.getPickupTimestamp()));
					}
					bookingEntity.setPktCount(dkt.getPktCount());
					bookingEntity.setPktNoStart(dkt.getStartPktNo());
					bookingEntity.setPktNoEnd(dkt.getEndPktNo());
					bookingEntity.setRemarks(dkt.getRemarks());
					bookingEntity.setShipMode(dkt.getShipMode());
					bookingEntity.setSvcType(dkt.getServiceType());
					bookingEntity.setTotalCharges(dkt.getTotalCharges());
					bookingEntity.setUom(dkt.getUom());
					bookingEntity.setVehicleNo(dkt.getVehicleNo());
					bookingEntity.setPaymentType(dkt.getPaymentType());
					bookingEntity.setManualPktSeries(dkt.getManualPktSeries() == true ? 1 : 0);
					if(CommonUtility.check(dkt.getXbOrigin())){
						bookingEntity.setXbOrigin(dkt.getXbOrigin());
					}
					if(CommonUtility.check(dkt.getXbDest())){
						bookingEntity.setXbDest(dkt.getXbDest());
					}
					bookingEntity.setRiskType(dkt.getRiskType());
					bookingEntity.setAppVersion(data.getVersion());
					bookingEntity = bookingDataJpaRepository.save(bookingEntity);
					bookingmap.put(bookingEntity.getDocketNo(), bookingEntity);
					markDocketConsumed(bookingEntity.getDocketNo());
					markPacketConsumed(bookingEntity.getPktNoEnd());
					//markPacket

					List<ChargesModel> charges = dkt.getCharges();
					if (charges != null && !charges.isEmpty()) {
						for (ChargesModel ch : charges) {
							BookingChargesEntity charge = new BookingChargesEntity();
							charge.setBookingData(bookingEntity);
							charge.setChargeAmount(ch.getAmount());
							charge.setChargeType(ch.getType());
							charge.setCreatedTimestamp(new Date());
							charge.setDocketNo(dkt.getDocketNo());
							List<TaxTypeAmount> taxes = ch.getTaxes();
							if (taxes != null && !taxes.isEmpty()) {
								Double taxAmt = 0.0;
								String taxDtls = "";
								for (TaxTypeAmount tx : taxes) {
									taxAmt += tx.getTaxAmount();
									taxDtls = taxDtls.concat(tx.getTaxType() + "-" + tx.getTaxAmount() + "; ");
								}
								charge.setTaxAmount(taxAmt);
								charge.setTaxDetails(taxDtls);
							}
							bookingChargesJpaRepository.save(charge);
						}
					}

					List<PickupInvoiceEwb> invoiceDtls = dkt.getInvoiceDetails();
					if (invoiceDtls != null && !invoiceDtls.isEmpty()) {
						String inimagepath = null;
						String filename = null;
						for (PickupInvoiceEwb inv : invoiceDtls) {
							BookingInvoicesEntity invoice = new BookingInvoicesEntity();
							invoice.setBookingData(bookingEntity);
							invoice.setCreatedTimestamp(new Date());
							invoice.setDocketNo(dkt.getDocketNo());
							if (inv.getEwbDate() != null && !inv.getEwbDate().trim().isEmpty()) {
								invoice.setEwbDate(shortformat.parse(inv.getEwbDate()));
								inv.setEwbDate(erplongformat.format(invoice.getEwbDate()));
							}
							invoice.setEwbNo(inv.getEwbNo());
							if (inv.getEwbValidTill() != null && !inv.getEwbValidTill().trim().isEmpty()) {
								invoice.setEwbValidTill(shortformat.parse(inv.getEwbValidTill()));
								inv.setEwbValidTill(erplongformat.format(invoice.getEwbValidTill()));
							}
							if (inv.getInvoiceDate() != null && !inv.getInvoiceDate().trim().isEmpty()) {
								invoice.setInvDate(shortformat.parse(inv.getInvoiceDate()));
								inv.setInvoiceDate(erplongformat.format(invoice.getInvDate()));
							}
							invoice.setInvNo(inv.getInvoiceNo());
							invoice.setInvAmout(inv.getInvoiceAmount());
							invoice.setVehicleNo(inv.getVehicleNo());

//							if (CommonUtility.check(inv.getBase64InvoiceImage())) {
							if (CommonUtility.check(inv.getBase64Data())) {
								
								filename = CommonUtility.toImgFromBase64(inv.getBase64Data(),
										inv.getInvoiceNo().replaceAll("[^a-zA-Z0-9]", ""), "png", 2, "Invoice");
//								inimagepath = baseUrl.concat(Constants.PICKUP_IMGS_API).concat(filename);
								if(CommonUtility.check(baseUrl)){
									inimagepath = baseUrl;
									inimagepath = inimagepath.replace("<?filepath?>", filename).replace("<?dktno?>", bookingEntity.getDocketNo());
								}
								invoice.setInvoiceImage(filename);
								invoice.setInvoiceImagePath(inimagepath);
								inv.setInvoiceImageURL(inimagepath);
							}
							bookingInvoicesJpaRepository.save(invoice);
						}
					}

					List<LbhData> lbhDetails = dkt.getLbhData();
					if (lbhDetails != null && !lbhDetails.isEmpty()) {
						for (LbhData lbh : lbhDetails) {
							BookingLbhEntity lbhData = new BookingLbhEntity();
							lbhData.setCreatedTimestamp(new Date());
							lbhData.setActualWeight(lbh.getActualWt());
							lbhData.setChargedWeight(lbh.getChargedWt());
							lbhData.setCustPktRefNo(lbh.getCustomerPktRefNo());
							lbhData.setMaterial(lbh.getMaterial());
							lbhData.setPackagingType(lbh.getPackagingType());
							lbhData.setPktCount(lbh.getPktCount());
							lbhData.setPktHeight(lbh.getPktHt());
							lbhData.setPktLen(lbh.getPktLen());
							lbhData.setPktWidth(lbh.getPktWidth());
							lbhData.setPktNo(lbh.getPktNo());
							lbhData.setUom(lbh.getUom());
							lbhData.setBookingData(bookingEntity);
							lbhData.setDocketNo(bookingEntity.getDocketNo());
							bookingLbhJpaRepository.save(lbhData);
						}
					}

					List<PaperWork> paperWork = dkt.getPaperWork();
					if (paperWork != null && !paperWork.isEmpty()) {
						String pwimagepath = null;
						String filename = null;
						int i = 0;
						for (PaperWork pw : paperWork) {
							String pwImg = pw.getBase64Data();
							if (CommonUtility.check(pwImg)) {
								i++;
								pwimagepath = null;
								BookingPaperworkEntity bpw = new BookingPaperworkEntity();
//								bpw.setBookingData(bookingEntity);
								bpw.setDocketNo(dkt.getDocketNo());
								bpw.setBookingId(bookingEntity.getId());
								bpw.setCreatedTimestamp(new Date());
								
								filename = CommonUtility.toImgFromBase64(pw.getBase64Data(), pw.getName().replaceAll("[^a-zA-Z0-9]", "")+""+i,
										"png", 2, "PaperWork");
//								pwimagepath = baseUrl.concat(Constants.PICKUP_IMGS_API).concat(filename);
								
								if(CommonUtility.check(baseUrl)){
									pwimagepath = baseUrl;
									pwimagepath = pwimagepath.replace("<?filepath?>", filename).replace("<?dktno?>", bookingEntity.getDocketNo());
								}
								
								bpw.setImagePath(pwimagepath);
								bpw.setImageName(pw.getName());
								bpw.setPaperWorkName(pw.getName());
								bpw.setPaperWorkRefNo(pw.getDocumentNo());
								bookingPaperworkJpaRepository.save(bpw);
								pw.setPaperWorkImageUrl(pwimagepath);
								pw.setBase64Data(null);
							}
							
						}
					}
					
					List<PickupScanDataModel> scanData = dkt.getPickupScanData();
					if (scanData != null && !scanData.isEmpty()) {
						for (PickupScanDataModel ps : scanData) {
							BookingPkupScanDataEntity dbPickupScan = new BookingPkupScanDataEntity();
							dbPickupScan.setCreatedTimestamp(new Date());
							dbPickupScan.setDocketNo(dkt.getDocketNo());
							dbPickupScan.setManualScanFlag(ps.getManualScanFlag());
							dbPickupScan.setPktNo(ps.getPktNo());
							dbPickupScan.setSerialNo(ps.getSerialNo());
							bookingPkupScanDataJpaRepository.save(dbPickupScan);
						}
					}
					
					try{
						if(dkt.getPcsList() != null && !dkt.getPcsList().isEmpty()){
//							MPktMasterEntity pkt = mPktMasterJpaRepository.findByUserId("XB");
							List<BookingXbPktsEntity> xbpcs = new ArrayList<>();
							BookingXbPktsEntity x = null;
							for(XbPcsDataModel a : dkt.getPcsList()){
								x = new BookingXbPktsEntity();
								x.setBookingId(bookingEntity.getId());
								x.setCreatedTimestamp(new Date());
								x.setDocketNo(bookingEntity.getDocketNo());
								x.setPktNo(a.getRefNo());
								x.setXbAwbNo(bookingEntity.getXbAwbNo());
								x.setXbPktNo(a.getXBrefNo());
								a.setXBrefNo(x.getXbPktNo());
								xbpcs.add(x);
							}
							bookingXbPktsJpaRepository.save(xbpcs);
						}
					}
					catch(Exception x){
						x.printStackTrace();
						logger.info(x.getMessage(), x);
					}
					
				}
				if (data.getCloseFlag() && data.getPickupId() != null && data.getPickupId() > 0) {
					PickupRegistrationEntity pickupReg = pickupRegistrationJpaRepository.findOne(data.getPickupId());
//					UserMasterEntity user = userMasterJpaRepository.findByUserId(pickupReg.getUserId());
					if (pickupReg != null && user != null && (pickupReg.getIsFranchise() == null || pickupReg.getIsFranchise() == 0 || !CommonUtility.check(user.getFranchiseBranchCode()))) {
						pickupReg.setPickupStatus(Constants.PICKUP_STATUS_COMPLETED);
						pickupReg.setActiveFlag(0);
						pickupRegistrationJpaRepository.save(pickupReg);
	
						PickupAssignmentEntity pickupAssigned = pickupAssignmentJpaRepository.findByPickupRegistration(pickupReg);
						pickupAssigned.setActiveFlag(0);
						pickupAssignmentJpaRepository.save(pickupAssigned);
					}
				}
				String erpMessage = "";
				boolean erpResult = false;
				try{
					String url = null;
					String doxFlg = data.getDockets().get(0).getDoxFlag();
					
					if(data.getPickupId() != null && data.getPickupId() > 0 && CommonUtility.check(doxFlg) && Constants.DOX_FLG.equalsIgnoreCase(doxFlg)){
						url = FilesUtil.getProperty("erpPickupBookingDOX");
					}
					else if(!CommonUtility.check(data.getVersion())){
						url = FilesUtil.getProperty("erpPickupBooking");
					}
					else{
						url = FilesUtil.getProperty("erpPickupBookingNew");
					}
						
					if(registration != null && CommonUtility.check(registration.getErpPickupOrderId()))
						data.setPickupId(Integer.parseInt(registration.getErpPickupOrderId()));
					String erpResponseString = CommonUtility.postDataToErp(gson.toJson(data), url,logger);
					TypeToken<List<ErpPickupBookingResponse>> datasetType = new TypeToken<List<ErpPickupBookingResponse>>() {};
					erpresponselist = gson.fromJson(erpResponseString, datasetType.getType());
					if(erpresponselist != null && !erpresponselist.isEmpty()){
						try{
							for(ErpPickupBookingResponse r : erpresponselist){
								if(r.getResult() != null && r.getResult()){
									erpResult = true;
									erpMessage = ResponseMessages.SUCCESS;
									successDkts.add(r.getAWBNumber());
									if(bookingmap.containsKey(r.getAWBNumber())){
										bookingmap.get(r.getAWBNumber()).setErpStatus(1);
										if(CommonUtility.check(r.getMessege())){
											if(r.getMessege().length() > 125){
												bookingmap.get(r.getAWBNumber()).setErpMessage(r.getMessege().substring(0, 120));
											}
											else{
												bookingmap.get(r.getAWBNumber()).setErpMessage(r.getMessege());
											}
										}
										bookingDataJpaRepository.save(bookingmap.get(r.getAWBNumber()));
									}
								}else{
									erpMessage = r.getMessege();
								}
							}
							
						}
						catch(Exception e){
							erpMessage = "Failed : FAILED TO SAVE ERP RESPONSE";
							e.printStackTrace();
							logger.info("!!! FAILED TO SAVE ERP RESPONSE !!!");
						}
					}
					else{
						erpMessage = "Failed : No response from ERP";
						logger.info("!!! FAILED TO PARSE ERP RESPONSE !!!");
					}
				}
				catch(Exception e){
					erpMessage = "Failed : Failed to send to ERP";
					e.printStackTrace();
					logger.info("EXCEPTION : SENDING TO ERP"+ e.getMessage(), e);
				}

				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(erpResult, erpMessage, null, null), HttpStatus.OK);
			}

		} catch (Exception e) {
			logger.info(e.getMessage(), e);
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	private void markDocketConsumed(String docketNo) {
		// TODO Auto-generated method stub
		try{
			MDktMasterEntity dkt = mDktMasterJpaRepository.findByDktNo(docketNo);
			if (dkt != null) {
				dkt.setConsumed(1);
				mDktMasterJpaRepository.save(dkt);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void markPacketConsumed(String endPktNo) {
		// TODO Auto-generated method stub
//		Integer lastUsedPktNo = Integer.parseInt(endPktNo);
		if(CommonUtility.check(endPktNo)){
			
			try{
				Long lastUsedPktNo = Long.parseLong(endPktNo);
				MPktMasterEntity pkt = mPktMasterJpaRepository.findByStartPktNoLessThanEqualAndEndPktNoGreaterThanEqual(lastUsedPktNo, lastUsedPktNo);
				if(pkt != null){
					Long dbLastPktUsed = pkt.getLastPktUsed();
					if (dbLastPktUsed == null) {
						dbLastPktUsed = pkt.getStartPktNo() -1;
					}
					if (lastUsedPktNo.compareTo(dbLastPktUsed) > 0) {	// just in case if dockets are coming in wrong order - to avoid overriding already consumed
						pkt.setLastPktUsed(lastUsedPktNo);
						if (lastUsedPktNo.equals(pkt.getEndPktNo())) {
							pkt.setCompleteFlag(1);
						}
						mPktMasterJpaRepository.save(pkt);
					}
				}
			}
			catch(Exception e){
//			e.printStackTrace();
			}
		}
	}

	public ResponseEntity<AppGeneralResponse> pickupScan(PickupScanSubmitDataModel data, String userId, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PickupScanDataModel> scanData = data.getScanData();
			if (scanData != null && !scanData.isEmpty()) {
				BookingPkupScanSummaryEntity dbPickupSummary = new BookingPkupScanSummaryEntity();
				dbPickupSummary.setAgentCode(userId);
				dbPickupSummary.setBranchCode("");
				dbPickupSummary.setCreatedTimestamp(new Date());
				dbPickupSummary.setCustCode(data.getCustCode());
				dbPickupSummary.setDispatchPersonName(data.getDispatchPersonName());
				dbPickupSummary.setDispatchPersonNo(data.getDispatchPersonName());
				dbPickupSummary.setImei(data.getDispatchContactNo());
				dbPickupSummary.setNoDkts(getDktCount(data.getScanData()));
				dbPickupSummary.setNoPkts(data.getScanData().size());
				BookingPkupScanSummaryEntity savedPupSummEntity = bookingPkupScanSummaryJpaRepository.save(dbPickupSummary);
				
				for (PickupScanDataModel ps : scanData) {
					BookingPkupScanDataEntity dbPickupScan = new BookingPkupScanDataEntity();
					dbPickupScan.setCreatedTimestamp(new Date());
					dbPickupScan.setDocketNo(ps.getDocketNo());
					dbPickupScan.setManualScanFlag(ps.getManualScanFlag());
					dbPickupScan.setPktNo(ps.getPktNo());
					dbPickupScan.setSerialNo(ps.getSerialNo());
					dbPickupScan.setRefNo(ps.getCustomerPktNo());
					dbPickupScan.setHasDeps(ps.getHasDeps() == true ? 1 : 0);
					dbPickupScan.setDepsReason(ps.getDepsReason());
					dbPickupScan = bookingPkupScanDataJpaRepository.save(dbPickupScan);
					
					if (ps.getDepsImages() != null && !ps.getDepsImages().isEmpty()) {
						int i = 0;
						for (String depsImg : ps.getDepsImages()) {
							if (CommonUtility.check(depsImg)) {
								String filename = ps.getDocketNo() + "_deps_" + i+1;
								String savedFileName = CommonUtility.toImgFromBase64(ps.getDepsImages().get(i), filename, "png", 4, "DEPS");
								BookingPickupScanDepsImagesEntity depsImage = new BookingPickupScanDepsImagesEntity();
								depsImage.setCreatedOn(new Date());
								depsImage.setDktNo(ps.getDocketNo());
								depsImage.setImage(savedFileName);
								bookingPickupScanDepsImagesJpaRepository.save(depsImage);
							}
						}
					}
					dbPickupScan.setSummaryId(savedPupSummEntity.getId());
					try {
						dbPickupScan.setScanTime(shortformat.parse(ps.getScanTime()));
					}catch(Exception e) {
						dbPickupScan.setScanTime(new Date());
					}
					bookingPkupScanDataJpaRepository.save(dbPickupScan);
				}
			}
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Exception : " + e.getStackTrace().toString());
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	private int getDktCount(List<PickupScanDataModel> data) {
		Set<String> dktSet = new HashSet<String>();
		for (PickupScanDataModel item: data) {
			dktSet.add(item.getDocketNo());
		}
		return dktSet.size();
	}

	public ErpDktValidationModel validateDktNo(DktValidationModel reqData, Gson gson, Logger log, int newApi) throws Exception {

		List<BookingDataEntity> booking = bookingDataJpaRepository.findByDocketNo(reqData.getDktNo());
		ErpDktValidationModel erpmodel = new ErpDktValidationModel();
		
		if(booking != null && !booking.isEmpty()){
			erpmodel.setResult(false);
			erpmodel.setMsg("Docket number used");
		}
		else{
			erpmodel.setAWBNo(reqData.getDktNo());
			erpmodel.setCustCode(reqData.getCustCode());
			erpmodel.setUserId(reqData.getUserId());
			erpmodel.setUserType(reqData.getUserType());
			erpmodel.setBranchCode(reqData.getBranchCode());
			erpmodel.setDocType(reqData.getDocType());
			if(reqData.getDestPin() != null)
				erpmodel.setDestPin(reqData.getDestPin().toString());
				
			String url = null;
			if(newApi == 1){
				url = FilesUtil.getProperty("erpDktValidationNew");
			}else{
				
				url = FilesUtil.getProperty("erpDktValidation");
			}
			String resString = CommonUtility.postDataToErp(gson.toJson(erpmodel), url, log);
			log.info("ERP RES : "+resString);
			erpmodel = gson.fromJson(resString, ErpDktValidationModel.class);
			
		}
		
		return erpmodel;
	}
	
	public void generatePickupRegistrations(PickupRegistrationEntity regEntity, String franchiseBranchCode, PickupRegistrationSubmit data){
		Logger log = Logger.getLogger("GeneratePickups");
		List<PickupRegistrationEntity> newregs = new ArrayList<>();
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{
			
			List<UserMasterEntity> users = userMasterJpaRepository.findByFranchiseBranchCodeAndActiveFlag(franchiseBranchCode, 1);
			Map<String, UserMasterEntity> usermap = new HashMap<>();
			if(users != null && !users.isEmpty()){
				
				for(UserMasterEntity u : users){
					if(regEntity.getUserId().equalsIgnoreCase(u.getUserId())){
						continue;
					}else{
						usermap.put(u.getUserId(), u);
					}
					PickupRegistrationEntity newpickup = new PickupRegistrationEntity();
					BeanUtils.copyProperties(regEntity, newpickup);
					newpickup.setId(null);
					newpickup.setIsFranchise(0);
					newpickup.setUserId(u.getUserId());
					
					try {
						data.setUserId(u.getUserId());
						data.setPickupDate(newpickup.getPickupDate() != null ? erpshortformat.format(newpickup.getPickupDate()) : null);
						
						String url = FilesUtil.getProperty("erpPickupRegistration");
						String erpResponseString = CommonUtility.postDataToErp(gson.toJson(data), url, log);
						ErpResponsePickupRegistration responseObj = gson.fromJson(erpResponseString,
								ErpResponsePickupRegistration.class);
						newpickup.setErpStatus(responseObj.getResult() ? 1 : 0);
						newpickup.setErpMessage(responseObj.getMessege());
						newpickup.setErpPickupOrderId(responseObj.getPickUpOrderId());

					} catch (Exception e) {
						e.printStackTrace();
						newpickup.setErpStatus(0);
						newpickup.setErpMessage(ResponseMessages.INTERNAL_SERVER_ERROR);
						log.info("EXCEPTION : SENDING TO ERP");
						log.info(e.getMessage(), e);
					}
					
					newregs.add(newpickup);
				}
				
				newregs = (List<PickupRegistrationEntity>) pickupRegistrationJpaRepository.save(newregs);
				
				List<PickupAssignmentEntity> assignedlist = new ArrayList<>();
				for(PickupRegistrationEntity p : newregs){	
					if(p.getErpStatus() != null && p.getErpStatus() == 1){						
						PickupAssignmentEntity dbPickupAssigned = new PickupAssignmentEntity();
						dbPickupAssigned.setAssignedTo(p.getUserId());
						dbPickupAssigned.setAssUsrTyp(usermap.get(p.getUserId()).getUserType());
						dbPickupAssigned.setCreatedTimestamp(new Date());
						dbPickupAssigned.setPickupRegistration(p);
						dbPickupAssigned.setActiveFlag(1);
						dbPickupAssigned.setAssignedBy(regEntity.getUserId());
						dbPickupAssigned.setPickupType(p.getPickupType());
						
						try {
							dbPickupAssigned.setPickupDate(p.getPickupDate());
						} catch (Exception e) {
							e.printStackTrace();
						}
						assignedlist.add(dbPickupAssigned);
						
						p.setPickupStatus(Constants.PICKUP_STATUS_ASSIGNED);
						
					}
				}

				pickupAssignmentJpaRepository.save(assignedlist);
				pickupRegistrationJpaRepository.save(newregs);
				log.info("DONE");
				
			}else{
				log.info("No users for franchise : "+franchiseBranchCode);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(),e);
		}
	}
	
	ResponseEntity<AppGeneralResponse> savePickupRegistration(PickupRegistrationSubmit data, Logger log) {
		
		try{
			if (CommonUtility.check(data.getCustCode())) {
				
				UserMasterEntity user = userMasterJpaRepository.findByUserId(data.getUserId());
				
				if(user != null){
					
					MaCustomerDetailsEntity customer = maCustomerDetailsJpaRepository.findByCustCode(data.getCustCode());
					if (customer != null) {
						data.setCustomerId(customer.getId());
						PickupRegistrationEntity registerEntity = new PickupRegistrationEntity();
						
						registerEntity.setErpPickupOrderId(data.getPickupID());
						registerEntity.setErpPickupOrderRef(data.getPickupOrderNo());
						registerEntity.setActiveFlag(1);
						
						//consignee
						if(data.getConsignee() != null){
							Consignee consignee = data.getConsignee();
							if (consignee.getConsigneeId() != null
									&& consignee.getConsigneeId() > 0) {
								registerEntity.setConsigneeCode(consignee.getConsigneeCode());
								registerEntity.setConsigneeId(consignee.getConsigneeId());
							} else if(CommonUtility.check(consignee.getConsigneeName(), consignee.getAddress1()) && data.getConsignee().getConsigneePincode() != null) {
								MConsigneeEntity consigneeEntity = persistConsignee(data.getConsignee());
								registerEntity.setConsigneeId(consigneeEntity.getId());
							}
						}
						
						//consignor
						if(data.getConsignor() != null){
							if (data.getConsignor().getConsignorId() != null
									&& data.getConsignor().getConsignorId() > 0) {
								registerEntity.setConsignorCode(data.getConsignor().getConsignorCode());
								registerEntity.setConsignorId(data.getConsignor().getConsignorId());
							} else {
								
								MConsignorEntity topVersionConsignor = mConsignorJpaRepository.findTopByOrderByVersionDesc();
								Integer version = 0;
								if (topVersionConsignor != null) {
									version = topVersionConsignor.getVersion();
								}
								String consignorBranch = maPincodeJpaRepository.getBranchCodeByPincode(data.getConsignor().getConsignorPincode());
								MConsignorEntity consignor = null;
								try{
									consignor = persistConsignor(data.getConsignor(), consignorBranch, version, customer);
									if (consignor != null) {
										registerEntity.setConsignorId(consignor.getId());
										registerEntity.setConsignorCode(consignor.getConsignorCode());
										data.setConsignor(EntityToModels.convertConsignorEntityToModel(consignor));
									}
									else{
										return null;
									}
								}
								catch(Exception e){
									log.info("PICKUP ID : "+registerEntity.getErpPickupOrderId()+"-"+e.getMessage(), e);
									return null;
								}
							}
						}
						registerEntity.setUserId(user.getUserId());
						registerEntity.setConsignorCode(data.getConsignor().getConsignorCode());
						registerEntity.setConsignorName(data.getConsignor().getConsignorName());
						registerEntity.setConsignorContactNo(data.getConsignor().getContactPhoneno());
						registerEntity.setCustomerName(data.getCustomerName());
						registerEntity.setPickupPincode(data.getOriginPincode());
						registerEntity.setDocketNo(data.getDocketNo());
						registerEntity.setCreatedTimestamp(new Date());
						registerEntity.setCustCode(data.getCustCode());
						registerEntity.setCustId(customer.getId());
						registerEntity.setDeliveryPincode(data.getDeliveryPincode());
						registerEntity.setDoxFlag(data.getDoxNonDox());
						registerEntity.setBillingFlag(data.getBillingFlag());
						registerEntity.setIsRecurring(data.getIsRecurring() ? 1 : 0);
						registerEntity.setOdaArea(data.getOdaArea());
						registerEntity.setOdaFlag(data.isOdaFlag() ? 1 : 0);
						if (CommonUtility.check(data.getPickupDate())) {
							registerEntity.setPickupDate(erpshortformat.parse(data.getPickupDate()));
						}
						registerEntity.setPickupStatus(Constants.PICKUP_STATUS_PENDING);
						registerEntity.setPickupPincode(data.getOriginPincode());
						registerEntity.setPickupSlot(data.getPickupSlot());
						registerEntity.setPktCount(data.getPktCount());
						registerEntity.setRecurFrequency(data.getRecurringFrequency());
						registerEntity.setShipMode(data.getShipMode());
						registerEntity.setSvcType(data.getSvcType());
						registerEntity.setUom(data.getUom());
						registerEntity.setWt(data.getWt());
						if (data.getPickupSoftData() == null) {
							registerEntity.setPickupType("N"); // Normal
																		// pickup
						}

						if (data.getPickupSoftData() != null) {
							registerEntity.setHasSoftdata(1);
							PickupRegnSoftDataModel softData = data.getPickupSoftData();
							if (softData.getCustomerRefNo() != null
									|| (softData.getPktData() != null && !softData.getPktData().isEmpty())) {
								registerEntity.setPickupType("I"); // Interchange
							} else {
								registerEntity.setPickupType("B");// Bulk
							}
						}

						data.setPickupDate(registerEntity.getPickupDate() != null ? erpshortformat.format(registerEntity.getPickupDate()) : null);
							
						registerEntity.setErpStatus(1);
						registerEntity.setIsFranchise(0);

						registerEntity = pickupRegistrationJpaRepository.save(registerEntity);

						if (data.getInsuranceDetails() != null) {
							PickupInsuranceDetails ins = data.getInsuranceDetails();
							PickupInsuranceDetailsEntity insurance = new PickupInsuranceDetailsEntity();
							insurance.setInsuranceCompany(ins.getInsuranceCompany());
							insurance.setInsuredPerson(ins.getInsuredPerson());
							insurance.setPickupRegistration(registerEntity);
							insurance.setPolicyNumber(ins.getPolicyNumber());
							insurance.setStartDate(erpshortformat.parse(ins.getStartDate()));
							insurance.setEndDate(erpshortformat.parse(ins.getEndDate()));
						}

						if (data.getPickupSoftData() != null) {
							PickupRegnSoftDataModel softData = data.getPickupSoftData();

							PickupSoftdataDetailsEntity psd = new PickupSoftdataDetailsEntity();
							psd.setCodAmount(softData.getCodAmount());
							psd.setMaterial(softData.getMaterial());
							psd.setPktNoStart(softData.getStartPktNo());
							psd.setPktNoEnd(softData.getEndPktNo());
							psd.setPickupDate(erpshortformat.parse((data.getPickupDate())));
							psd.setPickupRegistration(registerEntity);
							psd = pickupSoftdataDetailsJpaRepository.save(psd);

							if (softData.getPktData() != null && !softData.getPktData().isEmpty()) {
								psd.setCustPktDataAvlbl(1);
								for (CCPktModel ccPkt : softData.getPktData()) {
									PickupSoftPktRefEntity ccPktEntity = new PickupSoftPktRefEntity();
									ccPktEntity.setCustPktNo(ccPkt.getCustomerPktNo());
									ccPktEntity.setPktNo(ccPkt.getPktNo());
									ccPktEntity.setPickupRegistration(registerEntity);
									ccPktEntity.setCreatedTimestamp(new Date());
									ccPktEntity.setDocketNo(registerEntity.getDocketNo());
									pickupSoftPktRefJpaRepository.save(ccPktEntity);
								}
							}

							if (softData.getInvoiceDetails() != null && !softData.getInvoiceDetails().isEmpty()) {
								psd.setInvoiceEwbAvlbl(1);
								for (PickupInvoiceEwb inv : softData.getInvoiceDetails()) {
									PickupSoftInvEwbEntity invoice = new PickupSoftInvEwbEntity();
									if (inv.getEwbDate() != null) {
										invoice.setEwbDate(erpshortformat.parse(inv.getEwbDate()));
									}
									invoice.setEwbNo(inv.getEwbNo());
									if (inv.getEwbValidTill() != null) {
										invoice.setEwbValidTill(erpshortformat.parse(inv.getEwbValidTill()));
									}
									if (inv.getInvoiceDate() != null) {
										invoice.setInvDate(erpshortformat.parse(inv.getInvoiceDate()));
									}
									invoice.setInvNo(inv.getInvoiceNo());
									invoice.setPickupDate(erpshortformat.parse((data.getPickupDate())));
									invoice.setPickupRegistration(registerEntity);
									pickupSoftInvEwbJpaRepository.save(invoice);
								}
							}

							if (softData.getPaperWork() != null && !softData.getPaperWork().isEmpty()) {
								psd.setPaperWorkAvlbl(1);
								for (PaperWork pw : softData.getPaperWork()) {
									PickupSoftPaperworkEntity paper = new PickupSoftPaperworkEntity();
									paper.setPaperWorkDocumentName(pw.getName());
									paper.setPaperWorkName(pw.getName());
									paper.setPaperWorkRefNo(pw.getRefNo());
									paper.setPickupDate(erpshortformat.parse((data.getPickupDate())));

									String imgData = pw.getBase64Data();
									if (CommonUtility.check(imgData)) {
										String filename = CommonUtility.toImgFromBase64(imgData,
												data.getDocketNo() + "-PR-", "png", 2, "PW");
										paper.setDocPath(filename);
									}
									paper.setPickupRegistration(registerEntity);
									pickupSoftPaperworkJpaRepository.save(paper);
								}
							}

							if (softData.getLbhData() != null && !softData.getLbhData().isEmpty()) {
								psd.setLbhValuesAvlbl(1);
								for (LbhData lbh : softData.getLbhData()) {
									PickupSoftLbhDataEntity lbhData = new PickupSoftLbhDataEntity();
									lbhData.setActualWeight(lbh.getActualWt());
									lbhData.setCustPktRefNo(lbh.getCustomerPktRefNo());
									lbhData.setMaterial(lbh.getMaterial());
									lbhData.setPickupDate(erpshortformat.parse((data.getPickupDate())));
									lbhData.setPktCount(lbh.getPktCount());
									lbhData.setPktHeight(lbh.getPktHt());
									lbhData.setPktLen(lbh.getPktLen());
									lbhData.setPktWidth(lbh.getPktWidth());
									lbhData.setPktNo(lbh.getPktNo());
									lbhData.setUom(lbh.getUom());
									lbhData.setPickupRegistration(registerEntity);
									pickupSoftLbhDataJpaRepository.save(lbhData);
								}
							}
							pickupSoftdataDetailsJpaRepository.save(psd);
						}


						PickupAssignmentEntity dbPickupAssigned = new PickupAssignmentEntity();
						dbPickupAssigned.setAssignedTo(user.getUserId());
						dbPickupAssigned.setAssUsrTyp(user.getUserType());
						dbPickupAssigned.setCreatedTimestamp(new Date());
						dbPickupAssigned.setPickupRegistration(registerEntity);
						dbPickupAssigned.setActiveFlag(1);
						if (registerEntity.getHasSoftdata() != null &&
								registerEntity.getHasSoftdata() == 1) {
							if (registerEntity.getCustRefNo() != null) {
								dbPickupAssigned.setPickupType("I"); // Interchange
							 } else {
								 dbPickupAssigned.setPickupType("B");// Bulk
							 }
						} else {
							dbPickupAssigned.setPickupType("N");
						}

						dbPickupAssigned.setAssignedBy(user.getUserId());
						dbPickupAssigned.setPickupType(registerEntity.getPickupType());
						dbPickupAssigned.setPickupDate(registerEntity.getPickupDate());
						pickupAssignmentJpaRepository.save(dbPickupAssigned);

						registerEntity.setPickupStatus(Constants.PICKUP_STATUS_ASSIGNED);
						pickupRegistrationJpaRepository.save(registerEntity);

						return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true,
								ResponseMessages.SUCCESS, registerEntity.getId(), null),
								HttpStatus.OK);
					} else {
						return new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, "Customer not found", null, null), HttpStatus.OK);
					}
				}
				else{
					return new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, "User not found", null, null), HttpStatus.OK);
				}

			} else {
				return new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "Customer code not sent", null, null), HttpStatus.OK);
			}
		}catch(Exception e){
			e.printStackTrace();
			log.info("PICKUP ID : "+data.getPickupID()+"-"+e.getMessage(), e);
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			
		}
	}
	
	ResponseEntity<AppGeneralResponse>  updatePickupRegister(PickupRegistrationEntity p, PickupRegistrationSubmit data, Logger log){
		try{
			Date pickupdate = erpshortformat.parse(data.getPickupDate());
			if(p.getPickupDate().after(pickupdate) || p.getPickupDate().before(pickupdate)){
				p.setPickupDate(pickupdate);
				p.setUserId(data.getUserId());
				pickupRegistrationJpaRepository.save(p);
				
				PickupAssignmentEntity dbPickupAssigned = pickupAssignmentJpaRepository.findByPickupRegistration(p);
				if(dbPickupAssigned != null){
					dbPickupAssigned.setActiveFlag(1);
					dbPickupAssigned.setAssignedTo(p.getUserId());
					dbPickupAssigned.setPickupDate(pickupdate);
					pickupAssignmentJpaRepository.save(dbPickupAssigned);
				}
				return new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, data.getPickupID()+"- Updated", null, null), HttpStatus.OK);
			}else{
				log.info("PICKUP ID : "+data.getPickupID()+"- No change identified");
				return new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, data.getPickupID()+" : No changes", null, null), HttpStatus.OK);
			}
		}catch(Exception e){
			log.info("PICKUP ID : "+data.getPickupID()+"-"+e.getMessage(), e);
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
	}

	public ResponseEntity<AppGeneralResponse> getErpAssignedPickups() {
		
		Logger log = Logger.getLogger("GetErpAssignedPickups");
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{
			
			String erpurl = FilesUtil.getProperty("erpAssignedPickups");
			
			WSResponseObjectModel response = CommonUtility.doWSRequest(erpurl, "", Constants.HTTP_GET, log);
			log.info("data:" + response.getResponse());
			
			if(response.getResponseCode() == Constants.HTTP_OK){
				
				TypeToken<List<PickupRegistrationSubmit>> datasetType = new TypeToken<List<PickupRegistrationSubmit>>() {};
				List<PickupRegistrationSubmit> data = gson.fromJson(response.getResponse(), datasetType.getType());
				ResponseEntity<AppGeneralResponse> pickupresponse = null;
				if(data != null && !data.isEmpty()){
					
					for(PickupRegistrationSubmit p : data){
						PickupRegistrationEntity existingPickup = pickupRegistrationJpaRepository.findByErpPickupOrderId(p.getPickupID());
						if(existingPickup != null){
							pickupresponse = updatePickupRegister(existingPickup, p, log);
						}else{
							pickupresponse = savePickupRegistration(p, log);
						}
						log.info("PICKUP ID : "+p.getPickupID()+"-"+gson.toJson(pickupresponse));
					}
					return new ResponseEntity<>(new AppGeneralResponse(true, "Done", null, null), HttpStatus.OK);
				}else{
					log.info("Empty data set received");
					return new ResponseEntity<>(new AppGeneralResponse(false, "Empty data set received", null, null), HttpStatus.OK); 
				}
				
			}else{
				log.info("No response from ERP");
				return new ResponseEntity<>(new AppGeneralResponse(false, "No response from ERP", null, null), HttpStatus.OK); 
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
	}

	public ResponseEntity<AppGeneralResponse> saveBIpickup(String model) {
		
		Logger log = Logger.getLogger("SaveBIpickup");
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		log.info("REQUEST : "+model);
		try {
//			TypeToken<List<BulkInterchangePickupModel>> datasetType = new TypeToken<List<BulkInterchangePickupModel>>() {};
//			List<BulkInterchangePickupModel> data = gson.fromJson(model, datasetType.getType());
			BulkInterchangeSubmitModel data = gson.fromJson(model, BulkInterchangeSubmitModel.class);
			if(data.getDkts() != null && !data.getDkts().isEmpty()){
				String baseUrl = FilesUtil.getProperty("viewImageUrl");
				String filename = CommonUtility.toImgFromBase64(data.getPickupSignature(), data.getDkts().get(0).getDktno().concat("BI"),
						"png", 2, "BISIGN");
//				
				String pwimagepath = null;
				if(CommonUtility.check(baseUrl)){
					pwimagepath = baseUrl;
					pwimagepath = pwimagepath.replace("<?filepath?>", filename).replace("<?dktno?>", data.getDkts().get(0).getDktno());
				}
				
				List<BulkInterchangePickupsEntity> pickupsdata = new ArrayList<>();
				List<BulkInterchangePickupErpModel> erpPickupData = new ArrayList<>();
				
				for(BulkInterchangePickupModel b : data.getDkts()){
					
					BulkInterchangePickupsEntity e = new BulkInterchangePickupsEntity();
					e.setCancelReason(b.getCancelReason());
					e.setCreatedTimestamp(new Date());
					e.setDktNo(b.getDktno());
//					e.setErpMessage(erpMessage);
//					e.setErpStatus(erpStatus);
					e.setPickupId(b.getPickupId());
					e.setPickupOrderNo(b.getPickupOrderNo());
					e.setPickupSignature(pwimagepath);
					e.setPickupStatus(b.getPickupStatus());
					e.setRemarks(b.getRemarks());
					e.setVersion(data.getVersion());
					if(CommonUtility.check(b.getScanTimestamp()))
						e.setScanTimestamp(yyyyMMddHHmmss.parse(b.getScanTimestamp()));
					e.setUserId(b.getUserId());
					pickupsdata.add(e);
					
					BulkInterchangePickupErpModel m = new BulkInterchangePickupErpModel();
					m.setAWBNo(b.getDktno());
					m.setPickupCancelReason(b.getCancelReason());
					m.setPickupID(b.getPickupId().toString());
					m.setPickupOrderNo(b.getPickupOrderNo());
					m.setPickupRemarks(b.getRemarks());
					m.setPickupSignature(e.getPickupSignature());
					m.setPickupStatus(b.getPickupStatus());
					m.setScanDateTime(b.getScanTimestamp());
					m.setUserId(b.getUserId());
					
					erpPickupData.add(m);
				}
				
				
				
				BulkInterchangeSubmitList erpdata = new BulkInterchangeSubmitList();
				erpdata.setAppVersion(data.getVersion());
				erpdata.setPickupSubmitList(erpPickupData);
				
				String url = null;
				url = FilesUtil.getProperty("erpPostBIPickup");
				String erpRequestJson = gson.toJson(erpdata);
				log.info("ERP REQUEST : "+erpRequestJson);
				String erpResponseString = CommonUtility.postDataToErp(erpRequestJson, url, log);
				log.info("ERP RESPONSE : "+erpResponseString);
				
				if(CommonUtility.check(erpResponseString)){
					ErpResponseModel erpResponse = gson.fromJson(erpResponseString, ErpResponseModel.class);
					
					for(BulkInterchangePickupsEntity e : pickupsdata){
						e.setErpStatus(erpResponse.getResult() ? 1 : 0);
						e.setErpMessage(erpResponse.getErrMsg());
					}
					
					bulkInterchangePickupsJpaRepository.save(pickupsdata);
					if(erpResponse.getResult()){
						return new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
					}else{
						return new ResponseEntity<>(new AppGeneralResponse(false, "Failed at ERP : "+erpResponse.getErrMsg(), null, null), HttpStatus.OK);
					}
				}else{
					return new ResponseEntity<>(new AppGeneralResponse(false, "No response from ERP", null, null), HttpStatus.OK);
				}
				
				
			} else{
				return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage(), e);
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
	}

}
