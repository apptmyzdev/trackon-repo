package com.apptmyz.trackon.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.apptmyz.trackon.data.repository.jpa.DlyDataJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DlyDepsImagesJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DrAcknowledgementJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.TrackonDrsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UndlyImagesJpaRepository;
import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.entities.jpa.DlyDepsImagesEntity;
import com.apptmyz.trackon.entities.jpa.DrAcknowledgementEntity;
import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;
import com.apptmyz.trackon.entities.jpa.UndlyImagesEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.DRSResponseModel;
import com.apptmyz.trackon.model.DeliveryPostModel;
import com.apptmyz.trackon.model.DrAckList;
import com.apptmyz.trackon.model.DrAcknowledgeModel;
import com.apptmyz.trackon.model.ErpDrAckList;
import com.apptmyz.trackon.model.ErpDrAcknowledgeModel;
import com.apptmyz.trackon.model.ErpResponseModel;
import com.apptmyz.trackon.model.ImageData;
import com.apptmyz.trackon.model.RedirectionModel;
import com.apptmyz.trackon.model.TrackonDRSDetailsModel;
import com.apptmyz.trackon.model.TrackonDRSDetailsResponseModel;
import com.apptmyz.trackon.model.TrackonDRSRequestModel;
import com.apptmyz.trackon.model.TrackonDlyUndlyUpdateModel;
import com.apptmyz.trackon.model.TrackonDlyUndlyUpdatePostModel;
import com.apptmyz.trackon.model.UndlyPostModel;
import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.EntityToModels;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


@Component
public class DeliveryServices {

	@Autowired
	private UndlyImagesJpaRepository undlyImagesJpaRepository;

	@Autowired
	private DlyDataJpaRepository dlyDataJpaRepository;

	@Autowired
	private DlyDepsImagesJpaRepository dlyDepsImagesJpaRepository;

	@Autowired
	private TrackonDrsJpaRepository trackonDrsJpaRepository;
	
	@Autowired
	private DrAcknowledgementJpaRepository drAcknowledgementJpaRepository;

	private SimpleDateFormat shortformat = new SimpleDateFormat("dd-MM-yyyy");
	private SimpleDateFormat yyyy_MM_dd_HH_mm_ss = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public ResponseEntity<AppGeneralResponse> getAckPDCsByUser(UserMasterEntity user, boolean isParentUser) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;

		try {
			Map<String, DRSResponseModel> pdcResponseList = new HashMap<String,DRSResponseModel>();
			List<TrackonDrsEntity> pdcList = null;
			
//			Set<String> updatedDkts = dlyDataJpaRepository.getUpdatedDkts();
			List<Object []> updatedDktsList = dlyDataJpaRepository.getUpdatedDkts(user.getUserId(), user.getFranchiseBranchCode());
			Set<String> updatedDkts = new HashSet<>();
			if(updatedDktsList != null && !updatedDktsList.isEmpty()){
				for(Object [] o : updatedDktsList){
					String checkkey = ((String) o[0]).concat("-").concat((String) o[1]);
					updatedDkts.add(checkkey);
				}
			}
			if(isParentUser){
				pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndFranchiseBranchCodeAndAckRequired(CommonUtility.getPreviousDateByNoOfDays(3), user.getFranchiseBranchCode(), 1);
			}
			else{
				pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndAssignedToUserAndAckRequired(CommonUtility.getPreviousDateByNoOfDays(3), user.getUserId(), 1);
			}
			
			if (pdcList != null && !pdcList.isEmpty()) {
				String key = null;
				
				for(TrackonDrsEntity t : pdcList){
					key = t.getAwbNo();
					if(!(updatedDkts.contains(key)) && (t.getAckRequired() != null && t.getAckRequired() == 1 && t.getAcknowledged() == null)){
						
						DRSResponseModel model = new DRSResponseModel();
						model.setId(t.getId());
						model.setAgentId(t.getAssignedToUser());
						model.setDrsNo(t.getDrsNo());
						model.setAwbNo(t.getAwbNo());
						model.setConsigneeAddress(t.getAddress());
						model.setConsigneeName(t.getReceiverName());
						model.setWeight(t.getWeight());
						model.setNoPkts(t.getNoPkts());
						pdcResponseList.put(key, model);
					}
					
				}
				
				generalResponse = new ResponseEntity<>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, pdcResponseList.values(), null),
						HttpStatus.OK);
			}
			else{
				generalResponse = new ResponseEntity<>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, e.getMessage(), null),
					HttpStatus.OK);
		}

		return generalResponse;
	}

	
	public ResponseEntity<AppGeneralResponse> getPDCsByUser(UserMasterEntity user, boolean isParentUser) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;

		try {
//			Date fromDate = CommonUtility.getTodaysDate();
			Map<String, DRSResponseModel> pdcResponseList = new HashMap<>();
			List<TrackonDrsEntity> pdcList = null;
//			Set<String> deliveredSet = dlyDataJpaRepository.getUpdatedDeliveredDkts();

			Set<String> deliveredSet = new HashSet<>();
			List<Object []> updatedDktsList = dlyDataJpaRepository.getUpdatedDkts(user.getUserId(), user.getFranchiseBranchCode());
			Set<String> updatedDkts = new HashSet<>();
			if(updatedDktsList != null && !updatedDktsList.isEmpty()){
				for(Object [] o : updatedDktsList){
					String dlystatus = (String) o[2];
					String conno = (String) o[1];
					String drsNumber = (String) o[0];
					String checkkey = (drsNumber).concat("-").concat(conno);
					updatedDkts.add(checkkey);
					if(CommonUtility.check(dlystatus) && dlystatus.equalsIgnoreCase(Constants.DELIVERY)){
						deliveredSet.add(conno);
					}
				}
			}
//			System.out.println("tDRS : "+CommonUtility.getPreviousDateByNoOfDays(3));
			if(isParentUser){
//				pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfter(CommonUtility.getPreviousDateByNoOfDays(3));
				pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndFranchiseBranchCode(CommonUtility.getPreviousDateByNoOfDays(3), user.getFranchiseBranchCode());
//				franchiseUsers = userMasterJpaRepository.getFranchiseDeliveryUsers(user.getFranchiseBranchCode());
			}
			else{
				pdcList = trackonDrsJpaRepository.findByCreatedTimestampAfterAndAssignedToUser(CommonUtility.getPreviousDateByNoOfDays(2), user.getUserId());
			}
			
			if (pdcList != null && !pdcList.isEmpty()) {
				String key = null;
//				if(isFranchiseUser){
//					for(TrackonDrsEntity t : pdcList){
//						key = t.getDrsNo()+"-"+t.getAwbNo();
//						if((!updatedDkts.contains(key)) && ((franchiseUsers.contains(t.getAssignedToUser())) || (t.getAssignedToUser().equalsIgnoreCase(user.getUserId())))){
//							pdcResponseList.put(key, EntityToModels.getAssignedDrsModel(t));
//						}
//						
//					}
//				}
//				else{
//					for(TrackonDrsEntity t : pdcList){
//						key = t.getDrsNo()+"-"+t.getAwbNo();
//						if(!updatedDkts.contains(key))
//							pdcResponseList.put(key, EntityToModels.getAssignedDrsModel(t));
//					}
//				}
				
				for(TrackonDrsEntity t : pdcList){
					key = t.getDrsNo()+"-"+t.getAwbNo();
//					key = t.getAwbNo();
					if((!updatedDkts.contains(key)) && ((t.getAckRequired() != null && t.getAckRequired() == 1 && t.getAcknowledged() != null && t.getAcknowledged() ==1) || (t.getAckRequired() == null) || t.getAckRequired() == 0)){
						pdcResponseList.put(key, EntityToModels.getAssignedDrsModel(t));
					}
					
				}
				List<DRSResponseModel> finaldata = new ArrayList<>();
				for(DRSResponseModel r : pdcResponseList.values()){
					if(!deliveredSet.contains(r.getAwbNo())){
						finaldata.add(r);
					}
				}
				
				Collections.sort(finaldata, Comparator.comparingInt(DRSResponseModel::getAwbSerialNo));
				
				generalResponse = new ResponseEntity<>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, finaldata, null),
						HttpStatus.OK);
			}
			else{
				generalResponse = new ResponseEntity<>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, e.getMessage(), null),
					HttpStatus.OK);
		}

		return generalResponse;
	}

//	public ResponseEntity<AppGeneralResponse> getPDCsByUser(String userId) {
//
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//
//		Calendar cal = Calendar.getInstance();
//		Date fromDate = null;
//
//		try {
//			fromDate = longformat.parse(shortformat.format(cal.getTime()) + " 00:00:00");
//
//			List<PdcAssignedEntity> pdcList = pdcAssignedJpaRepository
//					.findByPdcDateGreaterThanEqualAndAgentUserName(fromDate, userId);
//
//			String dkey = "";
//			if (pdcList != null && !pdcList.isEmpty()) {
//				List<PDCModel> pdcResponseList = new ArrayList<PDCModel>();
//
//				for (PdcAssignedEntity pdc : pdcList) {
//					PDCModel pdcModel = new PDCModel();
//					pdcModel.setAgentId(userId);
//					pdcModel.setBranchCode(pdc.getBranchCode());
//					pdcModel.setPdcDate(shortformat.format(pdc.getPdcDate()));
//					pdcModel.setPdcNo(pdc.getPdcNo());
//					pdcModel.setVehicleNo(pdc.getVehicleNo());
//
//					Map<String, PDCDockets> pdcDockets = new HashMap<String, PDCDockets>();
//					List<PdcDktsEntity> pdcDktList = pdc.getListOfPdcDkts();
//					for (PdcDktsEntity pdcDkt : pdcDktList) {
//						dkey = pdc.getPdcNo() + "-" + pdcDkt.getDocketDetailsCc().getDocketNo();
//						PDCDockets pdcDocket = new PDCDockets();
//						pdcDocket.setActualWt(pdcDkt.getActualWt());
//						pdcDocket.setAttemptCount(pdcDkt.getAttemptCount());
//						pdcDocket.setBookingBranch(pdcDkt.getDocketDetailsCc().getBookingBranchCode());
//						pdcDocket.setBookingDate(shortformat.format(pdcDkt.getDocketDetailsCc().getBookingDate()));
//						pdcDocket.setCollectAmount(pdcDkt.getCollectAmount());
//						pdcDocket.setCollectPayment(pdcDkt.getCollectPaymentFlag());
//						pdcDocket.setCollectRemarks(pdcDkt.getCollectRemarks());
//						pdcDocket.setCollectType("");
//						pdcDocket.setConsigneeName(pdcDkt.getDocketDetailsCc().getConsigneeName());
//						pdcDocket.setConsigneeAdd1(pdcDkt.getDocketDetailsCc().getConsigneeAdd1());
//						pdcDocket.setConsigneeAdd2(pdcDkt.getDocketDetailsCc().getConsigneeAdd2());
//						pdcDocket.setConsigneeAdd3(pdcDkt.getDocketDetailsCc().getConsigneeAdd3());
//						pdcDocket.setConsigneeCity(pdcDkt.getDocketDetailsCc().getConsigneeCity());
//						pdcDocket.setConsigneeContactName(pdcDkt.getDocketDetailsCc().getConsigneeContactName());
//						pdcDocket.setConsigneePhoneNo(pdcDkt.getDocketDetailsCc().getConsigneePhoneNo());
//						pdcDocket.setConsigneePincode(pdcDkt.getDocketDetailsCc().getConsigneePin());
//						pdcDocket.setConsigneeState(pdcDkt.getDocketDetailsCc().getConsigneeState());
//						pdcDocket.setDocketNo(pdcDkt.getDocketDetailsCc().getDocketNo());
//						pdcDocket.setOdaFlag(pdcDkt.getOdaFlag());
//						pdcDocket.setPacketCount(pdcDkt.getPktCount());
//						pdcDockets.put(dkey, pdcDocket);
//					}
//					pdcModel.setPdcDockets(new ArrayList<PDCDockets>(pdcDockets.values()));
//					pdcResponseList.add(pdcModel);
//
//				}
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(true, ResponseMessages.SUCCESS, pdcResponseList, null), HttpStatus.OK);
//			}
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return generalResponse;
//	}

//	public ResponseEntity<AppGeneralResponse> updateUndelivery(UndlyPostModel data, Logger logger) {
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		UndlyDataEntity undlyData = new UndlyDataEntity();
//		try {
//			undlyData.setAgentUserName(data.getUserId());
//			undlyData.setCreatedTimestamp(new Date());
//			undlyData.setDktNo(data.getDocketNo());
//			undlyData.setDlyBranchCode(data.getBranchCode());
//			undlyData.setLatitude(data.getLatitude());
//			undlyData.setLongitude(data.getLongitude());
//			undlyData.setPdcNo(data.getPdcNo());
//			undlyData.setRemarks(data.getRemarks());
//			undlyData.setUndeliveryReason(data.getReason());
//			undlyData.setUndlyTime(longformat.parse(data.getUndlyTime()));
//			undlyDataJpaRepository.save(undlyData);
//			if (data.getImages() != null && !data.getImages().isEmpty()) {
//				int i = 1;
//				for (String img : data.getImages()) {
//					if (CommonUtility.check(img)) {
//						String filename = data.getDocketNo() + "_" + i++;
//						String savedFileName = CommonUtility.toImgFromBase64(img, filename, "png", 4, "UND");
//						UndlyImagesEntity undlyImage = new UndlyImagesEntity();
//						undlyImage.setCreatedTimestamp(new Date());
//						undlyImage.setDktNo(data.getDocketNo());
//						undlyImage.setImage(savedFileName);
//						undlyImage.setPdcNo(data.getPdcNo());
//						undlyImagesJpaRepository.save(undlyImage);
//					}
//				}
//				
//			}
//			boolean result = pushUndlyToErp(data, logger);
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
//		} catch (Exception e) {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//
//		return generalResponse;
//	}
	
	//undelivery update
	public ResponseEntity<AppGeneralResponse> updateUndelivery(UndlyPostModel data, Logger logger) {
		
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		DlyDataEntity undly = new DlyDataEntity();
		List<ImageData> erpImagesData = new ArrayList<>();
		String baseUrl = FilesUtil.getProperty("viewImageUrl");
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		try {
			
			undly.setAgentUserName(data.getUserId());
			undly.setCreatedTimestamp(new Date());
			undly.setDktNo(data.getDocketNo());
			undly.setDlyBranchCode(data.getBranchCode());
			undly.setLatitude(data.getLatitude());
			undly.setLongitude(data.getLongitude());
			undly.setPdcNo(data.getPdcNo());
			undly.setRemarks(data.getRemarks());
			undly.setUndeliveryReason(data.getReason());
			undly.setDlyStatus(Constants.UNDELIVERY);
			undly.setErpStatus(0);
			undly.setAttemptCount(1);
			if(data.getUndlyTime() != null && !data.getUndlyTime().isEmpty()){
				undly.setTxnDate(yyyy_MM_dd_HH_mm_ss.parse(data.getUndlyTime()));
				undly.setDeliveredTime(yyyy_MM_dd_HH_mm_ss.parse(data.getUndlyTime()));
			}
//			undly.setUndlyTime(longformat.parse(data.getUndlyTime()));
			undly.setAppVersion(data.getAppVersion());
			undly.setImei(data.getImei());
			undly = dlyDataJpaRepository.save(undly);
			
			data.setTransactionId(undly.getId());
			
//			if (data.getImages() != null && !data.getImages().isEmpty()) {
//				int i = 1;
//				for (String img : data.getImages()) {
//					if (CommonUtility.check(img)) {
//						String filename = data.getDocketNo() + "_" + i++;
//						String savedFileName = CommonUtility.toImgFromBase64(img, filename, "png", 4, "UND");
//						UndlyImagesEntity undlyImage = new UndlyImagesEntity();
//						undlyImage.setCreatedTimestamp(new Date());
//						undlyImage.setDktNo(data.getDocketNo());
//						undlyImage.setImage(savedFileName);
//						undlyImage.setPdcNo(data.getPdcNo());
//						undlyImagesJpaRepository.save(undlyImage);
//						erpImagesData.add(new ImageData(savedFileName, Constants.ERP_UNDELIVERY_IMG));
//					}
//				}
//				
//			}
			
			if (data.getImages() != null && !data.getImages().isEmpty()) {
				List<UndlyImagesEntity> imgsList = new ArrayList<>();
				UndlyImagesEntity undlyImage = null;
				int i = 1;
				for (String img : data.getImages()) {
					if (CommonUtility.check(img)) {
						String filename = null;
						if(i == 1){
							filename = "NDR_" + data.getDocketNo();
						}else{							
							filename = "NDR_" + data.getDocketNo() + "_" +i;
						}
						
						String savedFileName = CommonUtility.toImgFromBase64(img, filename, "png", 4, "UND");
						undlyImage = new UndlyImagesEntity();
						undlyImage.setCreatedTimestamp(new Date());
						undlyImage.setDktNo(data.getDocketNo());
						if(CommonUtility.check(baseUrl)){
							String pwimagepath = baseUrl;
							pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName).replace("<?dktno?>", data.getDocketNo());
							undlyImage.setImage(pwimagepath);
						}
						undlyImage.setPdcNo(data.getPdcNo());
						imgsList.add(undlyImage);
						i++;
//						undlyImagesJpaRepository.save(undlyImage);
						erpImagesData.add(new ImageData(savedFileName, Constants.ERP_UNDELIVERY_IMG));
					}
				}
				undlyImagesJpaRepository.save(imgsList);
				
			}
			
//			WSResponseObjectModel erpResponse = pushUndlyToErp(data, erpImagesData,logger);
//			if(erpResponse != null){
//				
//				if(erpResponse.getResponseCode() == 200){
//					
//					ErpResponseModel resdata = gson.fromJson(erpResponse.getResponse(), ErpResponseModel.class);
//					if(resdata.getResult()){
//						undly.setErpStatus(1);
//					}else{
//						undly.setErpStatus(0);
//					}
//					if(erpResponse.getResponse() != null)
//						undly.setErpMessage(erpResponse.getResponse().length() > 45 ? erpResponse.getResponse().substring(0, 45) : erpResponse.getResponse());
//				}
//				else{
//					undly.setErpStatus(0);
//					if(erpResponse.getErrorString() != null)
//						undly.setErpMessage(erpResponse.getErrorString().length() > 45  ? erpResponse.getErrorString().substring(0,45) : erpResponse.getErrorString());
//				}
//				
//			}
//			else{
//				undly.setErpStatus(0);
//			}
//			
//			undly = dlyDataJpaRepository.save(undly);
//			
			
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
		} catch (Exception e) {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}

		return generalResponse;
	}

	//delivery update
	public ResponseEntity<AppGeneralResponse> updateDelivery(DeliveryPostModel data, Logger logger) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		List<ImageData> erpImagesData = new ArrayList<>();
		String filename = null;
		String savedFileName = null;
		String baseUrl = FilesUtil.getProperty("viewImageUrl");
		try {
			
//			TrackonDrsEntity docketDrs = trackonDrsJpaRepository.findTop1ByDrsNoAndAwbNo(data.getPdcNo(), data.getDocketNo());
			
			DlyDataEntity dlyData = new DlyDataEntity();
			dlyData.setAgentUserName(data.getUserId());
			dlyData.setAmountCollected(data.getAmountCollected());
			dlyData.setAmountToBeCollected(data.getAmountToBeCollected());
			dlyData.setBadDelivery(data.getIsBadDelivery() ? 1 : 0);
			dlyData.setBankName(data.getBankName());
			dlyData.setCreatedTimestamp(new Date());
			dlyData.setDeliveredTime(yyyy_MM_dd_HH_mm_ss.parse(data.getDeliveredTime()));
			dlyData.setDeps(data.isHasDeps() ? 1 : 0);
			dlyData.setDepsReason(data.getDepsReason());
			dlyData.setDktNo(data.getDocketNo());
			dlyData.setDlyBranchCode(data.getBranchCode());
			dlyData.setLatitude(data.getLatitude());
			dlyData.setLongitude(data.getLongitude());
			dlyData.setPaymentCollected(data.getIsPaymentCollected() ? 1 : 0);
			dlyData.setPaymentMethod(data.getPaymentMethod());
			dlyData.setRecieverContactNo(data.getDeliveredContact());
			dlyData.setRelationship(data.getRelationship());
			dlyData.setDlyStatus(Constants.DELIVERY);
			dlyData.setErpStatus(0);
			dlyData.setAttemptCount(1);
			dlyData.setAppVersion(data.getAppVersion());
			dlyData.setDlyBranchCode(data.getBranchCode());
			if (data.getPaymentProofImages() != null && !data.getPaymentProofImages().isEmpty()) {
				int i = 1;
				filename = data.getDocketNo() + "_PayProof_" + i++;
				savedFileName = CommonUtility.toImgFromBase64(data.getPaymentProofImages().get(0), filename,
						"png", 1, "PP");
				if(CommonUtility.check(baseUrl)){
					String pwimagepath = baseUrl;
					pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName).replace("<?dktno?>", data.getDocketNo());
					dlyData.setPaymentProofImage(pwimagepath);
				}
				
				erpImagesData.add(new ImageData(savedFileName, Constants.ERP_PAYMENT_IMG));
				
				if (data.getPaymentProofImages().size() > 1) {
					String filename2 = data.getDocketNo() + "_PayProof_" + i++;
					String savedFileName2 = CommonUtility.toImgFromBase64(data.getPaymentProofImages().get(1), filename2,
							"png", 1, "PP");
					if(CommonUtility.check(baseUrl)){
						String pwimagepath = baseUrl;
						pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName2).replace("<?dktno?>", data.getDocketNo());
						dlyData.setPaymentProofImage2(pwimagepath);
					}
					erpImagesData.add(new ImageData(savedFileName2, Constants.ERP_PAYMENT_IMG));
				}
			}

			dlyData.setPaymentRefId(data.getPaymentReferenceId());
			dlyData.setPdcNo(data.getPdcNo());

			if (data.getPodImages() != null && !data.getPodImages().isEmpty()) {
				filename = "POD_" + data.getDocketNo();
				savedFileName = CommonUtility.toImgFromBase64(data.getPodImages().get(0), filename, "png", 1, "POD");
				if(CommonUtility.check(baseUrl)){
					String pwimagepath = baseUrl;
					pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName).replace("<?dktno?>", data.getDocketNo());
					dlyData.setPodImage(pwimagepath);
				}
			}
			else{
				
			}

			if (data.getSignatureImages() != null && !data.getSignatureImages().isEmpty()) {
				filename = "SIGN_"+data.getDocketNo();
				savedFileName = CommonUtility.toImgFromBase64(data.getSignatureImages().get(0), filename, "png",
						1, "SIG");
				if(CommonUtility.check(baseUrl)){
					String pwimagepath = baseUrl;
					pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName).replace("<?dktno?>", data.getDocketNo());
					dlyData.setPodSign(pwimagepath);
				}
			}

			dlyData.setRecieverName(data.getRecieverName());
			dlyData.setRemarks(data.getRemarks());
			if (CommonUtility.check(data.getTxnDate())) {
				dlyData.setTxnDate(yyyy_MM_dd_HH_mm_ss.parse(data.getTxnDate()));
			}
			dlyData.setImei(data.getImei());
			dlyData = dlyDataJpaRepository.save(dlyData);

			if (data.getDepsImages() != null && !data.getDepsImages().isEmpty()) {
				int i = 0;
				for (String depsImg : data.getDepsImages()) {
					if (CommonUtility.check(depsImg)) {
						 filename = data.getDocketNo() + "_deps_" + i + 1;
						 savedFileName = CommonUtility.toImgFromBase64(data.getDepsImages().get(i), filename,
								"png", 5, "DEPS");
						DlyDepsImagesEntity depsImage = new DlyDepsImagesEntity();
						depsImage.setCreatedTimestamp(new Date());
						depsImage.setDktNo(data.getDocketNo());
						if(CommonUtility.check(baseUrl)){
							String pwimagepath = baseUrl;
							pwimagepath = pwimagepath.replace("<?filepath?>", savedFileName).replace("<?dktno?>", data.getDocketNo());
							depsImage.setImage(pwimagepath);
						}
						dlyDepsImagesJpaRepository.save(depsImage);
						erpImagesData.add(new ImageData(savedFileName, Constants.ERP_DEPS_IMG));
					}
				}
			}
			
//			data.setTxnId(dlyData.getId());
//			WSResponseObjectModel erpResponse = pushDlyToErp(data, logger, dlyData, erpImagesData);
//			if(erpResponse != null){
//				if(erpResponse.getResponseCode() == 200){
//					ErpResponseModel resdata = gson.fromJson(erpResponse.getResponse(), ErpResponseModel.class);
//					if(resdata.getResult()){						
//						dlyData.setErpStatus(1);
//					}else{
//						dlyData.setErpStatus(0);
//					}
//					if(erpResponse.getResponse() != null)
//						dlyData.setErpMessage(erpResponse.getResponse().length() > 45 ? erpResponse.getResponse().substring(0, 45) : erpResponse.getResponse());
//					
//				}
//				else{
//					dlyData.setErpStatus(0);
//					if(erpResponse.getErrorString() != null)
//						dlyData.setErpMessage(erpResponse.getErrorString().length() > 45  ? erpResponse.getErrorString().substring(0,45) : erpResponse.getErrorString());
//				}
//				
//			}
//			else{
//				dlyData.setErpStatus(0);
//			}
//
//			dlyDataJpaRepository.save(dlyData);

			generalResponse = new ResponseEntity<>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	public WSResponseObjectModel pushUndlyToErp(UndlyPostModel data, List<ImageData> imagesdata,Logger logger) {
		
		WSResponseObjectModel postResponse = null;
		try{
			TrackonDlyUndlyUpdateModel undlyModel = new TrackonDlyUndlyUpdateModel();
			TrackonDlyUndlyUpdatePostModel postModel = new TrackonDlyUndlyUpdatePostModel();
			List<TrackonDlyUndlyUpdateModel> undlyModelList = new ArrayList<TrackonDlyUndlyUpdateModel>();
			Gson gson = new GsonBuilder().serializeNulls().create();
			
			undlyModel.setAwbNo(data.getDocketNo());
			undlyModel.setBranchCode(data.getBranchCode());
			undlyModel.setUndeliveredDateTime(data.getUndlyTime());
			undlyModel.setDrsNo(data.getPdcNo());
			undlyModel.setLatitude(data.getLatitude());
			undlyModel.setLongitude(data.getLongitude());
			undlyModel.setRemarks(data.getRemarks());
			undlyModel.setStatus(Constants.SUCCESSFUL_UNDELIVERY);
			undlyModel.setTransactionDate(data.getUndlyTime());
			undlyModel.setTransactionId(data.getTransactionId());
			undlyModel.setUndeliveredDateTime(data.getUndlyTime());
			undlyModel.setUndeliveredReason(data.getReason());
			undlyModel.setUserId(data.getUserId());
			undlyModel.setMiscImages(imagesdata);
			undlyModel.setAppVersion(data.getAppVersion());
			undlyModel.setImei(data.getImei());
			undlyModelList.add(undlyModel);
			postModel.setDrsUpdateInputList(undlyModelList);
			String postData = gson.toJson(postModel);
			String drsUpdationUrl = FilesUtil.getProperty("drsupdation");
			
			postResponse = CommonUtility.doWSRequest(drsUpdationUrl, postData, Constants.HTTP_POST, logger);
			logger.info("Data:" + postResponse.getResponse());
		}
		catch(Exception e){
			e.printStackTrace();
			logger.info(e.getMessage(), e);
		}
		return postResponse;
	}
	
	public WSResponseObjectModel pushDlyToErp(DeliveryPostModel data, Logger logger, DlyDataEntity entity, List<ImageData> imagesdata) {
		WSResponseObjectModel postResponse = null;
		try{
			TrackonDlyUndlyUpdateModel dlyModel = new TrackonDlyUndlyUpdateModel();
			TrackonDlyUndlyUpdatePostModel postModel = new TrackonDlyUndlyUpdatePostModel();
			List<TrackonDlyUndlyUpdateModel> undlyModelList = new ArrayList<TrackonDlyUndlyUpdateModel>();
			Gson gson = new GsonBuilder().serializeNulls().create();
			dlyModel.setAppVersion(data.getAppVersion());
			dlyModel.setAwbNo(data.getDocketNo());
			dlyModel.setBankName(data.getBankName());
			dlyModel.setBranchCode(data.getBranchCode());
			dlyModel.setBadDelivery(data.getIsBadDelivery());
			dlyModel.setDeliveredDateTime(data.getDeliveredTime());
			dlyModel.setDrsNo(data.getPdcNo());
			dlyModel.setLatitude(data.getLatitude());
			dlyModel.setLongitude(data.getLongitude());
			dlyModel.setPaymentMethod(data.getPaymentMethod());
			dlyModel.setPaymentReferenceId(data.getPaymentReferenceId());
			dlyModel.setRemarks(data.getRemarks());
			dlyModel.setReceiverName(data.getRecieverName());
			dlyModel.setRelationship(data.getRelationship());
			dlyModel.setStatus(Constants.SUCCESSFUL_DELIVERY);
			dlyModel.setTransactionId(data.getTxnId());
			dlyModel.setTransactionDate(data.getDeliveredTime());
			dlyModel.setUserId(data.getUserId());
			dlyModel.setAmountCollected(data.getAmountToBeCollected().doubleValue());
			dlyModel.setAmountToBeCollected(data.getAmountToBeCollected().doubleValue());
			dlyModel.setDeliveredContact(data.getDeliveredContact());
			dlyModel.setPaymentCollected(data.getIsPaymentCollected());
			dlyModel.setHasDeps(data.isHasDeps());
			dlyModel.setDepsReason(data.getDepsReason());
			
			dlyModel.setPodImageName(entity.getPodImage());
			dlyModel.setSignatureImageName(entity.getPodSign());
			dlyModel.setMiscImages(imagesdata);
			dlyModel.setImei(data.getImei());
			undlyModelList.add(dlyModel);
			postModel.setDrsUpdateInputList(undlyModelList);
			String postData = gson.toJson(postModel);
			String drsUpdationUrl = FilesUtil.getProperty("drsupdation");
			
			postResponse = CommonUtility.doWSRequest(drsUpdationUrl, postData, Constants.HTTP_POST, logger);
			logger.info("Data:" + postResponse.getResponse());
		}
		catch(Exception e){
			e.printStackTrace();
			logger.info(e.getMessage(), e);
		}
		
		return postResponse;
	}

	public ResponseEntity<AppGeneralResponse> getDeliveryOTP(String mobileNumber, String dktNo, Logger logger) {
		String otp = CommonUtility.getRandomNumberString();
		try {
			CommonUtility.sendOTPSMS(mobileNumber, dktNo, otp, logger);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, otp, null),
				HttpStatus.OK);
	}

	public ResponseEntity<AppGeneralResponse> getTrackonDRS(Logger logger) {
		
		
		logger.info("******** START ******"+new Date());
		String getDrsUrl = FilesUtil.getProperty("getdrs");
		Gson gson = new GsonBuilder().serializeNulls().create();
		TrackonDRSRequestModel reqObj = new TrackonDRSRequestModel();
		logger.info("******** ID FETCH ******"+new Date());
		reqObj.setDRSDetailID(getLastDrsId(Constants.DRS_STD_TYPE));
		logger.info("******** ID FETCH END ******"+new Date());
		String requestString = gson.toJson(reqObj);
		WSResponseObjectModel data = CommonUtility.doWSRequest(getDrsUrl, requestString, Constants.HTTP_POST, logger);
		logger.info("Data:" + data.getResponse());

		if (data.getResponseCode() == Constants.HTTP_OK) {
			TrackonDRSDetailsResponseModel drsResponse = gson.fromJson(data.getResponse(),
					TrackonDRSDetailsResponseModel.class);

			List<TrackonDRSDetailsModel> drsList = drsResponse.getDrsList();
			List<TrackonDrsEntity> dbDrsList = null;
			TrackonDrsEntity te = null;
			if (drsList != null && !drsList.isEmpty()) {
				logger.info("******** SAVE ******"+new Date());
				dbDrsList = new ArrayList<>();
//				List<TrackonDrsEntity> dbDrsList = EntityToModels.convertTrackonDRSDetailModelsToEntity(drsList, Constants.DRS_STD_TYPE);
				for(TrackonDRSDetailsModel t : drsList){
					te = EntityToModels.convertTrackonDRSDetailModelToEntity(t, Constants.DRS_STD_TYPE);
					dbDrsList.add(te);
				}
				trackonDrsJpaRepository.save(dbDrsList);
				logger.info("******** SAVE END ******"+new Date());
			} else {
				logger.info("!!!!Empty STD data!!!!");
			}
		} else {
			logger.info("Error in Webservice STD: " + data.getErrorString());
		}
		
		//PT drs pull
		getDrsUrl = FilesUtil.getProperty("getPtDrs");
		logger.info("******** ID FETCH ******"+new Date());
		reqObj.setDRSDetailID(getLastDrsId(Constants.DRS_PT_TYPE));
		logger.info("******** ID FETCH END ******"+new Date());
		requestString = gson.toJson(reqObj);
		data = CommonUtility.doWSRequest(getDrsUrl, requestString, Constants.HTTP_POST, logger);
		logger.info("Data:" + data.getResponse());
		
		if (data.getResponseCode() == Constants.HTTP_OK) {
			TrackonDRSDetailsResponseModel drsResponse = gson.fromJson(data.getResponse(),
					TrackonDRSDetailsResponseModel.class);

			List<TrackonDRSDetailsModel> drsList = drsResponse.getDrsList();
			List<TrackonDrsEntity> dbDrsList = null;
			TrackonDrsEntity te = null;
			if (drsList != null && !drsList.isEmpty()) {
				logger.info("******** SAVE ******"+new Date());
//				dbDrsList = EntityToModels.convertTrackonDRSDetailModelsToEntity(drsList, Constants.DRS_PT_TYPE);
				dbDrsList = new ArrayList<>();
				for (TrackonDRSDetailsModel t : drsList) {
					te = EntityToModels.convertTrackonDRSDetailModelToEntity(t, Constants.DRS_PT_TYPE);
					dbDrsList.add(te);
				}
				trackonDrsJpaRepository.save(dbDrsList);
				logger.info("******** SAVE END ******"+new Date());
			} else {
				logger.info("!!!!Empty PT data!!!!");
			}
		} else {
			logger.info("Error in Webservice PT: " + data.getErrorString());
		}
		logger.info("******** END ******"+new Date());
		return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, "DONE", null, null), HttpStatus.OK);
	}

	private int getLastDrsId(String apiType) {
//		TrackonDrsEntity entity = trackonDrsJpaRepository.findTopByOrderByDrsIdDesc();
		TrackonDrsEntity entity = trackonDrsJpaRepository.findTopByApiTypeOrderByDrsIdDesc(apiType);
		if (entity == null || entity.getDrsId() == null) return 1;
		return entity.getDrsId();
	}
	
	public ResponseEntity<AppGeneralResponse> redirectCons(RedirectionModel data, Logger log) throws Exception{
		
		ResponseEntity<AppGeneralResponse> response = null;
		
		if(data.getDocketIds() != null && !data.getDocketIds().isEmpty()){
			List<TrackonDrsEntity> drsdata = trackonDrsJpaRepository.findByIdIn(data.getDocketIds());
			if(drsdata != null && !drsdata.isEmpty()){
				for(TrackonDrsEntity d : drsdata){
					
					if(data.getRemarks() != null)
						d.setRedirectRemarks(data.getRemarks().length() > 45 ? data.getRemarks().substring(0,45) : data.getRemarks());
					d.setRedirectedBy(data.getFromUserId());
					d.setAssignedToUser(data.getToUserId());
				}
				
				drsdata = (List<TrackonDrsEntity>) trackonDrsJpaRepository.save(drsdata);
				
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "Dockets not found", null, null), HttpStatus.OK);
			}
		}
		else{
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, "Dockets not sent", null, null), HttpStatus.OK);
		}
		
		return response;
		
	}
	
	public String getDocketImgPath(String conNo) throws Exception{
		DlyDataEntity dly = dlyDataJpaRepository.findByDktNoAndErpStatus(conNo, 1);
		if(dly != null && dly.getPodImage() != null && !dly.getPodImage().isEmpty())
			return dly.getPodImage();
		else
			return null;
	}


	@Transactional
	public ResponseEntity<AppGeneralResponse> acknowledgeDr(String model) {
		Logger log = Logger.getLogger("AcknowledgeDr");
		log.info("REQUEST : "+model);
		Gson gson = new GsonBuilder().serializeNulls().create();
		SimpleDateFormat lf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
			String url = FilesUtil.getProperty("erpDrAck");
			if(CommonUtility.check(model)){
				
				DrAckList data = gson.fromJson(model, DrAckList.class);
				
				if(data != null && data.getDrAcknowledgeInputList() != null && !data.getDrAcknowledgeInputList().isEmpty()){
					Map<String, DrAcknowledgementEntity> datamap = new HashMap<>();
					List<ErpDrAcknowledgeModel> erpdataList = new ArrayList<>();
					String drsNo = data.getDrAcknowledgeInputList().get(0).getDrNumber();
					
					for(DrAcknowledgeModel d : data.getDrAcknowledgeInputList()){
						String key = d.getDrNumber().concat("-").concat(d.getAwbNo());
						if(!datamap.containsKey(key)){						
							DrAcknowledgementEntity e = new DrAcknowledgementEntity();
							e.setAcknowledged(d.isAcknowledged() ? 1 : 0);
							e.setAckTimestamp(new Date());
							e.setCreatedTimestamp(new Date());
							e.setDocketNo(d.getAwbNo());
							e.setDrNo(d.getDrNumber());
							e.setIsExtra(d.isExtra() ? 1 : 0);
							e.setIsShort(d.isShort() ? 1 : 0);
							e.setRemarks(d.getRemarks());
							e.setUserId(d.getAgentId());
							datamap.put(key, e);
							
							ErpDrAcknowledgeModel m = new ErpDrAcknowledgeModel();
							m.setAWBNo(d.getAwbNo());
							m.setDR_Number(d.getDrNumber());
							m.setIsAcknowledged(d.isAcknowledged());
							m.setIsExtra(d.isExtra());
							m.setIsShort(d.isShort());
							m.setUserID(d.getAgentId());
							m.setAcknowledgeDateTime(lf.format(e.getAckTimestamp()));
							m.setRemarks(d.getRemarks());
							
							erpdataList.add(m);
						}
						
					}
					
					drAcknowledgementJpaRepository.save(datamap.values());
					
					ErpDrAckList erpdata = new ErpDrAckList();
					erpdata.setDRAcknowledgeInputList(erpdataList);
					
					String erpResponseString = CommonUtility.postDataToErp(gson.toJson(erpdata), url, log);
					log.info("ERP RESPONSE : "+erpResponseString);
					if(CommonUtility.check(erpResponseString)){
						ErpResponseModel erpResponse = gson.fromJson(erpResponseString, ErpResponseModel.class);
						if(erpResponse.getResult() != null && erpResponse.getResult()){							
							int updated = trackonDrsJpaRepository.updateDrsAck(1 , drsNo);
							log.info("UPDATED : "+updated);
							return new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, updated, null), HttpStatus.OK);
						}else{
							return new ResponseEntity<>(new AppGeneralResponse(false, "Failed at erp", null, null), HttpStatus.OK);
						}
					}else{
						
						return new ResponseEntity<>(new AppGeneralResponse(false, "No response from ERP", null, null), HttpStatus.OK);
					}
					
				}else{
					return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
				}
				
			}else{
				return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		
	}
	
//	Font getFontfactoryForPara(BaseColor baseColor, float fontSize, int fontWeight, boolean underline) {
//		Font font = null;
//		font = FontFactory.getFont(FontFactory.HELVETICA, fontSize, fontWeight);
//		font.setColor(baseColor);
//		if(underline)
//			font.setStyle(Font.UNDERLINE);
//		return font;
//	}
//	
	

//	public ResponseEntity<AppGeneralResponse> getDktPodPdf(String drsno, String dktno) throws Exception {
//
//		ResponseEntity<AppGeneralResponse> response = null;
//
//		TrackonDrsEntity dkt = trackonDrsJpaRepository.findFirstByDrsNoAndAwbNo(drsno, dktno);
//		
//		DlyDataEntity dly = dlyDataJpaRepository.findFirstByPdcNoAndDktNo(drsno, dktno);
//
//		if (dkt != null) {
//
//			Calendar c = Calendar.getInstance();
//			c.setTime(new Date());
//
//			String path = FilesUtil.getProperty("imagesPath");
//			String logopath = FilesUtil.getProperty("trackonlogopath");
//			String wmpath = FilesUtil.getProperty("trackonwmpath");
//
//			String imgPath = "";
//			imgPath = "Deliveries/".concat(Integer.toString(c.get(Calendar.YEAR))).concat("/")
//					.concat(Integer.toString(c.getTime().getMonth() + 1)).concat("/")
//					.concat(Integer.toString(c.getTime().getDate())).concat("/");
//			path = path.concat(imgPath);
//
//			File fileImg = new File(path);
//
//			if (!fileImg.exists()){
//				fileImg.mkdirs();
//			}
//			
//				
//
//			path = path.concat("/").concat(drsno).concat("_").concat(dktno).concat(".pdf");
////			System.out.println(path);
//			fileImg = new File(path);
//			Document doc = new Document(PageSize.A3, 25, 25, 25, 25);
//			FileOutputStream fOut = new FileOutputStream(path);
//			PdfWriter.getInstance(doc, fOut);
//			doc.open();
//			
//			PdfPTable table1 = new PdfPTable(1);
//			table1.setWidthPercentage(100f);
//			table1.setWidths(new float[] { 40.0f });
////			table1.setSpacingBefore(10);
//			
//
//			byte[] imageBytes3 = IOUtils.toByteArray(new FileInputStream(logopath));
//			ByteArrayInputStream bis3 = new ByteArrayInputStream(imageBytes3);
//			BufferedImage bImage3 = ImageIO.read(bis3);
//
//			ByteArrayOutputStream bytes3 = new ByteArrayOutputStream();
//			ImageIO.write(bImage3, "jpeg", bytes3);
//			Image myImg3 = Image.getInstance(bytes3.toByteArray());
//			// myImg3.setAbsolutePosition(90, 780);
//			myImg3.setPaddingTop(50);
//			myImg3.scaleToFit(70, 60);
//			myImg3.setAlignment(Image.ALIGN_LEFT);
////			doc.add(myImg3);
//			
//			PdfPCell cell = new PdfPCell(myImg3);
//			cell.setBorder(0);
//			table1.addCell(cell);
//			
//			
//			
//			
////			PdfPCell dtcell = new PdfPCell();
////			dtcell.setBorder(0);
////			dtcell.addElement(heading);
////			table1.addCell(dtcell);
//			doc.add(table1);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			Paragraph heading = new Paragraph("PROOF OF DELIVERY", getFontfactoryForPara(BaseColor.BLACK, 10, Font.BOLD, true));
//			heading.setAlignment(Element.ALIGN_CENTER);
//			
//			doc.add(heading);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			PdfPTable table2 = new PdfPTable(2);
//			table2.setWidthPercentage(100f);
//			table2.setWidths(new float[] { 50.0f, 50.0f });
////			table2.setSpacingBefore(10);
//			table2.setPaddingTop(100.0f);
//			Phrase companyLabel = new Phrase("COMPANY NAME: TRACKON COURIERS PVT. LTD.", getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, false));
//			
//			PdfPCell cnamecell = new PdfPCell();
//			cnamecell.setBorder(0);
//			cnamecell.addElement(companyLabel);
//			table2.addCell(cnamecell);
//			
//			
//			Paragraph datephrase = new Paragraph("DATE : "+new SimpleDateFormat("dd-MM-yyyy").format(new Date()), getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, false));
//			
//			PdfPCell datecell = new PdfPCell();
//			datecell.setBorder(0);
//			datecell.setPhrase(datephrase);
//			datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			table2.addCell(datecell);
//			doc.add(table2);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			PdfPTable table3 = new PdfPTable(2);
//			table3.setWidths(new float[] { 50.0f, 50.0f });
//			table3.setWidthPercentage(100f);
//			
//			
//			//dktno label
//			Paragraph dktnoplabel = new Paragraph("Waybill No.:",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell dktlabcell = new PdfPCell();
//			dktlabcell.setBorder(0);
//			dktlabcell.setPhrase(dktnoplabel);
//			table3.addCell(dktlabcell);
//			
//			Paragraph deldethead = new Paragraph("Delivered At : ",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell deldetheadcell = new PdfPCell();
//			deldetheadcell.setBorder(0);
//			deldetheadcell.setPhrase(deldethead);
//			table3.addCell(deldetheadcell);
//			
//			doc.add(table3);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table4 = new PdfPTable(2);
//			table4.setWidths(new float[] { 50.0f, 50.0f });
//			table4.setWidthPercentage(100f);
//			
//
//			//dktno
//			Paragraph dktnop = new Paragraph(dkt.getAwbNo(), getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell dktcell = new PdfPCell();
//			dktcell.setBorder(0);
//			dktcell.setPhrase(dktnop);
//			table4.addCell(dktcell);
//			
//			Paragraph addresspara = new Paragraph(dkt.getAddress(),getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell addresscell = new PdfPCell();
//			addresscell.setBorder(0);
//			addresscell.setPhrase(addresspara);
//			table4.addCell(addresscell);
//			
//			doc.add(table4);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table5 = new PdfPTable(2);
//			table5.setWidths(new float[] { 50.0f, 50.0f });
//			table5.setWidthPercentage(100f);
//			
//			Paragraph colllabel = new Paragraph("Collected By :",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell clctdbylabelcell = new PdfPCell();
//			clctdbylabelcell.setBorder(0);
//			clctdbylabelcell.setPhrase(colllabel);
//			table5.addCell(clctdbylabelcell);
//			
//			Paragraph mobilenumlab = new Paragraph("Contact No.:",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell mobilelabcell = new PdfPCell();
//			mobilelabcell.setBorder(0);
//			mobilelabcell.setPhrase(mobilenumlab);
//			table5.addCell(mobilelabcell);
//			
//			
//			doc.add(table5);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table6 = new PdfPTable(2);
//			table6.setWidths(new float[] { 50.0f, 50.0f });
//			table6.setWidthPercentage(100f);
//			
//			Paragraph receivedby = new Paragraph(dkt.getReceiverName(),
//					getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell receivedbycell = new PdfPCell();
//			receivedbycell.setBorder(0);
//			receivedbycell.setPhrase(receivedby);
//			table6.addCell(receivedbycell);
//			
//			Paragraph mobilenum = new Paragraph(dkt.getMobileNo(),getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell mobilecell = new PdfPCell();
//			mobilecell.setBorder(0);
//			mobilecell.setPhrase(mobilenum);
//			table6.addCell(mobilecell);
//			doc.add(table6);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table7 = new PdfPTable(3);
//			table7.setWidths(new float[] { 25.0f, 25.0f, 25.0f });
//			table7.setWidthPercentage(100f);
//			
//			Paragraph signlab = new Paragraph("Signature :",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell signlabelcell = new PdfPCell();
//			signlabelcell.setBorder(0);
//			signlabelcell.setPhrase(signlab);
//			table7.addCell(signlabelcell);
//			
//			Paragraph delonlab = new Paragraph("Delivered on :",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell delonlabelcell = new PdfPCell();
//			delonlabelcell.setBorder(0);
//			delonlabelcell.setPhrase(delonlab);
//			table7.addCell(delonlabelcell);
//			
//			Paragraph statuslab = new Paragraph("Status : ",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			PdfPCell statuslabcell = new PdfPCell();
//			statuslabcell.setBorder(0);
//			statuslabcell.setPhrase(statuslab);
//			table7.addCell(statuslabcell);
//			
////			Paragraph empty = new Paragraph(" ",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
////			PdfPCell emptycell = new PdfPCell();
////			emptycell.setBorder(0);
////			emptycell.setPhrase(empty);
////			table7.addCell(emptycell);
//			
//			
//			doc.add(table7);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table8 = new PdfPTable(3);
//			table8.setWidths(new float[] { 25.0f, 25.0f, 25.0f });
//			table8.setWidthPercentage(100f);
//			
//			Paragraph signpara = new Paragraph("Signature not required. Verified by OTP",
//					getFontfactoryForPara(BaseColor.BLACK, 7.0f, Font.BOLD, true));
//			
//			PdfPCell signcell = new PdfPCell();
//			signcell.setBorder(0);
//			signcell.setPhrase(signpara);
//			table8.addCell(signcell);
//			
//			Paragraph delon = new Paragraph(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()),getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			
//			PdfPCell deloncell = new PdfPCell();
//			deloncell.setBorder(0);
//			deloncell.setPhrase(delon);
//			table8.addCell(deloncell);
//			
//			String dktStatus = "-";
//			if(dly != null){
//				if(dly.getDlyStatus() != null && dly.getDlyStatus().equals("D")){
//					dktStatus = "Delivered";
//				}
//				else{
//					dktStatus = "Not Delivered - "+dly.getUndeliveryReason();
//				}
//				
//			}
//			Paragraph status = new Paragraph(dktStatus,
//					getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			
//			
//			PdfPCell statuscell = new PdfPCell();
//			statuscell.setBorder(0);
//			statuscell.setPhrase(status);
//			table8.addCell(statuscell);
//			
//			doc.add(table8);
//			
//			byte[] imageBytes4 = IOUtils.toByteArray(new FileInputStream(wmpath));
//			ByteArrayInputStream bis4 = new ByteArrayInputStream(imageBytes4);
//			BufferedImage bImage4 = ImageIO.read(bis4);
//
//			ByteArrayOutputStream bytes4 = new ByteArrayOutputStream();
//			ImageIO.write(bImage4, "png", bytes4);
//			Image myImg4 = Image.getInstance(bytes4.toByteArray());
//			 myImg3.setAbsolutePosition(90, 150);
////			myImg4.setPaddingTop(50);
//			myImg4.scaleToFit(150, 150);
//			myImg4.setAlignment(Image.ALIGN_LEFT);
//			doc.add(myImg4);
//			
//			doc.close();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, "success", null, null),
//					HttpStatus.OK);
//
//		} else {
//			response = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, "dkt not found", null, null), HttpStatus.OK);
//		}
//
//		return response;
//	}

}
