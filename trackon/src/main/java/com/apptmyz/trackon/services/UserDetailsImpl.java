package com.apptmyz.trackon.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.apptmyz.trackon.model.LoginAuthenticationModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private String userid;
	@JsonIgnore
	private String userName;
	private String emailId;
	@JsonIgnore
	private String password;
	private String firstName;
	private String lastName;

	private Collection<? extends GrantedAuthority> authorities;
//    private Collection<? extends GrantedAuthority> screenDataActions;
	
	public UserDetailsImpl(String id, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.userid = id;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserDetailsImpl build(LoginAuthenticationModel user, String password) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		UserDetailsImpl userDetailsImpl = new UserDetailsImpl(
				user.getUserId(),
				password,
				authorities);
		
		
		return userDetailsImpl;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(userid, user.userid);
	}

	@Override
	public String getUsername() {
		return userName;
	}
}
