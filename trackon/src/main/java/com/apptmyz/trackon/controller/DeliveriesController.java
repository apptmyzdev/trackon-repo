package com.apptmyz.trackon.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apptmyz.trackon.data.repository.jpa.AuditLogJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.AuditLogEntity;
import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.model.DeliveryPostModel;
import com.apptmyz.trackon.model.DlyUndlyPostModel;
import com.apptmyz.trackon.model.RedirectionModel;
import com.apptmyz.trackon.model.UndlyPostModel;
import com.apptmyz.trackon.services.DeliveryServices;
import com.apptmyz.trackon.services.UserDetailsImpl;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@CrossOrigin
@RestController
@RequestMapping(path = "/pd")
public class DeliveriesController {

	@Autowired
	AuditLogJpaRepository auditLogJpaRepository;

	@Autowired
	private DeliveryServices deliveryServices;

	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	
	@Autowired
	private UserSessionJpaRepository userSessionJpaRepository;
	
	@PostMapping(path = "/dly")
	ResponseEntity<AppGeneralResponse> updateDly(@RequestHeader("imei") String imei, @RequestHeader("apkVersion") String apkVersion,
			@RequestBody DlyUndlyPostModel data) {
		Logger logger = Logger.getLogger("DeliveryUpdate");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("imei: " + imei + ", app version: " + apkVersion);
		logger.info("Data from device: " + gson.toJson(data));

		if (imei != null && data != null) {
			if(data.getDlydata() != null && !data.getDlydata().isEmpty()){
				UserMasterEntity user = userMasterJpaRepository.findByUserId(data.getDlydata().get(0).getUserId());
				for(DeliveryPostModel d : data.getDlydata()){	
					d.setAppVersion(apkVersion);
					d.setBranchCode(user.getBranchCode());
					d.setImei(imei);
					generalResponse = deliveryServices.updateDelivery(d, logger);
					logger.info("RESPONSE : "+gson.toJson(generalResponse));
				}
				
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, "Done", null, null),
						HttpStatus.OK);
			}
			else{
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
						HttpStatus.OK);
			}
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}

	@PostMapping(path = "/undly")
	ResponseEntity<AppGeneralResponse> updateUndly(@RequestHeader("imei") String imei, @RequestHeader("apkVersion") String apkVersion,
			@RequestBody DlyUndlyPostModel data) {
		Logger logger = Logger.getLogger("Undelivery");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("imei: " + imei + ", app version: " + apkVersion);
		logger.info("Data from device: " + gson.toJson(data));

		if (imei != null && data != null) {
			
			if(data.getUndlydata() != null && !data.getUndlydata().isEmpty()){
				UserMasterEntity user = userMasterJpaRepository.findByUserId(data.getUndlydata().get(0).getUserId());
				for(UndlyPostModel d : data.getUndlydata()){
					d.setAppVersion(apkVersion);
					d.setBranchCode(user.getBranchCode());
					d.setImei(imei);
					generalResponse = deliveryServices.updateUndelivery(d, logger);
					logger.info("RESPONSE : "+gson.toJson(generalResponse));
				}
				generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, "Done", null, null),
						HttpStatus.OK);
			}
			else{
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
						HttpStatus.OK);
			}
			
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		return generalResponse;
	}
	
	@GetMapping(path = "/pdc/fetch")
	ResponseEntity<AppGeneralResponse> getAssignedPDCs() {
		Logger logger = Logger.getLogger("GetAssignedPDCs");	
//		LoginAuthenticationModel userModel = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		boolean isParentUser = false;
		AuditLogEntity audit = new AuditLogEntity();
		audit.setServiceType("PDC Fetch");

		logger.info(Constants.LOG_START);
//		if (token != null && !token.isEmpty()) {

			try {
//				JwtUtil j = new JwtUtil();
//				userModel = gson.fromJson(j.parseJWT(token), LoginAuthenticationModel.class);
//				logger.info("user details :" + j.parseJWT(token));
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				String userId = null;
				if (principal instanceof UserDetailsImpl) {
					userId = ((UserDetailsImpl) principal).getUserid();
				}
				UserMasterEntity userModel = userMasterJpaRepository.findByUserId(userId);
				UserSessionEntity userSession = userSessionJpaRepository.findByUserId(userId);
				if (userSession != null && userModel != null) {
					logger.info("User Id:" + userModel.getUserId());
					logger.info("Branch Id:" + userModel.getBranchCode());
					audit.setUserId(userModel.getUserId());
					audit.setApkVersion(String.valueOf(userSession.getCurrentAppVersion()));
					audit.setDeviceImei(userSession.getDeviceImei());
					
					if(CommonUtility.check(userModel.getUserId(), userModel.getFranchiseBranchCode()) && userModel.getUserId().equalsIgnoreCase(userModel.getFranchiseBranchCode()))
						isParentUser = true;

					generalResponse = deliveryServices.getPDCsByUser(userModel, isParentUser);

				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.USER_DETAILS_NOT_FOUND, null, null),
							HttpStatus.OK);
				}

//			} 
//			catch (ExpiredJwtException e2) {
//				e2.printStackTrace();
//				logger.info(ResponseMessages.TOKEN_EXPIRED, e2);
//				logger.error(e2);
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, ResponseMessages.TOKEN_EXPIRED, null, token), HttpStatus.OK);
			} 
			catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
				logger.info(ResponseMessages.INTERNAL_SERVER_ERROR, e);
				logger.error(e);
			}
//		else {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.MISSING_TOKEN, null, token), HttpStatus.OK);
//		}

		try {
			audit.setResultMessage(generalResponse != null ? generalResponse.getBody().getMessage() : null);
			auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception while saving audit log");
			logger.info(e);
		}

		logger.info("****************Response***************");
		logger.info(gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@GetMapping(path="/ack/dr/list")
	ResponseEntity<AppGeneralResponse> getAckDrList(){

		Logger logger = Logger.getLogger("GetAssignedPDCs");	
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		boolean isParentUser = false;
		AuditLogEntity audit = new AuditLogEntity();
		audit.setServiceType("PDC Fetch");

		logger.info(Constants.LOG_START);

			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				String userId = null;
				if (principal instanceof UserDetailsImpl) {
					userId = ((UserDetailsImpl) principal).getUserid();
				}
				UserMasterEntity userModel = userMasterJpaRepository.findByUserId(userId);
				UserSessionEntity userSession = userSessionJpaRepository.findByUserId(userId);
				if (userSession != null && userModel != null) {
					logger.info("User Id:" + userModel.getUserId());
					logger.info("Branch Id:" + userModel.getBranchCode());
					audit.setUserId(userModel.getUserId());
					audit.setApkVersion(String.valueOf(userSession.getCurrentAppVersion()));
					audit.setDeviceImei(userSession.getDeviceImei());
					
					if(CommonUtility.check(userModel.getUserId(), userModel.getFranchiseBranchCode()) && userModel.getUserId().equalsIgnoreCase(userModel.getFranchiseBranchCode()))
						isParentUser = true;

					generalResponse = deliveryServices.getAckPDCsByUser(userModel, isParentUser);

				} else {
					generalResponse = new ResponseEntity<>(
							new AppGeneralResponse(false, ResponseMessages.USER_DETAILS_NOT_FOUND, null, null),
							HttpStatus.OK);
				}

//			} 
//			catch (ExpiredJwtException e2) {
//				e2.printStackTrace();
//				logger.info(ResponseMessages.TOKEN_EXPIRED, e2);
//				logger.error(e2);
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, ResponseMessages.TOKEN_EXPIRED, null, token), HttpStatus.OK);
			} 
			catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
				logger.info(ResponseMessages.INTERNAL_SERVER_ERROR, e);
				logger.error(e);
			}

		try {
			audit.setResultMessage(generalResponse != null ? generalResponse.getBody().getMessage() : null);
			auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception while saving audit log");
			logger.info(e);
		}

		logger.info("****************Response***************");
		logger.info(gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	
	}
	
	@PostMapping(path="/ack/dr")
	ResponseEntity<AppGeneralResponse> acknowledgeDr(@RequestBody String model){
		
		return deliveryServices.acknowledgeDr(model);
	}
	
	@GetMapping(path = "/dly/otp")
	ResponseEntity<AppGeneralResponse> getDeliveryOTP(@RequestParam("mobileNumber") String mobileNumber, @RequestParam("dktNo") String dktNo) {
		Logger logger = Logger.getLogger("DeliveryUpdate");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);

		if (mobileNumber != null) {
			generalResponse = deliveryServices.getDeliveryOTP(mobileNumber, dktNo, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@GetMapping(path = "/cron/trackon/getdrs")
	ResponseEntity<AppGeneralResponse> getTrackonDRS() {
		Logger logger = Logger.getLogger("TrackonGetDRS");
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = deliveryServices.getTrackonDRS(logger);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	

	@PostMapping(path="/redirect")
	ResponseEntity<AppGeneralResponse> redirectCons(@RequestBody String model){
		
		Logger logger = Logger.getLogger("RedirectCons");
		ResponseEntity<AppGeneralResponse> response = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		logger.info(Constants.LOG_START);
		logger.info("REQUEST : "+model);
		if(CommonUtility.check(model)){
			try {
				RedirectionModel data = gson.fromJson(model, RedirectionModel.class);
				response = deliveryServices.redirectCons(data, logger);
			} catch (Exception e) {
				e.printStackTrace();
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
		}
		else{
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
		}
		logger.info("RESPONSE : "+gson.toJson(response));
		logger.info(Constants.LOG_END);
		return response;
		
	}
	
//	@GetMapping(path="/view/pod/image")
//	public ResponseEntity<InputStreamResource> downloadInvoicePdf(@RequestParam("conNo") String conNo){
//		try{
//			
//			String podImgPath = deliveryServices.getDocketImgPath(conNo);
//			
//			if(podImgPath != null && !podImgPath.isEmpty()){
//				String imagesPath = FilesUtil.getProperty("imagesPath");
//				File file = new File(imagesPath.concat(podImgPath));
//				HttpHeaders header = new HttpHeaders();
//				MediaType mediaType = MediaType.parseMediaType("image/png");
//				header.setContentType(mediaType);
//				header.setContentLength(file.length());
//				header.setContentDispositionFormData("attachment", file.getName());
//				
//				InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
//				
//				return new ResponseEntity<InputStreamResource>(isr, header, HttpStatus.OK);
//				
//			}else{
//				return null;
//			}
//			
//
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			return new ResponseEntity<InputStreamResource>(null, null, HttpStatus.OK);
//		}
//	}
	
	@GetMapping(path="/view/pod/image/src/{dktNo}")
	ResponseEntity<byte[]> getBagImage(@PathVariable("dktNo") String dktNo){
		
		String imagesPath = null;
		try{
			
			String podImgPath = deliveryServices.getDocketImgPath(dktNo);
			
			if (podImgPath != null && !podImgPath.isEmpty()) {

				imagesPath = FilesUtil.getProperty("imagesPath");
				String filePath = imagesPath.concat(podImgPath);
				byte[] bytearray =  Files.readAllBytes(Paths.get(imagesPath.concat(podImgPath)));

				if(filePath.split("\\.")[1].equalsIgnoreCase("pdf"))
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(bytearray);
				else if(filePath.split("\\.")[1].equalsIgnoreCase("xml")) {
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(bytearray);
				}else {
					
					return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(bytearray);	
				}
//				
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		return null;
	}
	
//	@GetMapping(path="/generate/pod/pdf/{drsno}/{dktno}")
//	ResponseEntity<AppGeneralResponse> generatePodPdf(@PathVariable("drsno") String drsno, @PathVariable("dktno") String dktno){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		
//		try{
//			response = deliveryServices.getDktPodPdf(drsno, dktno);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
	
//	@GetMapping(path="/show/pod/image")
//	public void downloadInvoicePdf(@RequestParam("conNo") String conNo, HttpServletResponse response){
//		try{
//			final int DEFAULT_BUFFER_SIZE = 10240;
//			String podImgPath = deliveryServices.getDocketImgPath(conNo);
//			
//			if(podImgPath != null && !podImgPath.isEmpty()){
//				String imagesPath = FilesUtil.getProperty("imagesPath");
//				File file = new File(imagesPath.concat(podImgPath));
//				HttpHeaders header = new HttpHeaders();
////				MediaType mediaType = MediaType.parseMediaType("image/png");
////				header.setContentType(mediaType);
////				header.setContentLength(file.length());
////				header.setContentDispositionFormData("attachment", file.getName());
////				header.setContentDispositionFormData("inline; filename=\"" + file.getName() + "\"");
//				header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//				response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
//				BufferedInputStream input = null;
//				BufferedOutputStream output = null;
//				
//				try {
//					// Open streams.
//					input = new BufferedInputStream(new FileInputStream(file));
//					output = new BufferedOutputStream(response.getOutputStream(),
//							DEFAULT_BUFFER_SIZE);
//
//					// Write file contents to response.
//					byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
//					int length;
//					while ((length = input.read(buffer)) > 0) {
//						output.write(buffer, 0, length);
//					}
//				} finally {
//					// Gently close streams.
//					close(output);
//					close(input);
//				}
//				
////				InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
////				
////				return new ResponseEntity<InputStreamResource>(isr, header, HttpStatus.OK);
//				
//			}else{
//				return;
//			}
//			
//
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			return;
//		}
//	}
//	
//	private static void close(Closeable resource) {
//		if (resource != null) {
//			try {
//				resource.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
	
}
