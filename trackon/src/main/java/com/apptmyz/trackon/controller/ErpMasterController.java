package com.apptmyz.trackon.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apptmyz.trackon.services.ErpMasterService;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.ResponseMessages;

@RestController
@RequestMapping(path="/erp/masters")
public class ErpMasterController {
	
	@Autowired
	ErpMasterService erpMasterService;
	
	@GetMapping(path="/pincode")
	ResponseEntity<AppGeneralResponse> getPincodes(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("Get Pincode Details");
	
		try{
			generalResponse = erpMasterService.getPincodeDetails(logger);
			
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
		
	}
	@GetMapping(path="/customerDetails")
	ResponseEntity<AppGeneralResponse> getCustomerDetails(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("GetErpCustomerDetails");
	
		try{
			generalResponse = erpMasterService.getcustomerDetails(logger);
			
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
		
	}
	@GetMapping(path="/contentTypes")
	ResponseEntity<AppGeneralResponse> getContentTypes(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("Get Content Types");
	
		try{
			generalResponse = erpMasterService.getContentTypes(logger);
			
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
		
	}
	@GetMapping(path="/branchDetails")
	ResponseEntity<AppGeneralResponse> getBranchDetails(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("Get Branch Details");
	
		try{
			generalResponse = erpMasterService.getBranchDetails(logger);
			
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
		
	}
	@GetMapping(path="/paperWorkDetails")
	ResponseEntity<AppGeneralResponse> getPaperWorkDetails(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("Get PaperWork Details");
	
		try{
			generalResponse = erpMasterService.getPaperWorkDetails(logger);
			
		
			
		}
		catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return generalResponse;
		
	}
	
	
	

}
