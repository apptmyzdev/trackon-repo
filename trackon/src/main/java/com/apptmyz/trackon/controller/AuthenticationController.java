package com.apptmyz.trackon.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apptmyz.trackon.data.repository.jpa.AppVersionsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.AuditLogJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.BranchMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DeviceInfoJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaBranchJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.MBranchJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.PortalUsersMasterJpaRepository;
//import com.apptmyz.trackon.data.repository.jpa.PortalUsersMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UrlsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserAppVersionsJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionHistoryJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.AppVersionsEntity;
import com.apptmyz.trackon.entities.jpa.AuditLogEntity;
import com.apptmyz.trackon.entities.jpa.BranchMasterEntity;
import com.apptmyz.trackon.entities.jpa.DeviceInfoEntity;
//import com.apptmyz.trackon.entities.jpa.MBranchEntity;
import com.apptmyz.trackon.entities.jpa.PortalUsersMasterEntity;
//import com.apptmyz.trackon.entities.jpa.PortalUsersMasterEntity;
import com.apptmyz.trackon.entities.jpa.UrlsEntity;
import com.apptmyz.trackon.entities.jpa.UserAppVersionsEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionHistoryEntity;
import com.apptmyz.trackon.model.AppUpdateModel;
import com.apptmyz.trackon.model.LoginAuthenticationModel;
import com.apptmyz.trackon.model.UrlsModel;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.JWTUtils;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import io.jsonwebtoken.ExpiredJwtException;

@CrossOrigin
@RestController
@RequestMapping("/authenticate")
public class AuthenticationController {

	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	@Autowired
	private UserSessionJpaRepository userSessionJpaRepository;
	@Autowired
	private UserSessionHistoryJpaRepository userSessionHistoryJpaRepository;

	@Autowired
	private AppVersionsJpaRepository appVersionsJpaRepository;

	@Autowired
	private UrlsJpaRepository urlsJpaRepository;

	// @Autowired
	// private BranchMasterJpaRepository branchMasterJpaRepository;

	@Autowired
	private AuditLogJpaRepository auditLogJpaRepository;

	@Autowired
	private DeviceInfoJpaRepository deviceInfoJpaRepository;

	@Autowired
	private UserAppVersionsJpaRepository userAppVersionsJpaRepository;

	@Autowired
	private PortalUsersMasterJpaRepository portalUsersMasterJpaRepository;

	// @Autowired
	// private MBranchJpaRepository mBranchJpaRepository;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private MaBranchJpaRepository maBranchJpaRepository;

	@Autowired
	JWTUtils jwtUtils;

	private static Logger authenticateLogin = Logger.getLogger("AuthenticateLogin");
	private static Logger userLogout = Logger.getLogger("UserLogout");
	private static Logger checkUpdates = Logger.getLogger("CheckUpdates");

	// user app login authentication
	@PostMapping(path = "/login")
	ResponseEntity<AppGeneralResponse> authenticateLogin(HttpServletRequest request, HttpServletResponse response,
			@RequestBody String credentials) {

		authenticateLogin.info("--REQUEST--");
		authenticateLogin.info("JSON received : " + credentials);
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		AuditLogEntity audit = null;
		String ocrapikey = FilesUtil.getProperty("ocrapikey");
		if (credentials != null && credentials != "") {
			try {
				LoginAuthenticationModel model = gson.fromJson(credentials, LoginAuthenticationModel.class);

				if (model != null) {
					audit = new AuditLogEntity();
					audit.setInputParameters(model.getUserId() + "-" + model.getPassword() + "-" + model.getAppVersion()
							+ "-" + model.getImei());
					audit.setServiceType("Login");
					audit.setTimestamp(new Date());

					authenticateLogin.info("Parsed input json : " + gson.toJson(model));
					model.setOcrApiKey(ocrapikey);
					if (model.getUserId() != null && !model.getUserId().isEmpty()) {
						audit.setUserId(model.getUserId());
						audit.setApkVersion(String.valueOf(model.getAppVersion()));

						if (model.getPassword() != null && !model.getPassword().isEmpty()) {
							if (model.getImei() != null && !model.getImei().isEmpty()) {
								audit.setDeviceImei(model.getImei());
								Authentication authentication = authenticationManager
										.authenticate(new UsernamePasswordAuthenticationToken(model.getUserId(),
												model.getPassword().toUpperCase()));

								SecurityContextHolder.getContext().setAuthentication(authentication);

								UserMasterEntity agent = userMasterJpaRepository.findByUserId(model.getUserId());
								PortalUsersMasterEntity admin = null;
								if (agent == null)
									admin = portalUsersMasterJpaRepository.findByUserId(model.getUserId());

								if (agent != null) {
									if (agent.getActiveFlag() == 1) {
										
										if(model.getAppVersion() > 0 && model.getAppVersion() < 30){
											return new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
													false, "Please install new version from Playstore", null, null), HttpStatus.OK);
										}

										UserSessionEntity userSession = userSessionJpaRepository.findByUserId(agent.getUserId());
										if (userSession != null) {
											if (userSession.getDeviceImei().equals(model.getImei())) {

												model.setUserType(agent.getUserType());
												if (agent.getAllowPickupRegistration() != null)
													model.setAllowPickupRegistration(
															agent.getAllowPickupRegistration() == 1 ? true : false);
												else
													model.setAllowPickupRegistration(false);

												if (agent.getAllowPickupBooking() != null)
													model.setAllowPickupBooking(
															agent.getAllowPickupBooking() == 1 ? true : false);
												else
													model.setAllowPickupBooking(false);

												if (agent.getAllowDelivery() != null)
													model.setAllowDelivery(
															agent.getAllowDelivery() == 1 ? true : false);
												else
													model.setAllowDelivery(false);
												model.setBranchCode(agent.getBranchCode());
												model.setRole(Constants.VA_USER_ROLE_ID);
												model.setRoleType("");
												model.setFranchiseBranchCode(agent.getFranchiseBranchCode());

												userSession.setActiveFlag(1);
												userSession.setCurrentAppVersion(model.getAppVersion());
												userSession.setLoginTimestamp(new Date());
												userSession.setLogoutTimestamp(null);
												userSession.setAndroidVersion(model.getAndroidVersion());
												userSession.setDeviceBrand(model.getDeviceBrand());
												userSession.setDeviceModel(model.getDeviceModel());
												userSessionJpaRepository.save(userSession);

												String token = jwtUtils.createJWTToken(gson.toJson(model),
														Constants.TOKEN_TIMEOUT_SECS, authentication);
												response.setHeader("token", token);
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
																token),
														HttpStatus.OK);

											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 0) {
												model.setUserType(agent.getUserType());
												model.setBranchCode(agent.getBranchCode());
												model.setFranchiseBranchCode(agent.getFranchiseBranchCode());
												
												if (agent.getAllowPickupRegistration() != null)
													model.setAllowPickupRegistration(
															agent.getAllowPickupRegistration() == 1 ? true : false);
												else
													model.setAllowPickupRegistration(false);

												if (agent.getAllowPickupBooking() != null)
													model.setAllowPickupBooking(
															agent.getAllowPickupBooking() == 1 ? true : false);
												else
													model.setAllowPickupBooking(false);

												if (agent.getAllowDelivery() != null)
													model.setAllowDelivery(
															agent.getAllowDelivery() == 1 ? true : false);
												else
													model.setAllowDelivery(false);
												

												userSession.setActiveFlag(1);
												userSession.setDeviceImei(model.getImei());
												userSession.setCurrentAppVersion(model.getAppVersion());
												userSession.setLoginTimestamp(new Date());
												userSession.setAndroidVersion(model.getAndroidVersion());
												userSession.setDeviceBrand(model.getDeviceBrand());
												userSession.setDeviceModel(model.getDeviceModel());
												userSessionJpaRepository.save(userSession);

												String token = jwtUtils.createJWTToken(gson.toJson(model),
														Constants.TOKEN_TIMEOUT_SECS, authentication);
												response.setHeader("token", token);
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
																token),
														HttpStatus.OK);
											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 1) {
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false,
																ResponseMessages.DUPLICATE_LOGIN_ATTEMPT, null, null),
														HttpStatus.OK);
											}
										} else {
											model.setUserType(agent.getUserType());
											model.setBranchCode(agent.getBranchCode());
											if (agent.getAllowPickupRegistration() != null)
												model.setAllowPickupRegistration(
														agent.getAllowPickupRegistration() == 1 ? true : false);
											else
												model.setAllowPickupRegistration(false);

											if (agent.getAllowPickupBooking() != null)
												model.setAllowPickupBooking(
														agent.getAllowPickupBooking() == 1 ? true : false);
											else
												model.setAllowPickupBooking(false);

											if (agent.getAllowDelivery() != null)
												model.setAllowDelivery(agent.getAllowDelivery() == 1 ? true : false);
											else
												model.setAllowDelivery(false);

											model.setRole(Constants.VA_USER_ROLE_ID);
											model.setRoleType("");
											model.setFranchiseBranchCode(agent.getFranchiseBranchCode());

											userSession = new UserSessionEntity();
											userSession.setUserId(agent.getUserId());
											userSession.setDeviceImei(model.getImei());
											userSession.setLoginTimestamp(new Date());
											userSession.setLogoutTimestamp(null);
											userSession.setCurrentAppVersion(model.getAppVersion());
											userSession.setActiveFlag(1);
											userSession.setCreatedTimestamp(new Date());
											userSession.setAndroidVersion(model.getAndroidVersion());
											userSession.setDeviceBrand(model.getDeviceBrand());
											userSession.setDeviceModel(model.getDeviceModel());
											userSessionJpaRepository.save(userSession);

											// JwtUtil j = new JwtUtil();
											String token = jwtUtils.createJWTToken(gson.toJson(model),
													Constants.TOKEN_TIMEOUT_SECS, authentication);
											response.setHeader("token", token);
											generalResponse = new ResponseEntity<AppGeneralResponse>(
													new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
															token),
													HttpStatus.OK);
										}
									} else {
										generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
												false, ResponseMessages.INACTIVE_USER, null, null), HttpStatus.OK);
									}
								} else if (admin != null) {
									// admin
									if (admin.getActiveFlag() == 1) {
										UserSessionEntity userSession = userSessionJpaRepository
												.findByUserId(admin.getUserId());
										if (userSession != null) {
											// admin login
											if (userSession.getDeviceImei().equals(model.getImei())) {

												model.setRegionCode(admin.getRegionCode());
												model.setBranchCode(admin.getBranchCode());
												model.setRole(admin.getPortalUserRoles().getId());
												model.setRoleType(admin.getPortalUserRoles().getRole());

												userSession.setActiveFlag(1);
												userSession.setCurrentAppVersion(model.getAppVersion());
												userSession.setLoginTimestamp(new Date());
												userSession.setLogoutTimestamp(null);
												userSession.setAndroidVersion(model.getAndroidVersion());
												userSession.setDeviceBrand(model.getDeviceBrand());
												userSession.setDeviceModel(model.getDeviceModel());
												userSessionJpaRepository.save(userSession);

												// JwtUtil j = new JwtUtil();
												String token = jwtUtils.createJWTToken(gson.toJson(model),
														Constants.TOKEN_TIMEOUT_SECS, authentication);
												response.setHeader("token", token);
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
																token),
														HttpStatus.OK);
											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 0) {

												model.setRegionCode(admin.getRegionCode());
												model.setBranchCode(admin.getBranchCode());
												model.setRole(admin.getPortalUserRoles().getId());
												model.setRoleType(admin.getPortalUserRoles().getRole());

												userSession.setActiveFlag(1);
												userSession.setDeviceImei(model.getImei());
												userSession.setCurrentAppVersion(model.getAppVersion());
												userSession.setLoginTimestamp(new Date());
												userSession.setAndroidVersion(model.getAndroidVersion());
												userSession.setDeviceBrand(model.getDeviceBrand());
												userSession.setDeviceModel(model.getDeviceModel());
												userSessionJpaRepository.save(userSession);

												String token = jwtUtils.createJWTToken(gson.toJson(model),
														Constants.TOKEN_TIMEOUT_SECS, authentication);
												response.setHeader("token", token);
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
																token),
														HttpStatus.OK);

											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 1) {
												// admin duplicate login attempt
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false,
																ResponseMessages.DUPLICATE_LOGIN_ATTEMPT, null, null),
														HttpStatus.OK);
											}
										} else {
											// admin new login
											model.setRegionCode(admin.getRegionCode());
											model.setBranchCode(admin.getBranchCode());
											model.setRole(admin.getPortalUserRoles().getId());
											model.setRoleType(admin.getPortalUserRoles().getRole());

											userSession = new UserSessionEntity();
											userSession.setUserId(admin.getUserId());
											userSession.setDeviceImei(model.getImei());
											userSession.setLoginTimestamp(new Date());
											userSession.setLogoutTimestamp(null);
											userSession.setCurrentAppVersion(model.getAppVersion());
											userSession.setActiveFlag(1);
											userSession.setCreatedTimestamp(new Date());
											userSession.setAndroidVersion(model.getAndroidVersion());
											userSession.setDeviceBrand(model.getDeviceBrand());
											userSession.setDeviceModel(model.getDeviceModel());
											userSessionJpaRepository.save(userSession);

											// JwtUtil j = new JwtUtil();
											String token = jwtUtils.createJWTToken(gson.toJson(model),
													Constants.TOKEN_TIMEOUT_SECS, authentication);
											response.setHeader("token", token);
											generalResponse = new ResponseEntity<AppGeneralResponse>(
													new AppGeneralResponse(true, ResponseMessages.SUCCESS, model,
															token),
													HttpStatus.OK);

										}
									} else {
										generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
												false, ResponseMessages.INACTIVE_USER, null, null), HttpStatus.OK);
									}
								} else {
									generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
											false, ResponseMessages.INVALID_CREDENTIALS, null, null), HttpStatus.OK);
								}
							} else {
								generalResponse = new ResponseEntity<AppGeneralResponse>(
										new AppGeneralResponse(false, ResponseMessages.EMPTY_DEVICE_IMEI, null, null),
										HttpStatus.OK);
							}
						} else {
							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(false, ResponseMessages.EMPTY_PASSWORD, null, null),
									HttpStatus.OK);
						}
					} else {
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, ResponseMessages.EMPTY_USER_ID, null, null),
								HttpStatus.OK);
					}
				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
				}
			} catch (BadCredentialsException e) {
				authenticateLogin.error(ResponseMessages.INVALID_CREDENTIALS, e);
				authenticateLogin.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INVALID_CREDENTIALS, null, null), HttpStatus.OK);
			} catch (JsonParseException e) {
				e.printStackTrace();
				authenticateLogin.error(ResponseMessages.ERROR_PARSING_REQUEST_DATA, e);
				authenticateLogin.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INCORRECT_DATA_FORMAT, null, null),
						HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				authenticateLogin.error(ResponseMessages.INTERNAL_SERVER_ERROR, e);
				authenticateLogin.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
			}
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
		}

		audit.setResultMessage(generalResponse.getBody().getMessage());
		try {
			if (audit != null)
				auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			authenticateLogin.error(e);
			authenticateLogin.error("Exception while saving audit");
		}
		authenticateLogin.info("--Response-- : " + gson.toJson(generalResponse));

		return generalResponse;
	}

	// user app logout
	@PostMapping(path = "/logout")
	ResponseEntity<AppGeneralResponse> userLogout(@RequestBody String credentials) {

		userLogout.info("--REQUEST--");
		userLogout.info("JSON received : " + credentials);
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		AuditLogEntity audit = null;

		if (credentials != null && credentials != "") {
			audit = new AuditLogEntity();
			try {
				LoginAuthenticationModel model = gson.fromJson(credentials, LoginAuthenticationModel.class);

				if (model != null) {

					audit.setUserId(model.getUserId());
					audit.setApkVersion(String.valueOf(model.getAppVersion()));
					audit.setDeviceImei(model.getImei());
					audit.setServiceType("Logout service");
					userLogout.info("Parsed input json : " + gson.toJson(model));

					if (model.getUserId() != null && !model.getUserId().isEmpty()) {
						if (model.getPassword() != null && !model.getPassword().isEmpty()) {
							if (model.getImei() != null && !model.getImei().isEmpty()) {
								PortalUsersMasterEntity admin = null;
								UserMasterEntity agent = null;
								agent = userMasterJpaRepository.findByUserId(model.getUserId());
								if (agent == null)
									admin = portalUsersMasterJpaRepository.findByUserId(model.getUserId());
								if (agent != null) {
									if (agent.getActiveFlag() == 1) {
										UserSessionEntity userSession = userSessionJpaRepository
												.findByUserIdAndActiveFlag(agent.getUserId(), 1);
										if (userSession != null) {
											if (userSession.getDeviceImei().equals(model.getImei())) {
												userSession.setActiveFlag(0);
												userSession.setLogoutTimestamp(new Date());
												userSessionJpaRepository.save(userSession);

												UserSessionHistoryEntity sessionHistory = new UserSessionHistoryEntity();
												sessionHistory.setDeviceImei(userSession.getDeviceImei());
												sessionHistory.setUserId(userSession.getUserId());
												sessionHistory.setAppVersion(model.getAppVersion());
												sessionHistory.setLoggedInTimestamp(userSession.getLoginTimestamp());
												sessionHistory.setLoggedOutTimestamp(new Date());
												sessionHistory.setCreatedTimestamp(new Date());
												sessionHistory.setAndroidVersion(model.getAndroidVersion());
												sessionHistory.setDeviceBrand(model.getDeviceBrand());
												sessionHistory.setDeviceModel(model.getDeviceModel());
												userSessionHistoryJpaRepository.save(sessionHistory);

												DeviceInfoEntity deviceInfo = deviceInfoJpaRepository
														.findByDeviceImei(model.getImei());
												if (deviceInfo != null && deviceInfo.getActiveFlag()) {
													try {
														deviceInfo.setActiveFlag(false);
														deviceInfoJpaRepository.save(deviceInfo);
													} catch (Exception e) {
														e.printStackTrace();
														userLogout.error(e);
														userLogout.error("***Exception while updating device info***");
													}
												}

												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, null,
																null),
														HttpStatus.OK);

											} else if (!userSession.getDeviceImei().equals(model.getImei())) {

												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false, ResponseMessages.DEVICE_MISMATCH,
																null, null),
														HttpStatus.OK);

											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 1) {
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false,
																ResponseMessages.DUPLICATE_LOGIN_ATTEMPT, null, null),
														HttpStatus.OK);
											}
										} else {
											generalResponse = new ResponseEntity<AppGeneralResponse>(
													new AppGeneralResponse(true, ResponseMessages.USER_NOT_SIGNED_IN,
															null, null),
													HttpStatus.OK);
										}
									} else {
										generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
												false, ResponseMessages.INACTIVE_USER, null, null), HttpStatus.OK);
									}
								}
								// admin logout
								else if (admin != null) {
									if (admin.getActiveFlag() == 1) {
										UserSessionEntity userSession = userSessionJpaRepository
												.findByUserIdAndActiveFlag(admin.getUserId(), 1);
										if (userSession != null) {
											if (userSession.getDeviceImei().equals(model.getImei())) {

												userSession.setActiveFlag(0);
												userSession.setLogoutTimestamp(new Date());
												userSessionJpaRepository.save(userSession);

												UserSessionHistoryEntity sessionHistory = new UserSessionHistoryEntity();
												sessionHistory.setDeviceImei(userSession.getDeviceImei());
												sessionHistory.setUserId(userSession.getUserId());
												sessionHistory.setAppVersion(model.getAppVersion());
												sessionHistory.setLoggedInTimestamp(userSession.getLoginTimestamp());
												sessionHistory.setLoggedOutTimestamp(new Date());
												sessionHistory.setCreatedTimestamp(new Date());
												sessionHistory.setAndroidVersion(model.getAndroidVersion());
												sessionHistory.setDeviceBrand(model.getDeviceBrand());
												sessionHistory.setDeviceModel(model.getDeviceModel());
												userSessionHistoryJpaRepository.save(sessionHistory);

												DeviceInfoEntity deviceInfo = deviceInfoJpaRepository
														.findByDeviceImei(model.getImei());
												if (deviceInfo != null && deviceInfo.getActiveFlag()) {
													try {
														deviceInfo.setActiveFlag(false);
														deviceInfoJpaRepository.save(deviceInfo);
													} catch (Exception e) {
														e.printStackTrace();
														userLogout.error(e);
														userLogout.error("***Exception while updating device info***");
													}
												}

												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(true, ResponseMessages.SUCCESS, null,
																null),
														HttpStatus.OK);
											} else if (!userSession.getDeviceImei().equals(model.getImei())) {

												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false, ResponseMessages.DEVICE_MISMATCH,
																null, null),
														HttpStatus.OK);

											} else if (!userSession.getDeviceImei().equals(model.getImei())
													&& userSession.getActiveFlag() == 1) {
												generalResponse = new ResponseEntity<AppGeneralResponse>(
														new AppGeneralResponse(false,
																ResponseMessages.DUPLICATE_LOGIN_ATTEMPT, null, null),
														HttpStatus.OK);
											}
										} else {
											generalResponse = new ResponseEntity<AppGeneralResponse>(
													new AppGeneralResponse(true, ResponseMessages.USER_NOT_SIGNED_IN,
															null, null),
													HttpStatus.OK);
										}
									} else {
										generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
												false, ResponseMessages.INACTIVE_USER, null, null), HttpStatus.OK);
									}
								} else {
									generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(
											false, ResponseMessages.INVALID_CREDENTIALS, null, null), HttpStatus.OK);
								}
							} else {
								generalResponse = new ResponseEntity<AppGeneralResponse>(
										new AppGeneralResponse(false, ResponseMessages.EMPTY_DEVICE_IMEI, null, null),
										HttpStatus.OK);
							}
						} else {
							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(false, ResponseMessages.EMPTY_PASSWORD, null, null),
									HttpStatus.OK);
						}
					} else {
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, ResponseMessages.EMPTY_USER_ID, null, null),
								HttpStatus.OK);
					}
				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
				}
			} catch (JsonParseException e) {
				e.printStackTrace();
				userLogout.error(ResponseMessages.ERROR_PARSING_REQUEST_DATA, e);
				userLogout.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INCORRECT_DATA_FORMAT, null, null),
						HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				userLogout.error(ResponseMessages.ERROR_PARSING_REQUEST_DATA, e);
				userLogout.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
			}
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
		}

		try {
			audit.setResultMessage(generalResponse.getBody().getMessage());
			audit.setTimestamp(new Date());
			auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			userLogout.info("Exception while saving audit log");
			userLogout.error(e);

		}

		userLogout.info("--Response-- : " + gson.toJson(generalResponse));
		return generalResponse;
	}

	// check updates v2
	@GetMapping(path = "/checkUpdates")
	ResponseEntity<AppGeneralResponse> checkUpdatesV2(@RequestHeader("userId") String userId,
			@RequestHeader("imei") String imei, @RequestHeader("apkVersion") int apkVersion,
			@RequestHeader("token") String token) {

		checkUpdates.info("--REQUEST--");
		final String path = FilesUtil.getProperty("trackonBaseUrl");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		AuditLogEntity audit = new AuditLogEntity();
		List<UrlsModel> uData = null;
		AppUpdateModel appData = new AppUpdateModel();
		int userversion = 0, maxversion = 0;
		checkUpdates.info("User ID: " + userId + ", IMEI: " + imei + ", APK version: " + apkVersion);
		if (token != null && !token.isEmpty()) {
			try {
				// JwtUtil j = new JwtUtil();
				LoginAuthenticationModel host = gson.fromJson(jwtUtils.parseJWT(token), LoginAuthenticationModel.class);
				checkUpdates.info("User details : " + jwtUtils.parseJWT(token));

				if (host != null) {

					audit.setUserId(host.getUserId());
					audit.setApkVersion(String.valueOf(host.getAppVersion()));
					audit.setDeviceImei(host.getImei());
					audit.setServiceType("Check updates");

					if (host.getUserId() != null && !host.getUserId().isEmpty()) {
						AppVersionsEntity global = appVersionsJpaRepository.findTopByOrderByVersionDesc();
						if (global != null) {
							try {
								UserAppVersionsEntity userappversion = userAppVersionsJpaRepository
										.findByUserIdAndActiveFlagEquals(host.getUserId(), 1);
								if (userappversion == null) {
									userappversion = userAppVersionsJpaRepository.findFirst1ByBranchCodeAndActiveFlagEquals(host.getBranchCode(), 1);
									if(userappversion != null)
										userversion = userappversion.getVersion() != null ? userappversion.getVersion() : 0;
								}
								maxversion = global.getVersion() != null ? global.getVersion() : 0;

								if (userappversion != null && userversion > maxversion) {
									checkUpdates.info("*** branch specific update ***");
									appData.setUrl(userappversion.getUrl());
									appData.setVersion(userversion);
								} else {
									checkUpdates.info("*** global update ***");
									appData.setUrl(global.getUrl());
									appData.setVersion(maxversion);
								}
							} catch (Exception e) {
								e.printStackTrace();
								checkUpdates.error(e);
								appData.setUrl(global.getUrl());
								appData.setVersion(global.getVersion());
							}

							List<UrlsEntity> urlsdata = (List<UrlsEntity>) urlsJpaRepository.findAll();
							UrlsModel urlm = null;

							if (urlsdata != null && !urlsdata.isEmpty()) {
								uData = new ArrayList<>();
								for (UrlsEntity u : urlsdata) {
									urlm = new UrlsModel();
									urlm.setId(u.getId());
									urlm.setActiveFlag(u.getActiveFlag());
									urlm.setUrl(u.getUrl());
									urlm.setUrlName(u.getUrlName());
									urlm.setRemarks(u.getRemarks());
									uData.add(urlm);
								}
							}
							appData.setUrlData(uData);

							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(true, ResponseMessages.SUCCESS, appData, null),
									HttpStatus.OK);

						} else {
							checkUpdates.error("*** Global apk url not found ***");
							appData.setUrl("");
							appData.setNotes("");
							appData.setVersion(0);
							uData = new ArrayList<>();
							UrlsModel urls = new UrlsModel();
							urls.setActiveFlag(1);
							urls.setUrl(path);
							// urls.setUrl(Constants.BASE_URL);
							urls.setRemarks("");
							urls.setId(1);
							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(true, ResponseMessages.SUCCESS, appData, null),
									HttpStatus.OK);
						}

					} else {
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false,
										ResponseMessages.MISSING_USER_ID_IN_TOKEN + " (Update check)", null, null),
								HttpStatus.OK);
					}
				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
							HttpStatus.OK);
				}
			} catch (ExpiredJwtException e2) {
				e2.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "session expired, please login again", null, null),
						HttpStatus.OK);
				checkUpdates.info("--Response-- : " + gson.toJson(generalResponse));
				return generalResponse;

			} catch (Exception e) {
				e.printStackTrace();
				checkUpdates.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "Invalid session, please login again", null, null),
						HttpStatus.OK);
				checkUpdates.info("--Response-- : " + gson.toJson(generalResponse));
				return generalResponse;
			}
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, "Token missing, Please login again", null, null), HttpStatus.OK);
			checkUpdates.info("--Response-- : " + gson.toJson(generalResponse));
		}

		try {
			audit.setResultMessage(generalResponse.getBody().getMessage());
			auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			checkUpdates.info("exception while saving audit log");
			checkUpdates.error(e);
		}

		checkUpdates.info("Response :" + gson.toJson(generalResponse));
		return generalResponse;
	}
}
