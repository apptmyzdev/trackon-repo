package com.apptmyz.trackon.controller;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.apptmyz.trackon.data.repository.jpa.AuditLogJpaRepository;
import com.apptmyz.trackon.model.BookingData;
import com.apptmyz.trackon.model.BookingDataQuery;
import com.apptmyz.trackon.model.BookingReportModel;
import com.apptmyz.trackon.model.DeliveriesData;
import com.apptmyz.trackon.model.DeliveriesDataQuery;
import com.apptmyz.trackon.model.DeliveryReportRequestModel;
import com.apptmyz.trackon.model.LoginAuthenticationModel;
import com.apptmyz.trackon.model.PDCModel;
import com.apptmyz.trackon.model.TrackonDRSDetailsModel;
import com.apptmyz.trackon.model.UserSessionData;
import com.apptmyz.trackon.services.PickupServices;
import com.apptmyz.trackon.services.ReportsServices;
import com.apptmyz.trackon.services.UserDetailsImpl;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@CrossOrigin
@RestController
@RequestMapping(path = "/reports")
public class ReportsController {

	@Autowired
	AuditLogJpaRepository auditLogJpaRepository;

	@Autowired
	ReportsServices reportsServices;
	
	@Autowired
	private PickupServices pickupServices;


	@PostMapping(path = "/bookings")
	ResponseEntity<AppGeneralResponse> getBookings(@RequestBody String model) {
		Logger logger = Logger.getLogger("BookingReport");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> response = null;
		List<BookingData> data = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data : " +model);

		try{
			BookingDataQuery ip = gson.fromJson(model, BookingDataQuery.class);
			if (model != null) {
				data = reportsServices.getBookings(ip, logger);
				response = new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
			} else{
				response = new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			response = new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		
		logger.info("Response: " + gson.toJson(response));
		logger.info(Constants.LOG_END);
		return response;
	}
	
	@PostMapping(path = "/deliveries")
	ResponseEntity<AppGeneralResponse> getDeliveries(@RequestBody DeliveriesDataQuery data) {
		Logger logger = Logger.getLogger("DeliveriesReport");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> response = null;
		List<DeliveriesData> list = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data : " + gson.toJson(data));

		if (data != null) {
			
			if(CommonUtility.check(data.getAgentId())){
				
				response = reportsServices.getDeliveries(data, logger);
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "User ID not sent", null, null), HttpStatus.OK);
			}
		} 
		else{
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(response));
		logger.info(Constants.LOG_END);
		return response;
	}
	
	@PostMapping(path="/assigned/pdc")
	ResponseEntity<AppGeneralResponse> getAssignedPdcReport(@RequestBody String request){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("AssignedPdcReport");
		Gson gson = new GsonBuilder().serializeNulls().create();
		log.info(Constants.LOG_START);
		log.info("REQUEST : "+request);
		try{
			DeliveriesDataQuery data = gson.fromJson(request, DeliveriesDataQuery.class);
			List<PDCModel> pdcs = reportsServices.getPDCsData(data);
			if(pdcs != null && !pdcs.isEmpty())
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, pdcs, null), HttpStatus.OK);
			else
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage(), e);
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@PostMapping(path="/assigned/pdc/dockets")
	ResponseEntity<AppGeneralResponse> getAssignedPdcDocketsReport(@RequestBody String request){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("AssignedPdcDocketsReport");
		Gson gson = new GsonBuilder().serializeNulls().create();
		log.info(Constants.LOG_START);
		log.info("REQUEST : "+request);
		try{
			DeliveriesDataQuery data = gson.fromJson(request, DeliveriesDataQuery.class);
			List<PDCModel> pdcs = reportsServices.getPDCsData(data);
			if(pdcs != null && !pdcs.isEmpty())
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, pdcs, null), HttpStatus.OK);
			else
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage(), e);
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@PostMapping(path="/drs/dockets")
	ResponseEntity<AppGeneralResponse> getDrsDocketsReport(@RequestBody String request){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("GetDrsDocketsReport");
		Gson gson = new GsonBuilder().serializeNulls().create();
		log.info(Constants.LOG_START);
		log.info("REQUEST : "+request);
		try{
			DeliveriesDataQuery data = gson.fromJson(request, DeliveriesDataQuery.class);
			List<TrackonDRSDetailsModel> pdcs = reportsServices.getDrsDocketsData(data);
			if(pdcs != null && !pdcs.isEmpty())
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, pdcs, null), HttpStatus.OK);
			else
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			log.error(e.getMessage(), e);
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	@GetMapping(path = "/userSession/data/{flg}")
	ResponseEntity<AppGeneralResponse> getUserDta(@PathVariable("flg") Integer flg) {
		ResponseEntity<AppGeneralResponse> response = null;
//		Logger log = Logger.getLogger("Get User Session Details");
		try {
			List<UserSessionData> usd = reportsServices.getUserSessionDetails(flg);
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, usd, null), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@GetMapping(path = "/get/totalDokets")
	ResponseEntity<AppGeneralResponse> getBookingsData(
			@RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd") String from,
			@RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd") String to,
			@RequestParam("bt") String bookingType) {
		ResponseEntity<AppGeneralResponse> response = null;
		try {
			from = from + " 00:00:00";
			to = to + " 23:59:59";
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date fromdate = df.parse(from);
			Date todate = df.parse(to);
			Map<String, BookingReportModel> brm = reportsServices.getBookingsReport(fromdate, todate, bookingType);
			if (brm != null) {
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(true, ResponseMessages.SUCCESS, brm.values(), null), HttpStatus.OK);
			} else {
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	@GetMapping(path = "/get/reprint/dkt/data")
	ResponseEntity<AppGeneralResponse> getBookingData(@RequestParam("dktNo") String docketNo){
		ResponseEntity<AppGeneralResponse> response = null;
		try{
			response = reportsServices.getBookingDetails(docketNo);
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@GetMapping(path="/redirect/display/image")
	RedirectView redirectToExternalUrl(@RequestParam("imgPath") String imgPath, @RequestParam("dktno") String dktno) {
		
		String url = "";
//		System.out.println(imgPath+"-"+dktno);
		if(CommonUtility.check(dktno, imgPath)){
			url = reportsServices.getImgLink(dktno, imgPath);
		}
		
		
//		if(flg == 1){
//			baseUrl = "https://mobility.trackon.in/trackon/pickup/view/image?imgPath=";
//			redirectUrl = "Pickups/PaperWork/2023/7/7/800000899307JPEG1.png"; // Replace with your desired
//		}
//		else{
//			baseUrl = "https://trackon-efs-to-s3.s3.ap-south-1.amazonaws.com";
//			redirectUrl = "/deliveries/Undeliveries/2022/12/15/19441023.png";// redirect URL
//		}

		// Create a RedirectView and set the redirect URL
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(url);

		return redirectView;
	}
	
	@GetMapping(path="/view/image")
	ResponseEntity<Object> getPickupImage(@RequestParam("imgPath") String imgPath){
		String imagesPath = null;
		try{
			if (imgPath != null && !imgPath.isEmpty()) {
				if(imgPath.contains("Pickups")){					
					imagesPath = FilesUtil.getProperty("ftpFolderPath");
				}else{
					imagesPath = FilesUtil.getProperty("ftpTrackonDelFolderPath");
				}
				String filePath = imagesPath.concat(imgPath);
				byte[] bytearray =  Files.readAllBytes(Paths.get(imagesPath.concat(imgPath)));

				if(filePath.split("\\.")[1].equalsIgnoreCase("pdf"))
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(bytearray);
				else if(filePath.split("\\.")[1].equalsIgnoreCase("xml")) {
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(bytearray);
				}else {
					
					return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(bytearray);	
				}
			}
			else{
				return new ResponseEntity<Object>(new AppGeneralResponse(false, "Image path not sent", null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Object>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
	}
	
	@GetMapping(path = "/dly/data")
	public ResponseEntity<AppGeneralResponse> getDlyData(@RequestParam("userId") String userId,
			@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") String date) {
		ResponseEntity<AppGeneralResponse> response = null;
		try {
			response = reportsServices.getDeliveryStatus(userId, date);
		} catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@PostMapping(path="/dkt/repush/{dktno}")
	ResponseEntity<AppGeneralResponse> docketRepush(@PathVariable String dktno) {
		Logger logger = Logger.getLogger("DocketRepush");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> response = null;
		logger.info(Constants.LOG_START);					
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String userId = null;
		if (principal instanceof UserDetailsImpl) {
			userId = ((UserDetailsImpl) principal).getUserid();
		}
		if (dktno != null) {
			response = reportsServices.docketRepush(dktno, logger, null);
		}else {
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(response));
		logger.info(Constants.LOG_END);
		return response;
	}
	
	@GetMapping(path="/cron/bookings/repush")
	ResponseEntity<AppGeneralResponse> repushBookings(){
		return reportsServices.repushBookings();
	}

	@PostMapping(path="/deliveries/data")
	ResponseEntity<AppGeneralResponse> getDeliveriesData(@RequestBody DeliveryReportRequestModel model){
		ResponseEntity<AppGeneralResponse> response = null;
		try{
			response = reportsServices.getDeliveriesReport(model);
		}
		catch (Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response ;
		
	}
	
	@GetMapping(path="/cron/delivery/repush")
	ResponseEntity<AppGeneralResponse> repushFailedDeliveries(@RequestParam("finalPush") Integer finalPush){
		return reportsServices.repushFailedDeliveries(finalPush);
	}
	
	@GetMapping(path="/erp/assigned/pickups")
	ResponseEntity<AppGeneralResponse> getErpAssignedPickups(){
		return pickupServices.getErpAssignedPickups();
	}
	
	@PostMapping(path="/save/drs")
	ResponseEntity<AppGeneralResponse> saveDrs(@RequestBody String model){
		return reportsServices.saveDrs(model);
	}
	
	@GetMapping(path="/pincode/details")
	ResponseEntity<AppGeneralResponse> getPincodeDetails(@RequestParam("pincode") Integer pincode){
		return reportsServices.getPincodeDetails(pincode);
	}
	
//	@PostMapping(path =  "/logout/user" )
//	ResponseEntity<AppGeneralResponse> logout(@RequestBody LoginAuthenticationModel model) {
//		ResponseEntity<AppGeneralResponse> response = null;	
//		try{
//			Logger logger = Logger.getLogger("UserLogout");
//			logger.info("REQUEST : " + model.getUserId() + " --- " + model.getImei() );
//			response = reportsServices.logoutUser(model, logger);
//		}catch (Exception e) {
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response ;
//	}
}
