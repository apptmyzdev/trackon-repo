//package com.apptmyz.trackon.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.apptmyz.trackon.services.TcplMastersService;
//import com.apptmyz.trackon.tcpl.entities.jpa.CountrymasterEntity;
//import com.apptmyz.trackon.tcpl.repository.CountrymasterJpaRepository;
//import com.apptmyz.trackon.utils.AppGeneralResponse;
//import com.apptmyz.trackon.utils.ResponseMessages;
//
//@RestController
//@RequestMapping(path="/tcpl/masters")
//public class TcplMastersController {
//	
//	@Autowired
//	TcplMastersService tcplservice;
//	
//	@GetMapping(path="/countries")
//	ResponseEntity<AppGeneralResponse> getCountriesMaster(){
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateCountriesMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//		
//	}
//	
//	@GetMapping(path="/itemGroup")
//	ResponseEntity<AppGeneralResponse> getItemGroupMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateItemGroupMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/item")
//	ResponseEntity<AppGeneralResponse> getItemMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateItemMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//
//	@GetMapping(path="/ndrs")
//	ResponseEntity<AppGeneralResponse> getNdrsMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateNdrsMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/ndrStatus")
//	ResponseEntity<AppGeneralResponse> getNdrStatusMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateNdrStatusMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/opsAirlineVehicle")
//	ResponseEntity<AppGeneralResponse> getOpsAirlineVehicleMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsAirlineVehicleMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/opsAirlineVehicleName")
//	ResponseEntity<AppGeneralResponse> getOpsAirlineVehicleNameMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsAirlineVehicleNameMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/opsColoader")
//	ResponseEntity<AppGeneralResponse> getOpsColoaderMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsColoaderMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/opsColoaderType")
//	ResponseEntity<AppGeneralResponse> getOpsColoaderTypeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateGetOpsColoaderTypeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/paperwork")
//	ResponseEntity<AppGeneralResponse> getPaperworkMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updatePaperworkMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/pincodeArea")
//	ResponseEntity<AppGeneralResponse> getPincodeAreaMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updatePincodeAreaMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/pincode")
//	ResponseEntity<AppGeneralResponse> getPincodeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updatePincodeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/state")
//	ResponseEntity<AppGeneralResponse> getStateMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateStateMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblRole")
//	ResponseEntity<AppGeneralResponse> getTblRoleMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblRoleMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblUserInRole")
//	ResponseEntity<AppGeneralResponse> getTblUserInRoleMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblUserInRoleMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblUser")
//	ResponseEntity<AppGeneralResponse> getTblUserMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblUserMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/vendor")
//	ResponseEntity<AppGeneralResponse> getVendorMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateVendorMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/zone")
//	ResponseEntity<AppGeneralResponse> getZoneMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateZoneMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/city")
//	ResponseEntity<AppGeneralResponse> getCityMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateCityMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/content")
//	ResponseEntity<AppGeneralResponse> getContentMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateContentMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/customer")
//	ResponseEntity<AppGeneralResponse> getCustomerMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateCustomerMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/designation")
//	ResponseEntity<AppGeneralResponse> getDesignationMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateDesignationMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/issueDetails")
//	ResponseEntity<AppGeneralResponse> getIssueDetailsMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateIssueDetailsMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/issueType")
//	ResponseEntity<AppGeneralResponse> getIssueTypeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateIssueTypeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/mtBranch")
//	ResponseEntity<AppGeneralResponse> getMtBranchMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateMtBranchMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/office")
//	ResponseEntity<AppGeneralResponse> getOfficeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOfficeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/primeCountry")
//	ResponseEntity<AppGeneralResponse> getPrimeCountryMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updatePrimeCountryMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblRo")
//	ResponseEntity<AppGeneralResponse> getTblRoMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblRoMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblArea")
//	ResponseEntity<AppGeneralResponse> getTblAreaMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblAreaMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/tblDelvBoy")
//	ResponseEntity<AppGeneralResponse> getTblDelvBoyMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblDelvBoyMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/officeType")
//	ResponseEntity<AppGeneralResponse> getOfficeTypeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOfficeTypeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		
//		return response;
//	}
//	
//	@GetMapping(path="/opsHubCutoff")
//	ResponseEntity<AppGeneralResponse> getOpsHubCutoffMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsHubCutoffMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	
//	@GetMapping(path="/opsMode") 
//	ResponseEntity<AppGeneralResponse> getOpsModeMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsModeMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	@GetMapping(path="/opsNextTrack")
//	ResponseEntity<AppGeneralResponse> getOpsNextTrackMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsNextTrackMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	
//	@GetMapping(path="/opsStatus")
//	ResponseEntity<AppGeneralResponse> getOpsStatusMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateOpsStatusMaster();
//		}
//		catch(Exception e){
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	
//	@GetMapping(path="/release")
//	ResponseEntity<AppGeneralResponse> getReleaseMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateReleaseMaster();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	
//	@GetMapping(path="/tblRateZone")
//	ResponseEntity<AppGeneralResponse> getTblRateZoneMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblRateZoneMaster();
//		}
//		catch(Exception e){
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//	@GetMapping(path="/tblHoliday")
//	ResponseEntity<AppGeneralResponse> getTblHolidayMaster() {
//		
//		ResponseEntity<AppGeneralResponse> response = null;
//		try{
//			response = tcplservice.updateTblHolidayMaster();
//		}
//		catch(Exception e){
//			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return response;
//	}
//	
//}
