//package com.apptmyz.trackon.controller;
//
//import org.apache.log4j.Logger;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.apptmyz.trackon.entities.jpa.AuditLogEntity;
//import com.apptmyz.trackon.model.LoginAuthenticationModel;
//import com.apptmyz.trackon.utils.AppGeneralResponse;
//import com.apptmyz.trackon.utils.CommonUtility;
//import com.apptmyz.trackon.utils.ResponseMessages;
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//
//@RestController
//@RequestMapping(path="/new/authenticate")
//public class NewAuthenticationController {
//
//	@PostMapping(path="/app/login")
//	ResponseEntity<AppGeneralResponse> appLogin(@RequestBody String credentials){
//		
//		Logger log = Logger.getLogger("AuthenticateLogin");
//		log.info("--REQUEST--");
//		log.info("JSON received : "+credentials);
//		Gson gson = new GsonBuilder().serializeNulls().create();
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		AuditLogEntity audit = null;
//		
//		if (credentials != null && !credentials.isEmpty()) {
//			
//			try{
//				LoginAuthenticationModel model = gson.fromJson(credentials, LoginAuthenticationModel.class);
//				
//				if(model != null){
//					log.info("REQUEST DATA : "+gson.toJson(model));
//					
//					audit = new AuditLogEntity();
//					audit.setInputParameters(model.getUserId() + "-" + model.getPassword() + "-" + model.getAppVersion() + "-" + model.getImei());
//					audit.setServiceType("Login");
//					
//					if (CommonUtility.check(model.getUserId() ,model.getPassword(), model.getImei())) {
//						
//						audit.setUserId(model.getUserId());
//						audit.setApkVersion(String.valueOf(model.getAppVersion()));
//					}
//					else {
//						generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "User ID/Password/IMEI not sent", null, null), HttpStatus.OK);
//					}
//				}
//				else{
//					generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
//				}
//			}
//			catch(Exception e){
//				e.printStackTrace();
//				log.info(e.getMessage(), e);
//				generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null,null), HttpStatus.OK);
//			}
//			
//		}
//		else{
//			generalResponse = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null,null), HttpStatus.OK);
//		}
//		
//	}
//	
//}
