package com.apptmyz.trackon.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.apptmyz.trackon.data.repository.jpa.DeviceInfoJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.DeviceInfoEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.model.TrendChartModel;
import com.apptmyz.trackon.services.DashboardServices;
import com.apptmyz.trackon.services.ReportsServices;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.PushDataModel;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@RestController
@RequestMapping(path="/dashboard")
public class WebDashboardController {
	
	@Autowired
	private DashboardServices dashboardService;

	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	
	@Autowired
	private UserSessionJpaRepository userSessionJpaRepository;
	
	@Autowired
	private DeviceInfoJpaRepository deviceInfoJpaRepository;
	
	@Autowired
	private ReportsServices reportsService;
	
	@GetMapping(path="/trend/graph")
	ResponseEntity<AppGeneralResponse> getTrendGraph(){
		Logger log = Logger.getLogger("GetTrendGraph");
		ResponseEntity<AppGeneralResponse> response = null;
		try{
			TrendChartModel data = dashboardService.getTrendGraphData();
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, data, null), HttpStatus.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
		}
		
		return response;
		
	}
	
	@GetMapping(path =  "/force/logout" )
	ResponseEntity<AppGeneralResponse> logoutUser(@RequestParam("userId") String userId) {
		Logger logger = Logger.getLogger("ForceLogout");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info("REQUEST : " + userId);
		Set<String> tokenslist = null;
		try {
			if (userId != null && !userId.isEmpty()) {
				UserMasterEntity user = userMasterJpaRepository.findByUserId(userId);
				if (user != null) {
					UserSessionEntity session = userSessionJpaRepository.findByUserId(userId);
					if (session != null) {
						 if (session.getActiveFlag() == 1) {
								 session.setActiveFlag(0);
								 session.setLogoutTimestamp(new Date());
								 userSessionJpaRepository.save(session);

						List<DeviceInfoEntity> devices = deviceInfoJpaRepository
								.findByUserIdAndActiveFlagIsTrue(userId);
						if (devices != null && !devices.isEmpty()) {
							String OAuthToken = CommonUtility.getAccessToken();
							logger.info("No. of logged in devices : " + devices.size());
							tokenslist = new HashSet<>();
								for (DeviceInfoEntity d : devices) {
									d.setActiveFlag(false);
									deviceInfoJpaRepository.save(d);
									
									try {
										PushDataModel pdata = new PushDataModel();
										pdata.setId(Constants.PUSH_FOR_LOGOUT);
										CommonUtility.sendPushNotificationV2("Force Logout",
												"Log out user", logger, gson, pdata, d.getPushToken(), OAuthToken);
//										reportsService.multiplePushNotifications(new ArrayList<>(tokenslist), logger);
										
									} catch (Exception e) {
										e.printStackTrace();
									}
									
//									tokenslist.add(d.getPushToken());
								}
							

							
							
						}

						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(true, ResponseMessages.SUCCESS, null, null), HttpStatus.OK);
						 }
						 else {
						 generalResponse = new
						 ResponseEntity<AppGeneralResponse>(new
						 AppGeneralResponse(false, "User is not signed in",
						 null, null), HttpStatus.OK);
						 }
					} else {
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, "User is not signed in", null, null), HttpStatus.OK);
					}
				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, "Not an existing user", null, null), HttpStatus.OK);
				}
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "Enter user ID", null, null), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Exception ", e);
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}

		logger.info("Response : " + gson.toJson(generalResponse));
		return generalResponse;
	}
	
}
