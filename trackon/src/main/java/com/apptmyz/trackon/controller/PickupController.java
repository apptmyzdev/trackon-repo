package com.apptmyz.trackon.controller;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.apptmyz.trackon.data.repository.jpa.AuditLogJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.AuditLogEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.model.BookingDataModel;
import com.apptmyz.trackon.model.DktValidationModel;
import com.apptmyz.trackon.model.ErpDktValidationModel;
import com.apptmyz.trackon.model.PickupAssignmentModel;
import com.apptmyz.trackon.model.PickupCancelModel;
import com.apptmyz.trackon.model.PickupRegistrationData;
import com.apptmyz.trackon.model.PickupRegistrationDataQuery;
import com.apptmyz.trackon.model.PickupRegistrationSubmit;
import com.apptmyz.trackon.model.PickupRescheduleModel;
import com.apptmyz.trackon.model.PickupScanSubmitDataModel;
import com.apptmyz.trackon.model.PrsRequestModel;
import com.apptmyz.trackon.services.DeliveryServices;
import com.apptmyz.trackon.services.PickupServices;
import com.apptmyz.trackon.services.UserDetailsImpl;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.FilesUtil;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@CrossOrigin
@RestController
@RequestMapping(path = "/pickup")
public class PickupController {

	@Autowired
	AuditLogJpaRepository auditLogJpaRepository;

	@Autowired
	PickupServices pickupServices;


	@PostMapping(path = "/register")
	ResponseEntity<AppGeneralResponse> pickupRegistration(@RequestBody PickupRegistrationSubmit data) {
		Logger logger = Logger.getLogger("PickupRegitration");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Data from device: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.register(data, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}


	@PostMapping(path="/pickuplist")
	List<PickupRegistrationData> getPickupRegistrationData(@RequestBody PickupRegistrationDataQuery requestData){
		Logger logger = Logger.getLogger("PickupRegistrationList");
		Gson gson = new GsonBuilder().serializeNulls().create();
		logger.info(Constants.LOG_START);
		logger.info("Request Data : " + gson.toJson(requestData));
		if (requestData != null) {
			logger.info("Calling Data");
			return pickupServices.PickupList(requestData, logger);
		}else {
			logger.info("incorrect input");
			return new ArrayList<PickupRegistrationData>();
		}
	}
	
	@PostMapping(path="/assign")
	ResponseEntity<AppGeneralResponse> pickupAssign(@RequestBody PickupAssignmentModel data) {
		Logger logger = Logger.getLogger("PickupAssignment");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.assign(data, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}

	@PostMapping(path="/cancel")
	ResponseEntity<AppGeneralResponse> pickupCancel(@RequestBody PickupCancelModel data) {
		Logger logger = Logger.getLogger("PickupCancel");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.cancel(data, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@PostMapping(path="/reschedule")
	ResponseEntity<AppGeneralResponse> pickupReschedule(@RequestBody PickupRescheduleModel data) {
		Logger logger = Logger.getLogger("PickupReschedule");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.reschedule(data, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}

	@PostMapping(path="/prs")
	ResponseEntity<AppGeneralResponse> GetPRSData(@RequestBody PrsRequestModel data) {
		Logger logger = Logger.getLogger("PRSGet");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.getPrsData(data, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@PostMapping(path="/booking")
	ResponseEntity<AppGeneralResponse> bookingSubmit(@RequestBody String model) {
		Logger logger = Logger.getLogger("Booking");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + model);

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String userId = null;
		if (principal instanceof UserDetailsImpl) {
			userId = ((UserDetailsImpl) principal).getUserid();
		}
		
		try{
			BookingDataModel data = gson.fromJson(model, BookingDataModel.class);
			if (data != null) {
				generalResponse = pickupServices.bookingSubmit(data, logger, userId, data.getBookingBranchCode());
			} else {
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
						HttpStatus.OK);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
					HttpStatus.OK);
		}
		
		
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@PostMapping(path="/pickupscan")
	ResponseEntity<AppGeneralResponse> pickupScanning(@RequestBody PickupScanSubmitDataModel data){
		Logger logger = Logger.getLogger("PickupScan");
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String userId = null;
		if (principal instanceof UserDetailsImpl) {
			userId = ((UserDetailsImpl) principal).getUserid();
		}
		
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info(Constants.LOG_START);
		logger.info("Request Data: " + gson.toJson(data));

		if (data != null) {
			generalResponse = pickupServices.pickupScan(data, userId, logger);
		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.NO_DELIVERY_DATA_FROM_DEVICE, null, null),
					HttpStatus.OK);
		}
		logger.info("Response: " + gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@PostMapping(path="/validate/dkt")
	ResponseEntity<AppGeneralResponse> validateDktNo(@RequestBody String model){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("DocketValidation");
		Gson gson = new GsonBuilder().serializeNulls().create();
		log.info("REQUEST : "+model);
		if(CommonUtility.check(model)){
			try{
				DktValidationModel reqData = gson.fromJson(model, DktValidationModel.class);
				ErpDktValidationModel resData = pickupServices.validateDktNo(reqData, gson,log, 0);
				if(resData != null && resData.getResult()){
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, resData, null),
							HttpStatus.OK);
				}
				else{
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, resData.getMsg(), null, null),
							HttpStatus.OK);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
			}
			
		}
		else{
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
					HttpStatus.OK);
		}
		return response;
		
	}
	
	@GetMapping(path="/view/image")
	ResponseEntity<Object> getPickupImage(@RequestParam("imgPath") String imgPath){
		String imagesPath = null;
		try{
			if (imgPath != null && !imgPath.isEmpty()) {

				imagesPath = FilesUtil.getProperty("imagesPath");
				String filePath = imagesPath.concat(imgPath);
				byte[] bytearray =  Files.readAllBytes(Paths.get(imagesPath.concat(imgPath)));

				if(filePath.split("\\.")[1].equalsIgnoreCase("pdf"))
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_PDF).body(bytearray);
				else if(filePath.split("\\.")[1].equalsIgnoreCase("xml")) {
					return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(bytearray);
				}else {
					
					return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(bytearray);	
				}
			}
			else{
				return new ResponseEntity<Object>(new AppGeneralResponse(false, "Image path not sent", null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Object>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
	}
	
	@PostMapping(path="/validate/dkt/new")
	ResponseEntity<AppGeneralResponse> validateDktNoNew(@RequestBody String model){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("DocketValidation");
		Gson gson = new GsonBuilder().serializeNulls().create();
		log.info("REQUEST : "+model);
		if(CommonUtility.check(model)){
			try{
				DktValidationModel reqData = gson.fromJson(model, DktValidationModel.class);
				ErpDktValidationModel resData = pickupServices.validateDktNo(reqData, gson,log, 1);
				if(resData != null && resData.getResult()){
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(true, ResponseMessages.SUCCESS, resData, null),
							HttpStatus.OK);
				}
				else{
					response = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, resData.getMsg(), null, null),
							HttpStatus.OK);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
			}
			
		}
		else{
			response = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
					HttpStatus.OK);
		}
		return response;
		
	}
	
//	@GetMapping("/redirect/display/image/{flg}")
//	public RedirectView redirectToExternalUrl(@PathVariable("flg") Integer flg) {
//		
//		String redirectUrl = "";
//		String baseUrl = "";
//		if(flg == 1){
//			baseUrl = "https://mobility.trackon.in/trackon/pickup/view/image?imgPath=";
//			redirectUrl = "Pickups/PaperWork/2023/7/7/800000899307JPEG1.png"; // Replace with your desired
//		}
//		else{
//			baseUrl = "https://trackon-efs-to-s3.s3.ap-south-1.amazonaws.com";
//			redirectUrl = "/deliveries/Undeliveries/2022/12/15/19441023.png";// redirect URL
//		}
//
//		// Create a RedirectView and set the redirect URL
//		RedirectView redirectView = new RedirectView();
//		redirectView.setUrl(redirectUrl);
//
//		return redirectView;
//	}
	
	@PostMapping(path="/submit/BI/pickup")
	ResponseEntity<AppGeneralResponse> submitBIPickup(@RequestBody String model){
		
		if(CommonUtility.check(model)){
			return pickupServices.saveBIpickup(model);
		}else{
			return new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
		}
		
	}
	
}
