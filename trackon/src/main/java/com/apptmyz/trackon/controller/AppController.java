package com.apptmyz.trackon.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apptmyz.trackon.data.repository.jpa.AuditLogJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.DeviceInfoJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.MaPincodeJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserMasterJpaRepository;
import com.apptmyz.trackon.data.repository.jpa.UserSessionJpaRepository;
import com.apptmyz.trackon.entities.jpa.AuditLogEntity;
import com.apptmyz.trackon.entities.jpa.DeviceInfoEntity;
import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;
import com.apptmyz.trackon.entities.jpa.ReasonsMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.entities.jpa.UserSessionEntity;
import com.apptmyz.trackon.model.AssignedToModel;
import com.apptmyz.trackon.model.CashFreightRequestModel;
import com.apptmyz.trackon.model.DeviceRegistrationModel;
import com.apptmyz.trackon.model.LoginAuthenticationModel;
import com.apptmyz.trackon.model.PincodeModel;
import com.apptmyz.trackon.services.MasterDataServices;
import com.apptmyz.trackon.services.UserDetailsImpl;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.Constants;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@CrossOrigin
@RestController
@RequestMapping("/app")
public class AppController {

	@Autowired
	private MasterDataServices masterDataServices;

	@Autowired
	private AuditLogJpaRepository auditLogJpaRepository;

	@Autowired
	private DeviceInfoJpaRepository deviceInfoJpaRepository;
	
	@Autowired
	private UserMasterJpaRepository userMasterJpaRepository;
	
	@Autowired
	private UserSessionJpaRepository userSessionJpaRepository;
	
//	@Autowired
//	private MPincodeJpaRepository mPincodeJpaRepository;

	@Autowired
	private MaPincodeJpaRepository maPincodeJpaRepository;
	
	private SimpleDateFormat longformat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private SimpleDateFormat shortformat = new SimpleDateFormat("dd-MM-yyyy");

	@GetMapping(path = "/data/master/deps")
	ResponseEntity<AppGeneralResponse> getDepsMaster(@RequestParam("maxVersion") Optional<Integer> maxVersion) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getDeps(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/uom/{param}")
	ResponseEntity<AppGeneralResponse> getUom(@PathVariable String param, @RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getUoms(param, maxVersion);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/material")
	ResponseEntity<AppGeneralResponse> getMaterials(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getMaterials(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/bookingtypes")
	ResponseEntity<AppGeneralResponse> getBookingTypes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getBookingTypes(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	
	@GetMapping(path="/data/master/paymentmodes")
	ResponseEntity<AppGeneralResponse> getPaymentModes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getPaymentModes(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}


	@GetMapping(path="/data/master/pkgtypes")
	ResponseEntity<AppGeneralResponse> getPackageTypes( @RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getPackageTypes(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	
	@GetMapping(path="/data/master/pickupslots")
	ResponseEntity<AppGeneralResponse> getPickupSlots(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getPickupSlots(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	@GetMapping(path = "/data/master/reasons")
	ResponseEntity<AppGeneralResponse> getReasonsMaster(
			@RequestParam("maxVersion") Optional<Integer> maxVersion) {
		Logger logger = Logger.getLogger("GetAppReasonsMaster");
		LoginAuthenticationModel userModel = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		logger.info("--Request-- : " + maxVersion);
//		if (token != null && !token.isEmpty()) {
			try {

//				JwtUtil j = new JwtUtil();
//				userModel = gson.fromJson(j.parseJWT(token), LoginAuthenticationModel.class);
//				logger.info("user details :" + j.parseJWT(token));
//				if (userModel != null) {
					List<ReasonsMasterEntity> reasonsdata = null;
					generalResponse = masterDataServices.getReasons(maxVersion);
					logger.info("data retrieved :" + gson.toJson(reasonsdata));
				
//				else {
//					generalResponse = new ResponseEntity<AppGeneralResponse>(
//							new AppGeneralResponse(false, ResponseMessages.USER_DETAILS_NOT_FOUND, null, null),
//							HttpStatus.OK);
//				}

			}
//			catch (ExpiredJwtException e2) {
//				e2.printStackTrace();
//				logger.error(ResponseMessages.TOKEN_EXPIRED, e2);
//				logger.error(e2);
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, ResponseMessages.TOKEN_EXPIRED, null, null), HttpStatus.OK);
//			} 
			catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
				logger.error(ResponseMessages.INTERNAL_SERVER_ERROR, e);
				logger.error(e);
			}
//		} 
//		else {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.MISSING_TOKEN, null, null), HttpStatus.OK);
//		}

		logger.info("Response : " + gson.toJson(generalResponse));
		return generalResponse;
	}

	// device registration
	@PostMapping(path = "/device/registration")
	ResponseEntity<AppGeneralResponse> deviceRegistration(@RequestBody String data) {

		Logger logger = Logger.getLogger("DeviceRegistration");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		AuditLogEntity audit = new AuditLogEntity();
		audit.setServiceType("Device Registration");
		logger.info(Constants.LOG_START);
		logger.info("Input data");
		logger.info(data);
		if (data != null && !data.isEmpty()) {
			try {
				DeviceRegistrationModel model = gson.fromJson(data, DeviceRegistrationModel.class);
				if (model != null) {

					if (model.getDeviceImei() != null && !model.getDeviceImei().isEmpty()) {
						if (model.getPushToken() != null && !model.getPushToken().isEmpty()) {

							audit.setApkVersion(String.valueOf(model.getAppVersion()));
							audit.setUserId(model.getUserId());
							audit.setDeviceImei(model.getDeviceImei());

							DeviceInfoEntity device = deviceInfoJpaRepository.findByDeviceImei(model.getDeviceImei());
							if (device == null) {
								device = new DeviceInfoEntity();
							}
							device.setActiveFlag(true);
							device.setAppVersion(model.getAppVersion());
							device.setDeviceAvailableMemory(model.getDeviceAvailableMemory());
							device.setDeviceBattery(model.getDeviceBattery());
							device.setDeviceBrand(model.getDeviceBrand());
							device.setDeviceImei(model.getDeviceImei());
							device.setDeviceMac(model.getDeviceMac());
							device.setDeviceModel(model.getDeviceModel());
							device.setDeviceName(model.getDeviceName());
							device.setDeviceOsVersion(model.getDeviceOsVersion());
							device.setDeviceTotalMemory(model.getDeviceTotalMemory());
							device.setGpsStatus(model.getGpsStatus());
							device.setPushToken(model.getPushToken());
							device.setUserId(model.getUserId());
							device.setUpdatedTimestamp(new Date());

							device = deviceInfoJpaRepository.save(device);

							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(true, ResponseMessages.SUCCESS, device.getDeviceId(), null),
									HttpStatus.OK);

						} else {
							generalResponse = new ResponseEntity<AppGeneralResponse>(
									new AppGeneralResponse(false, "Push token not found", null, null), HttpStatus.OK);
						}

					} else {
						generalResponse = new ResponseEntity<AppGeneralResponse>(
								new AppGeneralResponse(false, "Device IMEI not found", null, null), HttpStatus.OK);
					}

				} else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null),
							HttpStatus.OK);
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e);
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
			}

		} else {
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
		}

		try {
			logger.info(gson.toJson(generalResponse));
			audit.setResultMessage(generalResponse.getBody().getMessage());
			auditLogJpaRepository.save(audit);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while paring response");
		}
		return generalResponse;
	}

	// manager dashboard data
	@GetMapping(path = "/manager/dashboard")
	ResponseEntity<AppGeneralResponse> managerDashboard(
			@RequestParam("fromdate") String fromdate, @RequestParam("todate") String todate) {
		Logger logger = Logger.getLogger("ManagerDashboard");
		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		LoginAuthenticationModel user = null;
		Gson gson = new GsonBuilder().serializeNulls().create();

		logger.info(Constants.LOG_START);
//		if (token != null && !token.isEmpty()) {
			try {
//				JwtUtil j = new JwtUtil();
//				user = gson.fromJson(j.parseJWT(token), LoginAuthenticationModel.class);
//				logger.info("user details :" + j.parseJWT(token));

				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				String userId = null;
				if (principal instanceof UserDetailsImpl) {
					userId = ((UserDetailsImpl) principal).getUserid();
				}
				UserMasterEntity userModel = userMasterJpaRepository.findByUserId(userId);
				UserSessionEntity userSession = userSessionJpaRepository.findByUserId(userId);
				if (userModel != null) {
					Date from = null;
					Date to = null;
					if (fromdate.isEmpty() || todate.isEmpty()) {
						from = longformat.parse(shortformat.format(new Date()) + " 00:00:00");
						to = longformat.parse(shortformat.format(new Date()) + " 23:59:59");
					} else {
						from = longformat.parse(fromdate + " 00:00:00");
						to = longformat.parse(todate + " 23:59:59");
					}

				} 
				else {
					generalResponse = new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, ResponseMessages.USER_DETAILS_NOT_FOUND, null, null),
							HttpStatus.OK);
				}
//			} 
//			catch (ExpiredJwtException e2) {
//				e2.printStackTrace();
//				logger.error(ResponseMessages.TOKEN_EXPIRED, e2);
//				logger.error(e2);
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, ResponseMessages.TOKEN_EXPIRED, null, null), HttpStatus.OK);
			} 
			catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null),
						HttpStatus.OK);
				logger.error(ResponseMessages.INTERNAL_SERVER_ERROR, e);
				logger.error(e);
			}

//		}
//		else {
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.MISSING_TOKEN, null, null), HttpStatus.OK);
//		}

		logger.info("****************Response***************");
		logger.info(gson.toJson(generalResponse));
		logger.info(Constants.LOG_END);
		return generalResponse;
	}
	
	@GetMapping(path = "/data/master/custcodemaster/{param}")
	ResponseEntity<AppGeneralResponse> getCustomersByCustomerCode(@PathVariable String param){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getCustomersByCustomerCode(param);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/consignorbycustcode/{param}")
	ResponseEntity<AppGeneralResponse> getConsignorsByCustomerCode(@PathVariable String param){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getConsignorsByCustCode(param);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/consignorbypincode/{param}")
	ResponseEntity<AppGeneralResponse> getConsignorsByPincode(@PathVariable Integer param){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getConsignorsByPincode(param);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	@GetMapping(path = "/data/master/consigneebypincode/{param}")
	ResponseEntity<AppGeneralResponse> getConsigneeByPincode(@PathVariable Integer param){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getConsigneeByPincode(param);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	
//	@GetMapping(path = "/data/master/odabypin/{param}")
//	ResponseEntity<AppGeneralResponse> getOdaByPin(@PathVariable Integer param){
//			ResponseEntity<AppGeneralResponse> generalResponse = null;
//			try {
//				generalResponse = masterDataServices.getOdaByPin(param);
//			} catch (Exception e) {
//				e.printStackTrace();
//				generalResponse = new ResponseEntity<AppGeneralResponse>(
//						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//			}
//			return generalResponse;
//		}
	
	@GetMapping(path = "/data/master/allodapincodes")
	ResponseEntity<AppGeneralResponse> getAllOdaPinData(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getAllOdaPinCodeData();
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	@GetMapping(path = "/data/master/svctypes")
	ResponseEntity<AppGeneralResponse> getServiceTypes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getServiceTypes(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/shipmodes")
	ResponseEntity<AppGeneralResponse> getShippingModes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getShipModes(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	
	@GetMapping(path = "/data/master/recurringfreq")
	ResponseEntity<AppGeneralResponse> getRecurringFrequency(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getRecurringFrequency(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/statemaster")
	ResponseEntity<AppGeneralResponse> getStateMaster(){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getStateMaster();
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/shipmenttype")
	ResponseEntity<AppGeneralResponse> getShipmentType(){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getShipmentType();
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	@GetMapping(path = "/data/master/pstatusvalues")
	ResponseEntity<AppGeneralResponse> getPickupStatus(){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getPickupStatus();
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/assignmenttypes")
	ResponseEntity<AppGeneralResponse> getPickupAssignmentTypes(){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getPickupAssigmentType();
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/pickupcancelrescreasons")
	ResponseEntity<AppGeneralResponse> getPickupCancelRescheduleReasonCodes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getPickupCancelRescheduleReasonCodes(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/getassignedto")
	List<AssignedToModel> getAssignedToList(){
		List<AssignedToModel> data = null;
			try {
				data = masterDataServices.getAssignedToList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return data;
		}
	
	@GetMapping(path = "/data/master/pickuptypes")
	ResponseEntity<AppGeneralResponse> getPickupTypes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getPickupTypes(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
//	@GetMapping(path="/data/master/pincodes")
//	ResponseEntity<AppGeneralResponse> getPincodes(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
//			@RequestParam("page") Optional<Integer> page){
//
//		Logger logger = Logger.getLogger("getPincodes");
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			
//			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			logger.info("ver::"+apkVersion);
//			int version = 0;
//			int pagelimit = 10000;
//			int pageno = 0;
//			int offset = 0;
//
//			
//			pageno = page.isPresent() ?	page.get() : 1;
//
//			offset = (pageno - 1) * pagelimit;
//
//
//			version = maxVersion.isPresent() ? maxVersion.get() : 0; 
//
//			logger.info("Version "+version);
//
//			Pageable paging = null;
//
//			if(pageno != -1) {
//				Sort order = new Sort(new Sort.Order(Direction.DESC, "id"));
//				paging = new PageRequest(pageno-1, 10000, order);
//			}
//			List<PincodeModel> pins = null; 
//			List<MPincodeEntity> pinCodes = (List<MPincodeEntity>) mPincodeJpaRepository.findAll();
//			pinCodes = mPincodeJpaRepository.findAll(paging).getContent();
//			if(pinCodes != null && pinCodes.size() > 0) {
//				pins = new ArrayList<>();
//				for (MPincodeEntity pincode:pinCodes){
//					PincodeModel pin = new PincodeModel();
//					pin.setActive(pincode.getActiveFlag() != null ? pincode.getActiveFlag() == 1?true:false : false);
// 					pin.setBranchCode(pincode.getMBranch() != null ?  pincode.getMBranch().getBranchCode() : null);
//					pin.setStateCode(pincode.getStateCode());
//					pin.setPinCode(pincode.getPincode());
//					pin.setDistFromBranch(pincode.getDistanceFromBranch());
//					//						pin.setServicable(pincode.getServicable());
//					//						pin.setVersion(pincode.getVersion());
//					pin.setOdaFlag(pincode.getOdaFlag());
//					pin.setArea(pincode.getArea());
//					if(pincode.getUpdatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getUpdatedTimestamp()));
//					}else if(pincode.getCreatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getCreatedTimestamp()));
//					}
//					pins.add(pin);
//				}
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();// add logger instead of print stack trace
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}
	
	@GetMapping(path="/data/master/pincode/{code}")
	ResponseEntity<AppGeneralResponse> getPincodeDetails(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
			@PathVariable("code") Integer code){

		if(code == null) {
			return new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
		}
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			List<PincodeModel> pins = null; 
			List<MaPincodeEntity> pinCodes = maPincodeJpaRepository.findByPincodeAndActiveFlag(code, 1);
			if(pinCodes != null && pinCodes.size() > 0) {
				pins = new ArrayList<>();
				for (MaPincodeEntity pincode:pinCodes){
					PincodeModel pin = new PincodeModel();
					pin.setActiveFlag(pincode.getActiveFlag());
					pin.setCityName(pincode.getCityName());
					pin.setCreatedTimestamp(pincode.getCreatedTimestamp());
					pin.setDox(pincode.getDox());
					pin.setIsServicable(pincode.getIsServicable());
					pin.setNonDox(pincode.getNonDox());
					pin.setOda(pincode.getOda());
					pin.setOfficeCode(pincode.getOfficeCode());
					pin.setOfficeName(pincode.getOfficeName());
					pin.setPincode(pincode.getPincode());
					pin.setPrimeTrack(pincode.getPrimeTrack());
					pin.setReversePickup(pincode.getReversePickup());
					pin.setRoadExpress(pincode.getRoadExpress());
					pin.setRoda(pincode.getRoda());
					pin.setStateExp(pincode.getStateExp());
					pin.setStateName(pincode.getStateName());
					pin.setToPay(pincode.getToPay());
					pins.add(pin);
				}
			}
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
//	@GetMapping(path="/data/master/allodapincodes")
//	ResponseEntity<AppGeneralResponse> getAllOdaPincodeDetails(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
//			@PathVariable("code") Integer code){
//
//		if(code == null) {
//			return new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
//		}
//		Logger logger = Logger.getLogger("getAllOdaPincodes");
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			logger.info("ver::"+apkVersion);
//
//			int version = maxVersion.isPresent() ? maxVersion.get() : 0; 
//
//			List<PincodeModel> pins = null; 
//			List<MPincodeEntity> pinCodes = mPincodeJpaRepository.findByActiveFlag(1);
//			if(pinCodes != null && pinCodes.size() > 0) {
//				pins = new ArrayList<>();
//				for (MPincodeEntity pincode:pinCodes){
//					PincodeModel pin = new PincodeModel();
//					pin.setActive(pincode.getActiveFlag() != null ? pincode.getActiveFlag() == 1?true:false : false);
// 					pin.setBranchCode(pincode.getMBranch() != null ?  pincode.getMBranch().getBranchCode() : null);
//					pin.setStateCode(pincode.getStateCode());
//					pin.setPinCode(pincode.getPincode());
//					pin.setDistFromBranch(pincode.getDistanceFromBranch());
//					//						pin.setServicable(pincode.getServicable());
//					//						pin.setVersion(pincode.getVersion());
//					pin.setOdaFlag(pincode.getOdaFlag());
//					pin.setArea(pincode.getArea());
//					if(pincode.getUpdatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getUpdatedTimestamp()));
//					}else if(pincode.getCreatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getCreatedTimestamp()));
//					}
//					pins.add(pin);
//				}
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();// add logger instead of print stack trace
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}
	
	@GetMapping(path = "/data/master/paperworktypes")
	ResponseEntity<AppGeneralResponse> getPaperWorkTypes(@RequestParam("maxVersion") Optional<Integer> maxVersion){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getPaperWorkTypes(maxVersion);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path = "/data/master/chargetypes")
	ResponseEntity<AppGeneralResponse> getChargeTypes(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getChargeTypes();
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path = "/data/master/consignorbybranch/{branch}")
	ResponseEntity<AppGeneralResponse> getConsignorsByBranch(@PathVariable String branch, @RequestParam Optional<Integer> version){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getConsignorsByBranch(branch, version);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}

	@GetMapping(path = "/data/master/consignees/all")
	ResponseEntity<AppGeneralResponse> getAllConsignees(@RequestParam Optional<Integer> version){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getAllConsignees(version);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path="/data/master/branch/all")
	ResponseEntity<AppGeneralResponse> getAllBranches(@RequestParam Optional<Integer> version){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getAllBranches();
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/docketstatus/all")
	ResponseEntity<AppGeneralResponse> getDocketStatus(@RequestParam Optional<Integer> version){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getDocketStatus();
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
//	@GetMapping(path="/data/master/pincodesbyarea/{area}")
//	ResponseEntity<AppGeneralResponse> getPincodesByArea(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
//			@RequestParam("page") Optional<Integer> page, @PathVariable String area) {
//
//		if(area == null) {
//			return new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
//		}
//		Logger logger = Logger.getLogger("getPincodes");
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			logger.info("ver::"+apkVersion);
//
//			int version = maxVersion.isPresent() ? maxVersion.get() : 0; 
//
//			List<PincodeModel> pins = null; 
//			List<MPincodeEntity> pinCodes = mPincodeJpaRepository.findByAreaAndActiveFlag(area, 1);
//			if(pinCodes != null && pinCodes.size() > 0) {
//				pins = new ArrayList<>();
//				for (MPincodeEntity pincode:pinCodes){
//					PincodeModel pin = new PincodeModel();
//					pin.setActive(pincode.getActiveFlag() != null ? pincode.getActiveFlag() == 1?true:false : false);
// 					pin.setBranchCode(pincode.getMBranch() != null ?  pincode.getMBranch().getBranchCode() : null);
//					pin.setStateCode(pincode.getStateCode());
//					pin.setPinCode(pincode.getPincode());
//					pin.setDistFromBranch(pincode.getDistanceFromBranch());
//					//						pin.setServicable(pincode.getServicable());
//					//						pin.setVersion(pincode.getVersion());
//					pin.setOdaFlag(pincode.getOdaFlag());
//					pin.setArea(pincode.getArea());
//					if(pincode.getUpdatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getUpdatedTimestamp()));
//					}else if(pincode.getCreatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getCreatedTimestamp()));
//					}
//					pins.add(pin);
//				}
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();// add logger instead of print stack trace
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}
	
//	@GetMapping(path="/data/master/pincodesbybranch/{branch}")
//	ResponseEntity<AppGeneralResponse> getPincodesByBranch(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
//			@RequestParam("page") Optional<Integer> page, @PathVariable String branch) {
//
//		if(branch == null) {
//			return new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INCOMPLETE_DATA_SENT, null, null), HttpStatus.OK);
//		}
//		Logger logger = Logger.getLogger("getPincodes");
//		ResponseEntity<AppGeneralResponse> generalResponse = null;
//		try {
//			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			logger.info("ver::"+apkVersion);
//
//			int version = maxVersion.isPresent() ? maxVersion.get() : 0; 
//
//			List<PincodeModel> pins = null; 
//			List<MPincodeEntity> pinCodes = mPincodeJpaRepository.findByMBranchBranchCodeAndActiveFlag(branch, 1);
//			if(pinCodes != null && pinCodes.size() > 0) {
//				pins = new ArrayList<>();
//				for (MPincodeEntity pincode:pinCodes){
//					PincodeModel pin = new PincodeModel();
//					pin.setActive(pincode.getActiveFlag() != null ? pincode.getActiveFlag() == 1?true:false : false);
// 					pin.setBranchCode(pincode.getMBranch() != null ?  pincode.getMBranch().getBranchCode() : null);
//					pin.setStateCode(pincode.getStateCode());
//					pin.setPinCode(pincode.getPincode());
//					pin.setDistFromBranch(pincode.getDistanceFromBranch());
//					//						pin.setServicable(pincode.getServicable());
//					//						pin.setVersion(pincode.getVersion());
//					pin.setOdaFlag(pincode.getOdaFlag());
//					pin.setArea(pincode.getArea());
//					if(pincode.getUpdatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getUpdatedTimestamp()));
//					}else if(pincode.getCreatedTimestamp()!=null) {
//						pin.setTimestamp(format.format(pincode.getCreatedTimestamp()));
//					}
//					pins.add(pin);
//				}
//			}
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
//		} catch (Exception e) {
//			e.printStackTrace();// add logger instead of print stack trace
//			generalResponse = new ResponseEntity<AppGeneralResponse>(
//					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
//		}
//		return generalResponse;
//	}
	
	@GetMapping(path="/data/master/dktmaster")
	ResponseEntity<AppGeneralResponse> getDocketMasterByUser(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("GetDocketMaster");
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			String userId = null;
			if (principal instanceof UserDetailsImpl) {
				userId = ((UserDetailsImpl) principal).getUserid();
			}
			generalResponse = masterDataServices.getDocketMaster(userId, logger);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/pktmaster")
	ResponseEntity<AppGeneralResponse> getPacketMasterByUser(){
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			String userId = null;
			if (principal instanceof UserDetailsImpl) {
				userId = ((UserDetailsImpl) principal).getUserid();
			}
			generalResponse = masterDataServices.getPacketMaster(userId);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

	@GetMapping(path = "/data/master/relationship")
	ResponseEntity<AppGeneralResponse> getRelationships(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getRelationships(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/agentsbybranch/{branch}")
	ResponseEntity<AppGeneralResponse> getAgentsByBranch(@PathVariable String branch){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

				String userId = null;
				if (principal instanceof UserDetailsImpl) {
					userId = ((UserDetailsImpl) principal).getUserid();
				}
				if(!CommonUtility.check(userId)){
					new ResponseEntity<AppGeneralResponse>(
							new AppGeneralResponse(false, "Token error : no user ID", null, null), HttpStatus.OK);
				}
				
				generalResponse = masterDataServices.getAgentsByBranch(branch, userId);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@GetMapping(path = "/data/master/redirectreasons")
	ResponseEntity<AppGeneralResponse> getRedirectReasons(@RequestParam("maxVersion") Optional<Integer> maxVersion){
			ResponseEntity<AppGeneralResponse> generalResponse = null;
			try {
				generalResponse = masterDataServices.getRedirectReasons(maxVersion);
			} catch (Exception e) {
				e.printStackTrace();
				generalResponse = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
			}
			return generalResponse;
		}
	
	@PostMapping(path = "/data/master/cashfreight")
	ResponseEntity<AppGeneralResponse> getCashFreight(@RequestBody CashFreightRequestModel data) {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			generalResponse = masterDataServices.getCashFreight(data);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/cron/trackon/usermaster")
	ResponseEntity<AppGeneralResponse> getTrackonUserMaster() {
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		Logger logger = Logger.getLogger("GetTrackonUserMaster");
		try {
			generalResponse = masterDataServices.getTrackonUserMaster(logger);
		} catch (Exception e) {
			e.printStackTrace();
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}
	
	@GetMapping(path="/data/master/pincodes")
	ResponseEntity<AppGeneralResponse> getPincodes(@RequestParam("maxVersion") Optional<Integer> maxVersion, @RequestParam("apkVersion") Optional<String> apkVersion, 
			@RequestParam("page") Optional<Integer> page){

		Logger logger = Logger.getLogger("getPincodes");
		ResponseEntity<AppGeneralResponse> generalResponse = null;
		try {
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			logger.info("ver::"+apkVersion);
			int version = 0;
			int pagelimit = 10000;
			int pageno = 0;
			int offset = 0;

			
			pageno = page.isPresent() ?	page.get() : 1;

			offset = (pageno - 1) * pagelimit;


			version = maxVersion.isPresent() ? maxVersion.get() : 0; 

			logger.info("Version "+version);

			Pageable paging = null;

			if(pageno != -1) {
				Sort order = new Sort(new Sort.Order(Direction.DESC, "id"));
				paging = new PageRequest(pageno-1, 10000, order);
			}
			List<PincodeModel> pins = null; 
//			List<MaPincodeEntity> pinCodes = (List<MaPincodeEntity>) maPincodeJpaRepository.findAll();
			List<MaPincodeEntity> pinCodes = maPincodeJpaRepository.findAll(paging).getContent();
			if(pinCodes != null && pinCodes.size() > 0) {
				pins = new ArrayList<>();
				for (MaPincodeEntity pincode:pinCodes){
					PincodeModel pin = new PincodeModel();
					pin.setActiveFlag(pincode.getActiveFlag());
					pin.setCityName(pincode.getCityName());
					pin.setCreatedTimestamp(pincode.getCreatedTimestamp());
					pin.setDox(pincode.getDox());
					pin.setIsServicable(pincode.getIsServicable());
					pin.setNonDox(pincode.getNonDox());
					pin.setOda(pincode.getOda());
					pin.setOfficeCode(pincode.getOfficeCode());
					pin.setOfficeName(pincode.getOfficeName());
					pin.setPincode(pincode.getPincode());
					pin.setPrimeTrack(pincode.getPrimeTrack());
					pin.setReversePickup(pincode.getReversePickup());
					pin.setRoadExpress(pincode.getRoadExpress());
					pin.setRoda(pincode.getRoda());
					pin.setStateExp(pincode.getStateExp());
					pin.setStateName(pincode.getStateName());
					pin.setToPay(pincode.getToPay());
					pin.setId(pincode.getId());
					pins.add(pin);
				}
			}
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(true, ResponseMessages.SUCCESS, pins, null), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();// add logger instead of print stack trace
			generalResponse = new ResponseEntity<AppGeneralResponse>(
					new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return generalResponse;
	}

}
