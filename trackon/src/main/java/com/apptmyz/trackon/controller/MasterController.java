package com.apptmyz.trackon.controller;

import java.math.BigInteger;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.apptmyz.trackon.services.MasterService;
import com.apptmyz.trackon.utils.AppGeneralResponse;
import com.apptmyz.trackon.utils.CommonUtility;
import com.apptmyz.trackon.utils.ResponseMessages;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@CrossOrigin
@RestController
@RequestMapping(path="/masters")
public class MasterController {
	
	@Autowired
	private MasterService masterService;

	@GetMapping(path="/get/pkt/series")
	ResponseEntity<AppGeneralResponse> getPktSeries(@RequestParam(name="userId", required=false) String userId, @RequestParam("count") Integer count){
		
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("GetPktSeries");
		log.info("User : "+userId+" - Count : "+count);
		try{
			if(count != null && count > 0){
				List<BigInteger> packets = (List<BigInteger>) masterService.getPktSeries(count, log, "N");
				
				if(packets != null && !packets.isEmpty()){
					response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, packets, null), HttpStatus.OK);
				}
				else{
					response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, "Required count not sent or 0", null, null), HttpStatus.OK); 
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		
		return response;
		
	}
	
	@GetMapping(path="/get/pkt/series/new")
	ResponseEntity<AppGeneralResponse> getPktSeriesNew(@RequestParam(name="userId", required=false) String userId, @RequestParam("count") Integer count, @RequestParam("isxb") String isxb){
		Gson gson = new GsonBuilder().serializeNulls().create();
		ResponseEntity<AppGeneralResponse> response = null;
		Logger log = Logger.getLogger("GetPktSeries");
		log.info("User : "+userId+" - Count : "+count);
		try{
			if(count != null && count > 0){
				List<?> packets = null;
				if(isxb.equalsIgnoreCase("Y")){					
					packets = (List<String>) masterService.getPktSeries(count, log, isxb);
				}else{
					packets = (List<BigInteger>) masterService.getPktSeries(count, log, "N");
				}
				
				if(packets != null && !packets.isEmpty()){
					response = new ResponseEntity<>(new AppGeneralResponse(true, ResponseMessages.SUCCESS, packets, null), HttpStatus.OK);
				}
				else{
					response = new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.NO_DATA_AVAILABLE, null, null), HttpStatus.OK);
				}
			}
			else{
				response = new ResponseEntity<>(new AppGeneralResponse(false, "Required count not sent or 0", null, null), HttpStatus.OK); 
			}
		}
		catch(Exception e){
			e.printStackTrace();
			log.info(e.getMessage(), e);
			response = new ResponseEntity<>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		if(response != null){
			log.info("RESPONSE : "+gson.toJson(response));
		}
		return response;
		
	}
	
	@GetMapping(path="/get/mConsignor")
	ResponseEntity<AppGeneralResponse> getMconsignorData(@RequestParam("pincode") Integer pincode,@RequestParam("name") String word){
		ResponseEntity<AppGeneralResponse> response = null;
		try{
			if(pincode != null && CommonUtility.check(word)){
				response = masterService.getMConsignorDetails(pincode, word.concat("%"));
			}
			else{
				response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}

	@GetMapping(path ="/get/mConsignee")
	ResponseEntity<AppGeneralResponse> getMconsigneeData(@RequestParam("pincode") Integer pincode,@RequestParam("name") String word){
		ResponseEntity<AppGeneralResponse> response = null;
		try {
			if (pincode != null && CommonUtility.check(word)) {
				response = masterService.getMConsigneeDetails(pincode,  word.concat("%"));
			} else {
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, ResponseMessages.INPUT_NOT_FOUND, null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
	
	@GetMapping(path ="/validate/xbDkt")
	ResponseEntity<AppGeneralResponse> validateXbDktNo(@RequestParam("dktno") String xbDktno){
		ResponseEntity<AppGeneralResponse> response = null;
		try {
			if (CommonUtility.check(xbDktno)) {
				response = masterService.validateXbDktNo(xbDktno);
			} else {
				response = new ResponseEntity<AppGeneralResponse>(
						new AppGeneralResponse(false, "XB Docket no. sent", null, null), HttpStatus.OK);
			}
		}
		catch(Exception e){
			e.printStackTrace();
			response = new ResponseEntity<AppGeneralResponse>(new AppGeneralResponse(false, ResponseMessages.INTERNAL_SERVER_ERROR, null, null), HttpStatus.OK);
		}
		return response;
	}
}
