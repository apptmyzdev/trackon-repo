package com.apptmyz.trackon.utils;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.apptmyz.trackon.services.UserDetailsImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JWTUtils {
	private static final Logger logger = LoggerFactory.getLogger(JWTUtils.class);

	private static String jwtSecret = "JaNdRgUkXp2s5u8x/A?D(G+KbPeShVmYq3t6w9y$B&E)H@McQfTjWnZr4u7x!A%C";
	private static String ISSUER = "com.apptmyz.trackon";

	@Value("${jwtExpirationMillis}")
	private int jwtExpirationMillis;

	public String generateJwtToken(Authentication authentication) {

		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuer(ISSUER)
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMillis))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}
	
	public String createJWTToken(String data, int min, Authentication authentication) throws UnsupportedEncodingException
	{
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, min);
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
		
		String jwt = Jwts.builder()
				.setIssuer(ISSUER)
				.setSubject(userPrincipal.getUserid())
				.setExpiration(c.getTime())
				.setIssuedAt(new Date())
				.claim("name", "TokenData")
				.claim("scope", "user/logins")
				.claim("data", data)
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
		
		return jwt;
	}

	public String getUserNameFromJwtToken(String token) throws Exception {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validateJwtToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException e) {
			logger.error("Invalid JWT signature: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Invalid JWT token: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("JWT token is expired: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error("JWT token is unsupported: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("JWT claims string is empty: {}", e.getMessage());
		}

		return false;
	}
	
	public String parseJWT(String jwt) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException, UnsupportedEncodingException
	{
		Jws<Claims> claims = Jwts.parser()
				.setSigningKey(jwtSecret)
				.parseClaimsJws(jwt);
		return (String) claims.getBody().get("data");
	}
}
