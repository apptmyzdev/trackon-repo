package com.apptmyz.trackon.utils;

public class Constants {

	public static final String LOG_START = "******************************************Request************************************";
	public static final String LOG_END = "****************************************END*******************************************";
	public static final String ERROR_IN_HOST_AUTH = "ERROR IN RETRIEVEING HOST AUTH TOKEN";
	
//	public static final String UAT_BASE_URL = "http://63.142.252.161:8080/trackon/";
	

//	public static final String FCM_API_URL = "https://fcm.googleapis.com/fcm/send";
	public static final String FCM_API_URL  ="https://fcm.googleapis.com/v1/projects/trackon-61b00/messages:send";
	public static final String FCM_SERVER_KEY = "AAAAYpVZFhM:APA91bG1Jw4s2r1pcBMxd5I_duQBhAzei34gsZsMM4p9kvOGUwC7i5Aus6EIHQI6wcYWMI9YgYJ15qsICZpbO2Q_PkaKiQ0NxE32-yO6pTRPNy-hX7pCHntpLf0ZPq9io1wzVa5dSnBb";
	public static final int PUSH_FOR_BOOKINGS = 1;
	public static final int PUSH_FOR_DELIVERIES = 2;
	public static final String PUSH_FOR_LOGOUT = "3";
	
	public static final String FORM_URL_ENCODED = "application/x-www-form-urlencoded";
	public static final String APPLICATION_JSON = "application/json";
	public static final String HOST_AUTH_URL_PARAMS = "grant_type=password&username=apptmyz";
	public static final String HOST_AUTH_URL_PASSWORD_PARAM = "&password=4a1d9aed7310f17b5cdf97d7b9c3c092b2aae43d318a1c9e8c0919fa74f18265";
	public static final String BEARER_TEXT = "bearer ";

	public static final String PICKUP_PENDING = "PENDING";
	public static final String PICKUP_BOOKING_ATTEMPTED = "ATTEMPTED";
	public static final String PICKUP_BOOKING_SUCCESS = "BOOKED";

	public static final String SUCCESSFUL_DELIVERY = "DELIVERED";
	public static final String SUCCESSFUL_UNDELIVERY = "UNDELIVERED";
	public static final int PENDING_DELIVERY_STATUS_ID = 1;
	public static final String DELIVERY = "D";
	public static final String UNDELIVERY = "U";

	public static final int SAVE_TO_DELIVERIES = 1;
	public static final int SAVE_TO_PICKUPS = 2;
	public static final int SAVE_TO_DEPS = 3;
	
	public static final int VA_USER_ROLE_ID = 0;
	public static final int ADMIN_ID = 1;
	public static final int REGION_ADMIN_ID = 2;
	public static final int ZONE_ADMIN_ID = 3;
	public static final int BRANCH_ADMIN_ID = 4;
	
	public static final String SHIP_MODE_AIR = "AIR";
	public static final String SHIP_MODE_SUFACE = "SURFACE";
	public static final String SHIP_MODE_TRAIN = "TRAIN";
	
	public static final int SHIPPING_MODE_AIR = 1;
	public static final int SHIPPING_MODE_SUFACE = 2;
	public static final int SHIPPING_MODE_TRAIN = 3;
	
	public static final String ZERO_PADDING = "0000000";
	
	public static final String PICKUP_STATUS_CANCELLED = "CANC";
	public static final String PICKUP_STATUS_PENDING = "PEND";
	public static final String PICKUP_STATUS_ASSIGNED = "ASSN";
	public static final String PICKUP_STATUS_COMPLETED = "DONE";
	public static final String DOX = "Dox";
	public static final String NONDOX = "NonDox";
	public static final String CUSTOM = "CUSTOM";
	public static final String AGENT = "A";
	public static final String FRANCHISEE = "F";
	public static final String BRANCH = "B";
	public static final String ALL = "ALL";
	public static final String PICKED = "PK";
	public static final String BAD_DELIVERY = "Bad Delivery";
	public static final String GOOD_DELIVERY = "Good Delivery";
	public static final String API_PICKUP = "API";
	public static final String NORMAL_PICKUP = "Normal";
	public static final String CATEGORY_FREIGHT_SAME_CITY = "city";
	public static final String CATEGORY_FREIGHT_SAME_STATE = "state";
	public static final String CATEGORY_FREIGHT_SAME_ZONE = "zone";
	public static final String CATEGORY_FREIGHT_METRO = "metro";
	public static final String CATEGORY_FREIGHT_ROI_A = "roi_a";
	public static final String CATEGORY_FREIGHT_ROI_B = "roi_b";
	public static final String CATEGORY_FREIGHT_SPL_REMOTE = "spl_remote";
	public static final String RECUR_FREQ_MTOF = "MTOF";
	public static final String RECUR_FREQ_MTOS = "MTOS";
	public static final String RECUR_FREQ_EVERYDAY = "EVERYDAY";
	public static final String RETAIL_CUSTCODE = "99999999";
	
	public static final String HTTP_GET = "GET";
	public static final String HTTP_POST = "POST";
	public static final int HTTP_OK = 200;
	
	public static final String FRANCHISE_USER = "F";
	public static final String DELIVERY_USER = "D";
	public static final String BRANCH_USER = "B";
	
	public static final String ERP_UNDELIVERY_IMG = "U";
	public static final String ERP_DEPS_IMG = "D";
	public static final String ERP_PAYMENT_IMG = "P";
	
	public static final int TOKEN_TIMEOUT_SECS = 525600;
//	public static final int TOKEN_TIMEOUT_SECS = 5;
	
	public static final String DRS_STD_TYPE = "STD";
	public static final String DRS_PT_TYPE = "PT";
	
	public static final String DOX_FLG = "D";
	public static final String NON_DOX_FLG = "N";
	
	public static final String PICKUP_IMGS_API="/pickup/view/image?imgPath=";
	public static final String APP_NAME ="Trackon Buddy";
}
