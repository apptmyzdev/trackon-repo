package com.apptmyz.trackon.utils;

public class FCMCustomDataModel {
	private Object data;

	public FCMCustomDataModel() {
		super();
	}

	public FCMCustomDataModel(Object data) {
		super();
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
