package com.apptmyz.trackon.utils;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;
import com.apptmyz.trackon.model.WSResponseObjectModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;
//import com.itextpdf.io.source.ByteArrayOutputStream;
//import com.itextpdf.text.BaseColor;
//import com.itextpdf.text.Chunk;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.Element;
//import com.itextpdf.text.Font;
//import com.itextpdf.text.FontFactory;
//import com.itextpdf.text.Image;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Paragraph;
//import com.itextpdf.text.Phrase;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
import com.google.gson.GsonBuilder;


public class CommonUtility {
	
	public static Date getPreviousDateByNoOfDays(int days){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -days);
		return cal.getTime();
	}

	public static int getShippingMode(String mode) {
		if (check(mode)) {
			mode = mode.trim();
			if (mode.equals(Constants.SHIP_MODE_AIR)) {
				return Constants.SHIPPING_MODE_AIR;
			} else if (mode.equals(Constants.SHIP_MODE_SUFACE)) {
				return Constants.SHIPPING_MODE_SUFACE;
			} else if (mode.equals(Constants.SHIP_MODE_TRAIN)) {
				return Constants.SHIPPING_MODE_TRAIN;
			}
		}
		return 0;
	}

	public static int getSvcType(String svc) {
		if (check(svc)) {
			svc = svc.trim();
			if (svc.equals(Constants.SHIP_MODE_AIR)) {
				return Constants.SHIPPING_MODE_AIR;
			} else if (svc.equals(Constants.SHIP_MODE_SUFACE)) {
				return Constants.SHIPPING_MODE_SUFACE;
			} else if (svc.equals(Constants.SHIP_MODE_TRAIN)) {
				return Constants.SHIPPING_MODE_TRAIN;
			}
		}
		return 0;
	}

	public static boolean check(String... args) {
		boolean result = true;

		for (String arg : args) {
			if (arg == null || arg.trim().length() == 0) {
				result = false;
				break;
			}
		}
		return result;
	}

	// convert string to SHA256
	public static String getSHA256(String text) throws Exception {

		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(text.getBytes());
		BigInteger n = new BigInteger(1, hash);
		String sha = n.toString(16);
		while (sha.length() < 32) {
			sha = "0" + sha;
		}
		return sha;
	}
	

	public static String toImgFromBase64(String bas64Txt, String filenameTxt, String extension, int imageof,
			String imageType) {
		
		String dupFilename = null;
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		Logger imageLogger = Logger.getLogger("ImagesLogger");
		imageLogger.info("****START****");
		imageLogger.info("conNumber:- " + filenameTxt + " extension:- " + extension + " serviceId:- " + imageof
				+ " imageType:- " + imageType);

		String ftppath = FilesUtil.getProperty("ftpFolderPath");
		String efsTrackonPath = FilesUtil.getProperty("ftpTrackonDelFolderPath");
		imageLogger.info("ImagePath " + ftppath);

		String imgPath = "";
		if (imageof == 1 || imageof == 3 || imageof == 5) {
			if (imageof == 1) {
				imgPath = "Deliveries/" + Integer.toString(c.get(Calendar.YEAR)) + "/"
						+ Integer.toString(c.getTime().getMonth() + 1) + "/" + Integer.toString(c.getTime().getDate())
						+ "/";
				ftppath = ftppath.concat(imgPath);
				dupFilename = new String(filenameTxt);
			} else {
				imgPath = "Deliveries/deps/" + Integer.toString(c.get(Calendar.YEAR)) + "/"
						+ Integer.toString(c.getTime().getMonth() + 1) + "/" + Integer.toString(c.getTime().getDate())
						+ "/";
				ftppath = ftppath.concat(imgPath);
				dupFilename = new String(filenameTxt);
			}

		} else if (imageof == 2) {
			imgPath = "Pickups/"+ ""+imageType+"/" + Integer.toString(c.get(Calendar.YEAR)) + "/"
					+ Integer.toString(c.getTime().getMonth() + 1) + "/" + Integer.toString(c.getTime().getDate())
					+ "/";
			ftppath = ftppath.concat(imgPath);
		} else if (imageof == 4) {
			imgPath = "Undeliveries/" + Integer.toString(c.get(Calendar.YEAR)) + "/"
					+ Integer.toString(c.getTime().getMonth() + 1) + "/" + Integer.toString(c.getTime().getDate())
					+ "/";
			ftppath = ftppath.concat(imgPath);
			dupFilename = new String(filenameTxt);
		}

		imageLogger.info("FTP path : "+ftppath);
		File ftpDir = null;
		if(CommonUtility.check(dupFilename)){
			ftpDir = new File(efsTrackonPath);
		}else{
			ftpDir = new File(ftppath);
		}
		
		imageLogger.info("fileImg " + ftpDir);
		imageLogger.info("fileImg.exists() " + ftpDir.exists());
		
		if(!ftpDir.exists())
			ftpDir.mkdirs();
		
		
		byte[] imgBytes = org.apache.commons.codec.binary.Base64.decodeBase64(bas64Txt);
		imageLogger.info("imgBytes " + imgBytes);

		String fileName = (filenameTxt + "." + extension).trim();
		imageLogger.info("fileName " + fileName);
		
		ftpDir = null;
		if(CommonUtility.check(dupFilename)){
			ftpDir = new File(efsTrackonPath.concat(dupFilename).concat(".").concat(extension));
		}else{			
			ftpDir = new File(ftppath + fileName); 
		}
		
		imageLogger.info(" Final fileImg " + ftpDir);
		imageLogger.info("Final fileImg.exists() " + ftpDir.exists());
		/*
		 * if (fileImg.exists()) { fileImg = null; fileImg = new File(path + fileName);
		 * }
		 */

		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(ftpDir);
			imageLogger.info("fOut " + fOut);
			fOut.write(imgBytes);
			fOut.close();
			
//			if(CommonUtility.check(dupFilename)){
//				try{
//					dupImg = new File(efsTrackonPath.concat(dupFilename).concat(".").concat(extension)); //src file
//					Files.copy(ftpDir.toPath(), dupImg.toPath());
//				}catch(Exception a){
//					a.printStackTrace();
//				}
//				
//			}
		} catch (IOException e) {
			e.printStackTrace();
			imageLogger.info(" Exception in image " + e);
		} finally{
			if(fOut != null){
				try{					
					fOut.close();
				}
				catch(Exception i){
					i.printStackTrace();
				}
			}
		}

		imageLogger.info("Output file saved: " + ftpDir.getAbsolutePath());
		imageLogger.info("****END****");
		
		if(CommonUtility.check(dupFilename)){
			return dupFilename.concat(".").concat(extension);
		}else{
			return imgPath + fileName;
		}
	}

	public static String getCsvFromArray(List<Integer> data) {
		String csv = "";
		if (data != null && !data.isEmpty()) {
			for (Integer item : data) {
				if (csv.length() > 0) {
					csv = csv + ",";
				}
				csv = csv + item;
			}
		}
		return csv;
	}

//	public static boolean sendOTPSMS(String mobileNo, String dktNo, String otp, Logger logger)
//			throws ClientProtocolException, IOException {
//		
//		boolean result = false;
//		try{
//			logger.info("**********sending OTP SMS**********");
//			URL url = new URL(FilesUtil.getProperty("smsApiUrl"));
//            
//			String data = "username=trackontxn&password=914989&sender=TRACKN&PEID=1701158054471588832&templateid=1707165717831145645"
//					+ "&sendto=91" + mobileNo + "&message=Arriving today: Dear Trackon Customer, use OTP " + otp
//					+ "at delivery to receive your package - AWB " + dktNo;
//			logger.info("OTP " + data);
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//            conn.setUseCaches(false);
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//            conn.setRequestMethod("POST");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//			wr.write(new ObjectMapper().writeValueAsString(URLEncoder.encode(data)));
//			wr.flush();
//            if(conn.getResponseCode() == 200)
//            	result = true;
//            else
//            	result = false;
//            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String inputLine;
//            StringBuffer response = new StringBuffer();
//
//            while ((inputLine = in.readLine()) != null) {
//                response.append(inputLine);
//            }
//            in.close();
//			logger.info("OTP Send response :" + response);            
//		}
//		catch(Exception e){
//			e.printStackTrace();
//			logger.error(e);
//			result = false;
//		}
//		
//		return result;
//	}

	public static boolean sendOTPSMS(String mobileNo, String dktNo, String otp, Logger logger)
			throws ClientProtocolException, IOException {

		boolean result = false;
		logger.info("**********sending OTP SMS**********");
		String baseUrl = FilesUtil.getProperty("smsApiUrl");
		String data = FilesUtil.getProperty("deliveryOtpSmsBody");
		try {
			if(CommonUtility.check(data)){
				data = data.replace("<?mobileno?>", mobileNo).replace("<?awbno?>", dktNo).replace("<?otpnum?>", otp);
				
//			String data = "username=trackontxn&password=914989&sender=TRACKN&PEID=1701158054471588832&templateid=1707165717831145645"
//					+ "&sendto=91" + mobileNo + "&message=Arriving+today:+Dear+Trackon+Customer,+use+OTP+" + otp
//					+ "at+delivery+to+receive+your+package+-+AWB+" + dktNo + ".";
				
				logger.info("OTP data " + baseUrl + "?" + data);
				
				URL url = new URL(baseUrl.concat(data));
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod("GET");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//			wr.write(new ObjectMapper().writeValueAsString(URLEncoder.encode(data)));
//			wr.flush();
				if (conn.getResponseCode() == 200)
					result = true;
				else
					result = false;
				BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				logger.info("OTP Send response :" + response);
			}else{
				logger.info("Failed to fetch SMS body");
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			result = false;
		}

		return result;
	}

	public static String getRandomNumberString() {
		// It will generate 6 digit random Number.
		// from 0 to 999999
		Random rnd = new Random();
		int number = rnd.nextInt(999999);

		// this will convert any number sequence into 6 character.
		return String.format("%06d", number);
	}

	public static WSResponseObjectModel doWSRequest(String urltext, String data, String method, Logger logger) {
		WSResponseObjectModel wsResponse = new WSResponseObjectModel();

		StringBuilder dataStringBuilder = null;
		String erpresponse = "";
		InputStreamReader inputStream = null;
		BufferedReader br = null;
		URL url = null;
		HttpURLConnection httpcon = null;
		try {

			if (method.equals(Constants.HTTP_GET) && CommonUtility.check(data)) {
				urltext = urltext + "?" + data;
 			}
			url = new URL(urltext);
			httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setRequestMethod(method);
			httpcon.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
			httpcon.setDoOutput(true);
			httpcon.setConnectTimeout(30000);
//			httpcon.setReadTimeout(10000);

			logger.info("Calling URL : " + urltext);
//			System.out.println("Calling URL : " + urltext);
			logger.info("Sending Data: " + data);

			if (method.equals(Constants.HTTP_POST)) {
				OutputStreamWriter streamWriter = new OutputStreamWriter(httpcon.getOutputStream());
				streamWriter.write(data);
				streamWriter.flush();
			}
			
			wsResponse.setResponseCode(httpcon.getResponseCode());

			if (httpcon.getResponseCode() == 200) {
				inputStream = new InputStreamReader(httpcon.getInputStream());
				br = new BufferedReader(inputStream);
				dataStringBuilder = new StringBuilder();
				while ((erpresponse = br.readLine()) != null) {
					dataStringBuilder.append(erpresponse);
				}
				br.close();

				wsResponse.setResponse(dataStringBuilder.toString());
				logger.info("Response from WS: " + dataStringBuilder.toString());
//				System.out.println("Response from WS: " + dataStringBuilder.toString());

			}else {
				inputStream = new InputStreamReader(httpcon.getErrorStream());
				br = new BufferedReader(inputStream);
				dataStringBuilder = new StringBuilder();
				while ((erpresponse = br.readLine()) != null) {
					dataStringBuilder.append(erpresponse);
				}
				br.close();
				wsResponse.setErrorString(dataStringBuilder.toString());
				logger.info("Error Response from WS: " + dataStringBuilder.toString());
			}

			if (inputStream != null)
				inputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(ResponseMessages.INTERNAL_SERVER_ERROR, e);
		} finally {
			if (httpcon != null)
				httpcon.disconnect();
		}
		return wsResponse;
	}

	public static Date getTodaysDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.AM_PM, Calendar.AM);
		return cal.getTime();
	}
	
	public static String encodeFileToBase64Binary(File file) throws Exception{
        String encodedfile = null;
        FileInputStream fileInputStreamReader = null;
        try {
            fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally{
        	if(fileInputStreamReader != null)
        		fileInputStreamReader.close();
        }

        return encodedfile;
    }
	
	public static String getAccessToken() throws IOException {
		String serviceAccountKeyJson = FilesUtil.getProperty("fcmServiceAccountFile");
        FileInputStream serviceAccount = 
            new FileInputStream(serviceAccountKeyJson);

        GoogleCredentials googleCredentials = GoogleCredentials.fromStream(serviceAccount)
                .createScoped(Collections.singleton("https://www.googleapis.com/auth/firebase.messaging"));

        googleCredentials.refreshIfExpired();
        return googleCredentials.getAccessToken().getTokenValue();
    }

	public static boolean sendPushNotificationV2(String title, String body,Logger logger, Gson gson, PushDataModel type, String devicetoken, String OAuthToken){
		boolean result = false;
		try{
			ObjectMapper objectMapper = new ObjectMapper();
			
			logger.info("**********sending push**********");
//			System.out.println("**********sending push**********");
			URL url = new URL(Constants.FCM_API_URL);
//            System.out.println("OauthToken : "+OAuthToken);
			PushNotificationPayload data = new PushNotificationPayload();
			
			PushNotificationModel push = new PushNotificationModel();
//			push.setRegistration_ids(pushtokens);
			push.setData(type);
			FCMNotificationContentModel content = new FCMNotificationContentModel();
			content.setBody(body);
			content.setTitle(title);
//			push.setPriority("high");
			push.setNotification(content);
			push.setToken(devicetoken);
			data.setMessage(push);
			logger.info("Notification sent " + gson.toJson(data));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Bearer " + OAuthToken);
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(objectMapper.writeValueAsString(data));
//			System.out.println("Notification sent " + gson.toJson(data));
			wr.flush();
            if(conn.getResponseCode() == 200)
            	result = true;
            else
            	result = false;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
			logger.info("Push response :" + response);            
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
			result = false;
		}
		
		return result;
	}
	
	
	
	public static boolean multiplePushNotifications(List<String> registrationTokens,Logger log) throws FirebaseMessagingException, IOException{
		Gson gson = new GsonBuilder().serializeNulls().create();
		FirebaseApp firebaseApp = null;
		try{
////		try {
//			firebaseApp = FirebaseApp.getInstance(Constants.APP_NAME);
//			
//			
////		} catch (IllegalStateException e) {
////			e.printStackTrace();
////			try {
////				FileInputStream serviceAccount = new FileInputStream(FilesUtil.getProperty("fcmServiceAccountFile"));
////
////				GoogleCredentials googleCredentials = GoogleCredentials.fromStream(serviceAccount)
////						.createScoped(Collections.singleton(FilesUtil.getProperty("googleCredsUrl")));
////
////				FirebaseOptions options = new FirebaseOptions.Builder()
////						.setCredentials(googleCredentials)
////						.setDatabaseUrl(FilesUtil.getProperty("firebaseDbUrl"))
////						.build();
////
////				firebaseApp = FirebaseApp.initializeApp(options, Constants.APP_NAME);
//				log.info("Initialized FirebaseApp 'Trackon Buddy'.");
//			} catch (IOException ioException) {
//				ioException.printStackTrace();
//				log.info("Failed to initialize FirebaseApp 'Trackon Buddy'.");
//				return false; 
//			}
//		}

		FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(firebaseApp);

		Notification notification = Notification.builder()
				.setTitle("Force Logout")
				.setBody("Log out user")
				.build();


		log.info("PUSHING FOR : "+ registrationTokens);
		MulticastMessage message = MulticastMessage.builder()
				.putData("id", Constants.PUSH_FOR_LOGOUT)
				.setNotification(notification)
				.addAllTokens(registrationTokens)
				.build();

		log.info("message req : "+ gson.toJson(message));
		BatchResponse response = firebaseMessaging.sendMulticast(message);
		log.info(response.getSuccessCount() + " messages were sent successfully");
		return response.getSuccessCount() > 0;
		} catch (Exception e) {
			e.printStackTrace();
			log.info(e.getMessage(),e);
			return false; 
		}
	}
	

	public static boolean sendPushNotification(String title, String body, Collection<String> pushtokens,Logger logger, Gson gson, PushDataModel type){
		boolean result = false;
		try{
			logger.info("**********sending push**********");
			URL url = new URL(Constants.FCM_API_URL);
            
			PushNotificationModel push = new PushNotificationModel();
//			push.setRegistration_ids(pushtokens);
			push.setData(type);
			FCMNotificationContentModel content = new FCMNotificationContentModel();
			content.setBody(body);
			content.setTitle(title);
//			push.setPriority("high");
			push.setNotification(content);
			logger.info("Notification sent " + gson.toJson(push));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + Constants.FCM_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(new ObjectMapper().writeValueAsString(push));
			wr.flush();
            if(conn.getResponseCode() == 200)
            	result = true;
            else
            	result = false;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
			logger.info("Push response :" + response);            
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
			result = false;
		}
		
		return result;
	}
	
	
	public static String postDataToErp(String data, String urlText,Logger logger) {

		StringBuilder dataStringBuilder = null;
		String erpresponse = "";
		InputStreamReader inputStream = null;
		BufferedReader br = null;
		URL url = null;
		HttpURLConnection httpcon = null;
		String responseString = null;
		try {

			url = new URL(urlText);
			httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setRequestMethod(Constants.HTTP_POST);
			httpcon.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
			httpcon.setDoOutput(true);
			httpcon.setConnectTimeout(5000);
			// httpcon.setReadTimeout(10000);

			logger.info("Calling URL : " + urlText);
			// System.out.println("Calling URL : " + urltext);
			logger.info("Sending Data: " + data);

			OutputStreamWriter streamWriter = new OutputStreamWriter(httpcon.getOutputStream());
			streamWriter.write(data);
			streamWriter.flush();

			if (httpcon.getResponseCode() == 200) {
				inputStream = new InputStreamReader(httpcon.getInputStream());

			} else {
				inputStream = new InputStreamReader(httpcon.getErrorStream());
			}
			br = new BufferedReader(inputStream);
			dataStringBuilder = new StringBuilder();
			while ((erpresponse = br.readLine()) != null) {
				dataStringBuilder.append(erpresponse);
			}
			br.close();
			responseString = dataStringBuilder.toString();
			logger.info("Response from WS: " +responseString);
			
			if (inputStream != null)
				inputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage(), e);
		} finally {
			if (httpcon != null)
				httpcon.disconnect();
		}
		return responseString;
	}
	
//	Font getFontfactoryForPara(BaseColor baseColor, float fontSize, int fontWeight, boolean underline) {
//		Font font = null;
//		font = FontFactory.getFont(FontFactory.HELVETICA, fontSize, fontWeight);
//		font.setColor(baseColor);
//		if(underline)
//			font.setStyle(Font.UNDERLINE);
//		return font;
//	}
//
//	public String generateDeliveryPod(TrackonDrsEntity dkt, DlyDataEntity dly) {
//		
//		String base64 = null;
//		try{
//			Calendar c = Calendar.getInstance();
//			c.setTime(new Date());
//
//			String path = FilesUtil.getProperty("imagesPath");
//			String logopath = FilesUtil.getProperty("trackonlogopath");
//			String wmpath = FilesUtil.getProperty("trackonwmpath");
//
//			String imgPath = "";
//			imgPath = "Deliveries/".concat(Integer.toString(c.get(Calendar.YEAR))).concat("/")
//					.concat(Integer.toString(c.getTime().getMonth() + 1)).concat("/")
//					.concat(Integer.toString(c.getTime().getDate())).concat("/");
//			path = path.concat(imgPath);
//
//			File fileImg = new File(path);
//
//			if (!fileImg.exists()){
//				fileImg.mkdirs();
//			}
//			
//				
//
//			path = path.concat("/").concat(dkt.getDrsNo()).concat("_").concat(dkt.getAwbNo()).concat(".pdf");
////			System.out.println(path);
//			fileImg = new File(path);
//			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
//			FileOutputStream fOut = new FileOutputStream(path);
//			PdfWriter.getInstance(doc, fOut);
//			doc.open();
//			
//			PdfPTable table1 = new PdfPTable(1);
//			table1.setWidthPercentage(100f);
//			table1.setWidths(new float[] { 40.0f });
////			table1.setSpacingBefore(10);
//			
//
//			byte[] imageBytes3 = IOUtils.toByteArray(new FileInputStream(logopath));
//			ByteArrayInputStream bis3 = new ByteArrayInputStream(imageBytes3);
//			BufferedImage bImage3 = ImageIO.read(bis3);
//
//			ByteArrayOutputStream bytes3 = new ByteArrayOutputStream();
//			ImageIO.write(bImage3, "jpeg", bytes3);
//			Image myImg3 = Image.getInstance(bytes3.toByteArray());
//			// myImg3.setAbsolutePosition(90, 780);
//			myImg3.setPaddingTop(50);
//			myImg3.scaleToFit(70, 60);
//			myImg3.setAlignment(Image.ALIGN_LEFT);
////			doc.add(myImg3);
//			
//			PdfPCell cell = new PdfPCell(myImg3);
//			cell.setBorder(0);
//			table1.addCell(cell);
//			
//			
//			
//			
////			PdfPCell dtcell = new PdfPCell();
////			dtcell.setBorder(0);
////			dtcell.addElement(heading);
////			table1.addCell(dtcell);
//			doc.add(table1);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			Paragraph heading = new Paragraph("PROOF OF DELIVERY", getFontfactoryForPara(BaseColor.BLACK, 10, Font.BOLD, true));
//			heading.setAlignment(Element.ALIGN_CENTER);
//			
//			doc.add(heading);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			PdfPTable table2 = new PdfPTable(2);
//			table2.setWidthPercentage(100f);
//			table2.setWidths(new float[] { 50.0f, 50.0f });
////			table2.setSpacingBefore(10);
//			table2.setPaddingTop(100.0f);
//			Phrase companyLabel = new Phrase("COMPANY NAME: TRACKON COURIERS PVT. LTD.", getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			
//			PdfPCell cnamecell = new PdfPCell();
//			cnamecell.setBorder(0);
//			cnamecell.addElement(companyLabel);
//			table2.addCell(cnamecell);
//			
//			
//			Paragraph datephrase = new Paragraph("DATE : "+new SimpleDateFormat("dd-MM-yyyy").format(new Date()), getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			
//			PdfPCell datecell = new PdfPCell();
//			datecell.setBorder(0);
//			datecell.setPhrase(datephrase);
//			datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			table2.addCell(datecell);
//			doc.add(table2);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			PdfPTable table3 = new PdfPTable(2);
//			table3.setWidths(new float[] { 50.0f, 50.0f });
//			table3.setWidthPercentage(100f);
//			
//			
//			//dktno label
//			Paragraph dktnoplabel = new Paragraph("Waybill No.:",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell dktlabcell = new PdfPCell();
//			dktlabcell.setBorder(0);
//			dktlabcell.setPhrase(dktnoplabel);
//			table3.addCell(dktlabcell);
//			
//			Paragraph deldethead = new Paragraph("Delivered At : ",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell deldetheadcell = new PdfPCell();
//			deldetheadcell.setBorder(0);
//			deldetheadcell.setPhrase(deldethead);
//			table3.addCell(deldetheadcell);
//			
//			doc.add(table3);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table4 = new PdfPTable(2);
//			table4.setWidths(new float[] { 50.0f, 50.0f });
//			table4.setWidthPercentage(100f);
//			
//
//			//dktno
//			Paragraph dktnop = new Paragraph(dkt.getAwbNo(), getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell dktcell = new PdfPCell();
//			dktcell.setBorder(0);
//			dktcell.setPhrase(dktnop);
//			table4.addCell(dktcell);
//			
//			Paragraph addresspara = new Paragraph(dkt.getAddress(),getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell addresscell = new PdfPCell();
//			addresscell.setBorder(0);
//			addresscell.setPhrase(addresspara);
//			table4.addCell(addresscell);
//			
//			doc.add(table4);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table5 = new PdfPTable(2);
//			table5.setWidths(new float[] { 50.0f, 50.0f });
//			table5.setWidthPercentage(100f);
//			
//			Paragraph colllabel = new Paragraph("Collected By :",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell clctdbylabelcell = new PdfPCell();
//			clctdbylabelcell.setBorder(0);
//			clctdbylabelcell.setPhrase(colllabel);
//			table5.addCell(clctdbylabelcell);
//			
//			Paragraph mobilenumlab = new Paragraph("Contact No.:",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell mobilelabcell = new PdfPCell();
//			mobilelabcell.setBorder(0);
//			mobilelabcell.setPhrase(mobilenumlab);
//			table5.addCell(mobilelabcell);
//			
//			
//			doc.add(table5);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table6 = new PdfPTable(2);
//			table6.setWidths(new float[] { 50.0f, 50.0f });
//			table6.setWidthPercentage(100f);
//			
//			Paragraph receivedby = new Paragraph(dkt.getReceiverName(),
//					getFontfactoryForPara(BaseColor.BLACK, 7.0f, Font.NORMAL, false));
//			PdfPCell receivedbycell = new PdfPCell();
//			receivedbycell.setBorder(0);
//			receivedbycell.setPhrase(receivedby);
//			table6.addCell(receivedbycell);
//			
//			Paragraph mobilenum = new Paragraph(dkt.getMobileNo(),getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			PdfPCell mobilecell = new PdfPCell();
//			mobilecell.setBorder(0);
//			mobilecell.setPhrase(mobilenum);
//			table6.addCell(mobilecell);
//			
//			doc.add(table6);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			PdfPTable table7 = new PdfPTable(2);
//			table7.setWidths(new float[] { 50.0f, 50.0f });
//			table7.setWidthPercentage(100f);
//			
//			Paragraph delonlab = new Paragraph("Delivered on :",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell delonlabelcell = new PdfPCell();
//			delonlabelcell.setBorder(0);
//			delonlabelcell.setPhrase(delonlab);
//			table7.addCell(delonlabelcell);
//			
//			Paragraph statuslab = new Paragraph("Status : ",getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, true));
//			PdfPCell statuslabcell = new PdfPCell();
//			statuslabcell.setBorder(0);
//			statuslabcell.setPhrase(statuslab);
//			table7.addCell(statuslabcell);
//			
//			
//			doc.add(table7);
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			
//			PdfPTable table8 = new PdfPTable(2);
//			table8.setWidths(new float[] { 50.0f, 50.0f });
//			table8.setWidthPercentage(100f);
//			Paragraph delon = new Paragraph(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()),getFontfactoryForPara(BaseColor.BLACK, 7.0f, Font.NORMAL, false));
//			
//			PdfPCell deloncell = new PdfPCell();
//			deloncell.setBorder(0);
//			deloncell.setPhrase(delon);
//			table8.addCell(deloncell);
//			
//			String dktStatus = "-";
//			if(dly != null){
//				if(dly.getDlyStatus() != null && dly.getDlyStatus().equals("D")){
//					dktStatus = "Delivered";
//				}
//				else{
//					dktStatus = "Not Delivered - "+dly.getUndeliveryReason();
//				}
//				
//			}
//			Paragraph status = new Paragraph(dktStatus,
//					getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.NORMAL, false));
//			
//			
//			PdfPCell statuscell = new PdfPCell();
//			statuscell.setBorder(0);
//			statuscell.setPhrase(status);
//			table8.addCell(statuscell);
//			
//			doc.add(table8);
//			
//			PdfPTable table9 = new PdfPTable(2);
//			table9.setWidths(new float[] { 50.0f, 50.0f });
//			table9.setWidthPercentage(100f);
//			
//			doc.add( new Paragraph(Chunk.NEWLINE) );
//			Paragraph signpara = new Paragraph("Signature : Signature not required. Verified by OTP",
//					getFontfactoryForPara(BaseColor.BLACK, 10.0f, Font.BOLD, true));
//			
//			PdfPCell signcell = new PdfPCell();
//			signcell.setBorder(0);
//			signcell.setPhrase(signpara);
//			table9.addCell(signcell);
//			
//			byte[] imageBytes4 = IOUtils.toByteArray(new FileInputStream(wmpath));
//			ByteArrayInputStream bis4 = new ByteArrayInputStream(imageBytes4);
//			BufferedImage bImage4 = ImageIO.read(bis4);
//
//			ByteArrayOutputStream bytes4 = new ByteArrayOutputStream();
//			ImageIO.write(bImage4, "png", bytes4);
//			Image myImg4 = Image.getInstance(bytes4.toByteArray());
//			// myImg3.setAbsolutePosition(90, 780);
//			myImg4.setPaddingTop(50);
//			myImg4.scaleToFit(150, 150);
//			myImg4.setAlignment(Image.ALIGN_LEFT);
////			doc.add(myImg3);
//			
//			PdfPCell wmcell = new PdfPCell(myImg4);
//			wmcell.setBorder(0);
//			table9.addCell(wmcell);
//			
//			doc.add(table9);
//			
//			doc.close();
//			
//			base64 = encodeFileToBase64Binary(fileImg);
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		return base64;
//	}
}
