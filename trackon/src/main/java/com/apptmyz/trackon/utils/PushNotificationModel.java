package com.apptmyz.trackon.utils;

import java.util.Collection;
import java.util.List;

public class PushNotificationModel {
//	private Collection<String> registration_ids;
	private FCMNotificationContentModel notification;
	private Object data;
//	private String priority;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public PushNotificationModel() {
		super();
	}

	public PushNotificationModel(List<String> registration_ids, FCMNotificationContentModel notification,
			FCMCustomDataModel data) {
		super();
//		this.registration_ids = registration_ids;
		this.notification = notification;
		this.data = data;
	}

//	public Collection<String> getRegistration_ids() {
//		return registration_ids;
//	}
//
//	public void setRegistration_ids(Collection<String> registration_ids) {
//		this.registration_ids = registration_ids;
//	}

	public FCMNotificationContentModel getNotification() {
		return notification;
	}

	public void setNotification(FCMNotificationContentModel notification) {
		this.notification = notification;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

//	public String getPriority() {
//		return priority;
//	}
//
//	public void setPriority(String priority) {
//		this.priority = priority;
//	}

}
