package com.apptmyz.trackon.utils;

/**
 * @author Apptmyz
 *
 */
public class AppGeneralResponse {
	
	private boolean status;
	private String message;
	private Object data;
	private String token;
	
	public AppGeneralResponse(boolean status, String message, Object data, String token) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
		this.token = token;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
