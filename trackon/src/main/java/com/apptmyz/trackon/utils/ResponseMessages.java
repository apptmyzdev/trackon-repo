package com.apptmyz.trackon.utils;

public class ResponseMessages {
	
	//app session response message
	public static final String TOKEN_EXPIRED = "session expired, please login again";
	public static final String INVALID_SESSION = "Invalid session. Login Again!";
	public static final String MISSING_TOKEN = "Token missing. Login again!";
	public static final String MISSING_USER_ID_IN_TOKEN = "User ID not found. Login again!(T)";
	//***
	public static final String INPUT_NOT_FOUND = "No input data sent to server!";
	public static final String INVALID_CREDENTIALS = "Invalid credentials!";
	public static final String EMPTY_USER_ID = "Enter user ID";
	public static final String EMPTY_PASSWORD = "Enter password";
	public static final String EMPTY_DEVICE_IMEI = "Device IMEI not received";
	public static final String SUCCESS = "SUCCESS!";
	public static final String INTERNAL_SERVER_ERROR = "Internal server error(Exception)!";
	public static final String INACTIVE_USER = "Not an active user.";
	public static final String INCORRECT_DATA_FORMAT = "Incorrect input data format-(Invalid JSON).";
	public static final String DUPLICATE_LOGIN_ATTEMPT = "Already logged in another device!";
	public static final String ERROR_PARSING_REQUEST_DATA = "Error parsing request data";
	public static final String USER_NOT_SIGNED_IN = "User is not signed in";
	public static final String DEVICE_MISMATCH = "Device used for sign in does not match device used for logout";
	public static final String USER_DETAILS_NOT_FOUND = "User details missing(Login again)";
	public static final String EMPTY_DATA_SET = "Empty dataset was received";
	public static final String SYNC_UPTODATE = "Data is up to date";
	public static final String MALFORMED_URL_ERROR = "Malformed URL exception";
	public static final String IO_EXCEPTION = "I/O exception";
	public static final String JSON_PARSE_EXCEPTION = "JSON parsing exception";
	public static final String MISSING_HOST_AUTH_TOKEN = "Host authentication token not found";
	public static final String ERP_SOCKET_TIMEOUT = "ERP socket time-out";
	public static final String FAILED_ERP_RESPONSE = "ERP response failed";
	public static final String ERP_CONNECTION_REFUSED = "Connection was refused remotely";
	public static final String NO_TODAYS_PICKUPS = "No pickups for today";
	public static final String NO_PICKUP_DATA_FROM_DEVICE = "Pickup data not sent from device";
	public static final String NO_DELIVERY_DATA_FROM_DEVICE = "Delivery data not sent from device";
	public static final String MISSING_DOCKET_NUMBER = "Missing docket number";
	public static final String NO_DOCKETS = "No dockets";
	public static final String PICKUP_RESCHEDULING_FAILED = "Pickup rescheduling failed(erp).";
	public static final String NO_DATA_FROM_ERP = "No data(erp)";
	public static final String NO_DATA_AVAILABLE = "No data available";
	public static final String USER_NOT_FOUND = "Not an existing user";
	public static final String INCOMPLETE_DATA_SENT = "Incomplete data sent to server";
	public static final String NO_CONSIGNORS_AVLBL = "No Consignors Available";
	public static final String NO_CONSIGNEE_AVLBL = "No Consignee Available";
}
