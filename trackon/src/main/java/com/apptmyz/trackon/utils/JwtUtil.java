//package com.apptmyz.trackon.utils;
//
//import java.io.UnsupportedEncodingException;
//import java.util.Calendar;
//import java.util.Date;
//
//import org.springframework.security.core.Authentication;
//
//import com.apptmyz.trackon.services.UserDetailsImpl;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.ExpiredJwtException;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.MalformedJwtException;
//import io.jsonwebtoken.SignatureAlgorithm;
//import io.jsonwebtoken.SignatureException;
//import io.jsonwebtoken.UnsupportedJwtException;
//
//public class JwtUtil {
//	private String key = "1D959218E73C3A62DA89D703CE428AB463258AD25E3A3DAF00F1F4834311E1D5";
//	private static String ISSUER = "com.apptmyz.trackon";
//	public String createJWTToken(String data, int min, Authentication authentication) throws UnsupportedEncodingException
//	{
//		Calendar c = Calendar.getInstance();
//		c.add(Calendar.MINUTE, min);
//		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
//		
//		String jwt = Jwts.builder()
//				.setIssuer(ISSUER)
//				.setSubject(userPrincipal.getUserid())
//				.setExpiration(c.getTime())
//				.setIssuedAt(new Date())
//				.claim("name", "TokenData")
//				.claim("scope", "user/logins")
//				.claim("data", data)
//				.signWith(SignatureAlgorithm.HS256, key.getBytes("UTF-8")).compact();
//		
//		return jwt;
//	}
//	
//	public String parseJWT(String jwt) throws ExpiredJwtException, UnsupportedJwtException, MalformedJwtException, SignatureException, IllegalArgumentException, UnsupportedEncodingException
//	{
//		Jws<Claims> claims = Jwts.parser()
//				.setSigningKey(key.getBytes("UTF-8"))
//				.parseClaimsJws(jwt);
////		String name = (String) claims.getBody().get("name");
////		String scope = (String) claims.getBody().get("scope");
//		return (String) claims.getBody().get("data");
//	}
//}
