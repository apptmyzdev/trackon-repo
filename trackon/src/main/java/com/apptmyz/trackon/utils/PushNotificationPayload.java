package com.apptmyz.trackon.utils;

public class PushNotificationPayload {

	private PushNotificationModel message;

	public PushNotificationModel getMessage() {
		return message;
	}

	public void setMessage(PushNotificationModel message) {
		this.message = message;
	}
	
}
