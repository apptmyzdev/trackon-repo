package com.apptmyz.trackon.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.apptmyz.trackon.entities.jpa.DlyDataEntity;
import com.apptmyz.trackon.entities.jpa.MConsigneeEntity;
import com.apptmyz.trackon.entities.jpa.MConsignorEntity;
import com.apptmyz.trackon.entities.jpa.MaBranchEntity;
import com.apptmyz.trackon.entities.jpa.MaContentEntity;
import com.apptmyz.trackon.entities.jpa.MaCustomerDetailsEntity;
import com.apptmyz.trackon.entities.jpa.MaPaperWorkEntity;
import com.apptmyz.trackon.entities.jpa.MaPincodeEntity;
import com.apptmyz.trackon.entities.jpa.PdcAssignedEntity;
import com.apptmyz.trackon.entities.jpa.TrackonDrsEntity;
import com.apptmyz.trackon.entities.jpa.UserMasterEntity;
import com.apptmyz.trackon.model.Consignee;
import com.apptmyz.trackon.model.Consignor;
import com.apptmyz.trackon.model.DRSResponseModel;
import com.apptmyz.trackon.model.DeliveriesData;
import com.apptmyz.trackon.model.MaBranchModel;
import com.apptmyz.trackon.model.MaContentTypeModel;
import com.apptmyz.trackon.model.MaCustomerDetailsModel;
import com.apptmyz.trackon.model.MaPaperWorkModel;
import com.apptmyz.trackon.model.MaPincodeModel;
import com.apptmyz.trackon.model.PDCModel;
import com.apptmyz.trackon.model.TrackonDRSDetailsModel;
import com.apptmyz.trackon.model.TrackonUserMasterModel;


public class EntityToModels {

	public static SimpleDateFormat ddMMyyyyHHmmss_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public static TrackonDrsEntity convertTrackonDRSDetailModelToEntity(TrackonDRSDetailsModel model, String apiType) {
		if (model == null) {
			return null;
		}
		TrackonDrsEntity entity = new TrackonDrsEntity();
		entity.setAddress(model.getAddress());
		entity.setAssignedToUser(model.getAssignedToUser());
		entity.setAwbNo(model.getAwbNo());
		entity.setAwbSerialNo(model.getAwbSerialNo());
		entity.setCity(model.getCity());
		//		drsEntity.setCodAmount(drs.get);
		entity.setCreatedTimestamp(new Date());
		entity.setPincode(model.getPincode());
		entity.setDrsNo(model.getDrsNo());
		entity.setEntryDateTime(model.getEntryDateTime());
		//		drsEntity.setFodAmount(fodAmount);
		entity.setIsCod(model.isCod() == true ? 1:0);
		entity.setIsFod(model.isFod() == true ? 1 : 0);
		entity.setMobileNo(model.getMobileNo());
		entity.setNoPkts(model.getNoPcs());
		entity.setOfdId((int) model.getOfdId());
		entity.setDrsId((int) model.getDrsDetailId());
		entity.setReceiverName(model.getReceiverName());
		entity.setRemarks(model.getRemarks());
		entity.setWeight(model.getWeight());
		entity.setCodAmount(model.getCodAmount());
		entity.setFodAmount(model.getFodAmount());
		entity.setApiType(apiType);
		entity.setIsOtpMandatory(model.isOTPMandatory() ? 1 : 0);
		entity.setUserType(model.getUserType());
		entity.setFranchiseBranchCode(model.getFranchiseeBranchCode());
		entity.setBranchCode(model.getBranchCode());
		entity.setAckRequired(model.isDRAcknowledgeReqd() ? 1 : 0);
		entity.setAcknowledged(null);
		entity.setAllowBulkUpdate(model.isBulkUpdateAllowed() ? 1 : 0);
		return entity;
	}

	public static List<TrackonDrsEntity> convertTrackonDRSDetailModelsToEntity(List<TrackonDRSDetailsModel> models, String apiType) {
		List<TrackonDrsEntity> list = new ArrayList<>();
		if (models != null && models.size() > 0) {
			models.forEach(model -> {
				if (model != null)
					list.add(convertTrackonDRSDetailModelToEntity(model, apiType));
			});
		}
		return list;

	}

	public static UserMasterEntity convertTrackonUserMasterModelToEntity(TrackonUserMasterModel model, UserMasterEntity entity) {

		if (model == null) {
			return null;
		}

		if(entity == null){
			entity = new UserMasterEntity();
			entity.setCreatedTimestamp(new Date());
		}
		else{
			entity.setUpdatedTimestamp(new Date());
		}
		entity.setFullName(model.getFullName());
		entity.setActiveFlag(1);
		entity.setBranchCode(model.getBranchCode());
		entity.setBranchName(model.getBranchName());
		entity.setCityName(model.getCityName());
		entity.setGstStateCode(model.getGstStateCode());
		entity.setMobileNo(model.getMobileNo());
		entity.setRoCode(model.getRoCode());
		entity.setStateName(model.getStateName());
		entity.setTransporterId(model.getTransporterId());
		entity.setUserId(model.getUserName());
		entity.setUserPassword(model.getUserPassword());
		entity.setUserType(model.getUserType());
		entity.setFranchiseBranchCode(model.getFranchiseBranchCode());
		return entity;
	}

	public static List<UserMasterEntity> convertTrackonUserModelsToEntity(List<TrackonUserMasterModel> models) {
		List<UserMasterEntity> list = new ArrayList<>();
		try{
			if (models != null && models.size() > 0) {
				models.forEach(model -> {
					if (model != null)
						list.add(convertTrackonUserMasterModelToEntity(model, null));
				});
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		return list;
	}

	public static PDCModel convertPdcAssignedEntityToPdcModel(PdcAssignedEntity entity) throws Exception{
		PDCModel p = new PDCModel();
		p.setAgentId(entity.getAgentUserName());
		p.setBranchCode(entity.getBranchCode());
		if(entity.getPdcDate() != null)
			p.setPdcDate(ddMMyyyyHHmmss_format.format(entity.getPdcDate()));
		p.setPdcNo(entity.getPdcNo());
		p.setVehicleNo(entity.getVehicleNo());

		return p;
	}

	public static List<PDCModel> convertAndGetPdcModelList(List<PdcAssignedEntity> entities) throws Exception {
		List<PDCModel> data = new ArrayList<>();
		if(entities != null && !entities.isEmpty()){
			for(PdcAssignedEntity p : entities){
				data.add(convertPdcAssignedEntityToPdcModel(p));
			}
		}
		return data;
	}

	public static TrackonDRSDetailsModel convertAndGetTrackonDrsDetailsModel(TrackonDrsEntity e) throws Exception{

		TrackonDRSDetailsModel d = new TrackonDRSDetailsModel();
		d.setAddress(e.getAddress());
		d.setAssignedToUser(e.getAssignedToUser());
		d.setAwbNo(e.getAwbNo());
		d.setAwbSerialNo(e.getAwbSerialNo());
		d.setCity(e.getCity());
		if(e.getIsCod() != null)
			d.setCod(e.getIsCod() == 1 ? true : false);
		d.setCodAmount(e.getCodAmount());
		d.setDrsNo(e.getDrsNo());
		d.setEntryDateTime(e.getEntryDateTime());
		if(e.getIsFod() != null)
			d.setFod(e.getIsFod() == 1 ? true : false);
		d.setFodAmount(e.getFodAmount());
		d.setMobileNo(e.getMobileNo());
		d.setNoPcs(e.getNoPkts());
		d.setPincode(e.getPincode());
		d.setReceiverName(e.getReceiverName());
		d.setRemarks(e.getRemarks());
		d.setWeight(e.getWeight());
		return d;

	}

	public static List<TrackonDRSDetailsModel> getTrackonDrsDetailsModelList(List<TrackonDrsEntity> entities) throws Exception {
		List<TrackonDRSDetailsModel> data = new ArrayList<>();
		if(entities != null && !entities.isEmpty()){
			for(TrackonDrsEntity p : entities){
				data.add(convertAndGetTrackonDrsDetailsModel(p));
			}
		}
		return data;
	}

	public static DRSResponseModel getAssignedDrsModel(TrackonDrsEntity pdc){

		DRSResponseModel docket = new DRSResponseModel();
		docket.setId(pdc.getId());
		docket.setAgentId(pdc.getAssignedToUser());
		docket.setDrsDate(pdc.getEntryDateTime());
		docket.setDrsNo(pdc.getDrsNo());
		docket.setWeight(pdc.getWeight());
		docket.setConsigneeName(pdc.getReceiverName());
		docket.setConsigneeAddress(pdc.getAddress());
		docket.setCity(pdc.getCity());
		docket.setAwbNo(pdc.getAwbNo());
		docket.setConsigneeMobileNo(pdc.getMobileNo());
		docket.setPincode(pdc.getPincode());
		docket.setNoPkts(pdc.getNoPkts());
		docket.setAwbSerialNo(pdc.getAwbSerialNo());
		docket.setCodAmount(pdc.getCodAmount());
		docket.setFodAmount(pdc.getFodAmount());
		if(pdc.getIsCod() != null)
			docket.setIsCod(pdc.getIsCod() == 1? true:false);
		else
			docket.setIsCod(false);

		if(pdc.getIsFod() != null)
			docket.setIsFod(pdc.getIsFod() == 1 ? true:false);
		else
			docket.setIsFod(false);
		docket.setRemarks(pdc.getRemarks());
		docket.setRedirectedBy(pdc.getRedirectedBy());
		docket.setIsOtpMandatory(pdc.getIsOtpMandatory());
		docket.setBulkUpdateAllowed(pdc.getAllowBulkUpdate() != null && pdc.getAllowBulkUpdate() == 1);
		docket.setBranchCode(pdc.getBranchCode());
		return docket;
	}

	public static DeliveriesData getDeliveryReportModel(DlyDataEntity dlyentity){

		DeliveriesData dd = new DeliveriesData();
		dd.setDeliveredTo(dlyentity.getRecieverName());
		dd.setDeliveryBranch(dlyentity.getDlyBranchCode());
		dd.setAmountCollected(dlyentity.getAmountCollected());
		if(dlyentity.getBadDelivery() != null)
			dd.setBadDellivery(dlyentity.getBadDelivery() == 1 ? Constants.BAD_DELIVERY:Constants.GOOD_DELIVERY);
		//		dd.setDeliveryDate(dlyentity.getDeliveredTime());
		if(dlyentity.getDeliveredTime() != null)
			dd.setDeliveryDate(ddMMyyyyHHmmss_format.format(dlyentity.getDeliveredTime()));
		dd.setDocketNo(dlyentity.getDktNo());
		dd.setRemarks(dlyentity.getRemarks());

		dd.setUserId(dlyentity.getAgentUserName());
		dd.setUndeliveryReason(dlyentity.getUndeliveryReason());
		dd.setStatus(dlyentity.getDlyStatus());
		dd.setRelationShip(dlyentity.getRelationship());
		dd.setDrsNo(dlyentity.getPdcNo());

		return dd;
	}

	public static MaPincodeEntity convertMaPincodeModelToEntity(MaPincodeModel maPincode, MaPincodeEntity pincodeEntity) {
		if (maPincode == null) {
			return null;
		}

		if(pincodeEntity == null){
			pincodeEntity = new MaPincodeEntity();
			
		}
		pincodeEntity.setActiveFlag(1);
		pincodeEntity.setCreatedTimestamp(new Date());
		pincodeEntity.setPincode(Integer.parseInt(maPincode.getPincode()));
		pincodeEntity.setOfficeCode(maPincode.getOfficeCode());
		pincodeEntity.setOfficeName(maPincode.getOfficeName());
		pincodeEntity.setCityName(maPincode.getCityName());
		pincodeEntity.setStateName(maPincode.getStateName());
		pincodeEntity.setIsServicable(maPincode.getIsServiceable().equals("Y")?1:0);
		pincodeEntity.setNonDox(maPincode.getNonDox().equals("Y")?1:0);
		pincodeEntity.setDox(maPincode.getDox().equals("Y")?1:0);
		pincodeEntity.setPrimeTrack(maPincode.getPrimeTrack().equals("Y")?1:0);
		pincodeEntity.setStateExp(maPincode.getStateExp().equals("Y")?1:0);
		pincodeEntity.setToPay(maPincode.getTopay().equals("Y")?1:0);
		pincodeEntity.setOda(maPincode.getODA().equals("Y")?1:0);
		pincodeEntity.setRoadExpress(maPincode.getRoadExpress().equals("Y")?1:0);
		pincodeEntity.setRoda(maPincode.getRODA().equals("Y")?1:0);
		pincodeEntity.setReversePickup(maPincode.getReversePickup().equals("Y")?1:0);
		return pincodeEntity;
	}

	public static MaCustomerDetailsEntity convertMaCustomerModelToEntity(MaCustomerDetailsModel maCustomer,
			MaCustomerDetailsEntity maCustomerDetails) {

		if (maCustomer == null) {
			return null;
		}

		if(maCustomerDetails == null){
			maCustomerDetails = new MaCustomerDetailsEntity();
		}
		maCustomerDetails.setActiveFlag(1);
		maCustomerDetails.setCreatedTimestamp(new Date());
		maCustomerDetails.setCustCode(maCustomer.getCustCode());
		maCustomerDetails.setCustomerName(maCustomer.getCustomerName());
		maCustomerDetails.setCustomerType(maCustomer.getCustomerType());
		maCustomerDetails.setDownloadType(maCustomer.getDownloadType());
		maCustomerDetails.setIndustryType(maCustomer.getIndustryType());
		return maCustomerDetails;
	}

	public static MaContentEntity convertMaContentModelToEntity(MaContentTypeModel maContent, MaContentEntity contentEntity) {
		if (maContent == null) {
			return null;
		}

		if(contentEntity == null){
			contentEntity= new MaContentEntity();
		}	
		contentEntity.setActiveFlag(1);
		contentEntity.setUpdatedTimestamp(new Date());
		contentEntity.setErpId(Integer.parseInt(maContent.getId()));
		contentEntity.setSkuCategory(maContent.getSkuCategory());
		contentEntity.setSkuCode(maContent.getSkuCode());
		return contentEntity;

	}

	public static MaBranchEntity convertMaBranchModelToEntity(MaBranchModel maBranch, MaBranchEntity branchEntity) {
		if (maBranch == null) {
			return null;
		}

		if(branchEntity == null){
			branchEntity = new MaBranchEntity();
		}	
		branchEntity.setActiveFlag(1);
		branchEntity.setCreatedTimestamp(new Date());
		branchEntity.setOfficeCode(maBranch.getOfficeCode());
		branchEntity.setOfficeName(maBranch.getOfficeName());
		branchEntity.setOfficeType(maBranch.getOfficeType());
		branchEntity.setConnectingBranch(maBranch.getConnectingBranch());
		branchEntity.setReportingZone(maBranch.getReportingZone());
		branchEntity.setRoCode(maBranch.getROCode());
		return branchEntity;
	}

	public static MaPaperWorkEntity convertMaPaperWorkModelToEntity(MaPaperWorkModel maPaperWork,
			MaPaperWorkEntity paperWork) {
		if (maPaperWork == null) {
			return null;
		}

		if(paperWork == null){
			paperWork= new MaPaperWorkEntity();
		}	
		paperWork.setActiveFlag(1);
		paperWork.setCreatedTimestamp(new Date());
		paperWork.setErpId(Integer.parseInt(maPaperWork.getID()));
		paperWork.setPwName(maPaperWork. getPwName());
		return paperWork;
	}
	
	public static Consignor convertConsignorEntityToModel(MConsignorEntity e) throws Exception{
		Consignor c = new Consignor();
		c.setAddress1(e.getConsignorAddress1());
		c.setAddress2(e.getConsignorAddress2());
		c.setBranchCode(e.getBranchCode());
		c.setCity(e.getCity());
		c.setConsignorCode(e.getConsignorCode());
		c.setConsignorId(e.getId());
		c.setConsignorName(e.getConsignorName());
		c.setConsignorPincode(e.getPincode());
		c.setContactEmailid(e.getContactEmailid());
		c.setContactName(e.getContactName());
		c.setContactPhoneno(e.getContactPhoneno());
//		c.setContractCustomer(e.get);
		c.setCustCode(e.getCustCode());
//		c.setCustomerId(e.get);
		c.setEmail(e.getEmail());
		c.setGstin(e.getGstin());
//		c.setMobileno(e.get);
		c.setPan(e.getPan());
		c.setState(e.getState());
		return c;
	}
	
	public static Consignee convertConsigneeEntityToModel(MConsigneeEntity e) throws Exception{
		Consignee c = new Consignee();
		c.setAddress1(e.getConsigneeAddress1());
		c.setAddress2(e.getConsigneeAddress2());
		c.setCity(e.getCity());
		c.setConsigneeCode(e.getConsigneeCode());
		c.setConsigneeId(e.getId());
		c.setConsigneeName(e.getConsigneeName());
		c.setConsigneePincode(e.getPincode());
		c.setEmail(e.getEmail());
		c.setContactName(e.getContactName());
		c.setContactPhoneno(e.getContactPhoneno());
		c.setCustCode(e.getCustCode());
		c.setEmail(e.getEmail());
		c.setGstin(e.getGstin());
		c.setPan(e.getPan());
		c.setState(e.getState());
		return c;
	}
	
	public static MConsignorEntity convertConsignorModelToEntity(Consignor m, MConsignorEntity c) {
		if(c == null){
			c = new MConsignorEntity();
			c.setActiveFlag(1);
			c.setCreatedTimestamp(new Date());
			c.setBranchCode(m.getBranchCode());
			c.setConsignorCode(m.getConsignorCode());
			c.setCustCode(m.getCustCode());
			c.setVersion(1);
		}
		else{
			c.setUpdatedTimestamp(new Date());
			c.setVersion(c.getVersion() != null ? c.getVersion() + 1 : 1);
		}
		
		c.setCity(m.getCity());
		c.setConsignorAddress1(m.getAddress1());
		c.setConsignorAddress2(m.getAddress2());
		c.setConsignorCode(m.getConsignorCode());
		c.setConsignorName(m.getConsignorName());
		c.setContactEmailid(m.getContactEmailid());
		c.setContactName(m.getContactName());
		c.setContactPhoneno(m.getContactPhoneno());
		c.setCreatedTimestamp(new Date());
		c.setEmail(m.getEmail());
		c.setGstin(m.getGstin());
//		c.setMaCustomerDetails(maCustomerDetails);
		c.setPan(m.getPan());
		c.setPhoneNum(m.getMobileno());
		c.setPincode(m.getConsignorPincode());
		c.setStateCode(m.getState());
	
		return c;
	}
}

