package com.apptmyz.trackon.model;

import java.util.Date;

public class UserSessionData {
	private String branchCode;
	private Integer currentAppVersion;
	private String deviceImei;
	private String loginTimestamp;
	private String logoutTimestamp;
	private Integer activeFlag;
	private String userId;

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Integer getCurrentAppVersion() {
		return currentAppVersion;
	}

	public void setCurrentAppVersion(Integer currentAppVersion) {
		this.currentAppVersion = currentAppVersion;
	}

	public String getDeviceImei() {
		return deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getLoginTimestamp() {
		return loginTimestamp;
	}

	public void setLoginTimestamp(String string) {
		this.loginTimestamp = string;
	}

	public String getLogoutTimestamp() {
		return logoutTimestamp;
	}

	public void setLogoutTimestamp(String logoutTimestamp) {
		this.logoutTimestamp = logoutTimestamp;
	}

	public Integer getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
