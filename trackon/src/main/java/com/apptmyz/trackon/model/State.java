package com.apptmyz.trackon.model;

public class State {
	String code;
	String name;
	String type;
	String stateGstCode;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStateGstCode() {
		return stateGstCode;
	}
	public void setStateGstCode(String stateGstCode) {
		this.stateGstCode = stateGstCode;
	}
	
	
}
