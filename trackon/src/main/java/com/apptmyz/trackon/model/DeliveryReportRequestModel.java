package com.apptmyz.trackon.model;

public class DeliveryReportRequestModel {
	
	private String fromDate;
	private String toDate;
	private String branchCode;
	private String dlyStatus;
	
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getDlyStatus() {
		return dlyStatus;
	}
	public void setDlyStatus(String dlyStatus) {
		this.dlyStatus = dlyStatus;
	}
	
	
}
