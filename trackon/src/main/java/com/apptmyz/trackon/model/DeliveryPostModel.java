package com.apptmyz.trackon.model;

import java.math.BigDecimal;
import java.util.List;

public class DeliveryPostModel {
	String userId;
	String branchCode;
	String pdcNo;
	String docketNo;
	String remarks;
	Double latitude;
	Double longitude;
	String deliveredTime;
	String recieverName;
	boolean isBadDelivery;
	boolean hasDeps;
	String depsReason;
	Integer txnId;

	List<String> podImages;
	List<String> signatureImages;
	List<String> paymentProofImages;
	List<String> depsImages;

	String paymentMethod;
	BigDecimal amountToBeCollected;
	BigDecimal amountCollected;
	boolean isPaymentCollected;
	String bankName;
	String paymentReferenceId;
	String txnDate;
	String deliveredContact;
	String relationship;
	private String appVersion;
	private String imei;

	private List<ImageData> erpImagesList;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getPdcNo() {
		return pdcNo;
	}

	public void setPdcNo(String pdcNo) {
		this.pdcNo = pdcNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getDeliveredTime() {
		return deliveredTime;
	}

	public void setDeliveredTime(String deliveredTime) {
		this.deliveredTime = deliveredTime;
	}

	public String getRecieverName() {
		return recieverName;
	}

	public void setRecieverName(String recieverName) {
		this.recieverName = recieverName;
	}

	public boolean getIsBadDelivery() {
		return isBadDelivery;
	}

	public void setIsBadDelivery(boolean isBadDelivery) {
		this.isBadDelivery = isBadDelivery;
	}

	public boolean isHasDeps() {
		return hasDeps;
	}

	public void setHasDeps(boolean hasDeps) {
		this.hasDeps = hasDeps;
	}

	public String getDepsReason() {
		return depsReason;
	}

	public void setDepsReason(String depsReason) {
		this.depsReason = depsReason;
	}

	public List<String> getPodImages() {
		return podImages;
	}

	public void setPodImages(List<String> podImages) {
		this.podImages = podImages;
	}

	public List<String> getSignatureImages() {
		return signatureImages;
	}

	public void setSignatureImages(List<String> signatureImages) {
		this.signatureImages = signatureImages;
	}

	public List<String> getPaymentProofImages() {
		return paymentProofImages;
	}

	public void setPaymentProofImages(List<String> paymentProofImages) {
		this.paymentProofImages = paymentProofImages;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public BigDecimal getAmountToBeCollected() {
		return amountToBeCollected;
	}

	public void setAmountToBeCollected(BigDecimal amountToBeCollected) {
		this.amountToBeCollected = amountToBeCollected;
	}

	public BigDecimal getAmountCollected() {
		return amountCollected;
	}

	public void setAmountCollected(BigDecimal amountCollected) {
		this.amountCollected = amountCollected;
	}

	public boolean getIsPaymentCollected() {
		return isPaymentCollected;
	}

	public void setIsPaymentCollected(boolean isPaymentCollected) {
		this.isPaymentCollected = isPaymentCollected;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPaymentReferenceId() {
		return paymentReferenceId;
	}

	public void setPaymentReferenceId(String paymentReferenceId) {
		this.paymentReferenceId = paymentReferenceId;
	}

	public String getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}

	public List<String> getDepsImages() {
		return depsImages;
	}

	public void setDepsImages(List<String> depsImages) {
		this.depsImages = depsImages;
	}

	public String getDeliveredContact() {
		return deliveredContact;
	}

	public void setDeliveredContact(String deliveredContact) {
		this.deliveredContact = deliveredContact;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public Integer getTxnId() {
		return txnId;
	}

	public void setTxnId(Integer txnId) {
		this.txnId = txnId;
	}

	public List<ImageData> getErpImagesList() {
		return erpImagesList;
	}

	public void setErpImagesList(List<ImageData> erpImagesList) {
		this.erpImagesList = erpImagesList;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

}
