package com.apptmyz.trackon.model;

public class HostAuthenticationModel {
	private String hostname;
	private String hostPassword;

	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getHostPassword() {
		return hostPassword;
	}
	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}
}
