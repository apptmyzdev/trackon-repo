package com.apptmyz.trackon.model;

import java.util.List;

public class PickupCancelModel {
	String userId;
	int pickupId;
	String pickupRegistrationId;
	String reason;
	String remarks;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getPickupId() {
		return pickupId;
	}
	public void setPickupId(int pickupId) {
		this.pickupId = pickupId;
	}
	public String getPickupRegistrationId() {
		return pickupRegistrationId;
	}
	public void setPickupRegistrationId(String pickupRegistrationId) {
		this.pickupRegistrationId = pickupRegistrationId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
