package com.apptmyz.trackon.model;

import java.util.List;

public class BookingDataModel {

	Integer pickupId;
	boolean closeFlag;
	String bookingAgentId;
	String bookingBranchCode;
	Integer bookingPincode;
	String imei;
	String version;
	String bookingUserType;

	List<BookingDocketDataModel> dockets;

	public Integer getPickupId() {
		return pickupId;
	}

	public void setPickupId(Integer pickupId) {
		this.pickupId = pickupId;
	}

	public boolean getCloseFlag() {
		return closeFlag;
	}

	public void setCloseFlag(boolean closeFlag) {
		this.closeFlag = closeFlag;
	}

	public String getBookingAgentId() {
		return bookingAgentId;
	}

	public void setBookingAgentId(String bookingAgentId) {
		this.bookingAgentId = bookingAgentId;
	}

	public String getBookingBranchCode() {
		return bookingBranchCode;
	}

	public void setBookingBranchCode(String bookingBranchCode) {
		this.bookingBranchCode = bookingBranchCode;
	}

	public Integer getBookingPincode() {
		return bookingPincode;
	}

	public void setBookingPincode(Integer bookingPincode) {
		this.bookingPincode = bookingPincode;
	}

	public List<BookingDocketDataModel> getDockets() {
		return dockets;
	}

	public void setDockets(List<BookingDocketDataModel> dockets) {
		this.dockets = dockets;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBookingUserType() {
		return bookingUserType;
	}

	public void setBookingUserType(String bookingUserType) {
		this.bookingUserType = bookingUserType;
	}

}
