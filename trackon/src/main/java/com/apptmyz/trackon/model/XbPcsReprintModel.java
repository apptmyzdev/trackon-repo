package com.apptmyz.trackon.model;

public class XbPcsReprintModel {

	private String awbNum;
	private String xbAwbNum;
	private String refNo;
	private String xbRefNo;

	public String getAwbNum() {
		return awbNum;
	}

	public void setAwbNum(String awbNum) {
		this.awbNum = awbNum;
	}

	public String getXbAwbNum() {
		return xbAwbNum;
	}

	public void setXbAwbNum(String xbAwbNum) {
		this.xbAwbNum = xbAwbNum;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getXbRefNo() {
		return xbRefNo;
	}

	public void setXbRefNo(String xbRefNo) {
		this.xbRefNo = xbRefNo;
	}

}
