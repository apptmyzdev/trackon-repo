package com.apptmyz.trackon.model;

public class AgentModel {
	String userId;
	String fullName;
	String branch;
	String userType;
	boolean isRegistrationAllowed;
	boolean isDeliveryAllowed;
	boolean isBookingAllowed;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public boolean getIsRegistrationAllowed() {
		return isRegistrationAllowed;
	}

	public void setIsRegistrationAllowed(boolean isRegistrationAllowed) {
		this.isRegistrationAllowed = isRegistrationAllowed;
	}

	public boolean getIsDeliveryAllowed() {
		return isDeliveryAllowed;
	}

	public void setIsDeliveryAllowed(boolean isDeliveryAllowed) {
		this.isDeliveryAllowed = isDeliveryAllowed;
	}

	public boolean getIsBookingAllowed() {
		return isBookingAllowed;
	}

	public void setIsBookingAllowed(boolean isBookingAllowed) {
		this.isBookingAllowed = isBookingAllowed;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
