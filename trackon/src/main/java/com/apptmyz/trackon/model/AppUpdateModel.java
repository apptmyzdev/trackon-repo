package com.apptmyz.trackon.model;

import java.util.List;

public class AppUpdateModel {

	private int version;
	private String url;
	private String notes;
	private List<UrlsModel> urlData;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<UrlsModel> getUrlData() {
		return urlData;
	}

	public void setUrlData(List<UrlsModel> urlData) {
		this.urlData = urlData;
	}

}
