package com.apptmyz.trackon.model;

import java.util.List;

public class PickupRegistrationSoftDataDetails {
	Double consignmentValue;
	String consignorGstin;
	String consineeGstin;
	String material;
	List<LbhData> lbhData;
	List<PaperWork> paperWork;
	List<PickupInvoiceEwb> invoiceEwb;
	public Double getConsignmentValue() {
		return consignmentValue;
	}
	public void setConsignmentValue(Double consignmentValue) {
		this.consignmentValue = consignmentValue;
	}
	public String getConsignorGstin() {
		return consignorGstin;
	}
	public void setConsignorGstin(String consignorGstin) {
		this.consignorGstin = consignorGstin;
	}
	public String getConsineeGstin() {
		return consineeGstin;
	}
	public void setConsineeGstin(String consineeGstin) {
		this.consineeGstin = consineeGstin;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public List<LbhData> getLbhData() {
		return lbhData;
	}
	public void setLbhData(List<LbhData> lbhData) {
		this.lbhData = lbhData;
	}
	public List<PaperWork> getPaperWork() {
		return paperWork;
	}
	public void setPaperWork(List<PaperWork> paperWork) {
		this.paperWork = paperWork;
	}
	public List<PickupInvoiceEwb> getInvoiceEwb() {
		return invoiceEwb;
	}
	public void setInvoiceEwb(List<PickupInvoiceEwb> invoiceEwb) {
		this.invoiceEwb = invoiceEwb;
	}
}
