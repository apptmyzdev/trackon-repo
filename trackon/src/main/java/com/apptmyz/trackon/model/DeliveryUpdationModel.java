package com.apptmyz.trackon.model;

import java.util.List;

public class DeliveryUpdationModel {

	private Integer deliverySheetId;
	private String sheetNumber;
	// private Integer docketNumber;
	private String docketNumber;
	private int deliveryBranchId;
	private String deliveryBranchCode;
	private String deliveryDate;
	private double crChequeAmount;
	private double crCashAmount;
	private double crTotalAmount;
	private String crChequeNumber;
	private String crChequeDate;
	private String crBankName;
	private String crBankBranch;
	private String crRemarks;
	private int reasonId;
	private String reason;
	private int createdBy;
	private String userId;
	private String paymentType;
	private String crPaymentMode;
	private int deliveryStatus;
	private String podImage;
	private String signatureImage;
	private boolean erpResult;
	private String erpMessage;
	private double latitude;
	private double longitude;
	private String deviceTimestamp;
	private double tdsAmount;
	private String codDodMode;
	private double codDodAmount;
	private String codDodChDDNo;
	private String codDodChDDDate;
	private String codDodBankName;
	private String codDodBankBranch;
	private String codDodRemarks;
	private int isCodDod;
	private String codDodImage;
	// added on 7-7-2020
	private String purpose; // undelivery
	private String podRemarks; // delivery

	private List<String> depsImages;

	public Integer getDeliverySheetId() {
		return deliverySheetId;
	}

	public void setDeliverySheetId(Integer deliverySheetId) {
		this.deliverySheetId = deliverySheetId;
	}

	public int getDeliveryBranchId() {
		return deliveryBranchId;
	}

	public String getDocketNumber() {
		return docketNumber;
	}

	public void setDocketNumber(String docketNumber) {
		this.docketNumber = docketNumber;
	}

	public void setDeliveryBranchId(int deliveryBranchId) {
		this.deliveryBranchId = deliveryBranchId;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public double getCrChequeAmount() {
		return crChequeAmount;
	}

	public void setCrChequeAmount(double crChequeAmount) {
		this.crChequeAmount = crChequeAmount;
	}

	public double getCrCashAmount() {
		return crCashAmount;
	}

	public void setCrCashAmount(double crCashAmount) {
		this.crCashAmount = crCashAmount;
	}

	public double getCrTotalAmount() {
		return crTotalAmount;
	}

	public void setCrTotalAmount(double crTotalAmount) {
		this.crTotalAmount = crTotalAmount;
	}

	public String getCrChequeNumber() {
		return crChequeNumber;
	}

	public void setCrChequeNumber(String crChequeNumber) {
		this.crChequeNumber = crChequeNumber;
	}

	public String getCrChequeDate() {
		return crChequeDate;
	}

	public void setCrChequeDate(String crChequeDate) {
		this.crChequeDate = crChequeDate;
	}

	public String getCrBankName() {
		return crBankName;
	}

	public void setCrBankName(String crBankName) {
		this.crBankName = crBankName;
	}

	public String getCrBankBranch() {
		return crBankBranch;
	}

	public void setCrBankBranch(String crBankBranch) {
		this.crBankBranch = crBankBranch;
	}

	public String getCrRemarks() {
		return crRemarks;
	}

	public void setCrRemarks(String crRemarks) {
		this.crRemarks = crRemarks;
	}

	public int getReasonId() {
		return reasonId;
	}

	public void setReasonId(int reasonId) {
		this.reasonId = reasonId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCrPaymentMode() {
		return crPaymentMode;
	}

	public void setCrPaymentMode(String crPaymentMode) {
		this.crPaymentMode = crPaymentMode;
	}

	public int getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(int deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getPodImage() {
		return podImage;
	}

	public void setPodImage(String podImage) {
		this.podImage = podImage;
	}

	public String getSignatureImage() {
		return signatureImage;
	}

	public void setSignatureImage(String signatureImage) {
		this.signatureImage = signatureImage;
	}

	public List<String> getDepsImages() {
		return depsImages;
	}

	public void setDepsImages(List<String> depsImages) {
		this.depsImages = depsImages;
	}

	public String getSheetNumber() {
		return sheetNumber;
	}

	public void setSheetNumber(String sheetNumber) {
		this.sheetNumber = sheetNumber;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getDeliveryBranchCode() {
		return deliveryBranchCode;
	}

	public void setDeliveryBranchCode(String deliveryBranchCode) {
		this.deliveryBranchCode = deliveryBranchCode;
	}

	public boolean isErpResult() {
		return erpResult;
	}

	public void setErpResult(boolean erpResult) {
		this.erpResult = erpResult;
	}

	public String getErpMessage() {
		return erpMessage;
	}

	public void setErpMessage(String erpMessage) {
		this.erpMessage = erpMessage;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDeviceTimestamp() {
		return deviceTimestamp;
	}

	public void setDeviceTimestamp(String deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public double getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public String getCodDodMode() {
		return codDodMode;
	}

	public void setCodDodMode(String codDodMode) {
		this.codDodMode = codDodMode;
	}

	public double getCodDodAmount() {
		return codDodAmount;
	}

	public void setCodDodAmount(double codDodAmount) {
		this.codDodAmount = codDodAmount;
	}

	public String getCodDodChDDNo() {
		return codDodChDDNo;
	}

	public void setCodDodChDDNo(String codDodChDDNo) {
		this.codDodChDDNo = codDodChDDNo;
	}

	public String getCodDodChDDDate() {
		return codDodChDDDate;
	}

	public void setCodDodChDDDate(String codDodChDDDate) {
		this.codDodChDDDate = codDodChDDDate;
	}

	public String getCodDodBankName() {
		return codDodBankName;
	}

	public void setCodDodBankName(String codDodBankName) {
		this.codDodBankName = codDodBankName;
	}

	public String getCodDodBankBranch() {
		return codDodBankBranch;
	}

	public void setCodDodBankBranch(String codDodBankBranch) {
		this.codDodBankBranch = codDodBankBranch;
	}

	public String getCodDodRemarks() {
		return codDodRemarks;
	}

	public void setCodDodRemarks(String codDodRemarks) {
		this.codDodRemarks = codDodRemarks;
	}

	public int isCodDod() {
		return isCodDod;
	}

	public void setCodDod(int isCodDod) {
		this.isCodDod = isCodDod;
	}

	public String getCodDodImage() {
		return codDodImage;
	}

	public void setCodDodImage(String codDodImage) {
		this.codDodImage = codDodImage;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getPodRemarks() {
		return podRemarks;
	}

	public void setPodRemarks(String podRemarks) {
		this.podRemarks = podRemarks;
	}

}
