package com.apptmyz.trackon.model;

import java.util.List;

public class ChargesModel {
	String type;
	Double amount;
	List<TaxTypeAmount> taxes;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public List<TaxTypeAmount> getTaxes() {
		return taxes;
	}
	public void setTaxes(List<TaxTypeAmount> taxes) {
		this.taxes = taxes;
	}
	
}
