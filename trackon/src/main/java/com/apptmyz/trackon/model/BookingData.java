package com.apptmyz.trackon.model;

import java.util.Date;

public class BookingData {
	String docketNo;
	String custCode;
	String serviceType;
	String serviceTypeDesc;
	String bookingDate;
	Integer originPincode;
	Integer destPincode;
	int noPkts;
	double actualWt;
	String uomDesc;
	String status;
	String bookingBranch;
	String deliveryBranch;
	String shipModeDesc;
	String doxFlag;
	String doxFlagDesc;
	Integer pickupId;
	private String erpMessage;

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Integer getOriginPincode() {
		return originPincode;
	}

	public void setOriginPincode(Integer originPincode) {
		this.originPincode = originPincode;
	}

	public Integer getDestPincode() {
		return destPincode;
	}

	public void setDestPincode(Integer destPincode) {
		this.destPincode = destPincode;
	}

	public int getNoPkts() {
		return noPkts;
	}

	public void setNoPkts(int noPkts) {
		this.noPkts = noPkts;
	}

	public double getActualWt() {
		return actualWt;
	}

	public void setActualWt(double actualWt) {
		this.actualWt = actualWt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookingBranch() {
		return bookingBranch;
	}

	public void setBookingBranch(String bookingBranch) {
		this.bookingBranch = bookingBranch;
	}

	public String getDeliveryBranch() {
		return deliveryBranch;
	}

	public void setDeliveryBranch(String deliveryBranch) {
		this.deliveryBranch = deliveryBranch;
	}

	public String getServiceTypeDesc() {
		return serviceTypeDesc;
	}

	public void setServiceTypeDesc(String serviceTypeDesc) {
		this.serviceTypeDesc = serviceTypeDesc;
	}

	public String getUomDesc() {
		return uomDesc;
	}

	public void setUomDesc(String uomDesc) {
		this.uomDesc = uomDesc;
	}

	public String getShipModeDesc() {
		return shipModeDesc;
	}

	public void setShipModeDesc(String shipModeDesc) {
		this.shipModeDesc = shipModeDesc;
	}

	public String getDoxFlag() {
		return doxFlag;
	}

	public void setDoxFlag(String doxFlag) {
		this.doxFlag = doxFlag;
	}

	public String getDoxFlagDesc() {
		return doxFlagDesc;
	}

	public void setDoxFlagDesc(String doxFlagDesc) {
		this.doxFlagDesc = doxFlagDesc;
	}

	public Integer getPickupId() {
		return pickupId;
	}

	public void setPickupId(Integer pickupId) {
		this.pickupId = pickupId;
	}

	public String getErpMessage() {
		return erpMessage;
	}

	public void setErpMessage(String erpMessage) {
		this.erpMessage = erpMessage;
	}

}
