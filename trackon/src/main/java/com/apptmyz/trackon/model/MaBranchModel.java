package com.apptmyz.trackon.model;

public class MaBranchModel {
	private String  OfficeCode;
	private String OfficeName;
	private String OfficeType;
	private String ROCode;
	private String ReportingZone;
	private String  ConnectingBranch;
	public String getOfficeCode() {
		return OfficeCode;
	}
	public void setOfficeCode(String officeCode) {
		OfficeCode = officeCode;
	}
	public String getOfficeName() {
		return OfficeName;
	}
	public void setOfficeName(String officeName) {
		OfficeName = officeName;
	}
	public String getOfficeType() {
		return OfficeType;
	}
	public void setOfficeType(String officeType) {
		OfficeType = officeType;
	}
	public String getROCode() {
		return ROCode;
	}
	public void setROCode(String rOCode) {
		ROCode = rOCode;
	}
	public String getReportingZone() {
		return ReportingZone;
	}
	public void setReportingZone(String reportingZone) {
		ReportingZone = reportingZone;
	}
	public String getConnectingBranch() {
		return ConnectingBranch;
	}
	public void setConnectingBranch(String connectingBranch) {
		ConnectingBranch = connectingBranch;
	}


}
