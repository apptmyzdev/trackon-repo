package com.apptmyz.trackon.model;

import java.math.BigDecimal;
import java.util.Date;

public class PDCDockets {
	String 	docketNo;
	String	bookingDate;
	String	bookingBranch;
	int		packetCount;
	Double	actualWt;
//	int		productId;
//	int		paymentType;
	int		odaFlag;
	int		attemptCount;
	int		collectPayment;
	String 	collectRemarks;
	BigDecimal	collectAmount;
//	int		collectType;
	String	collectType;
	
//	String	custCode;
	String 	consigneeName;
	String	consigneeAdd1;
	String	consigneeAdd2;
	String 	consigneeAdd3;
	String	consigneeCity;
	String	consigneeState;
	int		consigneePincode;
	String	consigneeContactName;
	String	consigneePhoneNo;
	public String getDocketNo() {
		return docketNo;
	}
	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getBookingBranch() {
		return bookingBranch;
	}
	public void setBookingBranch(String bookingBranch) {
		this.bookingBranch = bookingBranch;
	}
	public int getPacketCount() {
		return packetCount;
	}
	public void setPacketCount(int packetCount) {
		this.packetCount = packetCount;
	}
	public Double getActualWt() {
		return actualWt;
	}
	public void setActualWt(Double bigDecimal) {
		this.actualWt = bigDecimal;
	}
	public int getOdaFlag() {
		return odaFlag;
	}
	public void setOdaFlag(int odaFlag) {
		this.odaFlag = odaFlag;
	}
	public int getAttemptCount() {
		return attemptCount;
	}
	public void setAttemptCount(int attemptCount) {
		this.attemptCount = attemptCount;
	}
	public int getCollectPayment() {
		return collectPayment;
	}
	public void setCollectPayment(int collectPayment) {
		this.collectPayment = collectPayment;
	}
	public BigDecimal getCollectAmount() {
		return collectAmount;
	}
	public void setCollectAmount(BigDecimal collectAmount) {
		this.collectAmount = collectAmount;
	}
	public String getCollectType() {
		return collectType;
	}
	public void setCollectType(String collectType) {
		this.collectType = collectType;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	public String getConsigneeAdd1() {
		return consigneeAdd1;
	}
	public void setConsigneeAdd1(String consigneeAdd1) {
		this.consigneeAdd1 = consigneeAdd1;
	}
	public String getConsigneeAdd2() {
		return consigneeAdd2;
	}
	public void setConsigneeAdd2(String consigneeAdd2) {
		this.consigneeAdd2 = consigneeAdd2;
	}
	public String getConsigneeAdd3() {
		return consigneeAdd3;
	}
	public void setConsigneeAdd3(String consigneeAdd3) {
		this.consigneeAdd3 = consigneeAdd3;
	}
	public String getConsigneeCity() {
		return consigneeCity;
	}
	public void setConsigneeCity(String consigneeCity) {
		this.consigneeCity = consigneeCity;
	}
	public String getConsigneeState() {
		return consigneeState;
	}
	public void setConsigneeState(String consigneeState) {
		this.consigneeState = consigneeState;
	}
	public int getConsigneePincode() {
		return consigneePincode;
	}
	public void setConsigneePincode(int consigneePincode) {
		this.consigneePincode = consigneePincode;
	}
	public String getConsigneeContactName() {
		return consigneeContactName;
	}
	public void setConsigneeContactName(String consigneeContactName) {
		this.consigneeContactName = consigneeContactName;
	}
	public String getConsigneePhoneNo() {
		return consigneePhoneNo;
	}
	public void setConsigneePhoneNo(String consigneePhoneNo) {
		this.consigneePhoneNo = consigneePhoneNo;
	}
	@Override
	public String toString() {
		return "PDCDockets [docketNo=" + docketNo + ", bookingDate=" + bookingDate + ", bookingBranch=" + bookingBranch
				+ ", packetCount=" + packetCount + ", actualWt=" + actualWt + ", odaFlag=" + odaFlag + ", attemptCount="
				+ attemptCount + ", collectPayment=" + collectPayment + ", collectAmount=" + collectAmount
				+ ", collectType=" + collectType + ", consigneeName=" + consigneeName + ", consigneeAdd1="
				+ consigneeAdd1 + ", consigneeAdd2=" + consigneeAdd2 + ", consigneeAdd3=" + consigneeAdd3
				+ ", consigneeCity=" + consigneeCity + ", consigneeState=" + consigneeState + ", consigneePincode="
				+ consigneePincode + ", consigneeContactName=" + consigneeContactName + ", consigneePhoneNo="
				+ consigneePhoneNo + "]";
	}
	public String getCollectRemarks() {
		return collectRemarks;
	}
	public void setCollectRemarks(String collectRemarks) {
		this.collectRemarks = collectRemarks;
	}
	
	
}
