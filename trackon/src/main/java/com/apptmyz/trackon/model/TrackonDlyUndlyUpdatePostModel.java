package com.apptmyz.trackon.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TrackonDlyUndlyUpdatePostModel {

	@SerializedName(value="DRSUpdateInputList")
	List<TrackonDlyUndlyUpdateModel> drsUpdateInputList;

	public List<TrackonDlyUndlyUpdateModel> getDrsUpdateInputList() {
		return drsUpdateInputList;
	}

	public void setDrsUpdateInputList(List<TrackonDlyUndlyUpdateModel> drsUpdateInputList) {
		this.drsUpdateInputList = drsUpdateInputList;
	}
	
}
