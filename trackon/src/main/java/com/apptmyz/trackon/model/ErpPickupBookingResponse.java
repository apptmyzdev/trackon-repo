package com.apptmyz.trackon.model;

public class ErpPickupBookingResponse {

	private String AWBNumber;
	private Boolean Result;
	private String Messege;

	public String getAWBNumber() {
		return AWBNumber;
	}

	public void setAWBNumber(String aWBNumber) {
		AWBNumber = aWBNumber;
	}

	public Boolean getResult() {
		return Result;
	}

	public void setResult(Boolean result) {
		Result = result;
	}

	public String getMessege() {
		return Messege;
	}

	public void setMessege(String messege) {
		Messege = messege;
	}

}
