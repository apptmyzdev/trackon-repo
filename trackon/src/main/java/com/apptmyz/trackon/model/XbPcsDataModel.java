package com.apptmyz.trackon.model;

import com.google.gson.annotations.SerializedName;

public class XbPcsDataModel {

	@SerializedName("awbNum")
	private String AWBnum;
	@SerializedName("xbAwbNum")
	private String XBAWBnum;
	private String refNo;
	@SerializedName("xbRefNo")
	private String XBrefNo;

	public String getAWBnum() {
		return AWBnum;
	}

	public void setAWBnum(String aWBnum) {
		AWBnum = aWBnum;
	}

	public String getXBAWBnum() {
		return XBAWBnum;
	}

	public void setXBAWBnum(String xBAWBnum) {
		XBAWBnum = xBAWBnum;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getXBrefNo() {
		return XBrefNo;
	}

	public void setXBrefNo(String xBrefNo) {
		XBrefNo = xBrefNo;
	}

}
