package com.apptmyz.trackon.model;

public class ErpDktValidationModel {

	// request model
	private String AWBNo;
	private String CustCode;
	private String UserId;
	private String UserType;
	private String BranchCode;
	private String DocType;
	private String DestPin;

	// response model
	private String ProductType;
	private String Msg;
	private Boolean Result;
	private String IssuedTo;
	private String IssuedType;
	private String XBHandover; // y/n
	private String XBOrigin;
	private String XBDestination;
	private String XBRouteCode;
	private String TrackonDestination;
	private String PincodeServiceable; // y/n
	private String ServiceMode; // (A-Air, S-Surface, B-Both)
	private String ServiceBy;
	private String XBPcsLimit;
	private String XBPerPcsWtLimit;
	private String XBPinAlertMessage;

	public String getAWBNo() {
		return AWBNo;
	}

	public void setAWBNo(String aWBNo) {
		AWBNo = aWBNo;
	}

	public String getCustCode() {
		return CustCode;
	}

	public void setCustCode(String custCode) {
		CustCode = custCode;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getUserType() {
		return UserType;
	}

	public void setUserType(String userType) {
		UserType = userType;
	}

	public String getBranchCode() {
		return BranchCode;
	}

	public void setBranchCode(String branchCode) {
		BranchCode = branchCode;
	}

	public String getDocType() {
		return DocType;
	}

	public void setDocType(String docType) {
		DocType = docType;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getMsg() {
		return Msg;
	}

	public void setMsg(String msg) {
		Msg = msg;
	}

	public Boolean getResult() {
		return Result;
	}

	public void setResult(Boolean result) {
		Result = result;
	}

	public String getIssuedTo() {
		return IssuedTo;
	}

	public void setIssuedTo(String issuedTo) {
		IssuedTo = issuedTo;
	}

	public String getIssuedType() {
		return IssuedType;
	}

	public void setIssuedType(String issuedType) {
		IssuedType = issuedType;
	}

	public String getDestPin() {
		return DestPin;
	}

	public void setDestPin(String destPin) {
		DestPin = destPin;
	}

	public String getXBHandover() {
		return XBHandover;
	}

	public void setXBHandover(String xBHandover) {
		XBHandover = xBHandover;
	}

	public String getXBOrigin() {
		return XBOrigin;
	}

	public void setXBOrigin(String xBOrigin) {
		XBOrigin = xBOrigin;
	}

	public String getXBDestination() {
		return XBDestination;
	}

	public void setXBDestination(String xBDestination) {
		XBDestination = xBDestination;
	}

	public String getXBRouteCode() {
		return XBRouteCode;
	}

	public void setXBRouteCode(String xBRouteCode) {
		XBRouteCode = xBRouteCode;
	}

	public String getTrackonDestination() {
		return TrackonDestination;
	}

	public void setTrackonDestination(String trackonDestination) {
		TrackonDestination = trackonDestination;
	}

	public String getPincodeServiceable() {
		return PincodeServiceable;
	}

	public void setPincodeServiceable(String pincodeServiceable) {
		PincodeServiceable = pincodeServiceable;
	}

	public String getServiceMode() {
		return ServiceMode;
	}

	public void setServiceMode(String serviceMode) {
		ServiceMode = serviceMode;
	}

	public String getServiceBy() {
		return ServiceBy;
	}

	public void setServiceBy(String serviceBy) {
		ServiceBy = serviceBy;
	}

	public String getXBPcsLimit() {
		return XBPcsLimit;
	}

	public void setXBPcsLimit(String xBPcsLimit) {
		XBPcsLimit = xBPcsLimit;
	}

	public String getXBPerPcsWtLimit() {
		return XBPerPcsWtLimit;
	}

	public void setXBPerPcsWtLimit(String xBPerPcsWtLimit) {
		XBPerPcsWtLimit = xBPerPcsWtLimit;
	}

	public String getXBPinAlertMessage() {
		return XBPinAlertMessage;
	}

	public void setXBPinAlertMessage(String xBPinAlertMessage) {
		XBPinAlertMessage = xBPinAlertMessage;
	}

}
