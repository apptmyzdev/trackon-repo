package com.apptmyz.trackon.model;

import java.util.List;

public class CustomerMasterByCustCode {
	String custCode;
	String custName;
	List<Consignor> consignors;
	
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public List<Consignor> getConsignors() {
		return consignors;
	}
	public void setConsignors(List<Consignor> consignors) {
		this.consignors = consignors;
	}
	
}
