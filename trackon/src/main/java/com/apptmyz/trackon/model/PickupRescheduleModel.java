package com.apptmyz.trackon.model;

public class PickupRescheduleModel {
	String userId;
	int pickupId;
	String pickupRegistrationId;
	String reason;
	String remarks;
	String reschDate;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getPickupId() {
		return pickupId;
	}
	public void setPickupId(int pickupId) {
		this.pickupId = pickupId;
	}
	public String getPickupRegistrationId() {
		return pickupRegistrationId;
	}
	public void setPickupRegistrationId(String pickupRegistrationId) {
		this.pickupRegistrationId = pickupRegistrationId;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getReschDate() {
		return reschDate;
	}
	public void setReschDate(String reschDate) {
		this.reschDate = reschDate;
	}
	
}
