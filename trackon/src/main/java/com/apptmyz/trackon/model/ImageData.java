package com.apptmyz.trackon.model;

import com.google.gson.annotations.SerializedName;

public class ImageData {
	
	@SerializedName(value="ImageName")
	String imageName;
	
	@SerializedName(value="ImageFlag")
	String imageFlag;
	
	

	public ImageData(String imageName, String imageFlag) {
		super();
		this.imageName = imageName;
		this.imageFlag = imageFlag;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageFlag() {
		return imageFlag;
	}

	public void setImageFlag(String imageFlag) {
		this.imageFlag = imageFlag;
	}
	
	
}
