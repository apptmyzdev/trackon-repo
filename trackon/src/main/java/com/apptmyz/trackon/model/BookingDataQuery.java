package com.apptmyz.trackon.model;

public class BookingDataQuery {
	String bookingDateFrom;
	String bookingDateTo;
	String custCode;
	String bookingBranch;
	String docketStatus;
	String showFailed;

	public String getBookingDateFrom() {
		return bookingDateFrom;
	}

	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}

	public String getBookingDateTo() {
		return bookingDateTo;
	}

	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getBookingBranch() {
		return bookingBranch;
	}

	public void setBookingBranch(String bookingBranch) {
		this.bookingBranch = bookingBranch;
	}

	public String getDocketStatus() {
		return docketStatus;
	}

	public void setDocketStatus(String docketStatus) {
		this.docketStatus = docketStatus;
	}

	public String getShowFailed() {
		return showFailed;
	}

	public void setShowFailed(String showFailed) {
		this.showFailed = showFailed;
	}

}
