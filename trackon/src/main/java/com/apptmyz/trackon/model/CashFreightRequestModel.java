package com.apptmyz.trackon.model;

public class CashFreightRequestModel {
	String originPin;
	String destinationPin;
	String doxNondox;
	String shipMode;
	Integer weight;
	String uom;
	public String getOriginPin() {
		return originPin;
	}
	public void setOriginPin(String originPin) {
		this.originPin = originPin;
	}
	public String getDestinationPin() {
		return destinationPin;
	}
	public void setDestinationPin(String destinationPin) {
		this.destinationPin = destinationPin;
	}
	public String getDoxNondox() {
		return doxNondox;
	}
	public void setDoxNondox(String doxNondox) {
		this.doxNondox = doxNondox;
	}
	public String getShipMode() {
		return shipMode;
	}
	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}

	
}
