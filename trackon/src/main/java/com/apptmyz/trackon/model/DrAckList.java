package com.apptmyz.trackon.model;

import java.util.List;

public class DrAckList {

	private List<DrAcknowledgeModel> drAcknowledgeInputList;
	

	public List<DrAcknowledgeModel> getDrAcknowledgeInputList() {
		return drAcknowledgeInputList;
	}

	public void setDrAcknowledgeInputList(List<DrAcknowledgeModel> drAcknowledgeInputList) {
		this.drAcknowledgeInputList = drAcknowledgeInputList;
	}


}
