package com.apptmyz.trackon.model;

import java.util.List;

public class BulkInterchangeSubmitModel {

	private List<BulkInterchangePickupModel> dkts;
	private String pickupSignature;
	private String version;

	public List<BulkInterchangePickupModel> getDkts() {
		return dkts;
	}

	public void setDkts(List<BulkInterchangePickupModel> dkts) {
		this.dkts = dkts;
	}

	public String getPickupSignature() {
		return pickupSignature;
	}

	public void setPickupSignature(String pickupSignature) {
		this.pickupSignature = pickupSignature;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
