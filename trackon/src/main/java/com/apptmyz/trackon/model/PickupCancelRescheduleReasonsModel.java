package com.apptmyz.trackon.model;

public class PickupCancelRescheduleReasonsModel {
	String code;
	String desc;
	String cancelRescheduleFlag;
	int sortOrder;
	int version;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getCancelRescheduleFlag() {
		return cancelRescheduleFlag;
	}
	public void setCancelRescheduleFlag(String cancelRescheduleFlag) {
		this.cancelRescheduleFlag = cancelRescheduleFlag;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
}
