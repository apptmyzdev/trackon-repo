package com.apptmyz.trackon.model;

public class DeviceRegistrationModel {

	private int appVersion;
	private String deviceAvailableMemory;
	private String deviceBattery;
	private String deviceBrand;
	private String deviceImei;
	private String deviceMac;
	private String deviceModel;
	private String deviceName;
	private String deviceOsVersion;
	private String deviceServiceProvider;
	private String deviceTotalMemory;
	private int gpsStatus;
	private String pushToken;
	private String userId;

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public String getDeviceAvailableMemory() {
		return deviceAvailableMemory;
	}

	public void setDeviceAvailableMemory(String deviceAvailableMemory) {
		this.deviceAvailableMemory = deviceAvailableMemory;
	}

	public String getDeviceBattery() {
		return deviceBattery;
	}

	public void setDeviceBattery(String deviceBattery) {
		this.deviceBattery = deviceBattery;
	}

	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getDeviceImei() {
		return deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getDeviceMac() {
		return deviceMac;
	}

	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceOsVersion() {
		return deviceOsVersion;
	}

	public void setDeviceOsVersion(String deviceOsVersion) {
		this.deviceOsVersion = deviceOsVersion;
	}

	public String getDeviceServiceProvider() {
		return deviceServiceProvider;
	}

	public void setDeviceServiceProvider(String deviceServiceProvider) {
		this.deviceServiceProvider = deviceServiceProvider;
	}

	public String getDeviceTotalMemory() {
		return deviceTotalMemory;
	}

	public void setDeviceTotalMemory(String deviceTotalMemory) {
		this.deviceTotalMemory = deviceTotalMemory;
	}

	public int getGpsStatus() {
		return gpsStatus;
	}

	public void setGpsStatus(int gpsStatus) {
		this.gpsStatus = gpsStatus;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
