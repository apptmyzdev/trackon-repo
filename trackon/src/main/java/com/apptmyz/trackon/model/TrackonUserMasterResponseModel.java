package com.apptmyz.trackon.model;

import java.util.List;

public class TrackonUserMasterResponseModel {

	List<TrackonUserMasterModel> userDetails;
	boolean result;
	String errMsg;
	public List<TrackonUserMasterModel> getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(List<TrackonUserMasterModel> userDetails) {
		this.userDetails = userDetails;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	
}
