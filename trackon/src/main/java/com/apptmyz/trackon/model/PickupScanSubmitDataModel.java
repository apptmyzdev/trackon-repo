package com.apptmyz.trackon.model;

import java.util.List;

public class PickupScanSubmitDataModel {

	String imei;
	List<PickupScanDataModel> scanData;
	String dispatchPersonName;
	String dispatchContactNo;
	String signature;
	String custCode;
	
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public List<PickupScanDataModel> getScanData() {
		return scanData;
	}
	public void setScanData(List<PickupScanDataModel> scanData) {
		this.scanData = scanData;
	}
	public String getDispatchPersonName() {
		return dispatchPersonName;
	}
	public void setDispatchPersonName(String dispatchPersonName) {
		this.dispatchPersonName = dispatchPersonName;
	}
	public String getDispatchContactNo() {
		return dispatchContactNo;
	}
	public void setDispatchContactNo(String dispatchContactNo) {
		this.dispatchContactNo = dispatchContactNo;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	@Override
	public String toString() {
		return "PickupScanSubmitDataModel [imei=" + imei + ", scanData=" + scanData + ", dispatchPersonName="
				+ dispatchPersonName + ", dispatchContactNo=" + dispatchContactNo + ", signature=" + signature
				+ ", custCode=" + custCode + "]";
	}
	
}
