package com.apptmyz.trackon.model;

public class BulkInterchangePickupErpModel {

	private String PickupOrderNo;
	private String PickupID;
	private String UserId;
	private String AWBNo;
	private String ScanDateTime;
	private String PickupRemarks;
	private String PickupStatus;
	private String PickupCancelReason;
	private String PickupSignature;

	public String getPickupOrderNo() {
		return PickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		PickupOrderNo = pickupOrderNo;
	}

	public String getPickupID() {
		return PickupID;
	}

	public void setPickupID(String pickupID) {
		PickupID = pickupID;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getAWBNo() {
		return AWBNo;
	}

	public void setAWBNo(String aWBNo) {
		AWBNo = aWBNo;
	}

	public String getScanDateTime() {
		return ScanDateTime;
	}

	public void setScanDateTime(String scanDateTime) {
		ScanDateTime = scanDateTime;
	}

	public String getPickupRemarks() {
		return PickupRemarks;
	}

	public void setPickupRemarks(String pickupRemarks) {
		PickupRemarks = pickupRemarks;
	}

	public String getPickupStatus() {
		return PickupStatus;
	}

	public void setPickupStatus(String pickupStatus) {
		PickupStatus = pickupStatus;
	}

	public String getPickupCancelReason() {
		return PickupCancelReason;
	}

	public void setPickupCancelReason(String pickupCancelReason) {
		PickupCancelReason = pickupCancelReason;
	}

	public String getPickupSignature() {
		return PickupSignature;
	}

	public void setPickupSignature(String pickupSignature) {
		PickupSignature = pickupSignature;
	}

}
