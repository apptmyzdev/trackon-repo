package com.apptmyz.trackon.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DRSResponseModel {
	Integer id;
	String agentId;
	String drsNo;
	String awbNo;
	int awbSerialNo;
	int noPkts;
	double weight;
	String remarks;
	String consigneeName;
	String consigneeAddress;
	String drsDate;
	String consigneeMobileNo;
	String city;
	int pincode;
	boolean isCod;
	boolean isFod;
	double codAmount;
	double fodAmount;
	String redirectedBy;
	Integer isOtpMandatory;
	String status;
	@JsonProperty("isBulkUpdateAllowed")
	boolean isBulkUpdateAllowed;
	private String branchCode;

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getDrsNo() {
		return drsNo;
	}

	public void setDrsNo(String drsNo) {
		this.drsNo = drsNo;
	}

	public String getAwbNo() {
		return awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public int getAwbSerialNo() {
		return awbSerialNo;
	}

	public void setAwbSerialNo(int awbSerialNo) {
		this.awbSerialNo = awbSerialNo;
	}

	public int getNoPkts() {
		return noPkts;
	}

	public void setNoPkts(int noPkts) {
		this.noPkts = noPkts;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneeAddress() {
		return consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}

	public String getDrsDate() {
		return drsDate;
	}

	public void setDrsDate(String drsDate) {
		this.drsDate = drsDate;
	}

	public String getConsigneeMobileNo() {
		return consigneeMobileNo;
	}

	public void setConsigneeMobileNo(String consigneeMobileNo) {
		this.consigneeMobileNo = consigneeMobileNo;
	}

	public boolean getIsCod() {
		return isCod;
	}

	public void setIsCod(boolean isCod) {
		this.isCod = isCod;
	}

	public boolean getIsFod() {
		return isFod;
	}

	public void setIsFod(boolean isFod) {
		this.isFod = isFod;
	}

	public double getCodAmount() {
		return codAmount;
	}

	public void setCodAmount(double codAmount) {
		this.codAmount = codAmount;
	}

	public double getFodAmount() {
		return fodAmount;
	}

	public void setFodAmount(double fodAmount) {
		this.fodAmount = fodAmount;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRedirectedBy() {
		return redirectedBy;
	}

	public void setRedirectedBy(String redirectedBy) {
		this.redirectedBy = redirectedBy;
	}

	public Integer getIsOtpMandatory() {
		return isOtpMandatory;
	}

	public void setIsOtpMandatory(Integer isOtpMandatory) {
		this.isOtpMandatory = isOtpMandatory;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isBulkUpdateAllowed() {
		return isBulkUpdateAllowed;
	}

	public void setBulkUpdateAllowed(boolean isBulkUpdateAllowed) {
		this.isBulkUpdateAllowed = isBulkUpdateAllowed;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

}
