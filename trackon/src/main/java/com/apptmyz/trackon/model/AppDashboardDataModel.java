package com.apptmyz.trackon.model;

public class AppDashboardDataModel {

	private int totalPickups;
	private int bookedPickups;
	private int pendingPickups;
	private int totalDeliveries;
	private int delivered;
	private int unddelivered;
	private int pendingDeliveries;

	public int getTotalPickups() {
		return totalPickups;
	}

	public void setTotalPickups(int totalPickups) {
		this.totalPickups = totalPickups;
	}

	public int getBookedPickups() {
		return bookedPickups;
	}

	public void setBookedPickups(int bookedPickups) {
		this.bookedPickups = bookedPickups;
	}

	public int getPendingPickups() {
		return pendingPickups;
	}

	public void setPendingPickups(int pendingPickups) {
		this.pendingPickups = pendingPickups;
	}

	public int getTotalDeliveries() {
		return totalDeliveries;
	}

	public void setTotalDeliveries(int totalDeliveries) {
		this.totalDeliveries = totalDeliveries;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public int getUnddelivered() {
		return unddelivered;
	}

	public void setUnddelivered(int unddelivered) {
		this.unddelivered = unddelivered;
	}

	public int getPendingDeliveries() {
		return pendingDeliveries;
	}

	public void setPendingDeliveries(int pendingDeliveries) {
		this.pendingDeliveries = pendingDeliveries;
	}

}
