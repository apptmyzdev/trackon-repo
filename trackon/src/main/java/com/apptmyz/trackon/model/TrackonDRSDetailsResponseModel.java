package com.apptmyz.trackon.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TrackonDRSDetailsResponseModel {
	
	@SerializedName(value="GetDRSOutputList")
	List<TrackonDRSDetailsModel> drsList;
	
	boolean result;
	String errMsg;
	public List<TrackonDRSDetailsModel> getDrsList() {
		return drsList;
	}
	public void setDrsList(List<TrackonDRSDetailsModel> drsList) {
		this.drsList = drsList;
	}
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	
	
}
