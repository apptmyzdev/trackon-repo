package com.apptmyz.trackon.model;

import java.util.List;

public class BulkInterchangeSubmitList {

	List<BulkInterchangePickupErpModel> PickupSubmitList;
	private String appVersion;

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public List<BulkInterchangePickupErpModel> getPickupSubmitList() {
		return PickupSubmitList;
	}

	public void setPickupSubmitList(List<BulkInterchangePickupErpModel> pickupSubmitList) {
		PickupSubmitList = pickupSubmitList;
	}

}
