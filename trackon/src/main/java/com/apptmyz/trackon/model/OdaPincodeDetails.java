package com.apptmyz.trackon.model;

import java.util.List;

public class OdaPincodeDetails {
	Integer pincode;
	List<String> area;
	
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	public List<String> getArea() {
		return area;
	}
	public void setArea(List<String> area) {
		this.area = area;
	}
}
