package com.apptmyz.trackon.model;

public class PickupAssignedDetailsModel {
	int pickupId;
	String pickupRegistrationId;
	String pickupDate;
	public int getPickupId() {
		return pickupId;
	}
	public void setPickupId(int pickupId) {
		this.pickupId = pickupId;
	}
	public String getPickupRegistrationId() {
		return pickupRegistrationId;
	}
	public void setPickupRegistrationId(String pickupRegistrationId) {
		this.pickupRegistrationId = pickupRegistrationId;
	}
	public String getPickupDate() {
		return pickupDate;
	}
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	
	
}
