package com.apptmyz.trackon.model;

import java.util.List;

public class PickupRegnSoftDataModel {

	String startPktNo;
	String endPktNo;
	String material;
	
	Double codAmount;
	
	Double consignmentValue;
	boolean isAppointmentDelivery;
	String consignorGstin;
	String consineeGstin;
	

	List<LbhData> lbhData;
	List<PickupInvoiceEwb> invoiceDetails;
	List<PaperWork> paperWork;
	
	String customerRefNo;
	List<CCPktModel> pktData;
	String remarks;
	
	public String getStartPktNo() {
		return startPktNo;
	}
	public void setStartPktNo(String startPktNo) {
		this.startPktNo = startPktNo;
	}
	public String getEndPktNo() {
		return endPktNo;
	}
	public void setEndPktNo(String endPktNo) {
		this.endPktNo = endPktNo;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public Double getCodAmount() {
		return codAmount;
	}
	public void setCodAmount(Double codAmount) {
		this.codAmount = codAmount;
	}
	public Double getConsignmentValue() {
		return consignmentValue;
	}
	public void setConsignmentValue(Double consignmentValue) {
		this.consignmentValue = consignmentValue;
	}
	public boolean isAppointmentDelivery() {
		return isAppointmentDelivery;
	}
	public void setAppointmentDelivery(boolean isAppointmentDelivery) {
		this.isAppointmentDelivery = isAppointmentDelivery;
	}
	public List<LbhData> getLbhData() {
		return lbhData;
	}
	public void setLbhData(List<LbhData> lbhData) {
		this.lbhData = lbhData;
	}
	public List<PickupInvoiceEwb> getInvoiceDetails() {
		return invoiceDetails;
	}
	public void setInvoiceDetails(List<PickupInvoiceEwb> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}
	public List<PaperWork> getPaperWork() {
		return paperWork;
	}
	public void setPaperWork(List<PaperWork> paperWork) {
		this.paperWork = paperWork;
	}
	public String getCustomerRefNo() {
		return customerRefNo;
	}
	public void setCustomerRefNo(String customerRefNo) {
		this.customerRefNo = customerRefNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getConsignorGstin() {
		return consignorGstin;
	}
	public void setConsignorGstin(String consignorGstin) {
		this.consignorGstin = consignorGstin;
	}
	public String getConsineeGstin() {
		return consineeGstin;
	}
	public void setConsineeGstin(String consineeGstin) {
		this.consineeGstin = consineeGstin;
	}
	public List<CCPktModel> getPktData() {
		return pktData;
	}
	public void setPktData(List<CCPktModel> pktData) {
		this.pktData = pktData;
	}
	
}
