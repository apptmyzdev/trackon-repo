package com.apptmyz.trackon.model;

public class DktValidationModel {

	private String dktNo;
	private String custCode;
	private String userId;
	private String userType;
	private String branchCode;
	private String docType;
	private String productType;
	private String msg;
	private Boolean result;
	private String issuedTo;
	private String issuedType;
	private Integer destPin;

	public String getDktNo() {
		return dktNo;
	}

	public void setDktNo(String dktNo) {
		this.dktNo = dktNo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getIssuedTo() {
		return issuedTo;
	}

	public void setIssuedTo(String issuedTo) {
		this.issuedTo = issuedTo;
	}

	public String getIssuedType() {
		return issuedType;
	}

	public void setIssuedType(String issuedType) {
		this.issuedType = issuedType;
	}

	public Integer getDestPin() {
		return destPin;
	}

	public void setDestPin(Integer destPin) {
		this.destPin = destPin;
	}

}
