package com.apptmyz.trackon.model;

public class DocketMasterModel {
	String dktNo;
	Integer dktType;
	public String getDktNo() {
		return dktNo;
	}
	public void setDktNo(String dktNo) {
		this.dktNo = dktNo;
	}
	public Integer getDktType() {
		return dktType;
	}
	public void setDktType(Integer dktType) {
		this.dktType = dktType;
	}
	
}
