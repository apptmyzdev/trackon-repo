package com.apptmyz.trackon.model;

import java.util.List;

public class PickupAssignmentModel {
	List<PickupAssignedDetailsModel>	pickupdetails;
	String assignedToType;
	String assignedto;
	String userId;
	String reasonCode;
	int isRedirected;
	String remarks;
	
	public String getAssignedToType() {
		return assignedToType;
	}
	public void setAssignedToType(String assignedToType) {
		this.assignedToType = assignedToType;
	}
	public String getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<PickupAssignedDetailsModel> getPickupdetails() {
		return pickupdetails;
	}
	public void setPickupdetails(List<PickupAssignedDetailsModel> pickupdetails) {
		this.pickupdetails = pickupdetails;
	}
	public String getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	public int getIsRedirected() {
		return isRedirected;
	}
	public void setIsRedirected(int isRedirected) {
		this.isRedirected = isRedirected;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
