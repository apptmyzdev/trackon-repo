package com.apptmyz.trackon.model;

public class PickupRegistrationDataQuery {
	String pickupDateFrom;
	String pickupDateTo;
	String custCode;
	String pickupPincode;
	String pickupStatus;
	
	public String getPickupDateFrom() {
		return pickupDateFrom;
	}
	public void setPickupDateFrom(String pickupDateFrom) {
		this.pickupDateFrom = pickupDateFrom;
	}
	public String getPickupDateTo() {
		return pickupDateTo;
	}
	public void setPickupDateTo(String pickupDateTo) {
		this.pickupDateTo = pickupDateTo;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getPickupPincode() {
		return pickupPincode;
	}
	public void setPickupPincode(String pickupPincode) {
		this.pickupPincode = pickupPincode;
	}
	public String getPikcupStatus() {
		return pickupStatus;
	}
	public void setPikcupStatus(String pikcupStatus) {
		this.pickupStatus = pikcupStatus;
	}
	public String getPickupStatus() {
		return pickupStatus;
	}
	public void setPickupStatus(String pickupStatus) {
		this.pickupStatus = pickupStatus;
	}
	
	
}
