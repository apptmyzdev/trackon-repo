package com.apptmyz.trackon.model;

import java.util.List;

public class PickupRegistrationSubmit {
	String userId;
	String userType;
	String customerName;
	Consignor consignor;
	Consignee consignee;
	String custCode;
	Integer customerId;
	Integer originPincode;
	String shipMode;
	String doxNonDox;
	String svcType;
	Integer deliveryPincode;
	Integer pktCount;
	Double wt;
	String uom;
	String pickupDate;
	String pickupSlot;
	List<Integer> customRecurFrequency;
	String erpCustomRecurFrequency;
	boolean isRecurring;
	String recurringStartDate;
	String recurringEndDate;
	String recurringFrequency;
	boolean odaFlag;
	String odaArea;
	String billingFlag;
	String docketNo;
	String customerRefNo;

	PickupRegnSoftDataModel pickupSoftData;
	PickupInsuranceDetails insuranceDetails;

	private String PickupOrderNo;
	private String PickupID;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Consignor getConsignor() {
		return consignor;
	}

	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getOriginPincode() {
		return originPincode;
	}

	public void setOriginPincode(Integer originPincode) {
		this.originPincode = originPincode;
	}

	public String getShipMode() {
		return shipMode;
	}

	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}

	public String getDoxNonDox() {
		return doxNonDox;
	}

	public void setDoxNonDox(String doxNonDox) {
		this.doxNonDox = doxNonDox;
	}

	public String getSvcType() {
		return svcType;
	}

	public void setSvcType(String svcType) {
		this.svcType = svcType;
	}

	public Integer getDeliveryPincode() {
		return deliveryPincode;
	}

	public void setDeliveryPincode(Integer deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}

	public Integer getPktCount() {
		return pktCount;
	}

	public void setPktCount(Integer pktCount) {
		this.pktCount = pktCount;
	}

	public Double getWt() {
		return wt;
	}

	public void setWt(Double wt) {
		this.wt = wt;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupSlot() {
		return pickupSlot;
	}

	public void setPickupSlot(String pickupSlot) {
		this.pickupSlot = pickupSlot;
	}

	public boolean getIsRecurring() {
		return isRecurring;
	}

	public void setIsRecurring(boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public String getRecurringStartDate() {
		return recurringStartDate;
	}

	public void setRecurringStartDate(String recurringStartDate) {
		this.recurringStartDate = recurringStartDate;
	}

	public String getRecurringEndDate() {
		return recurringEndDate;
	}

	public void setRecurringEndDate(String recurringEndDate) {
		this.recurringEndDate = recurringEndDate;
	}

	public String getRecurringFrequency() {
		return recurringFrequency;
	}

	public void setRecurringFrequency(String recurringFrequency) {
		this.recurringFrequency = recurringFrequency;
	}

	public boolean isOdaFlag() {
		return odaFlag;
	}

	public void setOdaFlag(boolean odaFlag) {
		this.odaFlag = odaFlag;
	}

	public String getOdaArea() {
		return odaArea;
	}

	public void setOdaArea(String odaArea) {
		this.odaArea = odaArea;
	}

	public String getBillingFlag() {
		return billingFlag;
	}

	public void setBillingFlag(String billingFlag) {
		this.billingFlag = billingFlag;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<Integer> getCustomRecurFrequency() {
		return customRecurFrequency;
	}

	public void setCustomRecurFrequency(List<Integer> customRecurFrequency) {
		this.customRecurFrequency = customRecurFrequency;
	}

	public String getCustomerRefNo() {
		return customerRefNo;
	}

	public void setCustomerRefNo(String customerRefNo) {
		this.customerRefNo = customerRefNo;
	}

	public PickupRegnSoftDataModel getPickupSoftData() {
		return pickupSoftData;
	}

	public void setPickupSoftData(PickupRegnSoftDataModel pickupSoftData) {
		this.pickupSoftData = pickupSoftData;
	}

	public PickupInsuranceDetails getInsuranceDetails() {
		return insuranceDetails;
	}

	public void setInsuranceDetails(PickupInsuranceDetails insuranceDetails) {
		this.insuranceDetails = insuranceDetails;
	}

	public void setRecurring(boolean isRecurring) {
		this.isRecurring = isRecurring;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPickupOrderNo() {
		return PickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		PickupOrderNo = pickupOrderNo;
	}

	public String getPickupID() {
		return PickupID;
	}

	public void setPickupID(String pickupID) {
		PickupID = pickupID;
	}

	public String getErpCustomRecurFrequency() {
		return erpCustomRecurFrequency;
	}

	public void setErpCustomRecurFrequency(String erpCustomRecurFrequency) {
		this.erpCustomRecurFrequency = erpCustomRecurFrequency;
	}

}
