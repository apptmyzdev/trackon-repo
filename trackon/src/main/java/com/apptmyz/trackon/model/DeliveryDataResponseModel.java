package com.apptmyz.trackon.model;

import java.sql.Date;

public class DeliveryDataResponseModel {
	private String drsNo;
	private String dktNo;
	private String drsDate;
	private Integer pktCount;
	private String consigneeName;
	private String status;
	private String img;
	private String erpMessage;
	private String deliveredBy;
	private String depsImg;
	public String getDrsNo() {
		return drsNo;
	}
	public void setDrsNo(String drsNo) {
		this.drsNo = drsNo;
	}
	public String getDktNo() {
		return dktNo;
	}
	public void setDktNo(String dktNo) {
		this.dktNo = dktNo;
	}
	public String getDrsDate() {
		return drsDate;
	}
	public void setDrsDate(String drsDate) {
		this.drsDate = drsDate;
	}
	public Integer getPktCount() {
		return pktCount;
	}
	public void setPktCount(Integer pktCount) {
		this.pktCount = pktCount;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getErpMessage() {
		return erpMessage;
	}
	public void setErpMessage(String erpMessage) {
		this.erpMessage = erpMessage;
	}
	public String getDepsImg() {
		return depsImg;
	}
	public void setDepsImg(String depsImg) {
		this.depsImg = depsImg;
	}
	public String getDeliveredBy() {
		return deliveredBy;
	}
	public void setDeliveredBy(String deliveredBy) {
		this.deliveredBy = deliveredBy;
	}
	
	
}
