package com.apptmyz.trackon.model;

public class ErpDrAcknowledgeModel {

	private String DR_Number;
	private String AWBNo;
	private String UserID;
	private boolean IsAcknowledged;
	private boolean IsShort;
	private boolean IsExtra;
	private String AcknowledgeDateTime;
	private String Remarks;

	public String getDR_Number() {
		return DR_Number;
	}

	public void setDR_Number(String dR_Number) {
		DR_Number = dR_Number;
	}

	public String getAWBNo() {
		return AWBNo;
	}

	public void setAWBNo(String aWBNo) {
		AWBNo = aWBNo;
	}

	public String getUserID() {
		return UserID;
	}

	public void setUserID(String userID) {
		UserID = userID;
	}

	public boolean isIsAcknowledged() {
		return IsAcknowledged;
	}

	public void setIsAcknowledged(boolean isAcknowledged) {
		IsAcknowledged = isAcknowledged;
	}

	public boolean isIsShort() {
		return IsShort;
	}

	public void setIsShort(boolean isShort) {
		IsShort = isShort;
	}

	public boolean isIsExtra() {
		return IsExtra;
	}

	public void setIsExtra(boolean isExtra) {
		IsExtra = isExtra;
	}

	public String getAcknowledgeDateTime() {
		return AcknowledgeDateTime;
	}

	public void setAcknowledgeDateTime(String acknowledgeDateTime) {
		AcknowledgeDateTime = acknowledgeDateTime;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
	
	
}
