package com.apptmyz.trackon.model;

public class PacketMasterModel {
	Long startPktNo;
	Long endPktNo;
	Long lastPktUsed;
	Integer pktType;
	public Long getStartPktNo() {
		return startPktNo;
	}
	public void setStartPktNo(Long startPktNo) {
		this.startPktNo = startPktNo;
	}
	public Long getEndPktNo() {
		return endPktNo;
	}
	public void setEndPktNo(Long endPktNo) {
		this.endPktNo = endPktNo;
	}
	public Long getLastPktUsed() {
		return lastPktUsed;
	}
	public void setLastPktUsed(Long lastPktUsed) {
		this.lastPktUsed = lastPktUsed;
	}
	public Integer getPktType() {
		return pktType;
	}
	public void setPktType(Integer pktType) {
		this.pktType = pktType;
	}
	
}
