package com.apptmyz.trackon.model;

import java.util.List;

public class PickupScanDataModel {
	String docketNo;
	String pktNo;
	String customerPktNo;
	int serialNo;
	int manualScanFlag;
	boolean	  hasDeps;
	String depsReason;
	List<String> depsImages;
	String scanTime;
	
	public String getDocketNo() {
		return docketNo;
	}
	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}
	public String getPktNo() {
		return pktNo;
	}
	public void setPktNo(String pktNo) {
		this.pktNo = pktNo;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public int getManualScanFlag() {
		return manualScanFlag;
	}
	public void setManualScanFlag(int manualScanFlag) {
		this.manualScanFlag = manualScanFlag;
	}
	public String getCustomerPktNo() {
		return customerPktNo;
	}
	public void setCustomerPktNo(String refNo) {
		this.customerPktNo = refNo;
	}
	public boolean getHasDeps() {
		return hasDeps;
	}
	public void setHasDeps(boolean hasDeps) {
		this.hasDeps = hasDeps;
	}
	public String getDepsReason() {
		return depsReason;
	}
	public void setDepsReason(String depsReason) {
		this.depsReason = depsReason;
	}
	public List<String> getDepsImages() {
		return depsImages;
	}
	public void setDepsImages(List<String> depsImages) {
		this.depsImages = depsImages;
	}
	public String getScanTime() {
		return scanTime;
	}
	public void setScanTime(String scanTime) {
		this.scanTime = scanTime;
	}
	@Override
	public String toString() {
		return "PickupScanDataModel [docketNo=" + docketNo + ", pktNo=" + pktNo + ", refNo=" + customerPktNo + ", serialNo="
				+ serialNo + ", manualScanFlag=" + manualScanFlag + ", hasDeps=" + hasDeps + ", depsReason="
				+ depsReason + ", depsImages=" + depsImages + ", scanTime=" + scanTime + "]";
	}
	
	
}
