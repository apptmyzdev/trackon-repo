package com.apptmyz.trackon.model;

import java.math.BigDecimal;
import java.util.Date;

public class DeliveriesData {
	String docketNo;
	String deliveryBranch;
	String deliveryDate;
	String deliveredTo;
	String badDellivery;
	String remarks;
	BigDecimal amountCollected;
	String userId;
	String drsNo;
	String relationShip;
	String undeliveryReason;
	String status;

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getDeliveryBranch() {
		return deliveryBranch;
	}

	public void setDeliveryBranch(String deliveryBranch) {
		this.deliveryBranch = deliveryBranch;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveredTo() {
		return deliveredTo;
	}

	public void setDeliveredTo(String deliveredTo) {
		this.deliveredTo = deliveredTo;
	}

	public String isBadDellivery() {
		return badDellivery;
	}

	public void setBadDellivery(String badDellivery) {
		this.badDellivery = badDellivery;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public BigDecimal getAmountCollected() {
		return amountCollected;
	}

	public void setAmountCollected(BigDecimal amountCollected) {
		this.amountCollected = amountCollected;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDrsNo() {
		return drsNo;
	}

	public void setDrsNo(String drsNo) {
		this.drsNo = drsNo;
	}

	public String getRelationShip() {
		return relationShip;
	}

	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}

	public String getUndeliveryReason() {
		return undeliveryReason;
	}

	public void setUndeliveryReason(String undeliveryReason) {
		this.undeliveryReason = undeliveryReason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBadDellivery() {
		return badDellivery;
	}

}
