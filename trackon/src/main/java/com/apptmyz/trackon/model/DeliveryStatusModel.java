package com.apptmyz.trackon.model;

import java.util.List;


public class DeliveryStatusModel {
	List<DRSResponseModel> delivered;
	List<DRSResponseModel> unDelivered;
	List<DRSResponseModel> pending;
	public List<DRSResponseModel> getDelivered() {
		return delivered;
	}
	public void setDelivered(List<DRSResponseModel> delivered) {
		this.delivered = delivered;
	}
	public List<DRSResponseModel> getUnDelivered() {
		return unDelivered;
	}
	public void setUnDelivered(List<DRSResponseModel> unDelivered) {
		this.unDelivered = unDelivered;
	}
	public List<DRSResponseModel> getPending() {
		return pending;
	}
	public void setPending(List<DRSResponseModel> pending) {
		this.pending = pending;
	}
	
}
