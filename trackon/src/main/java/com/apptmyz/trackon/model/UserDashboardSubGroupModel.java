package com.apptmyz.trackon.model;

public class UserDashboardSubGroupModel {

	private String name;
	private long totalPickups;
	private long pendingPickups;
	private long attemptedPickups;
	private long bookedPickups;
	private long directBookedPickups;
	private long totalDeliveries;
	private long pendingDeliveries;
	private long deliveries;
	private long undeliveries;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTotalPickups() {
		return totalPickups;
	}

	public void setTotalPickups(long totalPickups) {
		this.totalPickups = totalPickups;
	}

	public long getPendingPickups() {
		return pendingPickups;
	}

	public void setPendingPickups(long pendingPickups) {
		this.pendingPickups = pendingPickups;
	}

	public long getBookedPickups() {
		return bookedPickups;
	}

	public void setBookedPickups(long bookedPickups) {
		this.bookedPickups = bookedPickups;
	}

	public long getDirectBookedPickups() {
		return directBookedPickups;
	}

	public void setDirectBookedPickups(long directBookedPickups) {
		this.directBookedPickups = directBookedPickups;
	}

	public long getTotalDeliveries() {
		return totalDeliveries;
	}

	public void setTotalDeliveries(long totalDeliveries) {
		this.totalDeliveries = totalDeliveries;
	}

	public long getPendingDeliveries() {
		return pendingDeliveries;
	}

	public void setPendingDeliveries(long pendingDeliveries) {
		this.pendingDeliveries = pendingDeliveries;
	}

	public long getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(long deliveries) {
		this.deliveries = deliveries;
	}

	public long getUndeliveries() {
		return undeliveries;
	}

	public void setUndeliveries(long undeliveries) {
		this.undeliveries = undeliveries;
	}

	public long getAttemptedPickups() {
		return attemptedPickups;
	}

	public void setAttemptedPickups(long attemptedPickups) {
		this.attemptedPickups = attemptedPickups;
	}

}
