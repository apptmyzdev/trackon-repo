package com.apptmyz.trackon.model;

import java.util.List;

public class UndlyPostModel {
	String userId;
	String branchCode;
	String pdcNo;
	String docketNo;
	String reason;
	String remarks;
	Double latitude;
	Double longitude;
	String undlyTime;
	String imei;
	Integer transactionId;
	private String appVersion;

	List<String> images;
	private List<ImageData> erpImagesList;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getPdcNo() {
		return pdcNo;
	}

	public void setPdcNo(String pdcNo) {
		this.pdcNo = pdcNo;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getUndlyTime() {
		return undlyTime;
	}

	public void setUndlyTime(String undlyTime) {
		this.undlyTime = undlyTime;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public List<ImageData> getErpImagesList() {
		return erpImagesList;
	}

	public void setErpImagesList(List<ImageData> erpImagesList) {
		this.erpImagesList = erpImagesList;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

}
