package com.apptmyz.trackon.model;

import java.util.List;

public class DlyUndlyPostModel {

	private List<DeliveryPostModel> dlydata;
	private List<UndlyPostModel> undlydata;

	public List<DeliveryPostModel> getDlydata() {
		return dlydata;
	}

	public void setDlydata(List<DeliveryPostModel> dlydata) {
		this.dlydata = dlydata;
	}

	public List<UndlyPostModel> getUndlydata() {
		return undlydata;
	}

	public void setUndlydata(List<UndlyPostModel> undlydata) {
		this.undlydata = undlydata;
	}

}
