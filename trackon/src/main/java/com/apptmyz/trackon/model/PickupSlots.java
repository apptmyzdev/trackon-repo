package com.apptmyz.trackon.model;

public class PickupSlots {
	String pickupSlotValue;
	String pickupSlotDisplay;
	Integer version;
	
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getPickupSlotValue() {
		return pickupSlotValue;
	}
	public void setPickupSlotValue(String pickupSlotValue) {
		this.pickupSlotValue = pickupSlotValue;
	}
	public String getPickupSlotDisplay() {
		return pickupSlotDisplay;
	}
	public void setPickupSlotDisplay(String pickupSlotDisplay) {
		this.pickupSlotDisplay = pickupSlotDisplay;
	}
	
	
}
