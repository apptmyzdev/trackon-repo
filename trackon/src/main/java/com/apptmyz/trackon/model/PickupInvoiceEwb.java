package com.apptmyz.trackon.model;

public class PickupInvoiceEwb {

	String invoiceNo;
	String invoiceDate;
	Double invoiceAmount;
	String ewbNo;
	String ewbValidTill;
	String ewbDate;
	String vehicleNo;
	// String base64InvoiceImage;
	String base64Data;
	String InvoiceImageURL;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getEwbNo() {
		return ewbNo;
	}

	public void setEwbNo(String ewbNo) {
		this.ewbNo = ewbNo;
	}

	public String getEwbValidTill() {
		return ewbValidTill;
	}

	public void setEwbValidTill(String ewbValidTill) {
		this.ewbValidTill = ewbValidTill;
	}

	public String getEwbDate() {
		return ewbDate;
	}

	public void setEwbDate(String ewbDate) {
		this.ewbDate = ewbDate;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	// public String getBase64InvoiceImage() {
	// return base64InvoiceImage;
	// }
	//
	// public void setBase64InvoiceImage(String base64InvoiceImage) {
	// this.base64InvoiceImage = base64InvoiceImage;
	// }

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public String getBase64Data() {
		return base64Data;
	}

	public void setBase64Data(String base64Data) {
		this.base64Data = base64Data;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getInvoiceImageURL() {
		return InvoiceImageURL;
	}

	public void setInvoiceImageURL(String invoiceImageURL) {
		InvoiceImageURL = invoiceImageURL;
	}

}
