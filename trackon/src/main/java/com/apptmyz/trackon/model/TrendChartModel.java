package com.apptmyz.trackon.model;

import java.util.Collection;
import java.util.List;

public class TrendChartModel {

	private long pointStart;
	private long pointInterval;
	private Collection<Integer> directBookings;
	private Collection<Integer> bookings;
	private Collection<Integer> deliveries;
	private Collection<Integer> undeliveries;

	public long getPointStart() {
		return pointStart;
	}

	public void setPointStart(long pointStart) {
		this.pointStart = pointStart;
	}

	public long getPointInterval() {
		return pointInterval;
	}

	public void setPointInterval(long pointInterval) {
		this.pointInterval = pointInterval;
	}

	public Collection<Integer> getDirectBookings() {
		return directBookings;
	}

	public void setDirectBookings(Collection<Integer> directBookings) {
		this.directBookings = directBookings;
	}

	public Collection<Integer> getBookings() {
		return bookings;
	}

	public void setBookings(Collection<Integer> bookings) {
		this.bookings = bookings;
	}

	public Collection<Integer> getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(Collection<Integer> deliveries) {
		this.deliveries = deliveries;
	}

	public Collection<Integer> getUndeliveries() {
		return undeliveries;
	}

	public void setUndeliveries(Collection<Integer> undeliveries) {
		this.undeliveries = undeliveries;
	}

}
