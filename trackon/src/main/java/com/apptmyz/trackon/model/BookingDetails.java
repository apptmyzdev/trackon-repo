package com.apptmyz.trackon.model;

import java.util.List;
import java.util.Set;

public class BookingDetails {
	private String docketNo;
	Consignee consignee;
	Consignor consignor;
	private Set<String> packList;
	private Integer pktCount;
	private String customerRefNo;
	private Double weight;
	private Double invoiceValue;
	private String shipmentMode;
	private List<XbPcsReprintModel> pcsList;
	private String xbOrigin;
	private String xbDest;
	private String xbAwbNo;

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

	public Consignor getConsignor() {
		return consignor;
	}

	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;
	}

	public Set<String> getPackList() {
		return packList;
	}

	public void setPackList(Set<String> packList) {
		this.packList = packList;
	}

	public Integer getPktCount() {
		return pktCount;
	}

	public void setPktCount(Integer pktCount) {
		this.pktCount = pktCount;
	}

	public String getCustomerRefNo() {
		return customerRefNo;
	}

	public void setCustomerRefNo(String customerRefNo) {
		this.customerRefNo = customerRefNo;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(Double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public String getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(String shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public List<XbPcsReprintModel> getPcsList() {
		return pcsList;
	}

	public void setPcsList(List<XbPcsReprintModel> pcsList) {
		this.pcsList = pcsList;
	}

	public String getXbOrigin() {
		return xbOrigin;
	}

	public void setXbOrigin(String xbOrigin) {
		this.xbOrigin = xbOrigin;
	}

	public String getXbDest() {
		return xbDest;
	}

	public void setXbDest(String xbDest) {
		this.xbDest = xbDest;
	}

	public String getXbAwbNo() {
		return xbAwbNo;
	}

	public void setXbAwbNo(String xbAwbNo) {
		this.xbAwbNo = xbAwbNo;
	}

}
