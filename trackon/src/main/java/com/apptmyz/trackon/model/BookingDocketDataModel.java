package com.apptmyz.trackon.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class BookingDocketDataModel {

	private Integer custId;
	private Integer consignorId;
	private Integer consigneeId;
	private String docketNo;
	private String custCode;
	private Consignee consignee;
	private Consignor consignor;
	private String destinationBranch;
	private String pickupTimestamp;
	private String shipMode;
	private String doxFlag;
	private String serviceType;
	private Double actualWt;
	private Double chargedWt;
	private String uom;
	private Integer pktCount;
	private String startPktNo;
	private String endPktNo;
	private String material;
	private Double codAmount;
	private Double consignmentValue;
	private boolean isAppointmentDelivery;
	private Integer deliveryPincode;
	private String vehicleNo;
	private List<LbhData> lbhData;
	private List<PickupInvoiceEwb> invoiceDetails;
	private List<PaperWork> paperWork;
	private String paymentMode;
	private String paymentType;
	private boolean manualPktSeries;
	private List<ChargesModel> charges;
	private Double totalCharges;
	private List<PickupScanDataModel> pickupScanData;
	private String customerRefNo;
	private String remarks;
	@SerializedName("xbAwbNo")
	private String XBAwbNo;
	@SerializedName("pcsList")
	private List<XbPcsDataModel> PcsList;
	private String xbOrigin;
	private String xbDest;
	private String riskType;

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public Integer getConsignorId() {
		return consignorId;
	}

	public void setConsignorId(Integer consignorId) {
		this.consignorId = consignorId;
	}

	public Integer getConsigneeId() {
		return consigneeId;
	}

	public void setConsigneeId(Integer consigneeId) {
		this.consigneeId = consigneeId;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

	public Consignor getConsignor() {
		return consignor;
	}

	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;
	}

	public String getDestinationBranch() {
		return destinationBranch;
	}

	public void setDestinationBranch(String destinationBranch) {
		this.destinationBranch = destinationBranch;
	}

	public String getPickupTimestamp() {
		return pickupTimestamp;
	}

	public void setPickupTimestamp(String pickupTimestamp) {
		this.pickupTimestamp = pickupTimestamp;
	}

	public String getShipMode() {
		return shipMode;
	}

	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}

	public String getDoxFlag() {
		return doxFlag;
	}

	public void setDoxFlag(String doxFlag) {
		this.doxFlag = doxFlag;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Double getActualWt() {
		return actualWt;
	}

	public void setActualWt(Double actualWt) {
		this.actualWt = actualWt;
	}

	public Double getChargedWt() {
		return chargedWt;
	}

	public void setChargedWt(Double chargedWt) {
		this.chargedWt = chargedWt;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Integer getPktCount() {
		return pktCount;
	}

	public void setPktCount(Integer pktCount) {
		this.pktCount = pktCount;
	}

	public String getStartPktNo() {
		return startPktNo;
	}

	public void setStartPktNo(String startPktNo) {
		this.startPktNo = startPktNo;
	}

	public String getEndPktNo() {
		return endPktNo;
	}

	public void setEndPktNo(String endPktNo) {
		this.endPktNo = endPktNo;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public Double getCodAmount() {
		return codAmount;
	}

	public void setCodAmount(Double codAmount) {
		this.codAmount = codAmount;
	}

	public Double getConsignmentValue() {
		return consignmentValue;
	}

	public void setConsignmentValue(Double consignmentValue) {
		this.consignmentValue = consignmentValue;
	}

	public boolean getIsAppointmentDelivery() {
		return isAppointmentDelivery;
	}

	public void setIsAppointmentDelivery(boolean isAppointmentDelivery) {
		this.isAppointmentDelivery = isAppointmentDelivery;
	}

	public Integer getDeliveryPincode() {
		return deliveryPincode;
	}

	public void setDeliveryPincode(Integer deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}

	public List<LbhData> getLbhData() {
		return lbhData;
	}

	public void setLbhData(List<LbhData> lbhData) {
		this.lbhData = lbhData;
	}

	public List<PickupInvoiceEwb> getInvoiceDetails() {
		return invoiceDetails;
	}

	public void setInvoiceDetails(List<PickupInvoiceEwb> invoiceDetails) {
		this.invoiceDetails = invoiceDetails;
	}

	public List<PaperWork> getPaperWork() {
		return paperWork;
	}

	public void setPaperWork(List<PaperWork> paperWork) {
		this.paperWork = paperWork;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCustomerRefNo() {
		return customerRefNo;
	}

	public void setCustomerRefNo(String customerRefNo) {
		this.customerRefNo = customerRefNo;
	}

	public Double getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(Double totalCharges) {
		this.totalCharges = totalCharges;
	}

	public List<ChargesModel> getCharges() {
		return charges;
	}

	public void setCharges(List<ChargesModel> charges) {
		this.charges = charges;
	}

	public List<PickupScanDataModel> getPickupScanData() {
		return pickupScanData;
	}

	public void setPickupScanData(List<PickupScanDataModel> pickupScanData) {
		this.pickupScanData = pickupScanData;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public boolean getManualPktSeries() {
		return manualPktSeries;
	}

	public void setManualPktSeries(boolean manualPktSeries) {
		this.manualPktSeries = manualPktSeries;
	}

	public String getXBAwbNo() {
		return XBAwbNo;
	}

	public void setXBAwbNo(String xBAwbNo) {
		XBAwbNo = xBAwbNo;
	}

	public List<XbPcsDataModel> getPcsList() {
		return PcsList;
	}

	public void setPcsList(List<XbPcsDataModel> pcsList) {
		PcsList = pcsList;
	}

	public String getXbOrigin() {
		return xbOrigin;
	}

	public void setXbOrigin(String xbOrigin) {
		this.xbOrigin = xbOrigin;
	}

	public String getXbDest() {
		return xbDest;
	}

	public void setXbDest(String xbDest) {
		this.xbDest = xbDest;
	}

	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

}
