package com.apptmyz.trackon.model;
//package com.apptmyz.vmiles.model;
//
//import java.util.List;
//
//public class DeliveryPdcModel {
//
//	private int deliverySheetId;
//	private String deliverySheetNumber;
//	private String deliverySheetDate;
//	private String vaName;
//	private int vaId;
//	private String userId;
//	private String branchCode;
//	private int docketsCount;
//	private String truckNumber;
//	private int deliveryBranchId;
//	private String deliveryBranchName;
//	private List<DeliveryDocketsModel> dockets;
//
//	public int getDeliverySheetId() {
//		return deliverySheetId;
//	}
//
//	public void setDeliverySheetId(int deliverySheetId) {
//		this.deliverySheetId = deliverySheetId;
//	}
//
//	public String getDeliverySheetNumber() {
//		return deliverySheetNumber;
//	}
//
//	public void setDeliverySheetNumber(String deliverySheetNumber) {
//		this.deliverySheetNumber = deliverySheetNumber;
//	}
//
//	public String getDeliverySheetDate() {
//		return deliverySheetDate;
//	}
//
//	public void setDeliverySheetDate(String deliverySheetDate) {
//		this.deliverySheetDate = deliverySheetDate;
//	}
//
//	public String getVaName() {
//		return vaName;
//	}
//
//	public void setVaName(String vaName) {
//		this.vaName = vaName;
//	}
//
//	public int getVaId() {
//		return vaId;
//	}
//
//	public void setVaId(int vaId) {
//		this.vaId = vaId;
//	}
//
//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
//
//	public String getBranchCode() {
//		return branchCode;
//	}
//
//	public void setBranchCode(String branchCode) {
//		this.branchCode = branchCode;
//	}
//
//	public int getDocketsCount() {
//		return docketsCount;
//	}
//
//	public void setDocketsCount(int docketsCount) {
//		this.docketsCount = docketsCount;
//	}
//
//	public String getTruckNumber() {
//		return truckNumber;
//	}
//
//	public void setTruckNumber(String truckNumber) {
//		this.truckNumber = truckNumber;
//	}
//
//	public int getDeliveryBranchId() {
//		return deliveryBranchId;
//	}
//
//	public void setDeliveryBranchId(int deliveryBranchId) {
//		this.deliveryBranchId = deliveryBranchId;
//	}
//
//	public String getDeliveryBranchName() {
//		return deliveryBranchName;
//	}
//
//	public void setDeliveryBranchName(String deliveryBranchName) {
//		this.deliveryBranchName = deliveryBranchName;
//	}
//
//	public List<DeliveryDocketsModel> getDockets() {
//		return dockets;
//	}
//
//	public void setDockets(List<DeliveryDocketsModel> dockets) {
//		this.dockets = dockets;
//	}
//
//}
