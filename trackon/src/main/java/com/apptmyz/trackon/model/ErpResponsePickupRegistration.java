package com.apptmyz.trackon.model;

public class ErpResponsePickupRegistration {

	private String PickUpOrderId;
	private Boolean Result;
	private String Messege;

	public String getPickUpOrderId() {
		return PickUpOrderId;
	}

	public void setPickUpOrderId(String pickUpOrderId) {
		PickUpOrderId = pickUpOrderId;
	}

	public Boolean getResult() {
		return Result;
	}

	public void setResult(Boolean result) {
		Result = result;
	}

	public String getMessege() {
		return Messege;
	}

	public void setMessege(String messege) {
		Messege = messege;
	}

}
