package com.apptmyz.trackon.model;

public class DeliveriesDataQuery {
	String deliveriesDateFrom;
	String deliveriesDateTo;
	String custCode;
	String deliveryBranch;
	int deliveryUndeliveryFlag;
	String agentId;

	public String getDeliveriesDateFrom() {
		return deliveriesDateFrom;
	}

	public void setDeliveriesDateFrom(String deliveriesDateFrom) {
		this.deliveriesDateFrom = deliveriesDateFrom;
	}

	public String getDeliveriesDateTo() {
		return deliveriesDateTo;
	}

	public void setDeliveriesDateTo(String deliveriesDateTo) {
		this.deliveriesDateTo = deliveriesDateTo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getDeliveryBranch() {
		return deliveryBranch;
	}

	public void setDeliveryBranch(String deliveryBranch) {
		this.deliveryBranch = deliveryBranch;
	}

	public int getDeliveryUndeliveryFlag() {
		return deliveryUndeliveryFlag;
	}

	public void setDeliveryUndeliveryFlag(int deliveryUndeliveryFlag) {
		this.deliveryUndeliveryFlag = deliveryUndeliveryFlag;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

}
