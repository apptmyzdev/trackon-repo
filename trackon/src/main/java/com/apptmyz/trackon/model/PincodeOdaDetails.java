package com.apptmyz.trackon.model;

import java.util.List;

public class PincodeOdaDetails {
	Integer pincode;
	boolean odaFlag;
	List<String> area;
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	public boolean isOdaFlag() {
		return odaFlag;
	}
	public void setOdaFlag(boolean odaFlag) {
		this.odaFlag = odaFlag;
	}
	public List<String> getArea() {
		return area;
	}
	public void setArea(List<String> area) {
		this.area = area;
	}
	
}
