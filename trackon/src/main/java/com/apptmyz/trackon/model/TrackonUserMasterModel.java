package com.apptmyz.trackon.model;

import com.google.gson.annotations.SerializedName;

public class TrackonUserMasterModel {

	@SerializedName(value = "UserName")
	String userName;

	@SerializedName(value = "UserPassword")
	String userPassword;

	@SerializedName(value = "FullName")
	String fullName;

	@SerializedName(value = "BranchCode")
	String branchCode;

	@SerializedName(value = "BranchName")
	String branchName;

	@SerializedName(value = "ROCode")
	String roCode;

	@SerializedName(value = "StateName")
	String stateName;

	@SerializedName(value = "GSTStateCode")
	String gstStateCode;

	@SerializedName(value = "TransporterID")
	String transporterId;

	@SerializedName(value = "CityName")
	String cityName;

	@SerializedName(value = "MobileNo")
	String mobileNo;

	@SerializedName(value = "UserType")
	String userType;

	@SerializedName(value = "FranchiseeBranchCode")
	String franchiseBranchCode;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getRoCode() {
		return roCode;
	}

	public void setRoCode(String roCode) {
		this.roCode = roCode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getGstStateCode() {
		return gstStateCode;
	}

	public void setGstStateCode(String gstStateCode) {
		this.gstStateCode = gstStateCode;
	}

	public String getTransporterId() {
		return transporterId;
	}

	public void setTransporterId(String transporterId) {
		this.transporterId = transporterId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getFranchiseBranchCode() {
		return franchiseBranchCode;
	}

	public void setFranchiseBranchCode(String franchiseBranchCode) {
		this.franchiseBranchCode = franchiseBranchCode;
	}

}
