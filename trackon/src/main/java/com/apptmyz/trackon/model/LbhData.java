package com.apptmyz.trackon.model;

public class LbhData {
	int pktCount;
	String uom;
	Double pktLen;
	Double pktWidth;
	Double pktHt;
	String material;
	String pktNo;
	String customerPktRefNo;
	String packagingType;
	Double actualWt;
	Double chargedWt;
	
	public int getPktCount() {
		return pktCount;
	}
	public void setPktCount(int pktCount) {
		this.pktCount = pktCount;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Double getPktLen() {
		return pktLen;
	}
	public void setPktLen(Double pktLen) {
		this.pktLen = pktLen;
	}
	public Double getPktWidth() {
		return pktWidth;
	}
	public void setPktWidth(Double pktWidth) {
		this.pktWidth = pktWidth;
	}
	public Double getPktHt() {
		return pktHt;
	}
	public void setPktHt(Double pktHt) {
		this.pktHt = pktHt;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getPktNo() {
		return pktNo;
	}
	public void setPktNo(String pktNo) {
		this.pktNo = pktNo;
	}
	public String getCustomerPktRefNo() {
		return customerPktRefNo;
	}
	public void setCustomerPktRefNo(String customerPktRefNo) {
		this.customerPktRefNo = customerPktRefNo;
	}
	public String getPackagingType() {
		return packagingType;
	}
	public void setPackagingType(String packagingType) {
		this.packagingType = packagingType;
	}
	public Double getActualWt() {
		return actualWt;
	}
	public void setActualWt(Double actualWt) {
		this.actualWt = actualWt;
	}
	public Double getChargedWt() {
		return chargedWt;
	}
	public void setChargedWt(Double chargedWt) {
		this.chargedWt = chargedWt;
	}
	
}
