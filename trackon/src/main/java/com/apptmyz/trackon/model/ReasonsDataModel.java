package com.apptmyz.trackon.model;

public class ReasonsDataModel {
	
	private int id;
	private String reason;
	private int pickupFlag;
	private int deliveryFlag;
	private int version;
	private int sortOrder;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public int getPickupFlag() {
		return pickupFlag;
	}
	public void setPickupFlag(int pickupFlag) {
		this.pickupFlag = pickupFlag;
	}
	public int getDeliveryFlag() {
		return deliveryFlag;
	}
	public void setDeliveryFlag(int deliveryFlag) {
		this.deliveryFlag = deliveryFlag;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	
}
