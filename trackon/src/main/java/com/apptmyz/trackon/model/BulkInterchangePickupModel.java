package com.apptmyz.trackon.model;

public class BulkInterchangePickupModel {

	private String pickupOrderNo;
	private Integer pickupId;
	private String userId;
	private String dktno;
	private String scanTimestamp;
	private String remarks;
	private String pickupStatus;
	private String cancelReason;
	private String pickupSignature;

	public String getPickupOrderNo() {
		return pickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}

	public Integer getPickupId() {
		return pickupId;
	}

	public void setPickupId(Integer pickupId) {
		this.pickupId = pickupId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDktno() {
		return dktno;
	}

	public void setDktno(String dktno) {
		this.dktno = dktno;
	}

	public String getScanTimestamp() {
		return scanTimestamp;
	}

	public void setScanTimestamp(String scanTimestamp) {
		this.scanTimestamp = scanTimestamp;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPickupStatus() {
		return pickupStatus;
	}

	public void setPickupStatus(String pickupStatus) {
		this.pickupStatus = pickupStatus;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getPickupSignature() {
		return pickupSignature;
	}

	public void setPickupSignature(String pickupSignature) {
		this.pickupSignature = pickupSignature;
	}

}
