package com.apptmyz.trackon.model;

import java.util.List;

public class ErpDrAckList {

	private List<ErpDrAcknowledgeModel> DRAcknowledgeInputList;

	public List<ErpDrAcknowledgeModel> getDRAcknowledgeInputList() {
		return DRAcknowledgeInputList;
	}

	public void setDRAcknowledgeInputList(List<ErpDrAcknowledgeModel> dRAcknowledgeInputList) {
		DRAcknowledgeInputList = dRAcknowledgeInputList;
	}
}
