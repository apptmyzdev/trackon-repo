package com.apptmyz.trackon.model;

import java.util.Date;

public class PincodeModel {

	private Integer id;

	private Integer pincode;

	private String officeCode;

	private String officeName;

	private String cityName;

	private String stateName;

	private Integer isServicable;

	private Integer dox;

	private Integer nonDox;

	private Integer primeTrack;

	private Integer stateExp;

	private Integer toPay;

	private Integer oda;

	private Integer roadExpress;

	private Integer roda;

	private Integer reversePickup;

	private Date createdTimestamp;

	private Integer activeFlag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Integer getIsServicable() {
		return isServicable;
	}

	public void setIsServicable(Integer isServicable) {
		this.isServicable = isServicable;
	}

	public Integer getDox() {
		return dox;
	}

	public void setDox(Integer dox) {
		this.dox = dox;
	}

	public Integer getNonDox() {
		return nonDox;
	}

	public void setNonDox(Integer nonDox) {
		this.nonDox = nonDox;
	}

	public Integer getPrimeTrack() {
		return primeTrack;
	}

	public void setPrimeTrack(Integer primeTrack) {
		this.primeTrack = primeTrack;
	}

	public Integer getStateExp() {
		return stateExp;
	}

	public void setStateExp(Integer stateExp) {
		this.stateExp = stateExp;
	}

	public Integer getToPay() {
		return toPay;
	}

	public void setToPay(Integer toPay) {
		this.toPay = toPay;
	}

	public Integer getOda() {
		return oda;
	}

	public void setOda(Integer oda) {
		this.oda = oda;
	}

	public Integer getRoadExpress() {
		return roadExpress;
	}

	public void setRoadExpress(Integer roadExpress) {
		this.roadExpress = roadExpress;
	}

	public Integer getRoda() {
		return roda;
	}

	public void setRoda(Integer roda) {
		this.roda = roda;
	}

	public Integer getReversePickup() {
		return reversePickup;
	}

	public void setReversePickup(Integer reversePickup) {
		this.reversePickup = reversePickup;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Integer getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		this.activeFlag = activeFlag;
	}

}
