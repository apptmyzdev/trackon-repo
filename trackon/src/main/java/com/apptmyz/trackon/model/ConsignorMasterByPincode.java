package com.apptmyz.trackon.model;

import java.util.List;

public class ConsignorMasterByPincode {
	Integer pincode;
	List<Consignor> consignors;
	public Integer getPincode() {
		return pincode;
	}
	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}
	public List<Consignor> getConsignors() {
		return consignors;
	}
	public void setConsignors(List<Consignor> consignors) {
		this.consignors = consignors;
	}

}
