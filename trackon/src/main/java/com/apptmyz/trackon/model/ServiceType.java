package com.apptmyz.trackon.model;

public class ServiceType {
	String serviceType;
	String code;
	int sortOrder;

	private Integer startSeries;

	private Integer seriesLength;

	private String shipmentType;
	Integer version;
	
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getStartSeries() {
		return startSeries;
	}

	public void setStartSeries(Integer startSeries) {
		this.startSeries = startSeries;
	}

	public Integer getSeriesLength() {
		return seriesLength;
	}

	public void setSeriesLength(Integer seriesLength) {
		this.seriesLength = seriesLength;
	}

	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

}
