package com.apptmyz.trackon.model;

import java.util.Date;

public class PickupRegistrationData {
	Integer pickupId;
	Integer pickupPincode;
	String pickupDate;
	String pickupSlot;
	String shipMode;
	String shipModeDesc;
	String doxFlag;
	String doxFlagDesc;
	String serviceType;
	String serviceTypeDesc;
	String custCode;
	String custName;
	String consignorCode;
	String consignorName;
	String contactNumber;
	Double wt;
	String uom;
	String uomDesc;
	String docketNo;
	Integer pktCount;
	Date createdTime;
	String pickupStatus;
	String pickupStatusDesc;
	String assignedTo;
	String createdBy;
	Integer deliveryPincode;
	PickupRegnSoftDataModel softData;
	String pickupType;
	Consignor consignor;
	Consignee consignee;
	Integer isFranchise;
	Integer custId;

	public Integer getPickupId() {
		return pickupId;
	}

	public void setPickupId(Integer pickupId) {
		this.pickupId = pickupId;
	}

	public Integer getPickupPincode() {
		return pickupPincode;
	}

	public void setPickupPincode(Integer pickupPincode) {
		this.pickupPincode = pickupPincode;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupSlot() {
		return pickupSlot;
	}

	public void setPickupSlot(String pickupSlot) {
		this.pickupSlot = pickupSlot;
	}

	public String getShipMode() {
		return shipMode;
	}

	public void setShipMode(String shipMode) {
		this.shipMode = shipMode;
	}

	public String getDoxFlag() {
		return doxFlag;
	}

	public void setDoxFlag(String doxFlag) {
		this.doxFlag = doxFlag;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getConsignorCode() {
		return consignorCode;
	}

	public void setConsignorCode(String consignorCode) {
		this.consignorCode = consignorCode;
	}

	public String getConsignorName() {
		return consignorName;
	}

	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Double getWt() {
		return wt;
	}

	public void setWt(Double wt) {
		this.wt = wt;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getDocketNo() {
		return docketNo;
	}

	public void setDocketNo(String docketNo) {
		this.docketNo = docketNo;
	}

	public Integer getPktCount() {
		return pktCount;
	}

	public void setPktCount(Integer pktCount) {
		this.pktCount = pktCount;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getPickupStatus() {
		return pickupStatus;
	}

	public void setPickupStatus(String pickupStatus) {
		this.pickupStatus = pickupStatus;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getDeliveryPincode() {
		return deliveryPincode;
	}

	public void setDeliveryPincode(Integer deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}

	public String getShipModeDesc() {
		return shipModeDesc;
	}

	public void setShipModeDesc(String shipModeDesc) {
		this.shipModeDesc = shipModeDesc;
	}

	public String getDoxFlagDesc() {
		return doxFlagDesc;
	}

	public void setDoxFlagDesc(String doxFlagDesc) {
		this.doxFlagDesc = doxFlagDesc;
	}

	public String getServiceTypeDesc() {
		return serviceTypeDesc;
	}

	public void setServiceTypeDesc(String serviceTypeDesc) {
		this.serviceTypeDesc = serviceTypeDesc;
	}

	public String getUomDesc() {
		return uomDesc;
	}

	public void setUomDesc(String uomDesc) {
		this.uomDesc = uomDesc;
	}

	public String getPickupStatusDesc() {
		return pickupStatusDesc;
	}

	public void setPickupStatusDesc(String pickupStatusDesc) {
		this.pickupStatusDesc = pickupStatusDesc;
	}

	public PickupRegnSoftDataModel getSoftData() {
		return softData;
	}

	public void setSoftData(PickupRegnSoftDataModel softData) {
		this.softData = softData;
	}

	public String getPickupType() {
		return pickupType;
	}

	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}

	public Consignor getConsignor() {
		return consignor;
	}

	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;
	}

	public Integer getIsFranchise() {
		return isFranchise;
	}

	public void setIsFranchise(Integer isFranchise) {
		this.isFranchise = isFranchise;
	}

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public Consignee getConsignee() {
		return consignee;
	}

	public void setConsignee(Consignee consignee) {
		this.consignee = consignee;
	}

}