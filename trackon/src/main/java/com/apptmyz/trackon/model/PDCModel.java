package com.apptmyz.trackon.model;

import java.util.Date;
import java.util.List;

public class PDCModel {
	String 	agentId;
	String 	branchCode;
	String	pdcDate;
	String 	vehicleNo;
	String 	pdcNo;
	List<PDCDockets>	pdcDockets;
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getPdcDate() {
		return pdcDate;
	}
	public void setPdcDate(String pdcDate) {
		this.pdcDate = pdcDate;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getPdcNo() {
		return pdcNo;
	}
	public void setPdcNo(String pdcNo) {
		this.pdcNo = pdcNo;
	}
	public List<PDCDockets> getPdcDockets() {
		return pdcDockets;
	}
	public void setPdcDockets(List<PDCDockets> pdcDockets) {
		this.pdcDockets = pdcDockets;
	}
	@Override
	public String toString() {
		return "PDCModel [agentId=" + agentId + ", branchCode=" + branchCode + ", pdcDate=" + pdcDate + ", vehicleNo="
				+ vehicleNo + ", pdcNo=" + pdcNo + ", pdcDockets=" + pdcDockets + "]";
	}
	
	
}
