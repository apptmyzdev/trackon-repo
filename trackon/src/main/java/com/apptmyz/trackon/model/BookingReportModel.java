package com.apptmyz.trackon.model;

public class BookingReportModel {
	
	private String bookingAgentId;
	private int totaldockets;
	private String regionCode;
	private String branchCode;
	private String branchName;
	public String getBookingAgentId() {
		return bookingAgentId;
	}
	public void setBookingAgentId(String bookingAgentId) {
		this.bookingAgentId = bookingAgentId;
	}
	public int getTotaldockets() {
		return totaldockets;
	}
	public void setTotaldockets(int totaldockets) {
		this.totaldockets = totaldockets;
	}
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	
}
