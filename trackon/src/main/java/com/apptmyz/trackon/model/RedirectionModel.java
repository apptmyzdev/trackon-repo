package com.apptmyz.trackon.model;

import java.util.List;

public class RedirectionModel {

	private String fromUserId;
	private String toUserId;
	private String remarks;
	List<Integer> docketIds;

	public String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(String fromUserId) {
		this.fromUserId = fromUserId;
	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<Integer> getDocketIds() {
		return docketIds;
	}

	public void setDocketIds(List<Integer> docketIds) {
		this.docketIds = docketIds;
	}

}
