package com.apptmyz.trackon.model;

public class MaContentTypeModel {

	private String  Id;
    private String SkuCode;
    private String SkuCategory;
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getSkuCode() {
		return SkuCode;
	}
	public void setSkuCode(String skuCode) {
		SkuCode = skuCode;
	}
	public String getSkuCategory() {
		return SkuCategory;
	}
	public void setSkuCategory(String skuCategory) {
		SkuCategory = skuCategory;
	}

}
