package com.apptmyz.trackon.model;

public class DrAcknowledgeModel {

	private Integer id;

	private String drNumber;

	private String awbNo;

	private String agentId;

	private boolean isAcknowledged;

	private boolean isShort;

	private boolean isExtra;

	private String acknowledgeDateTime;

	private String remarks;

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDrNumber() {
		return drNumber;
	}

	public void setDrNumber(String drNumber) {
		this.drNumber = drNumber;
	}

	public String getAwbNo() {
		return awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public boolean isAcknowledged() {
		return isAcknowledged;
	}

	public void setAcknowledged(boolean isAcknowledged) {
		this.isAcknowledged = isAcknowledged;
	}

	public boolean isShort() {
		return isShort;
	}

	public void setShort(boolean isShort) {
		this.isShort = isShort;
	}

	public boolean isExtra() {
		return isExtra;
	}

	public void setExtra(boolean isExtra) {
		this.isExtra = isExtra;
	}

	public String getAcknowledgeDateTime() {
		return acknowledgeDateTime;
	}

	public void setAcknowledgeDateTime(String acknowledgeDateTime) {
		this.acknowledgeDateTime = acknowledgeDateTime;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
