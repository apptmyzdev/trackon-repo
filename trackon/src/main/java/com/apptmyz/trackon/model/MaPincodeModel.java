package com.apptmyz.trackon.model;

public class MaPincodeModel {

    private String Pincode;
    private String OfficeCode;
    private String OfficeName;
    private String CityName;
    private String StateName;
    private String IsServiceable;
    private String Dox;
    private String NonDox;
    private String PrimeTrack;
    private String StateExp;
    private String Topay;
    private String ODA;
    private String RoadExpress;
    private String RODA;
    private String ReversePickup;
	public String getPincode() {
		return Pincode;
	}
	public void setPincode(String pincode) {
		Pincode = pincode;
	}
	public String getOfficeCode() {
		return OfficeCode;
	}
	public void setOfficeCode(String officeCode) {
		OfficeCode = officeCode;
	}
	public String getOfficeName() {
		return OfficeName;
	}
	public void setOfficeName(String officeName) {
		OfficeName = officeName;
	}
	public String getCityName() {
		return CityName;
	}
	public void setCityName(String cityName) {
		CityName = cityName;
	}
	public String getStateName() {
		return StateName;
	}
	public void setStateName(String stateName) {
		StateName = stateName;
	}
	public String getIsServiceable() {
		return IsServiceable;
	}
	public void setIsServiceable(String isServiceable) {
		IsServiceable = isServiceable;
	}
	public String getDox() {
		return Dox;
	}
	public void setDox(String dox) {
		Dox = dox;
	}
	public String getNonDox() {
		return NonDox;
	}
	public void setNonDox(String nonDox) {
		NonDox = nonDox;
	}
	public String getPrimeTrack() {
		return PrimeTrack;
	}
	public void setPrimeTrack(String primeTrack) {
		PrimeTrack = primeTrack;
	}
	public String getStateExp() {
		return StateExp;
	}
	public void setStateExp(String stateExp) {
		StateExp = stateExp;
	}
	public String getTopay() {
		return Topay;
	}
	public void setTopay(String topay) {
		Topay = topay;
	}
	public String getODA() {
		return ODA;
	}
	public void setODA(String oDA) {
		ODA = oDA;
	}
	public String getRoadExpress() {
		return RoadExpress;
	}
	public void setRoadExpress(String roadExpress) {
		RoadExpress = roadExpress;
	}
	public String getRODA() {
		return RODA;
	}
	public void setRODA(String rODA) {
		RODA = rODA;
	}
	public String getReversePickup() {
		return ReversePickup;
	}
	public void setReversePickup(String reversePickup) {
		ReversePickup = reversePickup;
	}

}
