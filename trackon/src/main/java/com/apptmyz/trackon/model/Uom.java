package com.apptmyz.trackon.model;

public class Uom {
	String uomCat;
	String uomValue;
	String uomDisplayValue;
	int version;
	
	public String getUomCat() {
		return uomCat;
	}
	public void setUomCat(String uomCat) {
		this.uomCat = uomCat;
	}
	public String getUomValue() {
		return uomValue;
	}
	public void setUomValue(String uomValue) {
		this.uomValue = uomValue;
	}
	public String getUomDisplayValue() {
		return uomDisplayValue;
	}
	public void setUomDisplayValue(String uomDisplayValue) {
		this.uomDisplayValue = uomDisplayValue;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	
	
}
