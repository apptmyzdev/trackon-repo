package com.apptmyz.trackon.model;

public class CCPktModel {
	String customerPktNo;
	String pktNo;
	public String getCustomerPktNo() {
		return customerPktNo;
	}
	public void setCustomerPktNo(String customerPktNo) {
		this.customerPktNo = customerPktNo;
	}
	public String getPktNo() {
		return pktNo;
	}
	public void setPktNo(String pktNo) {
		this.pktNo = pktNo;
	}
	
}
