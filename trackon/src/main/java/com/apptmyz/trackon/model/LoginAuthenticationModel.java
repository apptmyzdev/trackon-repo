package com.apptmyz.trackon.model;

public class LoginAuthenticationModel {

	private String userId;
	private String password;
	private int appVersion;
	private String userType;
	private String imei;
	private boolean allowPickupRegistration;
	private boolean allowPickupBooking;
	private boolean allowDelivery;
	private int branchErpId;
	private String branchCode;
	private int vaId;
	private int subVaId;
	private int role;
	private String roleType;
	private String zoneCode;
	private String regionCode;
	private double latitude;
	private double longitude;
	private String franchiseBranchCode;
	private String ocrApiKey;
	private String androidVersion;
	private String deviceBrand;
	private String deviceModel;
	
	public String getDeviceBrand() {
		return deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(int appVersion) {
		this.appVersion = appVersion;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public boolean isAllowPickupRegistration() {
		return allowPickupRegistration;
	}

	public void setAllowPickupRegistration(boolean allowPickupRegistration) {
		this.allowPickupRegistration = allowPickupRegistration;
	}

	public boolean isAllowPickupBooking() {
		return allowPickupBooking;
	}

	public void setAllowPickupBooking(boolean allowPickupBooking) {
		this.allowPickupBooking = allowPickupBooking;
	}

	public boolean isAllowDelivery() {
		return allowDelivery;
	}

	public void setAllowDelivery(boolean allowDelivery) {
		this.allowDelivery = allowDelivery;
	}

	public int getBranchErpId() {
		return branchErpId;
	}

	public void setBranchErpId(int branchErpId) {
		this.branchErpId = branchErpId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public int getVaId() {
		return vaId;
	}

	public void setVaId(int vaId) {
		this.vaId = vaId;
	}

	public int getSubVaId() {
		return subVaId;
	}

	public void setSubVaId(int subVaId) {
		this.subVaId = subVaId;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getZoneCode() {
		return zoneCode;
	}

	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getFranchiseBranchCode() {
		return franchiseBranchCode;
	}

	public void setFranchiseBranchCode(String franchiseBranchCode) {
		this.franchiseBranchCode = franchiseBranchCode;
	}

	public String getOcrApiKey() {
		return ocrApiKey;
	}

	public void setOcrApiKey(String ocrApiKey) {
		this.ocrApiKey = ocrApiKey;
	}

}
