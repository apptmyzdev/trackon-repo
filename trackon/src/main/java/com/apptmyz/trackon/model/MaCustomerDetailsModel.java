package com.apptmyz.trackon.model;

public class MaCustomerDetailsModel {

	private String CustCode;
	private String CustomerName;
	private String CustomerType;
	private String IndustryType;
	private String DownloadType;
	public String getCustCode() {
		return CustCode;
	}
	public void setCustCode(String custCode) {
		CustCode = custCode;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerType() {
		return CustomerType;
	}
	public void setCustomerType(String customerType) {
		CustomerType = customerType;
	}
	public String getIndustryType() {
		return IndustryType;
	}
	public void setIndustryType(String industryType) {
		IndustryType = industryType;
	}
	public String getDownloadType() {
		return DownloadType;
	}
	public void setDownloadType(String downloadType) {
		DownloadType = downloadType;
	}

}
