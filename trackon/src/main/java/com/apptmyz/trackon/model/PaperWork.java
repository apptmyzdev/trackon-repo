package com.apptmyz.trackon.model;

public class PaperWork {
	String name;
	String documentNo;
	String refNo;
	String base64Data;
	String paperWorkImageUrl;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getBase64Data() {
		return base64Data;
	}

	public void setBase64Data(String base64Data) {
		this.base64Data = base64Data;
	}

	public String getPaperWorkImageUrl() {
		return paperWorkImageUrl;
	}

	public void setPaperWorkImageUrl(String paperWorkImageUrl) {
		this.paperWorkImageUrl = paperWorkImageUrl;
	}

}
