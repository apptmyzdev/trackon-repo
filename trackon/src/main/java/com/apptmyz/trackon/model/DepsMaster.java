package com.apptmyz.trackon.model;

public class DepsMaster {
	String 	depsCategory;
	String 	depsDesc;
	String 	depsCode;
	int		sortOrder;
	int		version;
	public String getDepsCategory() {
		return depsCategory;
	}
	public void setDepsCategory(String depsCategory) {
		this.depsCategory = depsCategory;
	}
	public String getDepsDesc() {
		return depsDesc;
	}
	public void setDepsDesc(String depsDesc) {
		this.depsDesc = depsDesc;
	}
	public String getDepsCode() {
		return depsCode;
	}
	public void setDepsCode(String depsCode) {
		this.depsCode = depsCode;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "DepsMaster [depsCategory=" + depsCategory + ", depsDesc=" + depsDesc + ", depsCode=" + depsCode
				+ ", sortOrder=" + sortOrder + "]";
	}

}
