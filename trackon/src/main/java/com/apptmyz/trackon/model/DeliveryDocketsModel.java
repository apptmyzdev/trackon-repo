package com.apptmyz.trackon.model;

import java.math.BigDecimal;

import javax.persistence.Column;

public class DeliveryDocketsModel {

	private int preDeliverySheetId;
	private String preDeliverySheetNumber;
	private String sheetDate;
	private String pdcBranchCode;
	private String userId;
	private int serialNumber;
	private int articleId;
	private int docketId;
	// private int docketNumber;
	private String docketNumber;
	private String bookingDate;
	private int bookingBranchId;
	private String bookingBranchName;
	private int deliveryBranchId;
	private String deliveryBranchName;
	private int articlesCount;
	private double actualWeight;
	private int bookingTypeId;
	private String bookingType;
	private int paymentTypeId;
	private String paymentType;
	private double crCashAmount;
	private double crChequeAmount;
	private double crTotalAmount;
	private String crPaymentMode;
	private int codDodFlag;
	private double codDodAmount;
	// private int isOda;
	// 2020-10-20
	private int contractType;

	public int getPreDeliverySheetId() {
		return preDeliverySheetId;
	}

	public void setPreDeliverySheetId(int preDeliverySheetId) {
		this.preDeliverySheetId = preDeliverySheetId;
	}

	public String getPreDeliverySheetNumber() {
		return preDeliverySheetNumber;
	}

	public void setPreDeliverySheetNumber(String preDeliverySheetNumber) {
		this.preDeliverySheetNumber = preDeliverySheetNumber;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerailNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public int getArticleId() {
		return articleId;
	}

	public void setArticleId(int articleId) {
		this.articleId = articleId;
	}

	public int getDocketId() {
		return docketId;
	}

	public void setDocketId(int docketId) {
		this.docketId = docketId;
	}

	// public int getDocketNumber() {
	// return docketNumber;
	// }
	//
	// public void setDocketNumber(int docketNumber) {
	// this.docketNumber = docketNumber;
	// }

	public String getDocketNumber() {
		return docketNumber;
	}

	public void setDocketNumber(String docketNumber) {
		this.docketNumber = docketNumber;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getBookingBranchId() {
		return bookingBranchId;
	}

	public void setBookingBranchId(int bookingBranchId) {
		this.bookingBranchId = bookingBranchId;
	}

	public String getBookingBranchName() {
		return bookingBranchName;
	}

	public void setBookingBranchName(String bookingBranchName) {
		this.bookingBranchName = bookingBranchName;
	}

	public int getDeliveryBranchId() {
		return deliveryBranchId;
	}

	public void setDeliveryBranchId(int deliveryBranchId) {
		this.deliveryBranchId = deliveryBranchId;
	}

	public String getDeliveryBranchName() {
		return deliveryBranchName;
	}

	public void setDeliveryBranchName(String deliveryBranchName) {
		this.deliveryBranchName = deliveryBranchName;
	}

	public int getArticlesCount() {
		return articlesCount;
	}

	public void setArticlesCount(int articlesCount) {
		this.articlesCount = articlesCount;
	}

	public double getActualWeight() {
		return actualWeight;
	}

	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}

	public int getBookingTypeId() {
		return bookingTypeId;
	}

	public void setBookingTypeId(int bookingTypeId) {
		this.bookingTypeId = bookingTypeId;
	}

	public String getBookingType() {
		return bookingType;
	}

	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}

	public int getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(int paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	// public int getIsOda() {
	// return isOda;
	// }
	//
	// public void setIsOda(int isOda) {
	// this.isOda = isOda;
	// }

	public String getSheetDate() {
		return sheetDate;
	}

	public void setSheetDate(String sheetDate) {
		this.sheetDate = sheetDate;
	}

	public String getPdcBranchCode() {
		return pdcBranchCode;
	}

	public void setPdcBranchCode(String pdcBranchCode) {
		this.pdcBranchCode = pdcBranchCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public double getCrCashAmount() {
		return crCashAmount;
	}

	public void setCrCashAmount(double crCashAmount) {
		this.crCashAmount = crCashAmount;
	}

	public double getCrChequeAmount() {
		return crChequeAmount;
	}

	public void setCrChequeAmount(double crChequeAmount) {
		this.crChequeAmount = crChequeAmount;
	}

	public double getCrTotalAmount() {
		return crTotalAmount;
	}

	public void setCrTotalAmount(double crTotalAmount) {
		this.crTotalAmount = crTotalAmount;
	}

	public String getCrPaymentMode() {
		return crPaymentMode;
	}

	public void setCrPaymentMode(String crPaymentMode) {
		this.crPaymentMode = crPaymentMode;
	}

	public int getCodDodFlag() {
		return codDodFlag;
	}

	public void setCodDodFlag(int codDodFlag) {
		this.codDodFlag = codDodFlag;
	}

	public double getCodDodAmount() {
		return codDodAmount;
	}

	public void setCodDodAmount(double codDodAmount) {
		this.codDodAmount = codDodAmount;
	}

	/** added on Oct 20, 2020 **/
	public int getContractType() {
		return contractType;
	}

	/** added on Oct 20, 2020 **/
	public void setContractType(int contractType) {
		this.contractType = contractType;
	}

}
