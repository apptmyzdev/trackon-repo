package com.apptmyz.trackon.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class TrackonDlyUndlyUpdateModel {

	@SerializedName(value = "TranscationID")
	int transactionId;

	@SerializedName(value = "TranscationDate")
	String transactionDate;

	@SerializedName(value = "UserId")
	String userId;

	@SerializedName(value = "BranchCode")
	String branchCode;

	@SerializedName(value = "DRSNo")
	String drsNo;

	@SerializedName(value = "AWBNo")
	String awbNo;

	@SerializedName(value = "Status")
	String status;

	@SerializedName(value = "Remarks")
	String remarks;

	@SerializedName(value = "Latitude")
	double latitude;

	@SerializedName(value = "Longitude")
	double longitude;

	@SerializedName(value = "DeliveredDateTime")
	String deliveredDateTime;

	@SerializedName(value = "RecieverName")
	String receiverName;

	@SerializedName(value = "IsBadDelivery")
	boolean isBadDelivery;

	@SerializedName(value = "HasDeps")
	boolean hasDeps;

	@SerializedName(value = "DepsReason")
	String depsReason;

	@SerializedName(value = "PodImageName")
	String podImageName;

	@SerializedName(value = "SignatureImageName")
	String signatureImageName;

	@SerializedName(value = "MiscImages")
	List<ImageData> miscImages;

	@SerializedName(value = "PaymentMethod")
	String paymentMethod;

	@SerializedName(value = "AmountToBeCollected")
	double amountToBeCollected;

	@SerializedName(value = "AmountCollected")
	double amountCollected;

	@SerializedName(value = "IsPaymentCollected")
	boolean isPaymentCollected;

	@SerializedName(value = "BankName")
	String bankName;

	@SerializedName(value = "PaymentReferenceId")
	String paymentReferenceId;

	@SerializedName(value = "DeliveredContact")
	String deliveredContact;

	@SerializedName(value = "Relationship")
	String relationship;

	@SerializedName(value = "UndeliveredDateTime")
	String undeliveredDateTime;

	@SerializedName(value = "UndeliveredReason")
	String undeliveredReason;

	String appVersion;
	
	String imei;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getDrsNo() {
		return drsNo;
	}

	public void setDrsNo(String drsNo) {
		this.drsNo = drsNo;
	}

	public String getAwbNo() {
		return awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDeliveredDateTime() {
		return deliveredDateTime;
	}

	public void setDeliveredDateTime(String deliveredDateTime) {
		this.deliveredDateTime = deliveredDateTime;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public boolean isBadDelivery() {
		return isBadDelivery;
	}

	public void setBadDelivery(boolean isBadDelivery) {
		this.isBadDelivery = isBadDelivery;
	}

	public boolean isHasDeps() {
		return hasDeps;
	}

	public void setHasDeps(boolean hasDeps) {
		this.hasDeps = hasDeps;
	}

	public String getDepsReason() {
		return depsReason;
	}

	public void setDepsReason(String depsReason) {
		this.depsReason = depsReason;
	}

	public String getPodImageName() {
		return podImageName;
	}

	public void setPodImageName(String podImageName) {
		this.podImageName = podImageName;
	}

	public String getSignatureImageName() {
		return signatureImageName;
	}

	public void setSignatureImageName(String signatureImageName) {
		this.signatureImageName = signatureImageName;
	}

	public List<ImageData> getMiscImages() {
		return miscImages;
	}

	public void setMiscImages(List<ImageData> miscImages) {
		this.miscImages = miscImages;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public double getAmountToBeCollected() {
		return amountToBeCollected;
	}

	public void setAmountToBeCollected(double amountToBeCollected) {
		this.amountToBeCollected = amountToBeCollected;
	}

	public double getAmountCollected() {
		return amountCollected;
	}

	public void setAmountCollected(double amountCollected) {
		this.amountCollected = amountCollected;
	}

	public boolean isPaymentCollected() {
		return isPaymentCollected;
	}

	public void setPaymentCollected(boolean isPaymentCollected) {
		this.isPaymentCollected = isPaymentCollected;
	}

	public String isBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getPaymentReferenceId() {
		return paymentReferenceId;
	}

	public void setPaymentReferenceId(String paymentReferenceId) {
		this.paymentReferenceId = paymentReferenceId;
	}

	public String getDeliveredContact() {
		return deliveredContact;
	}

	public void setDeliveredContact(String deliveredContact) {
		this.deliveredContact = deliveredContact;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getUndeliveredDateTime() {
		return undeliveredDateTime;
	}

	public void setUndeliveredDateTime(String undeliveredDateTime) {
		this.undeliveredDateTime = undeliveredDateTime;
	}

	public String getUndeliveredReason() {
		return undeliveredReason;
	}

	public void setUndeliveredReason(String undeliveredReason) {
		this.undeliveredReason = undeliveredReason;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}
	
	

}
