package com.apptmyz.trackon.model;

import com.google.gson.annotations.SerializedName;

public class TrackonDRSDetailsModel {

	@SerializedName(value = "DRSDetailID")
	long drsDetailId;

	@SerializedName(value = "OFDId")
	long ofdId;

	@SerializedName(value = "DRSNo")
	String drsNo;

	@SerializedName(value = "AWBNo")
	String awbNo;

	@SerializedName(value = "AWBSerialNo")
	int awbSerialNo;

	@SerializedName(value = "AssignedToUser")
	String assignedToUser;

	@SerializedName(value = "NoOfPieces")
	int noPcs;

	@SerializedName(value = "Weight")
	double weight;

	@SerializedName(value = "remarks")
	String remarks;

	@SerializedName(value = "ReceiverName")
	String receiverName;

	@SerializedName(value = "Address")
	String address;

	@SerializedName(value = "EntryDateTime")
	String entryDateTime;

	@SerializedName(value = "MobileNo")
	String mobileNo;

	@SerializedName(value = "City")
	String city;

	@SerializedName(value = "Pincode")
	int pincode;

	@SerializedName(value = "IsCOD")
	boolean isCod;

	@SerializedName(value = "IsFOD")
	boolean isFod;

	@SerializedName(value = "CODAmount")
	double codAmount;

	@SerializedName(value = "FODAmount")
	double fodAmount;

	@SerializedName(value = "IsOTPMandatory")
	boolean isOTPMandatory;

	@SerializedName(value = "UserType")
	String userType;

	@SerializedName(value = "FranchiseeBranchCode")
	String franchiseeBranchCode;

	@SerializedName(value = "BranchCode")
	String branchCode;

	@SerializedName(value = "IsDRAcknowledgeReqd")
	boolean isDRAcknowledgeReqd;

	@SerializedName(value = "IsBulkUpdateAllowed")
	boolean isBulkUpdateAllowed;

	public long getDrsDetailId() {
		return drsDetailId;
	}

	public void setDrsDetailId(long drsDetailId) {
		this.drsDetailId = drsDetailId;
	}

	public long getOfdId() {
		return ofdId;
	}

	public void setOfdId(long ofdId) {
		this.ofdId = ofdId;
	}

	public String getDrsNo() {
		return drsNo;
	}

	public void setDrsNo(String drsNo) {
		this.drsNo = drsNo;
	}

	public String getAwbNo() {
		return awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public int getAwbSerialNo() {
		return awbSerialNo;
	}

	public void setAwbSerialNo(int awbSerialNo) {
		this.awbSerialNo = awbSerialNo;
	}

	public String getAssignedToUser() {
		return assignedToUser;
	}

	public void setAssignedToUser(String assignedToUser) {
		this.assignedToUser = assignedToUser;
	}

	public int getNoPcs() {
		return noPcs;
	}

	public void setNoPcs(int noPcs) {
		this.noPcs = noPcs;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEntryDateTime() {
		return entryDateTime;
	}

	public void setEntryDateTime(String entryDateTime) {
		this.entryDateTime = entryDateTime;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public boolean isCod() {
		return isCod;
	}

	public void setCod(boolean isCod) {
		this.isCod = isCod;
	}

	public boolean isFod() {
		return isFod;
	}

	public void setFod(boolean isFod) {
		this.isFod = isFod;
	}

	public double getCodAmount() {
		return codAmount;
	}

	public void setCodAmount(double codAmount) {
		this.codAmount = codAmount;
	}

	public double getFodAmount() {
		return fodAmount;
	}

	public void setFodAmount(double fodAmount) {
		this.fodAmount = fodAmount;
	}

	public boolean isOTPMandatory() {
		return isOTPMandatory;
	}

	public void setOTPMandatory(boolean isOTPMandatory) {
		this.isOTPMandatory = isOTPMandatory;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getFranchiseeBranchCode() {
		return franchiseeBranchCode;
	}

	public void setFranchiseeBranchCode(String franchiseeBranchCode) {
		this.franchiseeBranchCode = franchiseeBranchCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public boolean isDRAcknowledgeReqd() {
		return isDRAcknowledgeReqd;
	}

	public void setDRAcknowledgeReqd(boolean isDRAcknowledgeReqd) {
		this.isDRAcknowledgeReqd = isDRAcknowledgeReqd;
	}

	public boolean isBulkUpdateAllowed() {
		return isBulkUpdateAllowed;
	}

	public void setBulkUpdateAllowed(boolean isBulkUpdateAllowed) {
		this.isBulkUpdateAllowed = isBulkUpdateAllowed;
	}

}
