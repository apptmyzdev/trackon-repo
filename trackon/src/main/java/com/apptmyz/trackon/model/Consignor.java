package com.apptmyz.trackon.model;

public class Consignor {
	String consignorCode;
	String consignorName;
	Integer consignorPincode;
	String branchCode;
	String address1;
	String address2;
	String city;
	String state;
	String custCode;
	String contactName;
	String contactEmailid;
	String contactPhoneno;
	boolean contractCustomer;
	Integer consignorId;
	Integer customerId;
	String gstin;
	String mobileno;
	String email;
	String pan;

	
	public String getConsignorCode() {
		return consignorCode;
	}
	public void setConsignorCode(String consignorCode) {
		this.consignorCode = consignorCode;
	}
	public String getConsignorName() {
		return consignorName;
	}
	public void setConsignorName(String consignorName) {
		this.consignorName = consignorName;
	}
	public Integer getConsignorPincode() {
		return consignorPincode;
	}
	public void setConsignorPincode(Integer consignorPincode) {
		this.consignorPincode = consignorPincode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactEmailid() {
		return contactEmailid;
	}
	public void setContactEmailid(String contactEmailid) {
		this.contactEmailid = contactEmailid;
	}
	public String getContactPhoneno() {
		return contactPhoneno;
	}
	public void setContactPhoneno(String contactPhoneno) {
		this.contactPhoneno = contactPhoneno;
	}
	public boolean isContractCustomer() {
		return contractCustomer;
	}
	public void setContractCustomer(boolean contractCustomer) {
		this.contractCustomer = contractCustomer;
	}
	public Integer getConsignorId() {
		return consignorId;
	}
	public void setConsignorId(Integer consignorId) {
		this.consignorId = consignorId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getGstin() {
		return gstin;
	}
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	
}
