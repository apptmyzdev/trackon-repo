//package com.apptmyz.trackon.configuration;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//
//@Configuration
//@EnableAutoConfiguration
//@EnableJpaRepositories(basePackages = {"com.apptmyz.trackon.tcpl.repository"},
//        entityManagerFactoryRef = "tcplMasterEntityManagerFactory",
//        transactionManagerRef= "tcplMasterTransactionManager")
////@EntityScan(basePackages={"com.apptmyz.simplify.docengine.jpa"})
//@EnableTransactionManagement
//public class TcplMasterConfiguration {
//	
//	@Bean
//	@ConfigurationProperties(prefix="spring.datasource.tcpl-master")
//	public DataSource tcplMasterDataSource() {
//		return DataSourceBuilder.create().build();
//	}
//	
//	@Bean(name = "tcplMasterEntityManagerFactory")
//	public LocalContainerEntityManagerFactoryBean tcplMasterEntityManagerFactory(
//			EntityManagerFactoryBuilder builder) {
//		LocalContainerEntityManagerFactoryBean bean =  builder
//				.dataSource(tcplMasterDataSource())
//				.packages("com.apptmyz.trackon.tcpl.entities.jpa", "com.apptmyz.trackon.tcpl.repository")
//				.build();
//		return bean;
//	}
//
//	@Bean
//	public PlatformTransactionManager tcplMasterTransactionManager(
//			final @Qualifier("tcplMasterEntityManagerFactory") LocalContainerEntityManagerFactoryBean tcplMasterEntityManagerFactory) {
//		return new JpaTransactionManager(tcplMasterEntityManagerFactory.getObject());
//	}
//
//}
//
