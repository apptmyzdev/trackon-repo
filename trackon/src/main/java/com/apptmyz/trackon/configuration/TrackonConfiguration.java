//package com.apptmyz.trackon.configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//
//@Configuration
//@EnableAutoConfiguration
//@EnableJpaRepositories(basePackages={"com.apptmyz.trackon.data.repository.jpa"},
//        entityManagerFactoryRef = "trackonEntityManagerFactory",
//        transactionManagerRef= "trackonTransactionManager")
//@EntityScan(basePackages={"com.apptmyz.trackon.entities.jpa"})
//@EnableTransactionManagement
//public class TrackonConfiguration {
//	
//	@Bean
//    @Primary
//	@ConfigurationProperties(prefix="spring.datasource.trackon")
//	public DataSource trackonDataSource() {
//		DataSource source = DataSourceBuilder.create().build();
//		return source;
//	}
//
//	@Bean(name = "trackonEntityManagerFactory") 
//	@Primary
//	public LocalContainerEntityManagerFactoryBean trackonEntityManagerFactory(
//			EntityManagerFactoryBuilder builder) {
//		 Map<String, Object> properties = new HashMap<String, Object>();
//		    properties.put("hibernate.hbm2ddl.auto","none");
//		    properties.put("hibernate.id.new_generator_mappings", "false");
//		    properties.put("spring.jpa.generate-ddl", "false");
//		    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//		    LocalContainerEntityManagerFactoryBean bean = builder
//				.dataSource(trackonDataSource())
//				.packages("com.apptmyz.trackon.entities.jpa", "com.apptmyz.trackon.data.repository.jpa")
//				.properties(properties)
//				.build();
//		return bean;
//	}
//
//	@Bean
//	@Primary
//	public PlatformTransactionManager trackonTransactionManager(
//			final @Qualifier("trackonEntityManagerFactory") LocalContainerEntityManagerFactoryBean trackonEntityManagerFactory) {
//		return new JpaTransactionManager(trackonEntityManagerFactory.getObject());
//	}
//
//}
//
