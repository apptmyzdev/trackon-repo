/*
 * Created on 2022-05-31 ( Time 16:14:22 )
 * Generated by Telosys Tools Generator ( version 3.3.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;


import javax.persistence.*;

/**
 * Persistent class for entity stored in table "m_charge_types"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="m_charge_types", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="MChargeTypesEntity.countAll", query="SELECT COUNT(x) FROM MChargeTypesEntity x" )
} )
public class MChargeTypesEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="code", length=4)
    private String     code         ;

    @Column(name="charge_type", length=45)
    private String     chargeType   ;

    @Column(name="sort_order")
    private Integer    sortOrder    ;

    @Column(name="active_flag")
    private Integer    activeFlag   ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public MChargeTypesEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : code ( VARCHAR ) 
    public void setCode( String code ) {
        this.code = code;
    }
    public String getCode() {
        return this.code;
    }

    //--- DATABASE MAPPING : charge_type ( VARCHAR ) 
    public void setChargeType( String chargeType ) {
        this.chargeType = chargeType;
    }
    public String getChargeType() {
        return this.chargeType;
    }

    //--- DATABASE MAPPING : sort_order ( INT ) 
    public void setSortOrder( Integer sortOrder ) {
        this.sortOrder = sortOrder;
    }
    public Integer getSortOrder() {
        return this.sortOrder;
    }

    //--- DATABASE MAPPING : active_flag ( INT ) 
    public void setActiveFlag( Integer activeFlag ) {
        this.activeFlag = activeFlag;
    }
    public Integer getActiveFlag() {
        return this.activeFlag;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuilder sb = new StringBuilder(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(code);
        sb.append("|");
        sb.append(chargeType);
        sb.append("|");
        sb.append(sortOrder);
        sb.append("|");
        sb.append(activeFlag);
        return sb.toString(); 
    } 

}
