/*
 * Created on 2023-02-17 ( Time 14:52:00 )
 * Generated by Telosys Tools Generator ( version 3.3.0 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "m_user"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="m_user", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="MUserEntity.countAll", query="SELECT COUNT(x) FROM MUserEntity x" )
} )
public class MUserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="active_flag")
    private Integer    activeFlag   ;

    @Column(name="username", length=45)
    private String     username     ;

    @Column(name="password", length=128)
    private String     password     ;

    @Column(name="role", length=45)
    private String     role         ;

    @Column(name="branch_code", length=4)
    private String     branchCode   ;

    @Column(name="email", length=45)
    private String     email        ;

    @Column(name="phone_num")
    private Integer    phoneNum     ;

    @Column(name="created_by")
    private Integer    createdBy    ;

    @Column(name="updated_by")
    private Integer    updatedBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_timestamp")
    private Date       createdTimestamp ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_timestamp")
    private Date       updatedTimestamp ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------
    @OneToMany(mappedBy="mUser")
    private List<MConsignorEntity> listOfMConsignor;

    @OneToMany(mappedBy="mUser")
    private List<MStateEntity> listOfMState;

    @OneToMany(mappedBy="mUser")
    private List<MUserPermissionEntity> listOfMUserPermission;

    @OneToMany(mappedBy="mUser")
    private List<MPermissionTypeEntity> listOfMPermissionType;


    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public MUserEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : active_flag ( INT ) 
    public void setActiveFlag( Integer activeFlag ) {
        this.activeFlag = activeFlag;
    }
    public Integer getActiveFlag() {
        return this.activeFlag;
    }

    //--- DATABASE MAPPING : username ( VARCHAR ) 
    public void setUsername( String username ) {
        this.username = username;
    }
    public String getUsername() {
        return this.username;
    }

    //--- DATABASE MAPPING : password ( VARCHAR ) 
    public void setPassword( String password ) {
        this.password = password;
    }
    public String getPassword() {
        return this.password;
    }

    //--- DATABASE MAPPING : role ( VARCHAR ) 
    public void setRole( String role ) {
        this.role = role;
    }
    public String getRole() {
        return this.role;
    }

    //--- DATABASE MAPPING : branch_code ( VARCHAR ) 
    public void setBranchCode( String branchCode ) {
        this.branchCode = branchCode;
    }
    public String getBranchCode() {
        return this.branchCode;
    }

    //--- DATABASE MAPPING : email ( VARCHAR ) 
    public void setEmail( String email ) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    //--- DATABASE MAPPING : phone_num ( INT ) 
    public void setPhoneNum( Integer phoneNum ) {
        this.phoneNum = phoneNum;
    }
    public Integer getPhoneNum() {
        return this.phoneNum;
    }

    //--- DATABASE MAPPING : created_by ( INT ) 
    public void setCreatedBy( Integer createdBy ) {
        this.createdBy = createdBy;
    }
    public Integer getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : updated_by ( INT ) 
    public void setUpdatedBy( Integer updatedBy ) {
        this.updatedBy = updatedBy;
    }
    public Integer getUpdatedBy() {
        return this.updatedBy;
    }

    //--- DATABASE MAPPING : created_timestamp ( DATETIME ) 
    public void setCreatedTimestamp( Date createdTimestamp ) {
        this.createdTimestamp = createdTimestamp;
    }
    public Date getCreatedTimestamp() {
        return this.createdTimestamp;
    }

    //--- DATABASE MAPPING : updated_timestamp ( DATETIME ) 
    public void setUpdatedTimestamp( Date updatedTimestamp ) {
        this.updatedTimestamp = updatedTimestamp;
    }
    public Date getUpdatedTimestamp() {
        return this.updatedTimestamp;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    public void setListOfMConsignor( List<MConsignorEntity> listOfMConsignor ) {
        this.listOfMConsignor = listOfMConsignor;
    }
    public List<MConsignorEntity> getListOfMConsignor() {
        return this.listOfMConsignor;
    }

    public void setListOfMState( List<MStateEntity> listOfMState ) {
        this.listOfMState = listOfMState;
    }
    public List<MStateEntity> getListOfMState() {
        return this.listOfMState;
    }

    public void setListOfMUserPermission( List<MUserPermissionEntity> listOfMUserPermission ) {
        this.listOfMUserPermission = listOfMUserPermission;
    }
    public List<MUserPermissionEntity> getListOfMUserPermission() {
        return this.listOfMUserPermission;
    }

    public void setListOfMPermissionType( List<MPermissionTypeEntity> listOfMPermissionType ) {
        this.listOfMPermissionType = listOfMPermissionType;
    }
    public List<MPermissionTypeEntity> getListOfMPermissionType() {
        return this.listOfMPermissionType;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuilder sb = new StringBuilder(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(activeFlag);
        sb.append("|");
        sb.append(username);
        sb.append("|");
        sb.append(password);
        sb.append("|");
        sb.append(role);
        sb.append("|");
        sb.append(branchCode);
        sb.append("|");
        sb.append(email);
        sb.append("|");
        sb.append(phoneNum);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(updatedBy);
        sb.append("|");
        sb.append(createdTimestamp);
        sb.append("|");
        sb.append(updatedTimestamp);
        return sb.toString(); 
    } 

}
