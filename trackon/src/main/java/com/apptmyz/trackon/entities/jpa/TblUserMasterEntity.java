/*
 * Created on 14 Jul 2022 ( Time 16:39:10 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "tbl_user_master"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="tbl_user_master", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="TblUserMasterEntity.countAll", query="SELECT COUNT(x) FROM TblUserMasterEntity x" )
} )
public class TblUserMasterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="erp_id", nullable=false)
    private Integer    erpId        ;

    @Column(name="user_id", nullable=false, length=10)
    private String     userId       ;

    @Column(name="password", nullable=false, length=10)
    private String     password     ;

    @Column(name="user_full_name", nullable=false, length=150)
    private String     userFullName ;

    @Column(name="user_type_id", nullable=false)
    private Integer    userTypeId   ;

    @Column(name="user_role", length=100)
    private String     userRole     ;

    @Column(name="branch_code", length=10)
    private String     branchCode   ;

    @Column(name="state", length=100)
    private String     state        ;

    @Column(name="city", length=100)
    private String     city         ;

    @Column(name="pincode", length=6)
    private String     pincode      ;

    @Column(name="mobile_no", length=20)
    private String     mobileNo     ;

    @Column(name="email_id", length=100)
    private String     emailId      ;

    @Column(name="is_active")
    private Boolean    isActive     ;

    @Column(name="created_by", length=150)
    private String     createdBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on")
    private Date       createdOn    ;

    @Column(name="modify_by", length=150)
    private String     modifyBy     ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modify_on")
    private Date       modifyOn     ;

    @Column(name="ro_code", length=10)
    private String     roCode       ;

    @Column(name="drs_no")
    private Integer    drsNo        ;

    @Column(name="is_crm_user")
    private Boolean    isCrmUser    ;

    @Column(name="role_id")
    private Integer    roleId       ;

    @Column(name="phone", length=20)
    private String     phone        ;

    @Column(name="version")
    private Integer    version      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public TblUserMasterEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : erp_id ( INT ) 
    public void setErpId( Integer erpId ) {
        this.erpId = erpId;
    }
    public Integer getErpId() {
        return this.erpId;
    }

    //--- DATABASE MAPPING : user_id ( VARCHAR ) 
    public void setUserId( String userId ) {
        this.userId = userId;
    }
    public String getUserId() {
        return this.userId;
    }

    //--- DATABASE MAPPING : password ( VARCHAR ) 
    public void setPassword( String password ) {
        this.password = password;
    }
    public String getPassword() {
        return this.password;
    }

    //--- DATABASE MAPPING : user_full_name ( VARCHAR ) 
    public void setUserFullName( String userFullName ) {
        this.userFullName = userFullName;
    }
    public String getUserFullName() {
        return this.userFullName;
    }

    //--- DATABASE MAPPING : user_type_id ( INT ) 
    public void setUserTypeId( Integer userTypeId ) {
        this.userTypeId = userTypeId;
    }
    public Integer getUserTypeId() {
        return this.userTypeId;
    }

    //--- DATABASE MAPPING : user_role ( VARCHAR ) 
    public void setUserRole( String userRole ) {
        this.userRole = userRole;
    }
    public String getUserRole() {
        return this.userRole;
    }

    //--- DATABASE MAPPING : branch_code ( VARCHAR ) 
    public void setBranchCode( String branchCode ) {
        this.branchCode = branchCode;
    }
    public String getBranchCode() {
        return this.branchCode;
    }

    //--- DATABASE MAPPING : state ( VARCHAR ) 
    public void setState( String state ) {
        this.state = state;
    }
    public String getState() {
        return this.state;
    }

    //--- DATABASE MAPPING : city ( VARCHAR ) 
    public void setCity( String city ) {
        this.city = city;
    }
    public String getCity() {
        return this.city;
    }

    //--- DATABASE MAPPING : pincode ( VARCHAR ) 
    public void setPincode( String pincode ) {
        this.pincode = pincode;
    }
    public String getPincode() {
        return this.pincode;
    }

    //--- DATABASE MAPPING : mobile_no ( VARCHAR ) 
    public void setMobileNo( String mobileNo ) {
        this.mobileNo = mobileNo;
    }
    public String getMobileNo() {
        return this.mobileNo;
    }

    //--- DATABASE MAPPING : email_id ( VARCHAR ) 
    public void setEmailId( String emailId ) {
        this.emailId = emailId;
    }
    public String getEmailId() {
        return this.emailId;
    }

    //--- DATABASE MAPPING : is_active ( BIT ) 
    public void setIsActive( Boolean isActive ) {
        this.isActive = isActive;
    }
    public Boolean getIsActive() {
        return this.isActive;
    }

    //--- DATABASE MAPPING : created_by ( VARCHAR ) 
    public void setCreatedBy( String createdBy ) {
        this.createdBy = createdBy;
    }
    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : created_on ( DATETIME ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : modify_by ( VARCHAR ) 
    public void setModifyBy( String modifyBy ) {
        this.modifyBy = modifyBy;
    }
    public String getModifyBy() {
        return this.modifyBy;
    }

    //--- DATABASE MAPPING : modify_on ( DATETIME ) 
    public void setModifyOn( Date modifyOn ) {
        this.modifyOn = modifyOn;
    }
    public Date getModifyOn() {
        return this.modifyOn;
    }

    //--- DATABASE MAPPING : ro_code ( VARCHAR ) 
    public void setRoCode( String roCode ) {
        this.roCode = roCode;
    }
    public String getRoCode() {
        return this.roCode;
    }

    //--- DATABASE MAPPING : drs_no ( INT ) 
    public void setDrsNo( Integer drsNo ) {
        this.drsNo = drsNo;
    }
    public Integer getDrsNo() {
        return this.drsNo;
    }

    //--- DATABASE MAPPING : is_crm_user ( BIT ) 
    public void setIsCrmUser( Boolean isCrmUser ) {
        this.isCrmUser = isCrmUser;
    }
    public Boolean getIsCrmUser() {
        return this.isCrmUser;
    }

    //--- DATABASE MAPPING : role_id ( INT ) 
    public void setRoleId( Integer roleId ) {
        this.roleId = roleId;
    }
    public Integer getRoleId() {
        return this.roleId;
    }

    //--- DATABASE MAPPING : phone ( VARCHAR ) 
    public void setPhone( String phone ) {
        this.phone = phone;
    }
    public String getPhone() {
        return this.phone;
    }

    //--- DATABASE MAPPING : version ( INT ) 
    public void setVersion( Integer version ) {
        this.version = version;
    }
    public Integer getVersion() {
        return this.version;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(erpId);
        sb.append("|");
        sb.append(userId);
        sb.append("|");
        sb.append(password);
        sb.append("|");
        sb.append(userFullName);
        sb.append("|");
        sb.append(userTypeId);
        sb.append("|");
        sb.append(userRole);
        sb.append("|");
        sb.append(branchCode);
        sb.append("|");
        sb.append(state);
        sb.append("|");
        sb.append(city);
        sb.append("|");
        sb.append(pincode);
        sb.append("|");
        sb.append(mobileNo);
        sb.append("|");
        sb.append(emailId);
        sb.append("|");
        sb.append(isActive);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(modifyBy);
        sb.append("|");
        sb.append(modifyOn);
        sb.append("|");
        sb.append(roCode);
        sb.append("|");
        sb.append(drsNo);
        sb.append("|");
        sb.append(isCrmUser);
        sb.append("|");
        sb.append(roleId);
        sb.append("|");
        sb.append(phone);
        sb.append("|");
        sb.append(version);
        return sb.toString(); 
    } 

}
