/*
 * Created on 13 Jul 2022 ( Time 14:25:51 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "designation_master"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="designation_master", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="DesignationMasterEntity.countAll", query="SELECT COUNT(x) FROM DesignationMasterEntity x" )
} )
public class DesignationMasterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="designation_id", nullable=false, length=100)
    private String     designationId ;

    @Column(name="designation_name", nullable=false, length=150)
    private String     designationName ;

    @Column(name="is_active", nullable=false)
    private Boolean    isActive     ;

    @Column(name="created_by", nullable=false, length=100)
    private String     createdBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on")
    private Date       createdOn    ;

    @Column(name="modified_by", nullable=false, length=100)
    private String     modifiedBy   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_on")
    private Date       modifiedOn   ;

    @Column(name="version")
    private Integer    version      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public DesignationMasterEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : designation_id ( VARCHAR ) 
    public void setDesignationId( String designationId ) {
        this.designationId = designationId;
    }
    public String getDesignationId() {
        return this.designationId;
    }

    //--- DATABASE MAPPING : designation_name ( VARCHAR ) 
    public void setDesignationName( String designationName ) {
        this.designationName = designationName;
    }
    public String getDesignationName() {
        return this.designationName;
    }

    //--- DATABASE MAPPING : is_active ( BIT ) 
    public void setIsActive( Boolean isActive ) {
        this.isActive = isActive;
    }
    public Boolean getIsActive() {
        return this.isActive;
    }

    //--- DATABASE MAPPING : created_by ( VARCHAR ) 
    public void setCreatedBy( String createdBy ) {
        this.createdBy = createdBy;
    }
    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : created_on ( DATETIME ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : modified_by ( VARCHAR ) 
    public void setModifiedBy( String modifiedBy ) {
        this.modifiedBy = modifiedBy;
    }
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    //--- DATABASE MAPPING : modified_on ( DATETIME ) 
    public void setModifiedOn( Date modifiedOn ) {
        this.modifiedOn = modifiedOn;
    }
    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    //--- DATABASE MAPPING : version ( INT ) 
    public void setVersion( Integer version ) {
        this.version = version;
    }
    public Integer getVersion() {
        return this.version;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(designationId);
        sb.append("|");
        sb.append(designationName);
        sb.append("|");
        sb.append(isActive);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(modifiedBy);
        sb.append("|");
        sb.append(modifiedOn);
        sb.append("|");
        sb.append(version);
        return sb.toString(); 
    } 

}
