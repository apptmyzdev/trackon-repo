/*
 * Created on 13 Jul 2022 ( Time 15:20:35 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "office_type_master"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="office_type_master", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="OfficeTypeMasterEntity.countAll", query="SELECT COUNT(x) FROM OfficeTypeMasterEntity x" )
} )
public class OfficeTypeMasterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="office_type_id", nullable=false)
    private Integer    officeTypeId ;

    @Column(name="office_type_code", nullable=false, length=5)
    private String     officeTypeCode ;

    @Column(name="office_description", length=150)
    private String     officeDescription ;

    @Column(name="is_self_office")
    private Boolean    isSelfOffice ;

    @Column(name="is_active")
    private Boolean    isActive     ;

    @Column(name="created_by", nullable=false, length=150)
    private String     createdBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on", nullable=false)
    private Date       createdOn    ;

    @Column(name="modified_by", nullable=false, length=150)
    private String     modifiedBy   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_on", nullable=false)
    private Date       modifiedOn   ;

    @Column(name="version")
    private Integer    version      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public OfficeTypeMasterEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : office_type_id ( INT ) 
    public void setOfficeTypeId( Integer officeTypeId ) {
        this.officeTypeId = officeTypeId;
    }
    public Integer getOfficeTypeId() {
        return this.officeTypeId;
    }

    //--- DATABASE MAPPING : office_type_code ( VARCHAR ) 
    public void setOfficeTypeCode( String officeTypeCode ) {
        this.officeTypeCode = officeTypeCode;
    }
    public String getOfficeTypeCode() {
        return this.officeTypeCode;
    }

    //--- DATABASE MAPPING : office_description ( VARCHAR ) 
    public void setOfficeDescription( String officeDescription ) {
        this.officeDescription = officeDescription;
    }
    public String getOfficeDescription() {
        return this.officeDescription;
    }

    //--- DATABASE MAPPING : is_self_office ( BIT ) 
    public void setIsSelfOffice( Boolean isSelfOffice ) {
        this.isSelfOffice = isSelfOffice;
    }
    public Boolean getIsSelfOffice() {
        return this.isSelfOffice;
    }

    //--- DATABASE MAPPING : is_active ( BIT ) 
    public void setIsActive( Boolean isActive ) {
        this.isActive = isActive;
    }
    public Boolean getIsActive() {
        return this.isActive;
    }

    //--- DATABASE MAPPING : created_by ( VARCHAR ) 
    public void setCreatedBy( String createdBy ) {
        this.createdBy = createdBy;
    }
    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : created_on ( DATETIME ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : modified_by ( VARCHAR ) 
    public void setModifiedBy( String modifiedBy ) {
        this.modifiedBy = modifiedBy;
    }
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    //--- DATABASE MAPPING : modified_on ( DATETIME ) 
    public void setModifiedOn( Date modifiedOn ) {
        this.modifiedOn = modifiedOn;
    }
    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    //--- DATABASE MAPPING : version ( INT ) 
    public void setVersion( Integer version ) {
        this.version = version;
    }
    public Integer getVersion() {
        return this.version;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(officeTypeId);
        sb.append("|");
        sb.append(officeTypeCode);
        sb.append("|");
        sb.append(officeDescription);
        sb.append("|");
        sb.append(isSelfOffice);
        sb.append("|");
        sb.append(isActive);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(modifiedBy);
        sb.append("|");
        sb.append(modifiedOn);
        sb.append("|");
        sb.append(version);
        return sb.toString(); 
    } 

}
