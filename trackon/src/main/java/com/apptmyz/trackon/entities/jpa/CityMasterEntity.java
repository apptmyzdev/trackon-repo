/*
 * Created on 13 Jul 2022 ( Time 12:55:52 )
 * Generated by Telosys Tools Generator ( version 2.1.1 )
 */
// This Bean has a basic Primary Key (not composite) 

package com.apptmyz.trackon.entities.jpa;

import java.io.Serializable;

//import javax.validation.constraints.* ;
//import org.hibernate.validator.constraints.* ;

import java.util.Date;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "city_master"
 *
 * @author Telosys Tools Generator
 *
 */

@Entity
@Table(name="city_master", catalog="trackon" )
// Define named queries here
@NamedQueries ( {
  @NamedQuery ( name="CityMasterEntity.countAll", query="SELECT COUNT(x) FROM CityMasterEntity x" )
} )
public class CityMasterEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", nullable=false)
    private Integer    id           ;


    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    
    @Column(name="city_id", nullable=false)
    private Integer    cityId       ;

    @Column(name="city_code", nullable=false, length=5)
    private String     cityCode     ;

    @Column(name="city_name", nullable=false, length=100)
    private String     cityName     ;

    @Column(name="state_id", nullable=false)
    private Integer    stateId      ;

    @Column(name="ro_code", nullable=false, length=10)
    private String     roCode       ;

    @Column(name="master_code", length=5)
    private String     masterCode   ;

    @Column(name="category", length=50)
    private String     category     ;

    @Column(name="additional_tat")
    private Integer    additionalTat ;

    @Column(name="is_active", length=1)
    private String     isActive     ;

    @Column(name="created_by", nullable=false, length=25)
    private String     createdBy    ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_on", nullable=false)
    private Date       createdOn    ;

    @Column(name="modified_by", nullable=false, length=25)
    private String     modifiedBy   ;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modified_on", nullable=false)
    private Date       modifiedOn   ;

    @Column(name="version")
    private Integer    version      ;



    //----------------------------------------------------------------------
    // ENTITY LINKS ( RELATIONSHIP )
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // CONSTRUCTOR(S)
    //----------------------------------------------------------------------
    public CityMasterEntity() {
		super();
    }
    
    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setId( Integer id ) {
        this.id = id ;
    }
    public Integer getId() {
        return this.id;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    //--- DATABASE MAPPING : city_id ( INT ) 
    public void setCityId( Integer cityId ) {
        this.cityId = cityId;
    }
    public Integer getCityId() {
        return this.cityId;
    }

    //--- DATABASE MAPPING : city_code ( VARCHAR ) 
    public void setCityCode( String cityCode ) {
        this.cityCode = cityCode;
    }
    public String getCityCode() {
        return this.cityCode;
    }

    //--- DATABASE MAPPING : city_name ( VARCHAR ) 
    public void setCityName( String cityName ) {
        this.cityName = cityName;
    }
    public String getCityName() {
        return this.cityName;
    }

    //--- DATABASE MAPPING : state_id ( INT ) 
    public void setStateId( Integer stateId ) {
        this.stateId = stateId;
    }
    public Integer getStateId() {
        return this.stateId;
    }

    //--- DATABASE MAPPING : ro_code ( VARCHAR ) 
    public void setRoCode( String roCode ) {
        this.roCode = roCode;
    }
    public String getRoCode() {
        return this.roCode;
    }

    //--- DATABASE MAPPING : master_code ( VARCHAR ) 
    public void setMasterCode( String masterCode ) {
        this.masterCode = masterCode;
    }
    public String getMasterCode() {
        return this.masterCode;
    }

    //--- DATABASE MAPPING : category ( VARCHAR ) 
    public void setCategory( String category ) {
        this.category = category;
    }
    public String getCategory() {
        return this.category;
    }

    //--- DATABASE MAPPING : additional_tat ( INT ) 
    public void setAdditionalTat( Integer additionalTat ) {
        this.additionalTat = additionalTat;
    }
    public Integer getAdditionalTat() {
        return this.additionalTat;
    }

    //--- DATABASE MAPPING : is_active ( VARCHAR ) 
    public void setIsActive( String isActive ) {
        this.isActive = isActive;
    }
    public String getIsActive() {
        return this.isActive;
    }

    //--- DATABASE MAPPING : created_by ( VARCHAR ) 
    public void setCreatedBy( String createdBy ) {
        this.createdBy = createdBy;
    }
    public String getCreatedBy() {
        return this.createdBy;
    }

    //--- DATABASE MAPPING : created_on ( DATETIME ) 
    public void setCreatedOn( Date createdOn ) {
        this.createdOn = createdOn;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }

    //--- DATABASE MAPPING : modified_by ( VARCHAR ) 
    public void setModifiedBy( String modifiedBy ) {
        this.modifiedBy = modifiedBy;
    }
    public String getModifiedBy() {
        return this.modifiedBy;
    }

    //--- DATABASE MAPPING : modified_on ( DATETIME ) 
    public void setModifiedOn( Date modifiedOn ) {
        this.modifiedOn = modifiedOn;
    }
    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    //--- DATABASE MAPPING : version ( INT ) 
    public void setVersion( Integer version ) {
        this.version = version;
    }
    public Integer getVersion() {
        return this.version;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR LINKS
    //----------------------------------------------------------------------

    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
    public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append("["); 
        sb.append(id);
        sb.append("]:"); 
        sb.append(cityId);
        sb.append("|");
        sb.append(cityCode);
        sb.append("|");
        sb.append(cityName);
        sb.append("|");
        sb.append(stateId);
        sb.append("|");
        sb.append(roCode);
        sb.append("|");
        sb.append(masterCode);
        sb.append("|");
        sb.append(category);
        sb.append("|");
        sb.append(additionalTat);
        sb.append("|");
        sb.append(isActive);
        sb.append("|");
        sb.append(createdBy);
        sb.append("|");
        sb.append(createdOn);
        sb.append("|");
        sb.append(modifiedBy);
        sb.append("|");
        sb.append(modifiedOn);
        sb.append("|");
        sb.append(version);
        return sb.toString(); 
    } 

}
