package com.apptmyz.trackon.security;

import java.security.SecureRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.apptmyz.trackon.services.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class TrackOnSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	UserDetailsServiceImpl userDetailsService;

	@Autowired
	private JWTAuthEntryPoint jwtAuthEntryPoint;

	@Bean
	public JWTAuthTokenFilter jwtAuthenticationTokenFilter() {
		return new JWTAuthTokenFilter();
	}

	@Override
	public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(userDetailsService);   //.passwordEncoder(passwordEncoder());
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12/*strength*/, new SecureRandom());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable()
			.exceptionHandling().authenticationEntryPoint(jwtAuthEntryPoint).and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.authorizeRequests().antMatchers("/authenticate/login").permitAll()
			.antMatchers("/app/cron/trackon/usermaster").permitAll().antMatchers("/pd/cron/trackon/getdrs").permitAll()
			.antMatchers("/erp/masters/**").permitAll()
			.antMatchers("/pd/view/pod/image/src/**").permitAll()
			.antMatchers("/pickup/view/image").permitAll()
			.antMatchers("/reports/**").permitAll()
			.antMatchers("/pickup/redirect/display/image/**").permitAll()
			.antMatchers("/pd/generate/pod/pdf/**").permitAll()
			.antMatchers("/dashboard/**").permitAll()
			.antMatchers("/pd/**").permitAll()
			.antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
			.anyRequest().authenticated();
		
		http.headers().cacheControl();
		http.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	}

}
